﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using WcfService.DB_Server;
using XR.Dicom;

namespace WcfService
{
    public class ImageHelper
    {
         BusinessService businessservice = new BusinessService();
         SystemManage systemmanage = new SystemManage();
         BasicService basicservice = new BasicService();
         DicomFile df = null;                      //DICOM文件
         DicomAttributeCollection _baseDataSet;    //dicom文件属性
            
         string serversUid = "";
         string modality = "";
         string seriesDescription = "";
         int seriesNumber = 1;
         string dcmFileName = "";
         int isRbColorOrRbMonoChrome = 1;
         string sqdReportUrl = "";
        public ImageHelper()
        {
        }

        public  void ConvertTemp(object j)
        {
            sqdReportUrl = businessservice.GetSysValueByKey("SqdReportAddress").SysValue;
            if (j == null)
                return;
            var t = (Tuple<OutRqeustInfo,string,string>)j;
            if (t.Item1.RequestState == 11)
            {
                ConvertSQDImageNew(t.Item1, t.Item2.ToString());
            }
            else if (t.Item1.RequestState == 15)
            {
                ConvertSQDImageNew(t.Item1, t.Item2.ToString());
                ConvertBGImageNew(t.Item1, t.Item2.ToString());
            }
        }
        
        /// <summary>
        /// 生成申请单图片
        /// </summary>
        /// <param name="requestId"></param>
        public  void ConvertSQDImageNew(OutRqeustInfo requestInfoParm, string filePath)
        {
            try
            {
                string url = sqdReportUrl + "?requestId=" + requestInfoParm.RequestId + "&type=2";
                var imageByte = GetByte(url);
                if (imageByte != null)
                {
                    MemoryStream ms = new MemoryStream(imageByte);
                    Image img = Image.FromStream(ms);
                    if (!Directory.Exists(filePath + @"\" + requestInfoParm.PatientId + @"\"))
                        Directory.CreateDirectory(filePath + @"\" + requestInfoParm.PatientId + @"\");
                    var fileName = filePath + @"\" + requestInfoParm.PatientId + @"\" + requestInfoParm.RequestId + "_sqd.jpg";
                    img.Save(fileName);
                    basicservice.OnlyServiceLogContent(LogParam.I, "申请单图片信息：[" + fileName + "]", "");
                    ms.Dispose();
                    img.Dispose();
                }
            }
            catch (Exception ex)
            {
                basicservice.OnlyServiceLogContent(LogParam.I, "加载报告模板文档异常：[" + ex.Message.ToString() + "]", "");
            }
            finally
            {
                basicservice.OnlyServiceLogContent(LogParam.I, requestInfoParm.CPatientName + "," + requestInfoParm.RequestId + ",保存申请单图片成功", "");
            }
        }


        /// <summary>
        /// 会诊报告生成图片
        /// </summary>
        /// <param name="objRequestInfo"></param>
        public  void ConvertBGImageNew(OutRqeustInfo objRequestInfo, string filePath)
        {
            try
            {
                string url = sqdReportUrl + "?requestId=" + objRequestInfo.RequestId + "&type=1";
                var imageByte = GetByte(url);
                if (imageByte != null)
                {
                    basicservice.OnlyServiceLogContent(LogParam.I, "imageByte.Length:" + imageByte.Length, "");
                    MemoryStream ms = new MemoryStream(imageByte);
                    Image img = Image.FromStream(ms);
                    if (!Directory.Exists(filePath + @"\" + objRequestInfo.PatientId + @"\"))
                        Directory.CreateDirectory(filePath + @"\" + objRequestInfo.PatientId + @"\");
                    var fileName = filePath + @"\" + objRequestInfo.PatientId + @"\" + objRequestInfo.RequestId + "_bg.jpg";
                    img.Save(fileName);
                    basicservice.OnlyServiceLogContent(LogParam.I, "报告图片信息：[" + fileName + "]", "");
                    ms.Dispose();
                    img.Dispose();
                }
            }
            catch (Exception ex)
            {
                basicservice.OnlyServiceLogContent(LogParam.I, "保存图片异常：[" + ex.Message.ToString() + "]", "");
            }
            finally
            {
                basicservice.OnlyServiceLogContent(LogParam.I, objRequestInfo.CPatientName + "," + objRequestInfo.RequestId + ",保存报告单图片成功", "");
            }
        }
        
        /// <summary>
        /// 获取图片
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private  byte[] GetByte(string url)
        {
            byte[] result = new byte[0];
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                request.ContentType = "text/html;charset=UTF-8";
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream myResponseStream = response.GetResponseStream();
                StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.UTF8);
                string retString = myStreamReader.ReadToEnd();
                JObject jo = (JObject)JsonConvert.DeserializeObject(retString);
                basicservice.OnlyServiceLogContent(LogParam.I, url + ",retString:" + retString, "");
                string fileName = jo["items"].ToString(); //{"items":"E:\\cache\\aaaaaaaa\\baoGao","code":"1","msg":"成功获取路径","time":1510125558044} 
                string code = jo["code"].ToString();
                if (code == "1")
                {
                    result = systemmanage.GetTemplateData(fileName);
                }
                myStreamReader.Close();
                myResponseStream.Close();
            }
            catch (Exception ex)
            {
                basicservice.OnlyServiceLogContent(LogParam.I, url + ",获取图片信息异常:" + ex.Message.ToString(), "");
            }
            return result;
        }
        
        private  bool cancelled = false;
                
        /// <summary>
        /// 黑白字节
        /// </summary>
        /// <param name="image"></param>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        public  byte[] GetMonochromePixelData(Bitmap image, out int rows, out int columns)
        {
            rows = image.Height;
            columns = image.Width;
            if (rows % 2 != 0 && columns % 2 != 0)
            {
                --columns;
            }
            int size = rows * columns;
            byte[] pixelData = new byte[size];
            int i = 0;
            for (int row = 0; row < rows; ++row)
            {
                for (int column = 0; column < columns; column++)
                {
                    pixelData[i++] = image.GetPixel(column, row).R;
                }
            }
            return pixelData;
        }

        /// <summary>
        /// 获取有颜色的字节
        /// </summary>
        /// <param name="image"></param>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        public  byte[] GetColorPixelData(Bitmap image, out int rows, out int columns)
        {
            rows = image.Height;
            columns = image.Width;
            if (rows % 2 != 0 && columns % 2 != 0)
            {
                --columns;
            }
            BitmapData data = image.LockBits(new Rectangle(0, 0, columns, rows), ImageLockMode.ReadOnly, image.PixelFormat);
            IntPtr bmpData = data.Scan0;
            try
            {
                int stride = columns * 3;
                int size = rows * stride;
                byte[] pixelData = new byte[size];
                for (int i = 0; i < rows; ++i)
                {
                    Marshal.Copy(new IntPtr(bmpData.ToInt64() + i * data.Stride), pixelData, i * stride, stride);
                }
                SwapRedBlue(pixelData);
                return pixelData;
            }
            finally
            {
                image.UnlockBits(data);
            }
        }

        /// <summary>
        /// 加载图片
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public  Bitmap LoadImage(string file)
        {
            try
            {
                Bitmap image = Image.FromFile(file, true) as Bitmap;
                if (image == null)
                {
                    throw new ArgumentException(String.Format("The specified file cannot be loaded as a bitmap {0}.", file));
                }
                if (image.PixelFormat != PixelFormat.Format24bppRgb)
                {
                    Bitmap old = image;
                    using (old)
                    {
                        image = new Bitmap(old.Width, old.Height, PixelFormat.Format24bppRgb);
                        using (Graphics g = Graphics.FromImage(image))
                        {
                            g.DrawImage(old, 0, 0, old.Width, old.Height);
                        }
                    }
                }
                return image;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        /// <summary>
        /// 分组生成DICOM文件
        /// </summary>
        /// <param name="requestInfos"></param>
        public  void GroupFileType(List<OutRqeustInfo> requestInfos,string filePath)
        {
            foreach (var item in requestInfos)
            {
                //requestInfo = item;
                var requestFileItem = businessservice.GetFileItemsByRequestId(item.RequestId).Where(x => x.Source != 9 && x.FileType != "success").ToList();

                //病例图片生成DICOM文件                       
                var sourceGroup = requestFileItem.GroupBy(x => x.Source).ToList();
                seriesNumber = 1;
                foreach (var i in sourceGroup)
                {
                    serversUid = DicomUid.GenerateUid().UID;
                    if (i.Key.Value == 11)
                    {
                        modality = "Image";
                        seriesDescription = "S|" + item.RequestId + "|" + DateTime.Now.ToString("yyMMddHHmmss");
                    }
                    else if (i.Key.Value == 7)
                    {
                        modality = "Image";
                        seriesDescription = "C|" + item.RequestId + "|" + DateTime.Now.ToString("yyMMddHHmmss");
                    }
                    else if (i.Key.Value == 15)
                    {
                        modality = "Image";
                        seriesDescription = "B|" + item.RequestId + "|" + DateTime.Now.ToString("yyMMddHHmmss");
                    }
                    var m = requestFileItem.Where(x => x.Source == i.Key);
                    var instanceNumber = 1;
                    foreach (var n in m)
                    {
                        if (n.FileType == null || !n.FileType.Equals("success"))
                        {
                            if (ImageHelperConvertDcm(n.FileName, item.PatientId, i.Key.Value, instanceNumber, filePath, item))
                            {
                                n.FileType = "success";
                                businessservice.SaveFileItem(n);
                                instanceNumber++;
                            }
                        }
                    }
                    seriesNumber++;
                }                
            }
        }
        
        /// <summary>
        /// 图片转换为DICOM
        /// </summary>
        /// <param name="imageFileName"></param>
        /// <param name="patientId"></param>
        public  bool ImageHelperConvertDcm(string imageFileNamestr, string patientId, int type, int instanceNumber, string filePath, OutRqeustInfo requestInfo)
        {
            string imageFileName = "";
            bool result = false;
            if (type == 7)
            {
                imageFileName = filePath + imageFileNamestr;
            }
            else
            {
                imageFileName = filePath + patientId + @"\" + imageFileNamestr;
            }       
            Bitmap bm = LoadImage(imageFileName);
            CreateBaseDataSet(requestInfo);
            df = ImageConvertDcm(bm, instanceNumber);
            if (df != null)
            {
                if (!Directory.Exists(filePath + patientId))
                {
                    Directory.CreateDirectory(filePath + patientId);
                }
                dcmFileName = filePath + patientId + @"\" + imageFileNamestr.Substring(0, imageFileNamestr.LastIndexOf(".")) + ".dcm";
                df.Save(dcmFileName, DicomWriteOptions.Default);

                basicservice.OnlyServiceLogContent(LogParam.I, requestInfo.CPatientName + "," + requestInfo.RequestId + ",图片[" + imageFileNamestr + "]生成DICOM文件成功！", "");
                result = true;
                bm.Dispose();
            }
            else
            {
                result = false;
            }
            return result;
        }

        /// <summary>
        /// 将图片转换成DICOM
        /// </summary>
        /// <param name="image"></param>
        /// <param name="instanceNumber"></param>
        /// <returns></returns>
        private  DicomFile ImageConvertDcm(Bitmap image, int instanceNumber)
        {
            if (image == null)
            {
                return null;
            }
            DicomUid sopInstanceUid = DicomUid.GenerateUid();
            string fileName = dcmFileName;
            DicomFile dicomFile = new DicomFile(fileName, new DicomAttributeCollection(), _baseDataSet.Copy());
            //meta info
            dicomFile.MediaStorageSopInstanceUid = sopInstanceUid.UID;
            dicomFile.MediaStorageSopClassUid = SopClass.SecondaryCaptureImageStorageUid;
            //General Image
            dicomFile.DataSet[DicomTags.InstanceNumber].SetInt32(0, instanceNumber);
            DateTime now = DateTime.Now;
            DateTime time = DateTime.MinValue.Add(new TimeSpan(now.Hour, now.Minute, now.Second));
            //SC Image
            dicomFile.DataSet[DicomTags.DateOfSecondaryCapture].SetDateTime(0, now);
            dicomFile.DataSet[DicomTags.TimeOfSecondaryCapture].SetDateTime(0, time);

            //Sop Common
            dicomFile.DataSet[DicomTags.InstanceCreationDate].SetDateTime(0, now);
            dicomFile.DataSet[DicomTags.InstanceCreationTime].SetDateTime(0, time);
            dicomFile.DataSet[DicomTags.SopInstanceUid].SetStringValue(sopInstanceUid.UID);
            int rows, columns;
            //Image Pixel
            if (isRbColorOrRbMonoChrome == 0)
            {
                dicomFile.DataSet[DicomTags.PixelData].Values = GetMonochromePixelData(image, out rows, out columns);
                //Image Pixel
                dicomFile.DataSet[DicomTags.Rows].SetInt32(0, rows);
                dicomFile.DataSet[DicomTags.Columns].SetInt32(0, columns);
            }
            if (isRbColorOrRbMonoChrome == 1)
            {
                dicomFile.DataSet[DicomTags.PixelData].Values = GetColorPixelData(image, out rows, out columns);
                //Image Pixel
                dicomFile.DataSet[DicomTags.Rows].SetInt32(0, rows);
                dicomFile.DataSet[DicomTags.Columns].SetInt32(0, columns);
            }
            return dicomFile;
        }

        /// <summary>
        /// 创建DICOM文件
        /// </summary>
        private  void CreateBaseDataSet(OutRqeustInfo requestInfo)
        {
            _baseDataSet = new DicomAttributeCollection();
            //Sop Common
            _baseDataSet[DicomTags.SopClassUid].SetStringValue(SopClass.SecondaryCaptureImageStorageUid);
            //Patient
            _baseDataSet[DicomTags.PatientId].SetStringValue(requestInfo.NewPatientId);
            _baseDataSet[DicomTags.PatientsName].SetStringValue(String.Format(requestInfo.PatientName));
            _baseDataSet[DicomTags.PatientsBirthDate].SetString(0, requestInfo.PatientBirth.Replace("/", "").Replace("-", ""));
            _baseDataSet[DicomTags.StudyDate].SetDateTime(0, requestInfo.ExamineDate);
            _baseDataSet[DicomTags.StudyTime].SetDateTime(0, requestInfo.ExamineDate);
            _baseDataSet[DicomTags.PatientsSex].SetStringValue(requestInfo.PatientSex);
            _baseDataSet[DicomTags.PatientsAge].SetStringValue(requestInfo.PatientAge.ToString());
            //Study
            _baseDataSet[DicomTags.StudyInstanceUid].SetStringValue(requestInfo.StudyInstanceUid);
            _baseDataSet[DicomTags.StudyDate].SetDateTime(0, DateTime.Now);
            _baseDataSet[DicomTags.StudyTime].SetDateTime(0, DateTime.Now);
            _baseDataSet[DicomTags.AccessionNumber].SetStringValue(requestInfo.AccessionNumber);
            _baseDataSet[DicomTags.StudyDescription].SetStringValue("");
            _baseDataSet[DicomTags.ReferringPhysiciansName].SetNullValue();
            _baseDataSet[DicomTags.StudyId].SetNullValue();
            //Series
            _baseDataSet[DicomTags.SeriesInstanceUid].SetStringValue(serversUid);
            _baseDataSet[DicomTags.Modality].SetStringValue(modality);
            _baseDataSet[DicomTags.SeriesDescription].SetStringValue(seriesDescription);
            _baseDataSet[DicomTags.SeriesNumber].SetStringValue(seriesNumber.ToString());
            //SC Equipment
            _baseDataSet[DicomTags.ConversionType].SetStringValue("WSD");
            //General Image
            _baseDataSet[DicomTags.ImageType].SetStringValue(@"DERIVED\SECONDARY");
            _baseDataSet[DicomTags.PatientOrientation].SetNullValue();
            _baseDataSet[DicomTags.WindowWidth].SetStringValue("");
            _baseDataSet[DicomTags.WindowCenter].SetStringValue("");
            _baseDataSet[DicomTags.PixelSpacing].SetStringValue(@"16\16");

            //Image Pixel
            if (isRbColorOrRbMonoChrome == 0)
            {
                _baseDataSet[DicomTags.SamplesPerPixel].SetInt32(0, 1);
                _baseDataSet[DicomTags.PhotometricInterpretation].SetStringValue("MONOCHROME2");
                _baseDataSet[DicomTags.BitsAllocated].SetInt32(0, 8);
                _baseDataSet[DicomTags.BitsStored].SetInt32(0, 8);
                _baseDataSet[DicomTags.HighBit].SetInt32(0, 7);
                _baseDataSet[DicomTags.PixelRepresentation].SetInt32(0, 0);
                _baseDataSet[DicomTags.PlanarConfiguration].SetInt32(0, 0);
            }
            if (isRbColorOrRbMonoChrome == 1)
            {
                _baseDataSet[DicomTags.SamplesPerPixel].SetInt32(0, 3);
                _baseDataSet[DicomTags.PhotometricInterpretation].SetStringValue("RGB");
                _baseDataSet[DicomTags.BitsAllocated].SetInt32(0, 8);
                _baseDataSet[DicomTags.BitsStored].SetInt32(0, 8);
                _baseDataSet[DicomTags.HighBit].SetInt32(0, 7);
                _baseDataSet[DicomTags.PixelRepresentation].SetInt32(0, 0);
                _baseDataSet[DicomTags.PlanarConfiguration].SetInt32(0, 0);
            }
        }

        private  void SwapRedBlue(byte[] pixels)
        {
            for (int i = 0; i < pixels.Length; i += 3)
            {
                byte temp = pixels[i];
                pixels[i] = pixels[i + 2];
                pixels[i + 2] = temp;
            }
        }
    }
}