﻿using WcfService.DB_Server;

namespace WcfService
{
    public class ExcuteModel
    {
        private bool result = false;
        private string msg = "";
        private string wcfMessage = "";
        private string retValue = "";
        private string errormessage = "";
        public string ErrorMessage { get { return errormessage; } set { errormessage = value; } }
        public string WcfMessage { get { return wcfMessage; } set { wcfMessage = value; } }
        public bool Result { get { return result; } set { result = value; } }
        public string Msg { get { return msg; } set { msg = value; } }
        public string RetValue { get { return retValue; } set { retValue = value; } }
        public int TempViewValue { get; set; }
    }
}