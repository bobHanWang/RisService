﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WcfService.DB_Server;
using WcfService.DB_Server.SystemManage;
using WcfService.DB_Server.WorklistManage;
using WcfService.Models;

namespace WcfService
{
    [ServiceContract]
    public interface IBusinessService
    {
        #region 写报告给医生余额转账
        [OperationContract]
        ExcuteModel SaveSettleAccountsMain(SettleAccountsMain model);
        [OperationContract]
        List<SettleAccountsMain> GetSettleAccountsMain(string RequestID);
        [OperationContract]
        ExcuteModel SaveSettleAccounts(SettleAccounts model);
        [OperationContract]
        ExcuteModel UpdateCustSettleAccountsState(SettleAccounts model);
        [OperationContract]
        List<SettleAccountsList> GetSettleAccountsList(SettleAccountsList model);
        /// <summary>
        /// 结算平分
        /// </summary>
        /// <param name="paramRequestInfo"></param>
        /// <param name="paramUsers"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel SettlementBisection(RequestInfo paramRequestInfo, string paramUserID);
        #endregion

        #region   imageserver  数据同步
        //[OperationContract]
        //ExcuteModel SynPublicALLSaveImageServerData(Study model);
        //[OperationContract]
        //List<ServerPartition> GetServerPartitionList(ServerPartition model);
        //[OperationContract]
        //ExcuteModel SaveServerPartition(ServerPartition model, int type);
        //[OperationContract]
        //List<Patient> GetPatientList(Patient model);
        //[OperationContract]
        //ExcuteModel SavePatient(Patient model, int type);
        //[OperationContract]
        //List<RequestAttributes> GetRequestAttributesList(RequestAttributes model);
        //[OperationContract]
        //ExcuteModel SaveRequestAttributes(RequestAttributes model, int type);
        //[OperationContract]
        //List<Series> GetSeriesList(Series model);
        //[OperationContract]
        //ExcuteModel SaveSeries(Series model, int type);
        //[OperationContract]
        //List<Study> GetStudyList(Study model, DateTime BeginTime, DateTime EndTime);
        //[OperationContract]
        //List<Study> GetStudyList_All(Study model);
        //[OperationContract]
        //ExcuteModel SaveStudy(Study model, int type);
        #endregion

        #region 开通个人账户 充值 提现
        [OperationContract]
        ExcuteModel OpenCustAccount(CustAccount model);
        [OperationContract]
        ExcuteModel JudgeCustAccount(CustAccount model);
        [OperationContract]
        List<CustAccount> GetCustAccountList(CustAccount model);
        [OperationContract]
        ExcuteModel RechargeWithDrawBILL(RechargeWithDrawBILL model, CustAccountModel CustAccount_Model);
        [OperationContract]
        ExcuteModel RechargeWithDrawBILL_Log(RechargeWithDrawBILL model, CustAccountModel CustAccount_Model);
        [OperationContract]
        ExcuteModel UpdateRechargeWithDrawBILLState(RechargeWithDrawBILL model, CustAccountModel CustAccount_Model);
        [OperationContract]
        List<RechargeWithDrawBILL> GetRechargeWithDrawBILLList(RechargeWithDrawBILL model, DateTime? startDate = null, DateTime? endDate = null);
        #endregion

        #region  修改列表
        [OperationContract]
        ExcuteModel UpdateRequestInfo(string RequestId);
        #endregion

        #region  退费
        [OperationContract]
        ExcuteModel RefundInterface(string RequestId, string refund_reason, string Amount, string people);

        #endregion

        #region  支付订单记录表
        [OperationContract]
        ExcuteModel SavePayTransaction_State(PayTransaction model);
        [OperationContract]
        ExcuteModel SavePayTransactionLog(PayTransactionLog model);
        [OperationContract]
        ExcuteModel SavePayTransaction(PayTransaction model);
        /// <summary>
        /// 保存收费信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel SavePayTransactionQRCode(PayTransaction model);
        [OperationContract]
        ExcuteModel SavePayTransaction_Recharge(PayTransaction model);
        [OperationContract]
        List<PayTransaction> GetPayTransactionDetail(string PayTradeID, string RequestId);
        [OperationContract]
        int GetPayTransactionState(string RequestId);
        /// <summary>
        /// 根据时间查询订单信息
        /// </summary>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <returns></returns>
        [OperationContract]
        List<PayTransaction> SearchPayTransactionList(DateTime dtStart, DateTime dtEnd);
        /// <summary>
        /// 修改支付表中的状态
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel ChargePayTransactionState(PayTransaction param);
        #endregion

        #region  机构 支付账号维护
        [OperationContract]
        ExcuteModel SaveOrganPayAccounts(OrganPayAccount model);
        [OperationContract]
        List<OrganPayAccount> GetOrganPayAccountList(OrganPayAccount model);
        [OperationContract]
        List<OrganPayAccount> GetOneOrganPayAccount(string PaymentID, string HospitalID);
        [OperationContract]
        int DeleteOrganPayAccount(string PaymentID);
        #endregion

        #region 操作权限组 用户组 维护
        [OperationContract]
        List<Operationlimits> GetAllOlimits(string Userid);
        /// <summary>
        /// 获取用户对报告的操作权限
        /// </summary>
        /// <param name="UserID">用户ID</param>
        /// <param name="GroupID">权限组ID</param>
        /// <returns></returns>
        [OperationContract]
        List<string> GetAllOlimitsData(string UserID, string GroupID);
        [OperationContract]
        ExcuteModel SaveOperationlimits(Operationlimits model);
        [OperationContract]
        List<Operationlimits> GetOperationlimitsList(string type);
        [OperationContract]
        ExcuteModel SaveGroups(Groups model);
        [OperationContract]
        ExcuteModel SaveGroups_ArrayList(Groups model, string ArrayList);
        [OperationContract]
        List<Groups> GetGroupsList(string type);
        [OperationContract]
        ExcuteModel SaveUserGroup(UserGroup model);
        [OperationContract]
        List<UserGroup> GetUserGroupList(string type);
        [OperationContract]
        List<UserGroup> GetUserGroupInfo(string userID);
        [OperationContract]
        ExcuteModel SaveOperationGroup(OperationGroup model);
        [OperationContract]
        List<OperationGroup> GetOperationGroupList(string type);
        [OperationContract]
        List<Operationlimits> GetOneOperationlimits(string Groupid, string olimitid);
        [OperationContract]
        List<GroupOperation> GetOneGroupOperation(string Groupid, string olimitid);
        [OperationContract]
        List<UserGroup> GetOneusergroup(string Groupid, string UserId);
        #endregion

        #region   审核记录表
        [OperationContract]
        ExcuteModel SaveAuditRecord(AuditRecord Model);
        [OperationContract]
        List<AuditRecord> GetAuditRecordByRequestId(string RequestId);
        #endregion

        #region 关联下属医院

        [OperationContract]
        ExcuteModel SaveSubHospital_relevance(SubHospital model);
        [OperationContract]
        ExcuteModel SaveSubHospital(SubHospital model);
        [OperationContract]
        List<SubHospital> GetAllSubHospital(string PHospitalId, string SHname, string txt_name);
        /// <summary>
        /// 根据医院获取相关中心医院
        /// </summary>
        /// <param name="strHospitalID"></param>
        /// <returns></returns>
        [OperationContract]
        List<SubHospital> GetSubHospitalList(string strHospitalID);
        #endregion

        #region  clearcanvas
        [OperationContract]
        string GetRoot();
        [OperationContract]
        List<string> StudyFolder();
        [OperationContract]
        List<string> Get_path(string patientid, string studyInstanceUid);
        //[OperationContract]
        //List<Study> GetStudy();
        //[OperationContract]
        //List<Study> GetSearchStudy(string idorname, DateTime datetime1, DateTime datetime2, string patientId);
        /// <summary>
        /// 查询未缴费患者列表
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <param name="name"></param>
        /// <param name="patientId"></param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <returns></returns>
        [OperationContract]
        List<RequestStudy> GetRequestStudyList(string hospitalID, string name, string patientId, int? costState, DateTime dtStart, DateTime dtEnd);
        //[OperationContract]
        //List<Study> GetSearchStudyAPP(string idorname, DateTime datetime1, DateTime datetime2, string patientId, string HospitalId);
        ///// <summary>
        ///// 同步数据请勿修改
        ///// </summary>
        ///// <param name="strPatientId"></param>
        ///// <param name="strAccessNumber"></param>
        ///// <param name="strStudyInstanceUid"></param>
        ///// <returns></returns>
        //[OperationContract]
        //List<Study> SearchStudyList(string strPatientId, string strAccessNumber, string strStudyInstanceUid);
        //[OperationContract]
        //List<Study> GetStudyByPatientId(string patientId, string studyInstanceUID);
        [OperationContract]
        List<Patient_requestDetail> GetPatient_requestDetail(string patientId, string HospitalId, string H_TYPE, string H_TXT_NAME);
        [OperationContract]
        List<Patient_requestDetail> GetDoctors_AuditRecords(string patientId, string HospitalId, string ReviewStatus, string CPatientName, string DiagnoseDoctor);
        [OperationContract]
        List<Patient_requestDetail> Getwrite_Report(string patientId, string HospitalId, string ReviewStatus, string CPatientName, string DiagnoseDoctor);
        [OperationContract]
        List<Patient_requestDetail> Alreadypaycost_Nowrite_Report(string patientId, string HospitalId, string ReviewStatus, string CPatientName, string DiagnoseDoctor);
        [OperationContract]
        PatientInfo GetOnePatientInfo(string PatientId);
        #endregion

        [OperationContract]
        void CreateDirectory(string path);
        /// <summary>
        /// 判断文件是否存在
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        [OperationContract]
        bool JudgeFile(string path);

        #region  目录菜单维护
        /// <summary>
        /// DiagnoseType
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel SaveDiagnoseType_Name(DiagnoseType model);
        [OperationContract]
        List<DiagnoseType> GetAllDiagnoseType();
        [OperationContract]
        DiagnoseType GetONEDiagnoseType(string DiagnoseType_id);
        [OperationContract]
        ExcuteModel DelDiagnoseType_Name(DiagnoseType model);
        [OperationContract]
        ExcuteModel judge_Diagnosetype_name(DiagnoseType model);
        #region
        /// <summary>
        /// 模板操作
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel SaveDiagnoseTemp(DiagnoseTemp param);
        /// <summary>
        /// 保存模板内容信息
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel SaveDiagnoseTempContent(DiagnoseTemp param);
        /// <summary>
        /// 删除节点
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel DelDiagnoseTempNode(DiagnoseTemp param);
        /// <summary>
        /// 获取单个模板内容
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        DiagnoseTemp GetDiagnoseTemp(DiagnoseTemp param);
        /// <summary>
        /// 获取相应值的最大结果
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        int GetParentOrderbyValue(int param, string strName);
        /// <summary>
        /// 获取父节点值
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        int GetPatientIdValue();
        /// <summary>
        /// 判断是否有根节点
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        int GetPatientNodeCout(int param);
        /// <summary>
        /// 查询主节点和二级节点，排序为降序的公共模板
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<DiagnoseTemp> GetDiagnoseTempPatient(int isPublic, string userName);
        /// <summary>
        /// 获取单个节点数据
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        List<DiagnoseTemp> GetDiagnoseTempNodeData(int param, int nodeTypeParam, int nodeOrderParam, int isPublic, string userName);
        /// <summary>
        /// 获取自动编号
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        int GetDiagnoseTempRootID();
        /// <summary>
        /// 获取父节点排序值，为查询做准备
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        int GetParentNodeOrderValue();
        /// <summary>
        /// 获取DiagnsoeTemp表数据总数
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        int GetDiagnoseTempCount();
        /// <summary>
        /// 拖动节点修改值
        /// </summary>
        /// <param name="rootID"></param>
        /// <param name="nodeTypes"></param>
        /// <param name="pRootID"></param>
        /// <param name="pNodeTypes"></param>
        /// <param name="pNodeOrder"></param>
        /// <param name="strTempName"></param>
        [OperationContract]
        void UpdateDiagnoseTemp(int rootID, int nodeTypes, int pRootID, int pNodeTypes, int pNodeOrder, string strTempName);
        #endregion

        #endregion
        [OperationContract]
        List<OutRqeustInfo> GetRequestList(string Type, string HospitalId, string FeeState, DateTime dtStart, DateTime dtEnd, string PatientName, string PatientId, int smallAge, int bigAge, string Sex, string accessionNumber = null, string RequestDoctor = "", string requestJDR = null, string PatientType = "", string ExamineType = "");
        //待审核
        [OperationContract]
        List<OutRqeustInfo> GetRequestList_DSH(string Type, string HospitalId, string FeeState, DateTime dtStart, DateTime dtEnd, string PatientName, string PatientId, string Docator_Nmae);
        [OperationContract]
        List<OutRqeustInfo> GetRequestList_YSH(string Type, string HospitalId, string FeeState, DateTime dtStart, DateTime dtEnd, string PatientName, string PatientId, string Docator_Nmae);
        /// <summary>
        /// 获取待审核病人信息
        /// </summary>
        [OperationContract]
        List<OutRqeustInfo> GetPatient_CheckingDetail(string Type, string HospitalId, string PatientName, string PatientId, int ReviwState, DateTime dtStart, DateTime dtEnd, int Age = 0, string Sex = "", string roleType = "", string RequestDcotor = "");
        [OperationContract]
        List<OutRqeustInfo> GetPatient_CheckingDetail_bg(string Type, string HospitalId, string PatientName, string PatientId, int ReviwState, DateTime dtStart, DateTime dtEnd, int Age = 0, string Sex = "", string roleType = "", string RequestDcotor = "");
        [OperationContract]
        List<OutPayInfo> GetPayInfoList(string Type, string HospitalId, string FeeState, DateTime dtStart, DateTime dtEnd, string PatientName, string PatientId);

        /// <summary>
        /// 数据查询
        /// </summary>
        /// <param name="Type"></param>
        /// <param name="HospitalId"></param>
        /// <param name="FeeState"></param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <param name="PatientName"></param>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        [OperationContract]
        List<OutRqeustInfo> GetConDiscussionInfoList(string Type, string HospitalId, string FeeState, string dtStart, string dtEnd, string PatientName, string PatientId, string strUserID = "", string ExaminePosition = "", string ExamineMethod = "", string IdentityNum = "", string Sex = "");
        /// <summary>
        /// </summary>
        /// <param name="RequestStates"></param>
        /// <param name="HospitalId"></param>
        /// <param name="FeeState"></param>
        /// <param name="PageIndex"></param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <param name="PatientName"></param>
        /// <param name="PatientId"></param>
        /// <param name="pageCount"></param>
        /// <param name="patientCount"></param>
        /// <returns></returns>
        [OperationContract]
        List<OutRqeustInfo> GetConDiscussionPatientList(string RequestStates, string HospitalId, string FeeState, string strUserID, int PageIndex, string dtStart, string dtEnd, string PatientName, string PatientId, string strSex, int intAge, out int pageCount, out int patientCount);
        [OperationContract]
        List<OutRqeustInfo> GetPayInfoList_alllist(string Type, string HospitalId, string FeeState, DateTime dtStart, DateTime dtEnd, string PatientName, string PatientId);
        /// <summary>
        /// 会诊
        /// </summary>
        /// <param name="PatientName">病人姓名</param>
        /// <param name="ChooseType">类型</param>
        /// <param name="strDoctor">专家</param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <returns></returns>
        [OperationContract]
        List<OutRqeustInfo> GetPatient_ConsultationDetail(string PatientName, string PatientId, int ChooseType, string strDoctor, DateTime dtStart, DateTime dtEnd, int Age = 0, string Sex = null);
        /// <summary>
        /// 统计会诊数据量
        /// </summary>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <returns></returns>
        [OperationContract]
        List<OutRqeustInfo> GetConsulationStatistics(string HospitalID, DateTime dtStart, DateTime dtEnd);
        /// <summary>
        /// 获取申请、诊断或会诊患者数量
        /// </summary>
        /// <param name="strDoctor"></param>
        /// <param name="strHospitalID"></param>
        /// <param name="strConsulation"></param>
        /// <returns></returns>
        [OperationContract]
        int GetPatientCounts(string strDoctor, string strHospitalID, string strConsulation);
        [OperationContract]
        List<OutRqeustInfo> GetPatient_ConsultationDetail_allList(string state, string PatientName, string PatientId, int ChooseType, string strDoctor, DateTime dtStart, DateTime dtEnd, int Age = 0, string Sex = null);
        [OperationContract]
        List<OutRqeustInfo> GetDiagnosePatientInfoList(string RequestStateID, string HospitalId, string PatientName, string PatientId, int ReviwState, int PageIndex, DateTime dtStart, DateTime dtEnd, int smallAge, int bigAge, string Sex, string RequestDcotor, out int pageCount, out int patientCount);
        [OperationContract]
        List<OutRqeustInfo> GetDiagnosePatientBaseList(string requestState, string hospitalId, string hospitalSon, string patientName, string patientId, int reviwState, int pageIndex, DateTime dtStart, DateTime dtEnd, int smallAge, int bigAge, string Sex, string requestDcotor, string diagnoseDoctor, string examineType, out int pageCount, out int patientCount);
        /// <summary>
        /// 报告端数据查询
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        List<OutRqeustInfo> GetDiagnoseBaseList(InBaseParam param);
        [OperationContract]
        List<OutRqeustInfo> GetConsulationPatientInfoList(string PatientName, string PatientId, string HospitalID, int PageIndex, int ChooseType, string strDoctor, DateTime dtStart, DateTime dtEnd, int smallAge, int bigAge, string Sex, out int pageCount, out int patientCount);
        /// <summary>
        /// 获取子表记录信息
        /// </summary>
        /// <param name="requestidParam"></param>
        /// <returns></returns>
        [OperationContract]
        List<StudyRecordInfo> GetStudyRecordInfoList(string requestidParam);
        /// <summary>
        /// 查询子列表数据
        /// </summary>
        /// <param name="requestParam"></param>
        /// <returns></returns>
        [OperationContract]
        List<StudyRecordInfo> GetRequestStudyRecordAllList(string requestParam);
        [OperationContract]
        PatientInfo GetPatientInfoByPatientId(string PatientId);
        [OperationContract]
        RequestInfo GetRequestInfoByRequestId(string RequestId);
        [OperationContract]
        List<RequestInfoStatis> GetRequestStudyRecordByRequestId(string RequestId);
        /// <summary>
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        List<RequestInfo> GetRequestInfoListByStudyInstance(string hospital, string param);
        /// <summary>
        /// 错误方法，请查看
        /// </summary>
        /// <param name="RequestId"></param>
        /// <param name="StudyId"></param>
        /// <returns></returns>
        [OperationContract]
        RequestInfo GetRequestInfoByRequestIdOrStudyId(string RequestId, string StudyId);
        [OperationContract]
        List<RequestInfo> GetRequestInfoByPatientId(string PatientId);
        [OperationContract]
        ExcuteModel SaveRequestInfo(RequestInfo model);
        [OperationContract]
        ExcuteModel SaveUpdateRequestInfo(RequestInfo model);
        /// <summary>
        /// 修改打印状态
        /// </summary>
        /// <param name="RequestID"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdateRequestPrintState(string RequestID);
        [OperationContract]
        ExcuteModel SavePatientInfo(PatientInfo model);
        [OperationContract]
        ExcuteModel UpdatePatientInfo(PatientInfo model);
        /// <summary>
        /// 修改PatientInfo的PatientID
        /// </summary>
        /// <param name="newPatientId">新的PatientId</param>
        /// <param name="oldPatientId">旧PatientId</param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdatePatientId(string newPatientId, string oldPatientId);
        [OperationContract]
        PatientInfo GetPatientInfoByZJHM(string ZJLX, string ZJHM);
        [OperationContract]
        ExcuteModel RequestSubmit(InRequest InModel);
        [OperationContract]
        ExcuteModel ChangeRequestState(InRequestReport InModel);
        [OperationContract]
        int GetRequestState(string RequestId);
        [OperationContract]
        ExcuteModel SaveDiagnoseReport(InDianose InModel);
        [OperationContract]
        Diagnose GetDianoseByRequestId(string RequestId);
        /// <summary>
        /// 更新报告状态
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel ChangeDiagnoseState(Diagnose model);
        [OperationContract]
        Diagnose GetDiagnoseReportByRequesId(string RequestId);
        [OperationContract]
        ExcuteModel Charge(InChage InModel);
        [OperationContract]
        ExcuteModel ChangeDianoseDoctor(string RequestId, string DoctorName, string ServiceId = "");
        #region 费用

        /// <summary>
        /// 用于缴费
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel SaveRequestServiceCost(RequestInfo model);
        #endregion

        #region 病人文件
        [OperationContract]
        ExcuteModel SaveRequestFileRoot(RequestFileRoot model);
        [OperationContract]
        List<RequestFileRoot> GetRequestFileRootByRequestIdAndFileRoot(string RequestId, int FileRoot);
        /// <summary>
        /// 获取病人所有病例文件
        /// </summary>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        [OperationContract]
        List<RequestFileItem> GetAllRequestFileItems(string RequestId, string StudyInstanceUID);
        //根据RequestId,获取病历所有信息
        [OperationContract]
        List<RequestFileRoot> GetRequestFileRootByRequestIdAll(string RequestId);
        [OperationContract]
        List<RequestFile> GetRequestFileByRequestIdAll(string RequestId);
        [OperationContract]
        List<RequestFile> GetAllFileListByRequestId(string RequestId, int type);
        [OperationContract]
        ExcuteModel SaveFileItem(RequestFileItem model);
        [OperationContract]
        List<RequestFileItem> GetFileItemsBySource(int Source, string RequestId);
        [OperationContract]
        List<RequestFile> GetDicomFileByRequestIdAndMPID(string RequestId, string MPID);
        [OperationContract]
        RequestFile GetRequestFileByMPID(string RequestId, string MPID);
        [OperationContract]
        List<RequestFileItem> GetFileItemsByFileId(string FileId);
        [OperationContract]
        List<RequestFileItem> GetFileItemsByFileRootId(string FileRootId);
        [OperationContract]
        List<RequestFileItem> GetFileItemsByRequestId(string RequestId);
        [OperationContract]
        List<RequestFileItem> GetNoFileItemsByRequestId(string RequestId, string StudyUid = null);
        [OperationContract]
        ExcuteModel SaveFile(RequestFile model);
        [OperationContract]
        List<RequestFile> GetFileBySource(int Source, string RequestId);
        [OperationContract]
        ExcuteModel DeleteFile(string FileId);
        [OperationContract]
        ExcuteModel DeleteFileItemByItemId(string FileItemId);
        #endregion

        #region 驳回
        [OperationContract]
        ExcuteModel SaveRefusedRecord(RefusedRecord Model);
        [OperationContract]
        List<RefusedRecord> GetRefusedRecordByRequestId(string RequestId);
        #endregion

        #region 系统配置
        /// <summary>
        /// 系统配置
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        [OperationContract]
        WcfService.DB_Server.SystemManage.Sys_Settings GetSysValueByKey(string Key);
        [OperationContract]
        List<WcfService.DB_Server.SystemManage.Sys_Settings> GetSysList();
        [OperationContract]
        ExcuteModel SaveSys_Settings(WcfService.DB_Server.SystemManage.Sys_Settings InModel);
        [OperationContract]
        ExcuteModel DeleteSys_Settings(Sys_Settings model);
        #endregion

        #region   查询专家会诊记录
        [OperationContract]
        List<ExpertConsultationRecord> ExpertRecord(string ServiceId, string dept_id, string DiagnoseDoctor, string txt_name);
        [OperationContract]
        List<ExpertConsultationRecord> Search_ExpertRecord(string ServiceId, string dept_id);
        #endregion

        #region 上传和下载标记
        /// <summary>
        /// 保存和更新数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel SaveFileUpDownRecordDetail(FileUpDownRecord model);
        /// <summary>
        /// 获取上传下载记录信息
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="upLoadPerson"></param>
        /// <param name="downPerson"></param>
        /// <returns></returns>
        [OperationContract]
        List<FileUpDownRecord> GetFileUpDownRecordByRequestId(string requestId, string hospitalID, string upLoadPerson, string downPerson);

        /// <summary>
        /// 获取手机端已上传患者
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="state"></param>
        /// <param name="dtStar"></param>
        /// <param name="dtEnd"></param>
        /// <returns></returns>
        [OperationContract]
        List<FileUpDownRecord> GetFileUpDownRecordByIDMList(string requestId, string hospitalID, int? uploadFlag, string state, DateTime dtStar, DateTime dtEnd);
        #endregion

        #region 更新和关闭报告状态
        /// <summary>
        /// 正常关闭报告状态
        /// </summary>
        /// <param name="requestID"></param>
        /// <returns></returns>
        [OperationContract]
        void NormalRequestInfoReportState(string requestID);

        /// <summary>
        /// 更新报告状态
        /// </summary>
        /// <param name="requestID"></param>
        /// <param name="reportPerson"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdateRequestInfoReportState(string requestID, string reportPerson);

        /// <summary>
        /// 获取诊断报告列表信息
        /// </summary>
        /// <param name="PatientId"></param>
        /// <param name="RequestState"></param>
        /// <param name="ReviewState"></param>
        /// <param name="RequestId"></param>
        /// <param name="DtStart"></param>
        /// <param name="DtEnd"></param>
        /// <returns></returns>
        [OperationContract]
        List<OutDiagnoseReport> GetDiagnoseReportList(string PatientId, string HospitalId, string RequestId, string RequestState, int ReviewState, DateTime DtStart, DateTime DtEnd);
        #endregion
        /// <summary>
        /// 获取数据库连接
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        string GetConnStr();
        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="fileName"></param>
        [OperationContract]
        void DeleteFiles(string fileName);

        /// <summary>
        /// 删除文件夹
        /// </summary>
        /// <param name="pathParam"></param>
        [OperationContract]
        void DeleteDir(string pathParam);

        /// <summary>
        /// 保存同步记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel SaveSynRecord(SynRecord model);
        /// <summary>
        /// 获取同步记录
        /// </summary>
        /// <param name="HospitalId"></param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <param name="PatientId"></param>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        [OperationContract]
        List<SynRecord> GetSynRecordList(string HospitalId, DateTime dtStart, DateTime dtEnd, string PatientId, string RequestId);
        /// <summary>
        /// 修改状态
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdateSynRecord(SynRecord Model);
        #region
        /// <summary>
        /// 保存已拉取患者数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel SavePullPatient(PullPatient model);

        /// <summary>
        /// 获取拉取患者信息
        /// </summary>
        /// <param name="strRequestID">申请ID</param>
        /// <param name="strPatientID">患者ID</param>
        /// <param name="starDate">开始时间</param>
        /// <param name="endDate">结束时间</param>
        /// <returns></returns>
        [OperationContract]
        List<PullPatient> GetPullPatientList(string strRequestID, string strPatientID);

        [OperationContract]
        void DeleteFileInfo(string path, string name, string pwd, string domain);
        #endregion

        #region 记录手机端上传影像
        /// <summary>
        /// 保存和更新上传影像数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel SaveUpDicomRecordDetail(UpDicomRecord model);

        /// <summary>
        /// 获取影像信息
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="upLoadPerson"></param>
        /// <param name="downPerson"></param>
        /// <returns></returns>
        [OperationContract]
        List<UpDicomRecord> GetUpDicomRecordByRequestId(string requestId, DateTime dtStar, DateTime dtEnd);

        /// <summary>
        /// 获取影像库中是否有数据
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="studyInstanceUID"></param>
        /// <returns></returns>
        [OperationContract]
        int GetUpDicomRecordCountByRequestId(string requestId, string studyInstanceUID);
        #endregion

        #region 同步ImageServer库数据
        ////保存ServerPartition表
        //[OperationContract]
        //ExcuteModel SaveServerPartitionBase(ServerPartition model);

        //[OperationContract]
        //List<ServerPartition> GetServerPartitionAllList();

        ////获取患者基本信息
        //[OperationContract]
        //Patient GetPatientBase(Patient model);

        ////保存Patient基本信息
        //[OperationContract]
        //ExcuteModel SavePatientBase(Patient model);

        ////查询StudyStorage基本信息
        //[OperationContract]
        //StudyStorage GetStudyStorageBase(StudyStorage model);

        ////保存 StudyStorage基本信息
        //[OperationContract]
        //ExcuteModel SaveStudyStorageBase(StudyStorage model);

        ////查询Series基本信息
        //[OperationContract]
        //List<Series> GetSeriesStudyList(Series model);

        ////保存Series基本信息
        //[OperationContract]
        //ExcuteModel SaveSeriesStudyBase(Series model);

        ////查询Study基本信息
        //[OperationContract]
        //List<Study> GetStudyBaseList(Study model, DateTime BeginTime, DateTime EndTime);

        ////保存Study基本信息
        //[OperationContract]
        //ExcuteModel SaveStudyBase(Study model);

        /// <summary>
        /// 保存未缴费患者信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel SaveRequestStudy(RequestStudy model);

        /// <summary>
        /// 更新缴费状态
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdateRequestStudyState(RequestStudy model);
        #endregion

        #region 支付宝后台统计
        /// <summary>
        /// 统计总收入
        /// </summary>
        /// <param name="DateParam"></param>
        /// <returns></returns>
        [OperationContract]
        List<OutChargeCost> GetPayAllCount(string DateParam);

        /// <summary>
        /// 各机构总收入
        /// </summary>
        /// <param name="DateParam"></param>
        /// <returns></returns>
        [OperationContract]
        List<OutChargeCost> GetPayOrganCount(string DateParam);

        /// <summary>
        /// 机构收入情况
        /// </summary>
        /// <param name="DateParam"></param>
        /// <returns></returns>
        [OperationContract]
        List<OutChargeCost> GetPayEveryOrganCount(string DateParam);

        /// <summary>
        /// 机构会诊费收入统计
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<OutChargeCost> GetPayConsulOrganAllCount(string DateParam);

        /// <summary>
        /// 支付方式会诊费收入
        /// </summary>
        /// <param name="DateParam"></param>
        /// <returns></returns>
        [OperationContract]
        List<OutChargeCost> GetPayConsulAllCount(string DateParam);

        /// <summary>
        /// 服务项目会诊费收入
        /// </summary>
        /// <param name="DateParam"></param>
        /// <returns></returns>
        [OperationContract]
        List<OutChargeCost> GetPayConsulServiceAllCount(string DateParam);

        /// <summary>
        /// 机构统计收费报表
        /// </summary>
        /// <param name="DateParam"></param>
        /// <returns></returns>
        [OperationContract]
        List<OutChargeCost> GetPayOrganCountReport(string DateParam);

        /// <summary>
        ///  个人统计收费报表
        /// </summary>
        /// <param name="ServiceId"></param>
        /// <param name="PayType"></param>
        /// <param name="BeginDate"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        [OperationContract]
        List<OutChargeChart> GetPayPersonCountReport(string ServiceId, string PayType, DateTime BeginDate, DateTime EndDate);
        #endregion

        #region 讨论主题

        /// <summary>
        /// 发布主题
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel SaveDiscussionTopic(XR_DiscussionTopic param);

        /// <summary>
        /// 删除主题
        /// </summary>
        /// <param name="themeID"></param>
        /// <returns></returns>
        [OperationContract]
        int? DeleteDiscussionTopicByID(int themeID);

        /// <summary>
        /// 发布的主题（我创建的主题、已结束的主题 状态为3）
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<XR_DiscussionTopic> GetDiscussionTopics(XR_DiscussionTopic param);

        /// <summary>
        /// 我参与的主题
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<XR_DiscussionTopic> GetMyAttentionDiscussionTopics(XR_DiscussionTopic param);

        /// <summary>
        /// 我关注的主题
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel SaveMyAttention(XR_MyAttention param);

        #endregion

        /// <summary>
        /// 保存关键词
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel SaveXRKeywords(XR_Keywords param);

        /// <summary>
        /// 获取关键词中的PatientID
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        List<string> GetXRKeywords(XR_Keywords param);

        /// <summary>
        /// 清除搜索记录
        /// </summary>
        /// <param name="param"></param>
        [OperationContract]
        ExcuteModel ClearXRKeywords(XR_Keywords param);

        /// <summary>
        /// 查询未缴费患者列表(按照搜索)
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <param name="name"></param>
        /// <param name="patientId"></param>
        [OperationContract]
        List<RequestStudy> GetRequestStudyListByKeyWords(string hospitalID, string patientId, string userId);


        /// <summary>
        /// 创建用户钱包
        /// </summary>
        /// <param name="objCustAccount"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel AddUserAccount(CustAccount objCustAccount);

        /// <summary>
        /// 添加医生
        /// </summary>
        /// <param name="objCustAccount"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel AddDoctInfo(DoctorInfo objDoctorInfo);

        /// <summary>
        /// 更新专家机构信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdateDoctorInfo(DoctorInfo model);

        /// <summary>
        /// 更新专家状态
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdateDoctorInfoState(DoctorInfo model);

        /// <summary>
        /// 获取医生
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<DoctorInfo> GetDoctorInfo();

        /// <summary>
        /// 根据编号获取医生信息
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        DoctorInfo GetDoctorInfoById(string doctorId);

        /// <summary>
        /// 保存并更新医信息
        /// </summary>
        /// <param name="objDoctorInfo"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel SaveDoctorInfo(DoctorInfo objDoctorInfo);

        /// <summary>
        /// 获取排序最大值
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        int? GetMaxOrderValue();

        /// <summary>
        /// 联合查询专家信息
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<DoctorBaseInfo> GetDoctorInfoList();

        #region
        [OperationContract]
        ExcuteModel AddRequestImageViewInfo(RequestImageView param);

        /// <summary>
        /// 查看是否浏览影像
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel IsExistRequestImageView(string requestId, string userId);
        #endregion

        #region 查询会诊数量记录表
        /// <summary>
        /// 根据机构ID查询所有会诊数据量
        /// </summary>
        /// <param name="strHostitalID"></param>
        /// <returns></returns>
        [OperationContract]
        List<ConsulaDataRecord> GetConsulaDataRecordList(ConsulaDataRecord model);

        /// <summary>
        /// 保存会诊数据记录信息
        /// 数量不做判断
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel SaveConsulaDataRecord(ConsulaDataRecord model);

        /// <summary>
        /// 更新ConAdd数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdateConsulaDataRecordConAddGUID(ConsulaDataRecord model);

        /// <summary>
        /// 更新ConAll数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdateConsulaDataRecordConAllGUID(ConsulaDataRecord model);

        /// <summary>
        /// 更新ConFree
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdateConsulaDataRecordConFreeGUID(ConsulaDataRecord model);

        /// <summary>
        /// 更新ConOver
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdateConsulaDataRecordConOverGUID(ConsulaDataRecord model);

        /// <summary>
        /// 更新ConAlready
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdateConsulaDataRecordConAlreadyGUID(ConsulaDataRecord model);

        /// <summary>
        /// 更新ConReturn
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdateConsulaDataRecordConReturnGUID(ConsulaDataRecord model);

        /// <summary>
        /// 如果约定内的数据用完则重新添加
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel AddConCount(ConsulaDataRecord model);

        /// <summary>
        /// 根据机构、服务项目、年份
        /// 获取单个会诊记录数据
        /// </summary>
        /// <param name="strHospitalID"></param>
        /// <param name="strServiceUID"></param>
        /// <returns></returns>
        [OperationContract]
        ConsulaDataRecord GetConsulaDataRecordModel(ConsulaDataRecord modelParam);
        #endregion

        #region 关联机构信息
        /// <summary>
        /// 添加关联机构
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel InsertRelateHospital(RelateHospital model);

        /// <summary>
        /// 设置默认机构
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdateRelateHospital(RelateHospital model);

        /// <summary>
        /// 删除关联机构
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel DeleteRelateHospital(RelateHospital model);

        /// <summary>
        /// 子医院列表
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<Sys_Dept> GetRelateSubHospitalList(string hospitalID, string userID);

        /// <summary>
        /// 子医院关联列表
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        [OperationContract]
        List<RelateHospital> GetRelateHospitalList(string userID);
        #endregion

        #region 检查部位
        /// <summary>
        /// 获取检查部位列表
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<ExaminePosition> GetExamPosList();
        [OperationContract]
        List<ExaminePosition> GetExamPosListNew();

        [OperationContract]
        ExcuteModel AddExamPositionInfo(ExaminePosition objExaminePos);

        [OperationContract]
        ExaminePosition GetExamPositionById(int id);
        [OperationContract]
        ExcuteModel UpdateExamPositionInfo(int id, int state);
        [OperationContract]
        ExcuteModel UpdateExamPosition(int id, ExaminePosition objExamPos);
        [OperationContract]
        List<ExaminePosition> GetExaminePositionByExamineName(string examineName);
        [OperationContract]
        ExcuteModel IsExistedExaminePositionName(string positionName);
        /// <summary>
        /// 更新检查部位状态
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdateExaminePositionState(ExaminePosition param);

        /// <summary>
        ///  更新检查部位信息
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdateExaminePosition(ExaminePosition param);
        [OperationContract]
        ExcuteModel UpdateCheckPosition(int id, string subMethod, string mainMethod);
        [OperationContract]
        List<ExaminePosition> GetExaminePositionByParentNode(int parentNode);
        /// <summary>
        /// 获取检查部位排序最大值
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        int? GetExaminePositionMax();
        #endregion

        #region 检查方法
        [OperationContract]
        List<ExamineMethod> GetExamMethodList();
        [OperationContract]
        List<ExamineMethod> GetExamMethodListNew();
        [OperationContract]
        ExcuteModel AddExamMethodInfo(ExamineMethod objExamineMet);
        [OperationContract]
        ExcuteModel UpdateExamMethodInfo(int id, int state);
        [OperationContract]
        ExamineMethod GetExamMethodById(int id);
        [OperationContract]
        ExcuteModel UpdateExamMethod(int id, ExamineMethod objExamMet);
        [OperationContract]
        ExcuteModel IsExistedExamineMethodName(string positionName);
        /// <summary>
        /// 更新检查方法状态
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdateExamineMethodState(ExamineMethod param);
        /// <summary>
        ///  更新检查方法信息
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdateExamineMethod(ExamineMethod param);
        /// <summary>
        /// 获取检查方法排序最大值
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        int? GetExamineMethodMax();
        [OperationContract]
        List<ExamineMethod> GetExamineMethodByParentNode(int parentNode);
        #endregion

        #region 消息提醒
        [OperationContract]
        ExcuteModel AddMsgFrameState(MsgFrameState objMsgFrameState);
        [OperationContract]
        ExcuteModel UpdateMsgFrameState(string str, int intState);
        [OperationContract]
        List<MsgFrameState> GetMsgFrameState(string str);
        /// <summary>
        /// 获取申请患者数据
        /// </summary>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        [OperationContract]
        List<MsgFrameState> GetMsgFrameRequestData(string strUserID, int intState);
        [OperationContract]
        ExcuteModel UpdateSingleMsgFrameState(string requestId, int intState);
        #endregion

        #region 排班
        [OperationContract]
        List<ScheduleSettings> GetScheduleSettings(string deptId, string startTime, string endTime);
        [OperationContract]
        ExcuteModel InitScheduleSettings(List<ScheduleSettings> list, string deptId, string userId, string createName);
        [OperationContract]
        ExcuteModel SaveScheduling(List<string> sqlList);
        [OperationContract]
        List<UserType> GetUserType();
        [OperationContract]
        ExcuteModel InsertShiftSettings(ShiftSettings shiftSettings);
        [OperationContract]
        bool IsExistsTemplate(string deptId, string startTime, string endTime);
        [OperationContract]
        ExcuteModel AddScheduleTemplate(List<string> sqlList);
        [OperationContract]
        ExcuteModel AddShiftSettings(ShiftSettings shiftSettings);
        [OperationContract]
        List<ShiftSettings> GetShiftSettings(string deptId);
        [OperationContract]
        bool IsExistsScheduleSettings(string deptId, string startTime, string endTime);
        [OperationContract]
        ExcuteModel UpdateScheduleTemplate(List<string> sqlList);
        [OperationContract]
        ShiftSettings GetShiftSettingsById(int shiftId);
        [OperationContract]
        ExcuteModel UpdateShiftSettings(ShiftSettings shiftSettings);
        [OperationContract]
        ExcuteModel DeleteShiftSettings(int shiftId);
        [OperationContract]
        List<ScheduleTemplate> GetScheduleTemplate(string sql);
        [OperationContract]
        List<ScheduleTemplate> GetScheduleTemplateById(string deptId);
        [OperationContract]
        ExcuteModel UpdateScheduleSettings(List<string> sqlList);
        [OperationContract]
        List<DoctorInfo> GetDoctorInfoByDeptId(string deptId);
        [OperationContract]
        string GetDoctorNameByDeptIdAndId(string sql);
        [OperationContract]
        ShiftSettings GetScheduleUserType(string shiftName, string deptId);
        #endregion

        #region 申请检查记录接口

        [OperationContract]
        ExcuteModel AddRequestStudyRecord(Dictionary<string, RequestStudyRecord> dictionary);
        [OperationContract]
        ExcuteModel UpdateRequestStudyRecord(RequestStudyRecord requestRecord);
        [OperationContract]
        ExcuteModel DeleteRequestStudyRecord(RequestStudyRecord requestRecord);
        [OperationContract]
        List<RequestStudyRecord> GetRequestStudyRecord();
        [OperationContract]
        RequestStudyRecord GetRequestStudyRecordByUid(string strRequestID, string studyInstanceUid);
        [OperationContract]
        ExcuteModel AddSingleRequestStudyRecord(RequestStudyRecord requestStudyRecord);
        #endregion

        #region 退费申请确认
        [OperationContract]
        List<RefundRecordManager> GetRefundStateRecordInfo(string patientId, string patientName, DateTime startTime, DateTime endTime, string hospitalId, string remark = null);
        [OperationContract]
        ExcuteModel UpdateRefundStateRecord(string requestId, int isRefund);
        [OperationContract]
        ExcuteModel AddRefundStateRecord(RefundStateRecord refundRecord);
        [OperationContract]
        RefundStateRecord GetRefundStateRecordById(string requestId);
        [OperationContract]
        ExcuteModel UpdateReturnCause(string requestId, string returnCause);
        #endregion

        #region 单个患者多影像申请
        [OperationContract]
        ExcuteModel UpdateRequestStudyRecordExaminePort(string requestId, string studyInstanceUid, string examinePort);
        [OperationContract]
        ExcuteModel UpdateRequestStudyRecordExamineMethod(string requestId, string studyInstanceUid, string examineMethod);
        [OperationContract]
        ExcuteModel UpdateRequestInfoExaminePort(string requestId, string studyInstanceUid, string examinePort);
        [OperationContract]
        ExcuteModel UpdateRequestInfoExamineMethod(string requestId, string studyInstanceUid, string examinePort);
        [OperationContract]
        ExcuteModel UpdateRequestStudyRecordUploadCase(string requestId, string studyInstanceUid, int isUploadCase);
        [OperationContract]
        List<RequestStudyRecordInfo> GetRequestStudyRecordList(string requestId);
        #endregion

        #region 图片转换为dicom文件
        /// <summary>
        /// 图片转换成DICOM文件
        /// </summary>
        /// <param name="para"></param>
        [OperationContract]
        void ImageToDicom(DateTime startDate, DateTime endDate);
        #endregion

        #region
        /// <summary>
        /// 获取医院总数
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        int GetHospitalAllCounts(string HospitalID);

        /// <summary>
        /// 当前分诊总例数
        /// 统计当天
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        int GetDiagnoseTodayCounts(string HospitalID);

        /// <summary>
        /// 报告平均时长
        /// 统计当天
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        double GetReportTodayAvgTime(string HospitalID);

        /// <summary>
        /// 完成报告统计
        /// 表格统计
        /// </summary>
        [OperationContract]
        List<DeviceTypeModel> GetFinishedReportHistoryStatistics(string HospitalID);

        /// <summary>
        /// 设备类型统计
        /// 饼状图统计
        /// 统计本月
        /// </summary>
        [OperationContract]
        List<OutDeviceTypeCounts> GetDeviceTypeStatistics(string HospitalID);

        /// <summary>
        /// 分诊增长统计
        /// 柱形统计
        /// </summary>
        [OperationContract]
        List<OutReportCounts> GetIncreaseStatistics(string HospitalID);

        /// <summary>
        /// 完成报告统计
        /// 柱形统计
        /// </summary>
        [OperationContract]
        List<OutReportCounts> GetFinishedReportStatistics(string HospitalID);
        #endregion

        #region 医院坐标管理
        /// <summary>
        /// 添加或更新坐标
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel InsertCoorDinate(S_CoorDinate param);

        /// <summary>
        /// 删除坐标
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        int DeleteCoorDinate(string HospitalIdParam);

        /// <summary>
        /// 获取所属中心医院的所有子医院坐标
        /// </summary>
        /// <param name="HospitalIdParam"></param>
        /// <returns></returns>
        [OperationContract]
        List<S_CoorDinate> GetCoorDinateList(string HospitalIdParam);

        /// <summary>
        /// 获取单个医院坐标信息
        /// </summary>
        /// <param name="HospitalIdParam"></param>
        /// <returns></returns>
        [OperationContract]
        S_CoorDinate GetCoorDinateInfo(string HospitalIdParam);
        #endregion

        #region 删除用户组
        [OperationContract]
        ExcuteModel DeleteUserGroupInfo(string userID);
        [OperationContract]
        Groups GetGroupsById(string groupId);
        #endregion

        #region 收费端是否多次申请判断
        [OperationContract]
        ExcuteModel IsExistedPatientRequested(string patientId, string studyInstanceUid);
        #endregion

        #region 统计
        [OperationContract]
        List<StatisticsModel> AddUpReport(DateTime startTime, DateTime endTime, List<string> doctorNames, string userName, string userId);

        [OperationContract]
        List<OutReportRequestInfo> GetRequestDataList(DateTime dtStart, DateTime dtEnd, List<string> hospitalIds, List<string> requestDoctors, string doctorName, string doctorId);
        /// <summary>
        /// 按照检查方式进行统计
        /// </summary>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <returns></returns>
        [OperationContract]
        List<OutExamineMethodCount> GetExamineMethodCountList(DateTime dtStart, DateTime dtEnd, string hospitalId);
        #endregion

        #region 检查项目
        /// <summary>
        /// 保存检查项目
        /// </summary>
        /// <param name="modelParam"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel SaveExamineProject(ExamineProject modelParam);

        /// <summary>
        /// 获取检查项目列表
        /// </summary>
        /// <param name="modelParam"></param>
        /// <returns></returns>
        [OperationContract]
        List<OutExamineProject> GetExamineProjectList(OutExamineProject modelParam);

        /// <summary>
        /// 获取排序最大值
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        int? GetExamineProjectMaxSort();
        #endregion

        #region 检查部位维护

        /// <summary>
        /// 保存检查部位
        /// </summary>
        /// <param name="modelParam"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel SaveExaminePart(ExaminePart modelParam);

        /// <summary>
        /// 获取检查部位列表
        /// </summary>
        /// <param name="modelParam"></param>
        /// <returns></returns>
        [OperationContract]
        List<ExaminePart> GetExaminePartList(ExaminePart modelParam);

        /// <summary>
        /// 获取排序最大值
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        int? GetExaminePartMaxSort();
        #endregion

        #region 下载流
        [OperationContract]
        byte[] Down(string StrPath, string filename);
        #endregion

        #region 临时方法
        [OperationContract]
        List<DiagnosePart> GetALLDiagnosePart_name();
        #endregion

        #region 申请驳回记录表
        /// <summary>
        /// 保存驳回申请记录信息
        /// </summary>
        /// <param name="modelParam"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel SaveRequestRebutRecord(RequestRebutRecord modelParam);

        /// <summary>
        /// 获取驳回申请记录信息列表
        /// </summary>
        /// <param name="modelParam"></param>
        /// <returns></returns>
        [OperationContract]
        List<RequestRebutRecord> GetRequestRebutRecordList(RequestRebutRecord modelParam);
        #endregion

        #region 患者基本信息登记
        /// <summary>
        /// 患者信息登记
        /// </summary>
        /// <param name="patientModel"></param>
        /// <param name="requestModel"></param>
        /// <param name="workModel"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel SavePatientBaseInfo(PatientInfo patientModel, RequestInfo requestModel, Worklist workModel);

        /// <summary>
        ///  获取登记流水号（或供扫码使用）
        /// 设备类型+时间+位数+随机数（0-9）
        /// 例如：CT+20180907+0000001+3=CT2018090700000013
        /// </summary>
        /// <param name="paramType"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel GetAccessionNumber(string paramType, int length);

        /// <summary>
        /// 获取机房数据
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        List<OutRqeustInfo> GetMachinePatientData(InMachineBase param);
        #endregion
    }
}
