﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[assembly: log4net.Config.XmlConfigurator(ConfigFile = @"LogWCF.config", Watch = true)]
namespace WcfService
{
    /// <summary>
    /// 添加api服务日志和后台服务日志
    /// </summary>
    public class LogHelper
    {
        public static readonly ILog LogInfo = LogManager.GetLogger("LogInfo");
        public static readonly ILog LogError = LogManager.GetLogger("LogError");
        public static readonly ILog LogWarning = LogManager.GetLogger("LogWarning");
        //api日志
        public static readonly ILog LogApiInfo = LogManager.GetLogger("LogApiInfo");
        public static readonly ILog LogApiError = LogManager.GetLogger("LogApiError");
        public static readonly ILog LogApiWarning = LogManager.GetLogger("LogApiWarning");
        //服务日志
        public static readonly ILog LogServiceInfo = LogManager.GetLogger("LogServiceInfo");
        public static readonly ILog LogServiceError = LogManager.GetLogger("LogServiceError");
        public static readonly ILog LogServiceWarning = LogManager.GetLogger("LogServiceWarning");

        public static readonly ILog LogUploadServiceInfo = LogManager.GetLogger("LogUploadServiceInfo");
        public static readonly ILog LogUploadServiceError = LogManager.GetLogger("LogUploadServiceError");

        //Image服务日志
        public static readonly ILog LogImageServiceInfo = LogManager.GetLogger("LogImageServiceInfo");
        public static readonly ILog LogImageServiceError = LogManager.GetLogger("LogImageServiceError");

        //国宾服务日志
        public static readonly ILog LogGuoBinServiceInfo = LogManager.GetLogger("LogGuoBinServiceInfo");
        public static readonly ILog LogGuoBinServiceError = LogManager.GetLogger("LogGuoBinServiceError");

        #region 平台日志
        /// <summary>
        /// 正常信息
        /// </summary>
        /// <param name="info"></param>
        public static void WriteInfoLog(string info)
        {
            if (LogInfo.IsInfoEnabled)
            {
                LogInfo.Info(info);
            }
        }

        /// <summary>
        /// 错误信息
        /// </summary>
        /// <param name="info"></param>
        /// <param name="se"></param>
        public static void WriteErrorLog(string info, string strEx)
        {
            if (LogError.IsErrorEnabled)
            {
                LogError.Error(info, new Exception(strEx));
            }
        }

        /// <summary>
        /// 警告信息
        /// </summary>
        /// <param name="info"></param>
        public static void WriteWarningLog(string info)
        {
            if (LogWarning.IsWarnEnabled)
            {
                LogWarning.Warn(info);
            }
        }
        #endregion

        #region api日志
        /// <summary>
        /// api正常信息
        /// </summary>
        /// <param name="info"></param>
        public static void ApiInfoLog(string info)
        {
            if (LogApiInfo.IsInfoEnabled)
            {
                LogApiInfo.Info(info);
            }
        }

        /// <summary>
        /// api错误信息
        /// </summary>
        /// <param name="info"></param>
        /// <param name="se"></param>
        public static void ApiErrorLog(string info, string strEx)
        {
            if (LogApiError.IsErrorEnabled)
            {
                LogApiError.Error(info, new Exception(strEx));
            }
        }

        /// <summary>
        /// api警告信息
        /// </summary>
        /// <param name="info"></param>
        public static void ApiWarningLog(string info)
        {
            if (LogApiWarning.IsWarnEnabled)
            {
                LogApiWarning.Warn(info);
            }
        }
        #endregion

        #region service日志
        /// <summary>
        /// service正常信息
        /// </summary>
        /// <param name="info"></param>
        public static void ServiceInfoLog(string info)
        {
            if (LogServiceInfo.IsInfoEnabled)
            {
                LogServiceInfo.Info(info);
            }
        }

        /// <summary>
        /// service错误信息
        /// </summary>
        /// <param name="info"></param>
        /// <param name="se"></param>
        public static void ServiceErrorLog(string info, string strEx)
        {
            if (LogServiceError.IsErrorEnabled)
            {
                LogServiceError.Error(info, new Exception(strEx));
            }
        }

        /// <summary>
        /// service警告信息
        /// </summary>
        /// <param name="info"></param>
        public static void ServiceWarningLog(string info)
        {
            if (LogServiceWarning.IsWarnEnabled)
            {
                LogServiceWarning.Warn(info);
            }
        }
        #endregion

        #region Upload日志
        /// <summary>
        /// service正常信息
        /// </summary>
        /// <param name="info"></param>
        public static void UploadServiceInfoLog(string info)
        {
            if (LogUploadServiceInfo.IsInfoEnabled)
            {
                LogUploadServiceInfo.Info(info);
            }
        }

        /// <summary>
        /// service错误信息
        /// </summary>
        /// <param name="info"></param>
        /// <param name="se"></param>
        public static void UploadServiceErrorLog(string info, string strEx)
        {
            if (LogUploadServiceError.IsErrorEnabled)
            {
                LogUploadServiceError.Error(info, new Exception(strEx));
            }
        }
        #endregion

        #region Imgae日志
        /// <summary>
        /// Imgae正常信息
        /// </summary>
        /// <param name="info"></param>
        public static void ImageServiceInfoLog(string info)
        {
            if (LogImageServiceInfo.IsInfoEnabled)
            {
                LogImageServiceInfo.Info(info);
            }
        }

        /// <summary>
        /// Imgae错误信息
        /// </summary>
        /// <param name="info"></param>
        /// <param name="se"></param>
        public static void ImageServiceErrorLog(string info, string strEx)
        {
            if (LogImageServiceError.IsErrorEnabled)
            {
                LogImageServiceError.Error(info, new Exception(strEx));
            }
        }
        #endregion                
    }
}
