﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;

namespace WcfService
{
    /// <summary>
    /// 自动生成流水号
    /// </summary>
    public class NumericalOrder
    {
        private static volatile NumericalOrder instance = null;
        private static object lockHelper = new object();

        static ReaderWriterLockSlim _rw = new ReaderWriterLockSlim();
        private NumericalOrder() { }

        public static NumericalOrder Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (lockHelper)
                    {
                        if (instance == null)
                        {
                            instance = new NumericalOrder();
                        }
                    }
                }
                return instance;
            }
        }
        /// <summary>
        /// 暂定设备类型，长度为2位数
        /// </summary>
        /// <param name="deviceType"></param>
        /// <param name="dateFormate"></param>
        /// <param name="number"></param>
        /// <returns></returns>
        public string AutoNumber(string deviceType, string dateFormate, int number)
        {
            string strNumber = "HY";
            if (!string.IsNullOrEmpty(deviceType))
            {
                strNumber = deviceType;
            }
            if (!string.IsNullOrEmpty(dateFormate))
            {
                strNumber += DateTime.Now.ToString("yyyyMMdd");
            }
            if (number > 0)
            {
                string path = Thread.GetDomain().BaseDirectory + "number.txt";
                if (!File.Exists(path))
                {
                    Write(path, 1);
                    strNumber += 1.ToString().PadLeft(number, '0');
                }
                else
                {
                    long value = long.Parse(Read(path));
                    value += 1;
                    strNumber += value.ToString().PadLeft(number, '0');
                    if (value >= 99999)
                    {
                        value = 1;
                    }
                    Write(path, value);
                }
            }
            return strNumber;
        }

        string Read(string path)
        {
            string line = "";
            try
            {
                _rw.EnterReadLock();
                StreamReader sr = new StreamReader(path, Encoding.Default);
                line = sr.ReadLine();
                sr.Close();
            }
            finally
            {
                _rw.ExitReadLock();
            }
            return line;
        }

        void Write(string path, long value)
        {
            try
            {
                _rw.EnterUpgradeableReadLock();
                try
                {
                    _rw.EnterWriteLock();
                    FileStream fs = new FileStream(path, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
                    StreamWriter sw = new StreamWriter(fs);
                    sw.Write(value);
                    sw.Flush();
                    sw.Close();
                    fs.Close();
                }
                finally
                {
                    _rw.ExitWriteLock();
                }
                Thread.Sleep(TimeSpan.FromSeconds(0.1));
            }
            finally
            {
                _rw.ExitUpgradeableReadLock();
            }
        }
    }

}