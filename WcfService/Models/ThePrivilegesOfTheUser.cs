﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService.Models
{
    public class ThePrivilegesOfTheUser
    {
        public string UGroupId { get; set; }
        public string UserId { get; set; }
        public string GroupId { get; set; }
        public string GroupsName { get; set; }

    }
}