﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;

namespace WcfService.Models
{
    public class DataSetToList
    {

        /// <summary>
        ///DataSet 转化为IList的通用方法，可以转换为泛型类型
        /// </summary>
        /// <param name="ds">传入DataSet</param>
        /// <param name="tableName">表名</param>
        /// <returns></returns>
        public List<T> ChangeDataSet<T>(DataSet ds, string tableName)
        {
            //如果不存在此表 将返回空值
            if (ds.Tables[tableName] == null)
            {
                return null;
            }

            //获取操作DataTable
            DataTable dt = ds.Tables[tableName];

            //创建相应对象
            List<T> list = new List<T>();
            T model = default(T);

            foreach (DataRow dr in dt.Rows)
            {
                model = Activator.CreateInstance<T>();

                foreach (DataColumn dc in dr.Table.Columns)
                {
                    PropertyInfo pi = model.GetType().GetProperty(dc.ColumnName);
                    if (pi != null)
                    {
                        if (dr[dc.ColumnName] != DBNull.Value)
                        {
                            var val = dr[dc.ColumnName];

                            #region 根据字段类型，转换为对应数据类型
                            if ("String".Equals(pi.PropertyType.Name))
                            {
                                val = Convert.ToString(val);
                            }
                            else if ("Int32".Equals(pi.PropertyType.Name))
                            {
                                val = Convert.ToInt32(val);
                            }
                            else if ("Int64".Equals(pi.PropertyType.Name))
                            {
                                val = Convert.ToInt64(val);
                            }
                            else if ("Boolean".Equals(pi.PropertyType.Name))
                            {
                                val = Convert.ToBoolean(val);
                            }
                            else if ("DateTime".Equals(pi.PropertyType.Name))
                            {
                                val = Convert.ToDateTime(val);
                            }
                            else if ("Decimal".Equals(pi.PropertyType.Name))
                            {
                                val = Convert.ToDecimal(val);
                            }
                            else if ("Double".Equals(pi.PropertyType.Name))
                            {
                                val = Convert.ToDouble(val);
                            }
                            else if ("Byte".Equals(pi.PropertyType.Name))
                            {
                                val = Convert.ToByte(val);
                            }
                            else if ("Char".Equals(pi.PropertyType.Name))
                            {
                                val = Convert.ToChar(val);
                            }
                            else if ("Guid".Equals(pi.PropertyType.Name))
                            {
                                val = (Guid)(val);
                            }
                            #endregion

                            pi.SetValue(model, val, null);
                        }
                        else
                            pi.SetValue(model, null, null);
                    }
                }
                list.Add(model);
            }
            return list;
        }

        /// <summary>
        /// DataSet 转化为IList的通用方法，只能获取指定列
        /// </summary>
        /// <param name="ds">传入DataSet</param>
        /// <param name="tableName">表名</param>
        /// <param name="columnName">列名</param>
        /// <returns></returns>
        public List<object> ChangeDataSet(DataSet ds, string tableName, string columnName)
        {
            //如果不存在此表 将返回空值
            if (ds.Tables[tableName] == null)
            {
                return null;
            }

            //获取操作DataTable
            DataTable dt = ds.Tables[tableName];

            List<object> list = new List<object>();

            foreach (DataRow dr in dt.Rows)
            {
                list.Add(dr[columnName]);
            }
            return list;
        }



        /// <summary> 
        /// 利用反射将DataTable转换为List<T>对象
        /// </summary> 
        /// <param name="dt">DataTable 对象</param> 
        /// <returns>List<T>集合</returns> 
        public static List<T> DataTableToList<T>(DataTable dt) where T : class,new()
        {
            // 定义集合 
            List<T> ts = new List<T>();
            //定义一个临时变量 
            string tempName = string.Empty;
            //遍历DataTable中所有的数据行 
            foreach (DataRow dr in dt.Rows)
            {
                T t = new T();
                // 获得此模型的公共属性 
                PropertyInfo[] propertys = t.GetType().GetProperties();
                //遍历该对象的所有属性 
                foreach (PropertyInfo pi in propertys)
                {
                    tempName = pi.Name;//将属性名称赋值给临时变量 
                    //检查DataTable是否包含此列（列名==对象的属性名）  
                    if (dt.Columns.Contains(tempName))
                    {
                        //取值 
                        object value = dr[tempName];
                        //如果非空，则赋给对象的属性 
                        if (value != DBNull.Value)
                        {
                            pi.SetValue(t, value, null);
                        }
                    }
                }
                //对象添加到泛型集合中 
                ts.Add(t);
            }
            return ts;
        }
    }
}