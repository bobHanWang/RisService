﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService.Models
{
    public class UNLicense
    {
        /// <summary>
        /// 公司名称
        /// </summary>
        public string CompanyName { get; set; }
        /// <summary>
        /// 软件名称
        /// </summary>
        public string SoftwareName { get; set; }
        /// <summary>
        /// 软件版本
        /// </summary>
        public string SoftwareEdition { get; set; }

        /// <summary>
        /// 证书类型
        /// </summary>
        public int LicenseType { get; set; }

        /// <summary>
        /// 并发数量
        /// </summary>
        public int ConcurrentNumber { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime EndDate { get; set; }
        /// <summary>
        /// 申请人
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 申请时间
        /// </summary>
        public DateTime ApplyDate { get; set; }
        /// <summary>
        /// 申请部门
        /// </summary>
        public string DeptName { get; set; }
        /// <summary>
        /// 客户名称
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// 机构ID
        /// </summary>
        public string OrganizationCode { get; set; }

        /// <summary>
        /// 合同编号
        /// </summary>
        public string ContractNo { get; set; }

        /// <summary>
        /// 系统当前时间
        /// </summary>
        public DateTime CurrentDate { get; set; }
    }
}