﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService.Models
{
    public class AllDept
    {
        public string DeptId { get; set; }

        public string DeptDisplayName { get; set; }

        public string Deptname { get; set; }

        public int Htype { get; set; }

    }
}