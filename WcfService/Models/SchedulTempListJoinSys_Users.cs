﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService.Models
{
    public class SchedulTempListJoinSys_Users
    {
        public string SchedulTemplateId { get; set; }
        public string UserId { get; set; }
        public string SchedulingID { get; set; }
        public string Monday { get; set; }
        public string Tuesday { get; set; }
        public string Wednesday { get; set; }
        public string Thursday { get; set; }
        public string Friday { get; set; }
        public string Saturday { get; set; }
        public string Sunday { get; set; }
        public string Type { get; set; }
        public string CreatePeople { get; set; }
        public DateTime? CreateTime { get; set; }
        public string UpdatePeople { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string Standby { get; set; }
        public string Standby1 { get; set; }
        public string Name { get; set; }
    }
}