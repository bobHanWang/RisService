﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService.Models
{
    public class DeviceTypeModel
    {
        /// <summary>
        /// 分类
        /// </summary>
        public string ClassType { get; set; }

        /// <summary>
        /// 当日
        /// </summary>
        public int CurrentTodayCounts { get; set; }

        /// <summary>
        /// 当月
        /// </summary>
        public int CurrentMonthCounts { get; set; }

        /// <summary>
        /// 半年
        /// </summary>
        public int HalfCounts { get; set; }

        /// <summary>
        /// 一年
        /// </summary>
        public int YearCounts { get; set; }

        /// <summary>
        /// 累计
        /// </summary>
        public int AllCounts { get; set; }
    }
}