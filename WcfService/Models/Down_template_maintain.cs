﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService.Models
{
    public class Down_template_maintain
    {

        public int downloadT_ID { get; set; }
        public string UserId { get; set; }
        public string Dept_ID { get; set; }
        public int Template_ID { get; set; }
        public string Template_Name { get; set; }
        public int Default_T { get; set; }
        public DateTime Down_time { get; set; }

        public string Standby { get; set; }

        public string Down_operation { get; set; }

        //********************************************

        public string Template_Path { get; set; }
        public string Remark { get; set; }

        public string Standby1 { get; set; }
        public string Standby2 { get; set; }
        public string Operator { get; set; }
        public DateTime Create_Time { get; set; }

    }
}