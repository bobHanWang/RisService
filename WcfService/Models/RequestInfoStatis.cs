﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService.Models
{
    public class RequestInfoStatis
    {
        public string PatientId { get; set; }
        public string StudyInstanceUid { get; set; }
        public string RequestId { get; set; }
        public string NewPatientId { get; set; }
        public int Flag { get; set; }
    }
}