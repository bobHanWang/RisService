﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService.Models
{
    public class Patient_requestDetail
    {



        //****************************  PatientInfo

        public string PatientInfo { get; set; }

        public string PatientId { get; set; }

        public string PatientIdentityType { get; set; }


        public string PatientIdentityNum { get; set; }

        public string PatientName { get; set; }
        public string CPatientName { get; set; }

        public string PatientSex { get; set; }

        public int PatientAge { get; set; }

        public DateTime PatientBirth { get; set; }

        public DateTime PatientMobile { get; set; }

        public string PatientMarriageState { get; set; }

        public string PatientMZ { get; set; }

        public string PatientGJ { get; set; }

        public string PatientTel { get; set; }

        public string GZDWMC { get; set; }

        public string GZDWDZ { get; set; }

        public string JZDZ { get; set; }

        public string HKDZ { get; set; }
        public string HKDZYB { get; set; }

        public string LXRXM { get; set; }

        public string LXRGX { get; set; }

        public string LXRDZ { get; set; }
        public string LXRDH { get; set; }

        public DateTime DataCreateTime { get; set; }
        public DateTime DataUpdateTime { get; set; }

        ///********************


        public string RequestId { get; set; }
        //  public string PatientId { get; set; }

        public string HospitalId { get; set; }

        public string RequestJDR { get; set; }

        public string Diagnosticstate { get; set; }

        public string RequestDoctor { get; set; }

        public string AccessionNumber { get; set; }

        public int ReviewStatus { get; set; }


        public string RequestPurpose { get; set; }


        public DateTime RequestDate { get; set; }

        public string ExaminePart { get; set; }

        public string ExamineMethod { get; set; }

        public DateTime ExamineDate { get; set; }
        public string DiseaseSummary { get; set; }
        public string ClinicalDiagnose { get; set; }

        public string ExaminePurpose { get; set; }

        public int RequestState { get; set; }

        public string DiagnoseDoctor { get; set; }

        public DateTime DiagnoseDate { get; set; }
        public DateTime RefuseDate { get; set; }

        public string RefuseReason { get; set; }

        public int FileCount { get; set; }

        public string ServeiceType { get; set; }

        public int FeeState { get; set; }

        public Decimal Cost { get; set; }


        public string Remark { get; set; }

        public int Flag { get; set; }

        public DateTime RequestJDSJ { get; set; }


    }
}