﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService.Models
{
    public class RefundRecordManager
    {
        public string RequestId { get; set; }
        public string PatientName { get; set; }
        public string PatientSex { get; set; }
        public int PatientAge { get; set; }
        public string Name { get; set; }
        public DateTime? RefundDate { get; set; }
        public string HospitalId { get; set; }
        public int IsRefund { get; set; }
        public string Remark { get; set; }
        public string SysName { get; set; }
        public string PatientId { get; set; }
        public string CPatientName { get; set; }

        /// <summary>
        /// 服务项目ID
        /// </summary>
        public string ServeiceType { get; set; }

        /// <summary>
        /// 费用
        /// </summary>
        public decimal? Cost { get; set; }
    }
}