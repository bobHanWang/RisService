﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService.Models
{
    public class WCFSingleTransfer
    {
        public string out_biz_no { get; set; }

        public string payee_type { get; set; }

        public string payee_account { get; set; }

        public string amount { get; set; }

        public string payer_real_name { get; set; }

        public string payer_show_name { get; set; }
        public string payee_real_name { get; set; }

        public string remark { get; set; }

        public string ext_param { get; set; }

        public string order_title { get; set; }
    }
}