﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService
{
    public class OutReportRequestInfo
    {
        /// <summary>
        /// 医院名称
        /// </summary>
        public string HospitalName { get; set; }

        /// <summary>
        /// 医生姓名
        /// </summary>
        public string DoctorName { get; set; }

        /// <summary>
        /// 检查方法
        /// </summary>
        public string ExamineMethod { get; set; }

        /// <summary>
        /// 检查数量
        /// </summary>
        public int DiagnoseCount { get; set; }

        /// <summary>
        /// 总计
        /// </summary>
        public int TotalCount { get; set; }
    }
}