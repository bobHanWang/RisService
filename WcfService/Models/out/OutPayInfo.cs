﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService
{
    /// <summary>
    /// 缴费信息
    /// </summary>
    public class OutPayInfo
    {
        /// <summary>
        /// 会诊申请ID
        /// </summary>
        public string RequestId { get; set; }
        /// <summary>
        /// 英文姓名
        /// </summary>
        public string PatientName { get; set; }

        /// <summary>
        /// 中文姓名
        /// </summary>
        public string CPatientName { get; set; }

        /// <summary>
        /// 年龄
        /// </summary>
        public int PatientAge { get; set; }

        /// <summary>
        /// 病人ID
        /// </summary>
        public string PatientId { get; set; }

        /// <summary>
        /// StudyInstanceUid
        /// </summary>
        public string StudyInstanceUid { get; set; }

        /// <summary>
        /// 病人性别
        /// </summary>
        public string PatientSex { get; set; }

        /// <summary>
        /// 缴费状态
        /// </summary>
        public int FeeState { get; set; }

        /// <summary>
        /// 申请状态
        /// </summary>
        public int RequestState { get; set; }

        /// <summary>
        /// 审核状态
        /// </summary>
        public int ReviewStatus { get; set; }

        /// <summary>
        /// 身份证号
        /// </summary>
        public string PatientIdentityNum { get; set; }

        /// <summary>
        /// 服务项目
        /// </summary>
        public string ServiceItem { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public string RequestJDSJ { get; set; }
        
        /// <summary>
        /// 医院ID
        /// </summary>
        public string HospitalId { get; set; }

        /// <summary>
        /// 缴费状态
        /// </summary>
        public string DisplayPayState { get; set; }

        /// <summary>
        /// 申请状态
        /// </summary>
        public string DisplayRequestState { get; set; }

        /// <summary>
        /// 操作
        /// </summary>
        public string DisplayOperation { get; set; }
        
        /// <summary>
        /// 上传方式
        /// </summary>
        public int? UploadWay { get; set; }
    }
}