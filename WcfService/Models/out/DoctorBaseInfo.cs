﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService
{
    public class DoctorBaseInfo
    {
        public string DoctorId { get; set; }
        public string DoctorName { get; set; }
        public string DoctorIdentityType { get; set; }
        public string DoctorIdentityNum { get; set; }
        public string DoctorTel { get; set; }
        public string DoctorAdress { get; set; }
        public int? DoctorGrade { get; set; }
        public string GoodAt { get; set; }
        public string HospitalDept { get; set; }
        public string Dept { get; set; }
        public int? State { get; set; }
        public string UserName { get; set; }
        public string IntrodutionURL { get; set; }
        public string UserImgURL { get; set; }
        public string DiagnosisType { get; set; }
        public string DoctorTitle { get; set; }
        public decimal? DiagnosisPrice { get; set; }
        public string Introduction { get; set; }
        public DateTime? RequestDate { get; set; }
        public int? DoctorOrderBy { get; set; }
        public string HospitalName { get; set; }
        public string DiagnoseName { get; set; }
        public string Sex { get; set; }
        public int? Age { get; set; }
        public string Name { get; set; }
        public int UpState { get; set; }
        public int DownState { get; set; }
        public int APPState { get; set; }
    }
}