﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService.Models
{
	public class OutExamineProject
	{
        public int ID { get; set; }
        public int? DeviceID { get; set; }
        public string DeviceName { get; set; }
        public string ModalityType { get; set; }
        public string EXAM_G { get; set; }
        public string EXAM_T { get; set; }
        public string EXAM_B { get; set; }
        public char? IsViable { get; set; }
        public string SpellingName { get; set; }
        public string Speci { get; set; }
        public int? Exposure { get; set; }
        public int? FilmCount { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateUserID { get; set; }
        public string Remark { get; set; }
        public int? Sort { get; set; }
	}
}