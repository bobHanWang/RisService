﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService
{
    public class OutExamineMethodCount
    {
        /// <summary>
        /// 检查方式名称
        /// </summary>
        public string ExamineMethodName { get; set; }
        /// <summary>
        /// 检查方式数据
        /// </summary>
        public int ExamineMethodCount { get; set; }
    }
}