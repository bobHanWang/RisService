﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService
{
    /// <summary>
    /// 收费统计
    /// </summary>
    public class OutChargeChart
    {
        /// <summary>
        /// 序号
        /// </summary>
        public Int64 SerialID { get; set; }
        /// <summary>
        /// 申请ID
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// 缴费时间
        /// </summary>
        public string CreateTime { get; set; }

        /// <summary>
        /// 机构名称
        /// </summary>
        public string HospitalName { get; set; }

        /// <summary>
        /// 申请医生
        /// </summary>
        public string RequestDcotor { get; set; }

        /// <summary>
        /// 患者姓名
        /// </summary>
        public string PatientName { get; set; }

        /// <summary>
        /// 患者性别
        /// </summary>
        public string Sex { get; set; }

        /// <summary>
        /// 会诊项目
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// 收费金额
        /// </summary>
        public decimal TotalAmount { get; set; }

        /// <summary>
        /// 支付方式
        /// </summary>
        public string PayType { get; set; }

        /// <summary>
        /// 支付显示
        /// </summary>
        public string PayDisplay { get; set; }

        /// <summary>
        /// 支付状态
        /// </summary>
        public string ChargeState { get; set; }
    }
}