﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService
{
    public class OutReportCounts
    {
        public int Counts { get; set; }
        public string DiagnoseDate { get; set; }
    }
}