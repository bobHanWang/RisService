﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService
{
	public class OutDevice
	{
        public int DeviceId { get; set; }
        public int DepartmentId { get; set; }
        public string DeviceType { get; set; }
        public string MachineRoom { get; set; }
        public string DeviceName { get; set; }
        public string Description { get; set; }
        public DateTime? CreateTime { get; set; }
        public string DepartName { get; set; }
        public string DeviceAE { get; set; }
	}
}