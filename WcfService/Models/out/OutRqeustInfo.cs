﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService
{
    /// <summary>
    /// 输出申请单信息
    /// </summary>
    public class OutRqeustInfo
    {
        public string RequestId { get; set; }
        public string PatientId { get; set; }
        public string NewPatientId { get; set; }
        public string PatientMobile { get; set; }
        public string HospitalName { get; set; }
        public string HospitalId { get; set; }
        public string PatientName { get; set; }
        public string CPatientName { get; set; }
        public string RequestDate { get; set; }
        public string DiagnoseDate { get; set; }
        public string PatientBirth { get; set; }
        public string BirthTime { get; set; }
        public double? PatientWeight { get; set; }
        public string PatientSex { get; set; }
        public int RequestState { get; set; }
        public int FeeState { get; set; }
        public string PatientAge { get; set; }
        public string DisplayState { get; set; }
        public int? UpLoadFlag { get; set; }
        public int DownFlag { get; set; }
        public string UpLoadDisplay { get; set; }
        public string DownDispaly { get; set; }
        public string AccessionNumber { get; set; }
        public DateTime? ExamineDate { get; set; }
        public string Diagnosticstate { get; set; }
        public int ReportState { get; set; }
        public string ReportPerson { get; set; }
        public string DiagnoseDoctor { get; set; }
        public string StudyInstanceUid { get; set; }
        public string ExaminePart { get; set; }
        public string ExamineMethod { get; set; }
        public string RefusedPerson { get; set; }
        public string RefusedPersone { get; set; }
        public string SDDoctor { get; set; }
        public string SRDoctor { get; set; }
        public string RefusedDate { get; set; }
        public DateTime? RequestJDSJ { get; set; }
        public string RequestPurpose { get; set; }
        public string SRPDoctor { get; set; }
        public string PatientIdentityNum { get; set; }
        public double ProgressCount { get; set; }
        public string RequestDoctor { get; set; }
        public string RequestMobile { get; set; }
        public int? UploadWay { get; set; }
        public int ExamineCount { get; set; }
        public string OutPatientNum { get; set; }
        public string RegisterNum { get; set; }
        public int? NumberOfStudyRelatedSeries { get; set; }
        public int? NumberOfStudyRelatedInstances { get; set; }
        public decimal? StudySizeInKB { get; set; }
        public string UrgentType { get; set; }
        public string PatientType { get; set; }
        public string ExamineType { get; set; }
        public string Remark { get; set; }
        public int? IsPrint { get; set; }
        public string DisplayPrint { get; set; }
        public int? DeviceId { get; set; }
        public string DeviceName { get; set; }
        public string DiseaseSummary { get; set; }
        public string ClinicalDiagnose { get; set; }
        //是否预约
        public int IsAppointment { get; set; }
        public string IsAppointmentName { get; set; }
        public DateTime? AppointmentDate { get; set; }
        public string PatientIdentityType { get; set; }
        public string RequestJDR { get; set; }
        public string MachineRoom { get; set; }
        //摄片技师
        public string ShootTechnician { get; set; }
        //护士
        public string NursePerson { get; set; }
        //录入人员
        public string RecordPerson { get; set; }
        //摆位技师
        public string PositionTechnician { get; set; }
        //检查科室
        public string DepartmentName { get; set; }
        public string RequestStateDescription { get; set; }
        public string LXRDZ { get; set; }
        //检查状态
        public int? ReviewStatus { get; set; }
        public string ReviewDiaplay { get; set; }
    }
}