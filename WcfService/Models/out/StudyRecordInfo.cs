﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService
{
    public class StudyRecordInfo
    {
        /// <summary>
        /// 申请号
        /// </summary>
        public string RequestId { get; set; }
        /// <summary>
        /// 放射学检查号
        /// </summary>
        public string PatientId { get; set; }
        /// <summary>
        /// 放射学检查号
        /// </summary>
        public string NewPatientId { get; set; }
        /// <summary>
        /// 患者姓名
        /// </summary>
        public string PatientsName { get; set; }
        /// <summary>
        /// 患者性别
        /// </summary>
        public string PatientSex { get; set; }
        /// <summary>
        /// 检查设备号
        /// </summary>
        public string AccessionNumber { get; set; }
        /// <summary>
        /// 机构部门
        /// </summary>
        public string HospitalId { get; set; }
        /// <summary>
        /// 年龄
        /// </summary>
        public string PatientsAge { get; set; }
        /// <summary>
        /// 检查时间
        /// </summary>
        public string ExamineDate { get; set; }
        /// <summary>
        /// 检查描述
        /// </summary>
        public string StudyDescription { get; set; }
        /// <summary>
        /// 影像序列号
        /// </summary>
        public int StudySeries { get; set; }
        /// <summary>
        /// 影像数量
        /// </summary>
        public int StudyInstances { get; set; }
        /// <summary>
        /// 影像唯一序列号
        /// </summary>
        public string StudyInstanceUid { get; set; }
        /// <summary>
        /// 影像大小（KB）
        /// </summary>
        public decimal StudySizeInKB { get; set; }
        /// <summary>
        /// 检查方法
        /// </summary>
        public string Modality { get; set; }
        /// <summary>
        /// 进度
        /// </summary>
        public double ProgressCount { get; set; }
        /// <summary>
        /// 主检查记录
        /// </summary>
        public int IsMasterCheckRecord { get; set; }
        /// <summary>
        /// 检查部位
        /// </summary>
        public string ExaminePart { get; set; }
        /// <summary>
        /// 检查部位
        /// </summary>
        /// <summary>
        public string ExaminePosition { get; set; }
        /// 是否上传
        /// </summary>
        public int IsUploadCase { get; set; }
    }
}