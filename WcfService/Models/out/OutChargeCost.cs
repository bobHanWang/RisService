﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService
{
    /// <summary>
    /// 支付宝缴费类型统计
    /// </summary>
    public class OutChargeCost
    {
        /// <summary>
        /// 机构ID
        /// </summary>
        public string DepType { get; set; }

        /// <summary>
        /// 机构名称
        /// </summary>
        public string DepTypeName { get; set; }

        /// <summary>
        /// 支付ID
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 支付名称
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// 服务项目
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// 显示名称
        /// </summary>
        public string HospitalName { get; set; }

        /// <summary>
        /// 待缴费
        /// </summary>
        public decimal WaitingCost { get; set; }

        /// <summary>
        /// 已缴费
        /// </summary>
        public decimal ChargeCost { get; set; }

        /// <summary>
        /// 已退费
        /// </summary>
        public decimal ReturnCost { get; set; }

        /// <summary>
        /// 支付类型
        /// </summary>
        public string PayState { get; set; }

        /// <summary>
        /// 金额
        /// </summary>
        public decimal TotalAmount { get; set; }
    }
}