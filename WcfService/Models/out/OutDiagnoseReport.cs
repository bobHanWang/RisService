﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService
{
    /// <summary>
    /// 诊断报告信息
    /// </summary>
    public class OutDiagnoseReport
    {
        /// <summary>
        /// 患者ID
        /// </summary>
        public string PatientId { get; set; }

        /// <summary>
        /// 申请ID
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// 影像表现
        /// </summary>
        public string ReportDescription { get; set; }

        /// <summary>
        /// 诊断描述
        /// </summary>
        public string ReportConClusion { get; set; }

        /// <summary>
        /// 诊断时间
        /// </summary>
        public string DiagnoseDate { get; set; }

        /// <summary>
        /// 审核时间
        /// </summary>
        public string RefusedDate { get; set; }

        /// <summary>
        /// 审核医生简称
        /// </summary>
        public string RefusedAbb { get; set; }

        /// <summary>
        /// 审核人
        /// </summary>
        public string RefusedPerson { get; set; }

        /// <summary>
        /// 报告医生简称
        /// </summary>
        public string DiagnoseAbb { get; set; }

        /// <summary>
        /// 报告医生
        /// </summary>
        public string DiagnoseDoctor { get; set; }

        /// <summary>
        ///  报告人签名图片
        /// </summary>
        public byte[] DiagnoseDoctorImage { get; set; }

        /// <summary>
        /// 审核人签名图片
        /// </summary>
        public byte[] RefusedPersonImage { get; set; }
    }
}