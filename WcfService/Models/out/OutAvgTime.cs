﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService
{
    public class OutAvgTime
    {
        public DateTime? RequestDate { get; set; }
        public DateTime? DiagnoseDate { get; set; }
    }
}