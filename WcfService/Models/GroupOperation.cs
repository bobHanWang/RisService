﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService.Models
{
    public class GroupOperation
    {
        public string GroupId { get; set; }

        public string GroupsName { get; set; }

        public string Standby { get; set; }

        public string OGroupId { get; set; }

        public string UGroupId { get; set; }

        public string OlimitsId { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }

        public string Remark { get; set; }

        public string Standby1 { get; set; }

    }
}