﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService
{
    public class InDianose
    {
        public string RequestId { get; set; }
        public string ReportDescription { get; set; }
        public string ReportConClusion { get; set; }
        public string ReportDescriptionHtml { get; set; }
        public string ReportConClusionHtml { get; set; }
        public string ReportSuggestionHtml { get; set; }
        public string ReportSuggestion { get; set; }
        public string DiagnoseDoctor { get; set; }
        public string CheckPeople { get; set; }
        public string WriteReportPeople { get; set; }
        public DateTime? DiagnoseDate { get; set; }
        public string reportState { get; set; }
        public int submit_Param { get; set; }
        public bool SetTemplate { get; set; }
        public int AutoID_DiagnoseTip { get; set; }
        public int max_AutoID_DiagnoseTip { get; set; }
        public string Sys_dept_guid { get; set; }
        public string Crifically { get; set; }
        public int? StateYinYang { get; set; }

    }
}