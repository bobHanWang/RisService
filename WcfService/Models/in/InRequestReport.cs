﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService
{
    public class InRequestReport
    {
        public string RequestId { get; set; }
        public int RequestState { get; set; }
        public int? ReviewStatus { get; set; }
    }
}