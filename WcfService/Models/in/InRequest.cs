﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService
{
    public class InRequest
    {
        public string RequestId { get; set; }
        public string RequestPurpose { get; set; }
        public string DoctorName { get; set; }
        public string RequestDoctor { get; set; }
        public DateTime? RequestDate { get; set; }
        public string ExaminePart { get; set; }
        public string ExamineMethod { get; set; }
        public DateTime? ExamineDate { get; set; }
        public int? RequestState { get; set; }
    }
}