﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService.Models
{
	public class InMachineBase
	{
        public string PatientId { get; set; }
        public string OutPatientNum { get; set; }
        public string RegisterNum { get; set; }
        public string CPatientName { get; set; }
        public string PatientName { get; set; }
        public string PatientIdentityNum { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string DeviceID { get; set; }
        public string ReviewStatus { get; set; }
	}
}