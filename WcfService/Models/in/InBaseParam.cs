﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService.Models
{
    public class InBaseParam
    {
        public string RequestState { get; set; }
        public string HospitalId { get; set; }
        public string HospitalSon { get; set; }
        public string PatientId { get; set; }
        public string PatientName { get; set; }
        public DateTime DateTimeBegin { get; set; }
        public DateTime DateTimeEnd { get; set; }
        public int BigAge { get; set; }
        public int SmallAge { get; set; }
        public string Sex { get; set; }
        public string DiagnoseDoctor { get; set; }
        public string ExamineType { get; set; }
        public string PatientType { get; set; }
    }
}