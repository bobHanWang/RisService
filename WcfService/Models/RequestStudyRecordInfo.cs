﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService.Models
{
    public class RequestStudyRecordInfo
    {
        /// <summary>
        /// 申请编号
        /// </summary>
        public string RequestId { get; set; }
        public string NewPatientId { get; set; }
        /// <summary>
        /// 患者编号
        /// </summary>
        public string PatientId { get; set; }
        /// <summary>
        /// 患者姓名
        /// </summary>
        public string PatientsName { get; set; }
        /// <summary>
        /// 检查时间
        /// </summary>
        public string ExamineDate { get; set; }
        /// <summary>
        /// 检查方法
        /// </summary>
        public string Modality { get; set; }
        /// <summary>
        /// 检查部位
        /// </summary>
        public string ExaminePart { get; set; }
        /// <summary>
        /// 影像编号
        /// </summary>
        public string StudyInstanceUid { get; set; }
        /// <summary>
        /// 主检查记录
        /// </summary>
        public int IsMasterCheckRecord { get; set; }
        /// <summary>
        /// 是否上传病例
        /// </summary>
        public int IsUploadCase { get; set; }
    }
}