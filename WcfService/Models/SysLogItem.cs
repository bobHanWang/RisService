﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService.Models
{
    /// <summary>
    /// 系统操作日志对象
    /// </summary>
    public class SysLogItem
    {
        /// <summary>
        /// 表名
        /// </summary>
        public string TableName { get; set; }
        /// <summary>
        /// 列名
        /// </summary>
        public string ColumnName { get; set; }
        /// <summary>
        /// 旧值
        /// </summary>
        public string OldVal { get; set; }
        /// <summary>
        /// 新值
        /// </summary>
        public string NewVal { get; set; }
        /// <summary>
        /// 操作类型 新增、更新、删除
        /// </summary>
        public string OpreateType { get; set; }
    }
}