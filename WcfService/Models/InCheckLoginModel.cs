﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WcfService.DB_Server;
using WcfService.DB_Server.SystemManage;

namespace WcfService.Models
{
    public class InCheckLoginModel:Sys_Users
    {
        public string NetIp { get; set; } 
        public string LocalIp { get; set; } 


        public string MacAddress { get; set; }
        public string AppVersion { get; set; }
        public string MachineCode { get; set; }

        public string EquipmentType { get; set; }
        public string LoginType { get; set; }
    }
}