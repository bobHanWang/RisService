﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService.Models
{
    public class StatisticsModel
    {
        public string DoctorId { get; set; }
        public string DoctorName { get; set; }
        public int InitialReportNumber { get; set; }
        public int AuditReportNumber { get; set; }

        public string CombineExamineName { get; set; }
        public string ParentNodeName { get; set; }
        public string SubExamineName { get; set; }
        public int ExamineNumber { get; set; }
        public int SubExamineNumber { get; set; }

    }
}