﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService.Models
{
    public class ExpertConsultationRecord
    {


        public string PatientId { get; set; }

        public string CPatientName { get; set; }



        public string PatientIdentityType { get; set; }

        public string PatientIdentityNum { get; set; }



        public string PatientSex { get; set; }
        public int PatientAge { get; set; }

        public DateTime PatientBirth { get; set; }


        public string PatientMobile { get; set; }
        public string PatientMarriageState { get; set; }
        public string PatientMZ { get; set; }

        public string PatientGJ { get; set; }

        public string PatientTel { get; set; }

        public string GZDWMC { get; set; }

        public string GZDWDZ { get; set; }


        public string JZDZ { get; set; }

        public string HKDZ { get; set; }
        public string HKDZYB { get; set; }

        public string LXRXM { get; set; }

        public string LXRGX { get; set; }
        public string LXRDZ { get; set; }
        public string LXRDH { get; set; }

        public DateTime DataCreateTime { get; set; }

        public DateTime DataUpdateTime { get; set; }

        /// <summary>
        /// *****************************************
        /// </summary>

        public string RequestId { get; set; }

        //public string PatientId { get; set; }

        public string HospitalId { get; set; }



        public string RequestJDR { get; set; }

        public string RequestJDSJ { get; set; }

        public string RequestDoctor { get; set; }
        public string RequestPurpose { get; set; }

        public DateTime RequestDate { get; set; }
        public string ExaminePart { get; set; }


        public string ExamineMethod { get; set; }

        public DateTime ExamineDate { get; set; }


        public string DiseaseSummary { get; set; }

        public string ClinicalDiagnose { get; set; }

        public string ExaminePurpose { get; set; }
        public int RequestState { get; set; }


        public string DiagnoseDoctor { get; set; }

        public string RefuseReason { get; set; }

        public string Remark { get; set; }

        public DateTime DiagnoseDate { get; set; }

        public DateTime RefuseDate { get; set; }

        public string ServeiceType { get; set; }

        public Decimal Cost { get; set; }

        public int FileCount { get; set; }
        public int FeeState { get; set; }

        ////*************************


        // public string RequestId { get; set; }

        public DateTime ChargeDate { get; set; }

        //   public string ServeiceType { get; set; }


        //  public Decimal Cost { get; set; }


        public string ChargePerson { get; set; }

        public int ChargeState { get; set; }

        public Decimal RefundCost { get; set; }

        public DateTime RefundDate { get; set; }

        public string RefundPerson { get; set; }


        ///*********************************************
        ///


        public string ServiceId { get; set; }

        // public string HospitalId { get; set; }

        public string ServiceName { get; set; }

        public string ServiceJM { get; set; }
        public Decimal ServiceCost { get; set; }

        public int ServiceState { get; set; }


        public string Param { get; set; }

        public int ServiceType { get; set; }

        public string ServiceItemId { get; set; }


        //*************

        public string DeptDisplayName { get; set; }

        public string PatientName { get; set; }


        public string ReportDescription { get; set; }

        public string ReportConClusion { get; set; }

        public string ReportSuggestion { get; set; }
    }
}