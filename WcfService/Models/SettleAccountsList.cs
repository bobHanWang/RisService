﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService.Models
{
    public class SettleAccountsList
    {

        /// <summary>
        /// SettleAccounts
        /// </summary>

        public string SN { get; set; }

        public string SNMId { get; set; }

        public string AccountID { get; set; }

        public string RequestId { get; set; }

        public string ServiceId { get; set; }

        public string UserId { get; set; }

        public string HospitalID { get; set; }

        public string Type { get; set; }

        public string ShareMoney { get; set; }

        public string AccountBalance { get; set; }

        public string State { get; set; }

        public string CheckValue { get; set; }

        public string CreatePeople { get; set; }

        public string CreateTime { get; set; }

        public string UpdatePeople { get; set; }
        public string UpdateTime { get; set; }

        /// <summary>
        /// SettleAccountsMain
        /// </summary>
        //public string SNMId { get; set; }

        //public string UserId { get; set; }

        //public string HospitalID { get; set; }

        //public string RequestId { get; set; }

        //public string ServiceId { get; set; }

        public string Diagnosisdoctor { get; set; }

        public string DiagnosisTime { get; set; }
        public string ApplyDoctor { get; set; }

        public string ApplyTime { get; set; }

        //public string ApplyTime { get; set; }

    }
}