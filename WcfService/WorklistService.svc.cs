﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using WcfService.DB_Server.WorklistManage;

namespace WcfService
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class WorklistService : IWorklistService
    {
        WorklistManageContext dbContext = new WorklistManageContext();

        #region worklist数据操作
        public ExcuteModel AddWorklistInfo(Worklist model)
        {
            return dbContext.AddWorklistInfo(model);
        }

        public List<Worklist> GetWorklistInfo(Worklist model)
        {
            return dbContext.GetWorklistInfo(model);
        }

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateWorklistInfo(Worklist model)
        {
            return dbContext.UpdateWorklistInfo(model);
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel DeleteWorklistInfo(string param)
        {
            return dbContext.DeleteWorklistInfo(param);
        }
        #endregion
    }
}
