﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WcfService.DB_Server;
using WcfService.DB_Server.SystemManage;
using WcfService.Models;

namespace WcfService
{
    [ServiceContract]
    public interface ISystemManage
    {
        [OperationContract]
        void DoWork();

        #region   目录菜单维护
        [OperationContract]
        List<string> GetDiagnoseTip(string category);

        [OperationContract]
        string insertDiagnoseType(string DiagnoseType_name);
        [OperationContract]
        ExcuteModel SaveDiagnoseType_Name(DiagnoseType model);

        #endregion

        #region 用户管理

        [OperationContract]
        List<Sys_Users> GetUsers(int Type, string UserName, string RoleId, string DeptId);

        [OperationContract]
        ExcuteModel SaveUsers(Sys_Users model);

        [OperationContract]
        ExcuteModel SaveUsers_Group(Sys_Users model, List<string> group_id);

        [OperationContract]
        ExcuteModel SetEnableUser(string[] UserId, int State);

        [OperationContract]
        Sys_Users GetUserByUserName(string UserName);

        [OperationContract]
        Sys_Users GetUserByUserModel(string UserId);

        [OperationContract]
        ExcuteModel ResetPwd(string UserName, string OldPwd, string NewPwd);

        /// <summary>
        /// 重置密码
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel ResetPwdInfo(string UserID, string NewPwd);

        /// <summary>
        /// 更新用户操作状态
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdateState(Sys_Users model);

        /// <summary>
        /// 更新机构信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdateSysUserDept(Sys_Users model);
        #endregion

        #region 角色管理

        [OperationContract]
        List<Sys_Roles> GetRoles(string RoleId, int RoleType);

        [OperationContract]
        ExcuteModel SaveRoles(Sys_Roles model);

        [OperationContract]
        ExcuteModel SetEnableRole(string[] RoleId, int state);
        #endregion

        #region 用户角色

        [OperationContract]
        ExcuteModel AddUserToRole(Sys_RoleGroup model);

        [OperationContract]
        ExcuteModel DeleteUserFromRole(Sys_RoleGroup model);

        [OperationContract]
        List<Sys_RoleGroup> GetRoleIdByUserName(string UserName);
        #endregion

        #region 菜单角色
        [OperationContract]
        ExcuteModel AddMenuToRole(Sys_MenuGroup model);

        [OperationContract]
        ExcuteModel DeleteMenuFromRole(Sys_MenuGroup model);
        #endregion

        #region 权限应用
        [OperationContract]
        List<Sys_Menu> GetMenuListByUserName(string StrUserName);

        [OperationContract]
        List<Sys_Menu> GetMenuRootByUserName(string StrUserName);

        [OperationContract]
        List<Sys_Menu> GetMenuListByUserNameAndParentId(string StrUserName, int ParentId);
        #endregion

        #region 医生相关
        [OperationContract]
        List<Sys_Users> GetDoctorList(string strDoctorName);
        #endregion

        #region 菜单管理

        [OperationContract]
        List<Sys_Menu> GetMenu(int Type, string MenuId, string RoleId);

        [OperationContract]
        ExcuteModel SaveMenu(Sys_Menu model);

        [OperationContract]
        ExcuteModel SetEnableMenu(string[] MenuIds, int state);
        #endregion

        #region 用户登录
        [OperationContract]
        ExcuteModel CheckLogin(InCheckLoginModel model);
        [OperationContract]
        ExcuteModel LoginOutLog(Sys_UserLoginLog model);
        [OperationContract]
        List<Sys_UserLoginLog> GetLoginLogList();
        [OperationContract]
        List<Sys_UserLoginLog> GetNewLoginLogList(int pageSize, int pageIndex, out int totalRecord, out int totalPage);
        [OperationContract]
        List<Sys_UserLoginLog> GetLoginLogListByName(string name);
        #endregion

        #region 服务项目
        [OperationContract]
        List<Sys_DiagnoseService> GetDiagnoseServiceListBySiteId(string SiteId);

        [OperationContract]
        Sys_DiagnoseService GetDiagnoseServiceItemByServiceId(string ServiceId);

        [OperationContract]
        List<Sys_DiagnoseService> GetDiagnoseServiceAllList();

        [OperationContract]
        ExcuteModel SaveDiagnoseService(Sys_DiagnoseService model);

        [OperationContract]
        ExcuteModel SetEnableServiceItem(string ServiceId, int State);

        [OperationContract]
        ExcuteModel SaveDiagnoseServiceSub(WcfService.DB_Server.SystemManage.Sys_DiagnoseServiceSub model);

        [OperationContract]
        List<WcfService.DB_Server.SystemManage.Sys_DiagnoseServiceSub> GetDiagnoseServiceSubList();
        #endregion

        #region 数据字典

        [OperationContract]
        ExcuteModel SaveDictionary(Sys_Dictionary model);

        [OperationContract]
        List<Sys_Dictionary> GetDictionaryListByType(string Type);

        [OperationContract]
        Sys_Dictionary GetDictionaryDescriptionByTypeCode(string Type, string Code);

        [OperationContract]
        Sys_Dictionary GetDictionaryByTypeGUID(string GUID);
        #endregion

        #region 系统配置

        [OperationContract]
        Sys_Settings GetSysValueByKey(string Key);
        [OperationContract]
        List<Sys_Settings> GetSysList();
        [OperationContract]
        List<Sys_Settings> GetSysSettingList(string key);
        [OperationContract]
        List<Sys_Settings> GetSysSettingsListBySysType(string sysType);

        [OperationContract]
        ExcuteModel UpdateSysSetting(Sys_Settings model);
        #endregion

        #region 系统初始化
        [OperationContract]
        List<Sys_Settings> GetInitFolder(string SysCode);
        #endregion

        #region 部门
        [OperationContract]
        Sys_Dept GetDetpByDetpId(string DeptId);

        [OperationContract]
        List<Sys_Dept> GetAllDetps();


        [OperationContract]
        List<Sys_Dept> GetAllDetps_All(string DeptId);


        [OperationContract]
        List<AllDept> GetAllDetpsAndSub();


        [OperationContract]
        List<AllDept> GetAllDetpsRelevance(string Deptid);


        //移除关联
        [OperationContract]
        ExcuteModel del_SubHospital(SubHospital model);


        [OperationContract]
        ExcuteModel SaveDept(Sys_Dept model);
        #endregion

        [OperationContract]
        bool SystemConnectionSate();
        /// <summary>
        /// 根据编号获取用户信息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [OperationContract]
        Sys_Users GetSysUserById(string userId);
        [OperationContract]
        List<string> GetSysUserName(string userId);
        [OperationContract]
        string GetDepartmentName(string deptId);
        [OperationContract]
        string GetIDTypeName(string IdNum);
        [OperationContract]
        ExcuteModel SaveSysUserInfo(Sys_Users objSysUser);
        /// <summary>
        /// 将远端图片转换为流
        /// </summary>
        /// <param name="imagPath"></param>
        /// <returns></returns>
        [OperationContract]
        List<byte[]> GetImageData(string imagPath);

        /// <summary>
        /// 获取取模板数据流
        /// </summary>
        /// <param name="strTemplateName"></param>
        /// <returns></returns>
        [OperationContract]
        byte[] GetTemplateData(string strTemplateName);

        #region 省份
        /// <summary>
        /// 获取省份
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<Base_Area> GetBaseArea();
        #endregion

        #region 服务类型
        /// <summary>
        /// 获取服务类型数据
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<Sys_DiagnoseProgram> GetDiagnoseProgramList();

        /// <summary>
        /// 查询服务类型数据
        /// </summary>
        /// <param name="ServiceID"></param>
        /// <returns></returns>
        [OperationContract]
        Sys_DiagnoseProgram GetDiagnoseProgramModel(int ServiceID);

        /// <summary>
        /// 添加服务类型
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel AddServiceProgram(Sys_DiagnoseProgram param);

        /// <summary>
        /// 更新服务类型
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdateServiceProgram(Sys_DiagnoseProgram param);

        /// <summary>
        /// 更新状态成功
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdateServiceProgramState(Sys_DiagnoseProgram param);

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdateServiceProgramDel(Sys_DiagnoseProgram param);
        #endregion

        #region 用户组权限维护
        [OperationContract]
        List<ThePrivilegesOfTheUser> GetUserPrivileges(string userId);
        [OperationContract]
        Sys_Settings GetSys_SettingsInfo(string sysKey);
        #endregion

        #region 工作量统计
        [OperationContract]
        List<Sys_Users> GetSysUsersByDept(string dept);
        #endregion

        #region 消息维护
        [OperationContract]
        ExcuteModel AddAppUpdateLog(AppUpdateLog appUpdateLog);

        [OperationContract]
        ExcuteModel SaveAppUpdateLog(AppUpdateLog appUpdateLog);
        [OperationContract]
        ExcuteModel DelAppUpdateLog(AppUpdateLog appUpdateLog);
        [OperationContract]
        AppUpdateLog GetAppUpdateLogById(string id);
        [OperationContract]
        List<AppUpdateLog> GetAppUpdateLog();
        [OperationContract]
        List<AppUpdateLog> GetNewAppUpdateLogList(int pageSize, int pageIndex, out int totalRecord, out int totalPage);
        [OperationContract]
        List<AppUpdateLog> GetAppUpdateLogByDateTime(DateTime datetime);
        #endregion

        #region 解析License
        [OperationContract]
        UNLicense UnLicenseInfo(string fileNameParam);
        #endregion

        #region 科室类型维护
        /// <summary>
        /// 添加科室
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel AddDepartment(Sys_Department model);

        /// <summary>
        /// 更新科室表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdateDepartment(Sys_Department model);

        /// <summary>
        /// 获取科室类型
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        [OperationContract]
        List<Sys_Department> GetDepartmentList(Sys_Department model);
        #endregion

        #region 设备类型维护
        /// <summary>
        /// 添加设备类型
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel AddDevice(Sys_Device model);

        /// <summary>
        /// 更新设备类型表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdateDevice(Sys_Device model);

        /// <summary>
        /// 获取设备类型
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        [OperationContract]
        List<OutDevice> GetDeviceList(OutDevice model);

        /// <summary>
        /// 根据检查科室获取设备类型
        /// </summary>
        /// <param name="DepartmentID"></param>
        /// <returns></returns>
        [OperationContract]
        List<DiagnoseType> GetDiagnoseTypeData(int DepartmentID);
        #endregion

        #region 胶片规格维护
        /// <summary>
        /// 添加规格
        /// </summary>
        /// <param name="model"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel AddFilmSpeci(Sys_FilmSpeci model);

        /// <summary>
        /// 更新规格
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdateFilmSpeci(Sys_FilmSpeci model);

        /// <summary>
        /// 获取所有规格
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        List<Sys_FilmSpeci> GetFilmSpeciList(Sys_FilmSpeci model);
        #endregion

        #region 患者影像号累计
        /// <summary>
        /// 批量创建影像号
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel CreatePatientNumber();
        /// <summary>
        /// 获取影像号
        /// 同时判断，影像号的数量
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        string GetPatientNumber();
        /// <summary>
        /// 删除影像号
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel DeletePatientNumber(string param);
        /// <summary>
        /// 修改字段值
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdatePatientNumber(string param);
        #endregion

        #region 获取服务器时间
        [OperationContract]
        DateTime GetSystemTime();
        #endregion
    }
}
