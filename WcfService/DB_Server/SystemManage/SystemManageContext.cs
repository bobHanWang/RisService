﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;
using WcfService.Models;

namespace WcfService.DB_Server.SystemManage
{
    public class SystemManageContext
    {
        SystemManageDataContext db = new SystemManageDataContext();
        RemoteDianoseDataContext dbContext = new RemoteDianoseDataContext();

        #region   目录菜单维护
        public List<string> GetDiagnoseTip(string category)
        {
            string StrSql = "select distinct ltrim(RTRIM(" + category + ")) FROM [RemoteDianose].[dbo].[DiagnoseTip]";
            StrSql += "group by " + category + "";
            List<string> list = db.ExecuteQuery<string>(StrSql).ToList();
            return list;
        }

        public string insertDiagnoseType(string insertDiagnoseType_name)
        {
            return null;
        }

        public ExcuteModel SaveDiagnoseType_Name(DiagnoseType model)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                return null;
            }
            catch (Exception exp) { result.Result = false; result.Msg = exp.ToString(); }
            return result;
        }
        #endregion

        #region 用户管理

        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <param name="Type">0：获取全部用户，1:根据用户名获取单个用户2：获取角色RoleId下的用户，3：获取角色未添加的用户</param>
        /// <param name="UserName"></param>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        public List<Sys_Users> GetUsers(int Type, string UserName, string RoleId, string DeptId)
        {
            db = new SystemManageDataContext();
            List<Sys_Users> List = new List<Sys_Users>();

            string StrSql = "select * from Sys_Users where 1=1 and state=1 ";
            if (DeptId != "")
            {
                StrSql += " and Dept='" + DeptId + "' ";
            }
            switch (Type)
            {
                case 0: break;
                case 1: if (UserName != "") { StrSql += " and UserName='" + UserName + "'"; } break;
                case 2: if (RoleId != "") { if (!string.IsNullOrWhiteSpace(RoleId)) { StrSql += "and UserId in (SELECT [UserId] FROM [Sys_RoleGroup] where [RoleId] ='" + RoleId + "')"; } } else { StrSql += "and 1=2"; } break;
                case 3: if (RoleId != "") { if (!string.IsNullOrWhiteSpace(RoleId)) { StrSql += "and UserId not in (SELECT [UserId] FROM [Sys_RoleGroup] where [RoleId] ='" + RoleId + "')"; } } else { StrSql += "and 1=2"; } break;
                default: StrSql += " and 1=2"; break;
            }
            List<Sys_Users> UserList = db.ExecuteQuery<Sys_Users>(StrSql).ToList();
            return UserList;
        }

        /// <summary>
        /// save model Param 1: new Param 2: update
        /// </summary>
        /// <param name="model"></param>
        /// <returns>bool true,false</returns>
        public ExcuteModel SaveUsers(Sys_Users model)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                List<Sys_Users> listOld = new List<Sys_Users>();
                listOld = db.Sys_Users.Where(t => t.UserName == model.UserName).ToList();
                if (listOld.Count > 0)
                {
                    Sys_Users _model = listOld[0];
                    _model.UserName = model.UserName;
                    _model.Name = model.Name;
                    _model.Sex = model.Sex;
                    _model.Email = model.Email;
                    _model.Birthday = model.Birthday;
                    _model.Dept = model.Dept;
                    _model.Mobile = model.Mobile;
                    _model.HomePhone = model.HomePhone;
                    _model.DeptPhone = model.DeptPhone;
                    _model.UserImgURL = model.UserImgURL;
                    _model.Remarks = model.Remarks;
                    _model.UserGrade = model.UserGrade;
                    db.SubmitChanges();
                    result.Result = true;
                    result.Msg = "用户信息修改成功！";
                    result.RetValue = _model.UserId;
                    return result;
                }
                else
                {
                    db.Sys_Users.InsertOnSubmit(model);
                    db.SubmitChanges();
                    result.RetValue = model.UserId;
                    result.Result = true;
                    result.Msg = "新用户创建成功！";
                }
            }
            catch (Exception exp)
            {
                result.Result = false;
                result.Msg = exp.ToString();
            }
            return result;
        }

        /// <summary>
        /// save model Param 1: new Param 2: update
        /// </summary>
        /// <param name="model"></param>
        /// <returns>bool true,false</returns>
        public ExcuteModel SaveUsers_Group(Sys_Users model, List<string> groupIdList)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                if (model.Param == "1")
                {
                    model.Param = "";
                    List<Sys_Users> listOld = new List<Sys_Users>();
                    listOld = db.Sys_Users.Where(t => t.UserName == model.UserName).ToList();
                    if (listOld.Count == 0)
                    {
                        db.Sys_Users.InsertOnSubmit(model);
                        db.SubmitChanges();
                        result.Result = true;
                        result.Msg = "新用户创建成功！";
                    }
                    result.RetValue = model.UserId;
                    if (groupIdList.Count == 0) return result;
                    if (Insert_UserGroup(model, groupIdList))
                    {
                        result.Result = true;
                        result.Msg = "新用户创建成功！";
                    }
                    else
                    {
                        result.Result = false;
                        result.Msg = "新用户创建失败！";
                    }
                }
                //else if (model.Param == "2")
                //{
                //    List<Sys_Users> list = db.Sys_Users.Where(t => t.UserId == model.UserId).ToList();
                //    //待处理  
                //    if (list.Count < 0)
                //    {
                //        result.Result = false;
                //        result.Msg = "该用户不存在，请刷新列表重试！";
                //        return result;
                //    }
                //    else
                //    {
                //        Sys_Users _model = list[0];
                //        _model.UserName = model.UserName;
                //        _model.Name = model.Name;
                //        _model.Sex = model.Sex;
                //        _model.Email = model.Email;
                //        _model.Birthday = model.Birthday;
                //        _model.Dept = model.Dept;
                //        _model.Mobile = model.Mobile;
                //        _model.HomePhone = model.HomePhone;
                //        _model.DeptPhone = model.DeptPhone;
                //        _model.UserImgURL = model.UserImgURL;
                //        _model.Remarks = model.Remarks;
                //        _model.UserGrade = model.UserGrade;
                //        _model.DiagnosisType = model.DiagnosisType;
                //        db.SubmitChanges();
                //        if (Insert_UserGroup(model, group_id))
                //        {
                //            result.Result = true;
                //            result.Msg = "用户信息修改成功！";
                //            result.RetValue = _model.UserId;
                //        }
                //        else
                //        {
                //            result.Result = false;
                //            result.Msg = "用户信息修改失败！";
                //        }
                //    }
                //}
            }
            catch (Exception exp) { result.Result = false; result.Msg = exp.ToString(); }
            return result;
        }

        public bool Insert_UserGroup(Sys_Users model, List<string> group_id)
        {
            List<UserGroup> UserGroup = new List<DB_Server.UserGroup>();
            try
            {
                List<UserGroup> _modelUserGroup = dbContext.UserGroup.Where(u => u.UserId == model.UserId).ToList();
                if (_modelUserGroup.Count != 0)
                {
                    for (int i = 0; i < _modelUserGroup.Count; i++)
                    {
                        _modelUserGroup[i].GroupName = group_id[i].ToString();
                    }
                }
                else
                {
                    for (int i = 0; i < group_id.Count; i++)
                    {
                        UserGroup _model = new DB_Server.UserGroup() { UGroupId = Guid.NewGuid().ToString(), GroupName = group_id[i].ToString(), UserId = model.UserId };
                        dbContext.UserGroup.InsertOnSubmit(_model);
                    }
                }
                dbContext.SubmitChanges();
                return true;
            }
            catch (Exception) { return false; }
        }

        /// <summary>
        /// 添加用户组
        /// </summary>
        /// <param name="model"></param>
        /// <param name="group_id"></param>
        /// <returns></returns>
        public bool AddUserGroup(Sys_Users model, string group_id)
        {

            try
            {
                UserGroup _model = new DB_Server.UserGroup() { UGroupId = Guid.NewGuid().ToString(), GroupName = group_id.ToString(), UserId = model.UserId };
                dbContext.UserGroup.InsertOnSubmit(_model);
                dbContext.SubmitChanges();
                return true;
            }
            catch (Exception) { return false; }
        }

        /// <summary>
        /// 修改用户状态
        /// </summary>
        /// <param name="UserId">数组</param>
        /// <param name="State">状态，1正常，2禁用，3，登录锁定</param>
        /// <returns></returns>
        public ExcuteModel SetEnableUser(string[] UserId, int State)
        {
            ExcuteModel result = new ExcuteModel();
            if (UserId.Length < 1)
            {
                result.Msg = "尚未选择用户！";
                result.Result = false;
                return result;
            }
            string StrIds = "'" + string.Join("','", UserId) + "'";

            try
            {
                string strSql = string.Format(@"update Sys_Users set state={0}   where UserId in({1})", State, StrIds);
                db.ExecuteCommand(strSql);
                result.Result = true;
                result.Msg = "用户禁用成功！";
            }
            catch (Exception exp) { result.Result = false; result.Msg = exp.ToString(); }
            return result;
        }

        /// <summary>
        /// 根据登录名获取用户对象
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public Sys_Users GetUserByUserName(string UserName)
        {
            return db.Sys_Users.FirstOrDefault(t => t.UserName == UserName || t.Mobile == UserName);
        }

        /// <summary>
        /// 根据用户id获取用户对象
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public Sys_Users GetUserByUserModel(string UserId)
        {
            return db.Sys_Users.FirstOrDefault(t => t.UserId == UserId);
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="OldPwd"></param>
        /// <param name="NewPwd"></param>
        /// <returns></returns>
        public ExcuteModel ResetPwd(string UserName, string OldPwd, string NewPwd)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                List<Sys_Users> list = db.Sys_Users.Where(t => t.UserName == UserName).ToList();
                if (list.Count < 1)
                {
                    RetModel.Result = false;
                    RetModel.Msg = "用户名不存在！";
                    return RetModel;
                }
                list = db.Sys_Users.Where(t => t.UserName == UserName && t.UserPwd == OldPwd).ToList();
                if (list.Count < 1)
                {
                    RetModel.Result = false;
                    RetModel.Msg = "用户旧密码不正确！";
                    return RetModel;
                }

                Sys_Users model = list[0];
                model.UserPwd = NewPwd;
                db.SubmitChanges();
                RetModel.Result = true;
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        /// <summary>
        /// 重置密码
        /// </summary>
        /// <returns></returns>
        public ExcuteModel ResetPwdInfo(string UserID, string NewPwd)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                var userInfo = db.Sys_Users.Where(t => t.UserId == UserID).FirstOrDefault();
                if (userInfo != null && !string.IsNullOrEmpty(userInfo.UserId))
                {
                    userInfo.UserPwd = NewPwd;
                    db.SubmitChanges();
                    RetModel.Result = true;
                    RetModel.Msg = "重置密码成功！";
                }
                else
                {
                    RetModel.Result = false;
                    RetModel.Msg = "用户信息异常，不能重置密码";
                }
            }
            catch (Exception ex)
            {

            }
            return RetModel;
        }

        /// <summary>
        /// 更新用户操作状态
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateState(Sys_Users model)
        {
            ExcuteModel em = new ExcuteModel();
            try
            {
                Sys_Users modelInfo = db.Sys_Users.Where(o => o.UserId == model.UserId).FirstOrDefault();
                if (modelInfo != null && !string.IsNullOrEmpty(model.UserId))
                {
                    modelInfo.UserId = model.UserId;
                    modelInfo.UpState = model.UpState;
                    modelInfo.DownState = model.DownState;
                    modelInfo.APPState = model.APPState;
                    modelInfo.State = model.State;
                    if (model.UserType > 0)
                    {
                        modelInfo.UserType = model.UserType;
                    }
                    db.SubmitChanges();
                    em.Result = true;
                    em.Msg = "状态更新成功！";
                }
                else
                {
                    em.Msg = "未找到医生信息";
                    em.Result = false;
                }
            }
            catch (Exception ex)
            {
                em.Msg = "错误信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace;
                em.Result = false;
                LogHelper.WriteErrorLog("UpdateState", "错误信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return em;
        }

        /// <summary>
        /// 更新机构信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateSysUserDept(Sys_Users model)
        {
            ExcuteModel em = new ExcuteModel();
            try
            {
                Sys_Users modelInfo = db.Sys_Users.Where(o => o.UserId == model.UserId).FirstOrDefault();
                if (modelInfo != null && !string.IsNullOrEmpty(model.UserId))
                {
                    modelInfo.UserId = model.UserId;
                    modelInfo.Dept = model.Dept;
                    db.SubmitChanges();
                    em.Result = true;
                    em.Msg = "机构更新成功！";
                }
                else
                {
                    em.Msg = "未找到机构信息";
                    em.Result = false;
                }
            }
            catch (Exception ex)
            {
                em.Msg = "错误信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace;
                em.Result = false;
                LogHelper.WriteErrorLog("UpdateSysUserDept", "错误信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return em;
        }

        #endregion

        #region 角色管理

        /// <summary>
        /// 获取角色列表
        /// </summary>
        /// <param name="RoleId"></param>
        /// <param name="RoleType"></param>
        /// <returns></returns>
        public List<Sys_Roles> GetRoles(string RoleId, int RoleType)
        {
            db = new SystemManageDataContext();
            string StrSql = "select * from Sys_Roles where 1=1 ";
            if (RoleId != "") { StrSql += " and RoleId='" + RoleId + "'"; }
            if (RoleType != 0) { StrSql += " and RoleType='" + RoleType + "'"; }
            List<Sys_Roles> List = db.ExecuteQuery<Sys_Roles>(StrSql).ToList();
            return List;
        }

        /// <summary>
        /// save model RoleId -1: new else update
        /// </summary>
        /// <param name="model"></param>
        /// <returns>bool true,false</returns>
        public ExcuteModel SaveRoles(Sys_Roles model)
        {
            ExcuteModel result = new ExcuteModel();
            List<Sys_Roles> list = db.Sys_Roles.Where(t => t.RoleId == model.RoleId).ToList();
            try
            {
                if (model.Param == "1")
                {//new
                    model.Param = "";
                    if (list.Count > 0)
                    {
                        result.Result = false;
                        result.Msg = "角色保存异常，请联系管理员！";
                        return result;
                    }
                    List<Sys_Roles> listOld = db.Sys_Roles.Where(t => t.RoleName == model.RoleName).ToList();
                    if (listOld.Count > 0)
                    {
                        result.Result = false;
                        result.Msg = "该角色名称已存在，请重新定义！";
                        return result;
                    }
                    db.Sys_Roles.InsertOnSubmit(model);
                    db.SubmitChanges();
                    result.Result = true;
                    result.Msg = "新角色保存成功！";
                }
                else if (model.Param == "2")
                {//update
                    if (list.Count == 0)
                    {
                        result.Result = false;
                        result.Msg = "角色更新异常，请联系管理员！";
                        return result;
                    }
                    List<Sys_Roles> listOld = db.Sys_Roles.Where(t => t.RoleName == model.RoleName).ToList();
                    if (listOld.Count > 0)
                    {
                        result.Result = false;
                        result.Msg = "该角色名称已存在，请重新定义！";
                        return result;
                    }
                    else
                    {
                        Sys_Roles _model = list[0];
                        _model.RoleName = model.RoleName;
                        _model.Remarks = model.Remarks;
                        db.SubmitChanges();
                        result.Result = true;
                        result.Msg = "角色更新成功！";
                    }

                }

            }
            catch (Exception exp) { result.Result = false; result.Msg = exp.ToString(); }
            return result;
        }
        /// <summary>
        /// 调整角色状态
        /// </summary>
        /// <param name="RoleId"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public ExcuteModel SetEnableRole(string[] RoleId, int state)
        {
            ExcuteModel result = new ExcuteModel();

            if (RoleId.Length < 1)
            {
                result.Result = false;
                result.Msg = "尚未选择角色！";
                return result;
            }
            string StrIds = "'" + string.Join("','", RoleId) + "'";

            try
            {
                string strSql = string.Format(@"update  Sys_Roles  set State={0} where RoleId in({1})", state, StrIds);
                db.ExecuteCommand(strSql);
                result.Result = true;
                result.Msg = "角色状态调整成功！";
            }
            catch (Exception exp) { result.Result = false; result.Msg = exp.ToString(); return result; }
            return result;
        }
        #endregion

        #region 用户角色
        /// <summary>
        /// 给用户划分角色
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel AddUserToRole(Sys_RoleGroup model)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                List<Sys_RoleGroup> list = db.Sys_RoleGroup.Where(t => t.UserId == model.UserId && t.RoleId == model.RoleId).ToList();
                if (list.Count > 0)
                {
                    result.Msg = "该用户已存在该角色，请核实！";
                    result.Result = false;
                    return result;
                }
                {
                    db.Sys_RoleGroup.InsertOnSubmit(model);
                    db.SubmitChanges();
                    result.Msg = "用户权限分配成功！";
                    result.Result = true;
                }
            }
            catch (Exception exp)
            {
                result.Msg = exp.ToString();
                result.Result = false;
                return result;
            }
            return result;
        }

        public ExcuteModel DeleteUserFromRole(Sys_RoleGroup model)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                string strSql = string.Format(@"delete from  [Sys_RoleGroup] where UserName='{0}' and roleId='{1}'", model.UserId, model.RoleId);
                // db.Sys_RoleGroup.DeleteOnSubmit(model);
                db.ExecuteCommand(strSql);
                db.SubmitChanges();
                result.Result = true;
                result.Msg = "用户移除角色成功！";
            }
            catch (Exception exp) { result.Msg = exp.ToString(); }
            return result;
        }

        /// <summary>
        /// 根据用户名获取所在的角色
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public List<Sys_RoleGroup> GetRoleIdByUserName(string UserName)
        {
            List<Sys_RoleGroup> RoleList = new List<Sys_RoleGroup>();
            string StrSql = "select * from Sys_RoleGroup where UserId in(select UserId from Sys_Users where UserName='" + UserName + "')";
            RoleList = db.ExecuteQuery<Sys_RoleGroup>(StrSql).ToList();
            return RoleList;
        }

        #endregion

        //dt
        #region 菜单角色
        public ExcuteModel AddMenuToRole(Sys_MenuGroup model)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                string strSql = string.Format(@"insert into [Sys_MenuGroup]([MenuId],[RoleId])values('{0}','{1}')", model.MenuId, model.RoleId);
                db.ExecuteCommand(strSql);
                db.SubmitChanges();
                result.Result = true;
            }
            catch (Exception exp) { result.Msg = exp.ToString(); }
            return result;
        }

        public ExcuteModel DeleteMenuFromRole(Sys_MenuGroup model)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                string strSql = string.Format(@"delete from  [Sys_MenuGroup] where MenuId='{0}' and roleId='{1}'", model.MenuId, model.RoleId);
                db.ExecuteCommand(strSql);
                db.SubmitChanges();
                result.Result = true;
            }
            catch (Exception exp) { result.Msg = exp.ToString(); }
            return result;
        }
        #endregion

        //dt
        #region 权限应用
        public List<Sys_Menu> GetMenuListByUserName(string StrUserName)
        {
            db = new SystemManageDataContext();
            List<Sys_Menu> list = new List<Sys_Menu>();
            return list;
        }

        /// <summary>
        /// 获取根菜单list
        /// </summary>
        /// <param name="StrUserName"></param>
        /// <returns></returns>
        public List<Sys_Menu> GetMenuRootByUserName(string StrUserName)
        {
            db = new SystemManageDataContext();
            List<Sys_Menu> list = new List<Sys_Menu>();
            return list;
        }

        /// <summary>
        /// 获取根菜单下的菜单项list
        /// </summary>
        /// <param name="StrUserName"></param>
        /// <returns></returns>
        public List<Sys_Menu> GetMenuListByUserNameAndParentId(string StrUserName, int ParentId)
        {
            db = new SystemManageDataContext();
            List<Sys_Menu> list = new List<Sys_Menu>();
            return list;
        }
        #endregion

        #region 医生相关
        /// <summary>
        /// 获取医生列表 UserType 1：系统用户，2医生，3管理员，4测试用户
        /// </summary>
        /// <returns></returns>
        public List<Sys_Users> GetDoctorList(string strDoctorName)
        {
            List<Sys_Users> docList = db.Sys_Users.Where(t => t.UserType == 2).OrderByDescending(t => t.UserGrade).ToList();
            if (strDoctorName.Trim() != "")
            {
                docList = docList.Where(t => t.Name.Contains(strDoctorName)).ToList();
            }
            return docList;
        }
        #endregion

        #region 菜单管理
        /// <summary>
        /// 获取菜单表记录 
        /// </summary>
        /// <param name="Type">0：获取全部菜单，1:根据MenuId获取单个菜单2：获取角色RoleId下的菜单，3：获取角色未添加的菜单</param>
        /// <param name="MenuId"> MenuId为空获取全部，不为空则获取一条记录</param>
        /// <returns>泛型列表</returns>
        public List<Sys_Menu> GetMenu(int Type, string MenuId, string RoleId)
        {
            string StrSql = "select * from Sys_Menu where 1=1 ";
            switch (Type)
            {
                case 0: break;
                case 1: if (MenuId != "") { StrSql += " and MenuId='" + MenuId + "'"; } break;
                case 2: if (RoleId != "") { if (!string.IsNullOrWhiteSpace(RoleId)) { StrSql += " and MenuId in(select MenuId from Sys_MenuGroup where RoleId='" + RoleId + "')"; } } else { StrSql += "and 1=2"; } break;
                case 3: if (RoleId != "") { if (!string.IsNullOrWhiteSpace(RoleId)) { StrSql += " and MenuId not in(select MenuId from Sys_MenuGroup where RoleId='" + RoleId + "')"; } } else { StrSql += "and 1=2"; } break;
                default: StrSql += " and 1=2"; break;
            }
            List<Sys_Menu> List = db.ExecuteQuery<Sys_Menu>(StrSql).ToList();
            return List;
        }

        /// <summary>
        /// save model Param -1: new else 2 update
        /// </summary>
        /// <param name="model"></param>
        /// <returns>bool true,false</returns>
        public ExcuteModel SaveMenu(Sys_Menu model)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                List<Sys_Menu> list = db.Sys_Menu.Where(t => t.MenuId == model.MenuId).ToList();
                if (model.Param == "1")
                {//new
                    model.Param = "";
                    if (list.Count > 0)
                    {
                        result.Result = false;
                        result.Msg = "保存异常，请联系管理员！";
                        return result;
                    }
                    else
                    {
                        db.Sys_Menu.InsertOnSubmit(model);
                        db.SubmitChanges();
                        result.Result = true;
                        result.Msg = "新菜单项保存成功！";
                    }
                }
                else
                {//update
                    if (list.Count < 1)
                    {
                        result.Result = false;
                        result.Msg = "菜单更新异常，请联系管理员！";
                        return result;
                    }
                    else
                    {
                        Sys_Menu _model = db.Sys_Menu.FirstOrDefault(t => t.MenuId == model.MenuId);
                        _model = model;
                        db.SubmitChanges();
                        result.Result = true;
                        result.Msg = "菜单项更新成功！";
                    }
                }

            }
            catch (Exception exp) { result.Result = false; result.Msg = exp.ToString(); }
            return result;
        }

        /// <summary>
        /// 修订菜单状态
        /// </summary>
        /// <param name="MenuIds"></param>
        /// <param name="state">1启用，2禁用，3删除</param>
        /// <returns></returns>
        public ExcuteModel SetEnableMenu(string[] MenuIds, int state)
        {
            string StrIds = "'" + string.Join("','", MenuIds) + "'";
            ExcuteModel result = new ExcuteModel();
            if (MenuIds.Length == 0) { result.Result = false; result.Msg = "请选择要删除的菜单项！"; return result; }
            try
            {
                string strSql = string.Format(@"update Sys_Menu set state={0} where MenuId in({1})", state, StrIds);
                db.ExecuteCommand(strSql);
                result.Result = true;
                result.Msg = "菜单项删除成功！";
            }
            catch (Exception exp) { result.Result = false; result.Msg = exp.ToString(); }
            return result;
        }
        #endregion

        #region 用户登录
        public ExcuteModel CheckLogin(InCheckLoginModel model)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                List<Sys_Users> _model = db.Sys_Users.Where(t => t.UserName == model.UserName || t.Mobile == model.UserName).ToList();
                if (_model.Count < 1)
                {
                    try
                    {
                        SaveLoginLog(new Sys_UserLoginLog() { LogUsername = model.UserName, LogId = Guid.NewGuid().ToString(), LogDateTime = DateTime.Now, LocalIp = model.LocalIp, NetIp = model.NetIp, State = "anonymous", EquipmentType = model.EquipmentType, LoginType = model.LoginType });
                    }
                    catch (Exception exp)
                    {
                        LogHelper.WriteErrorLog("SaveLoginLog登录", "错误信息：" + exp.Message + "--堆栈信息：" + exp.StackTrace);
                    }
                    result.Result = false;
                    result.Msg = "用户不存在！";
                    return result;
                }
                _model = db.Sys_Users.Where(t => t.UserName == model.UserName && t.UserPwd == model.UserPwd || t.Mobile == model.UserName && t.UserPwd == model.UserPwd).ToList();
                if ((_model != null) && _model.Count > 0)
                {//状态，1正常，2禁用，3，登录锁定
                    if (_model[0].State == 2)
                    {
                        result.Result = false;
                        result.Msg = "您已被禁用，请联系管理员！";
                        return result;
                    }
                    else if (_model[0].State == 3)
                    {
                        result.Result = false;
                        result.Msg = "您登录错误次数过多，已被锁定，请联系管理员！";
                        return result;
                    }
                    try
                    {
                        SaveLoginLog(new Sys_UserLoginLog() { LogUsername = model.UserName, LogId = Guid.NewGuid().ToString(), LogDateTime = DateTime.Now, AppVersion = model.AppVersion, MachineCode = model.MachineCode, MacAddress = model.MacAddress, LocalIp = model.LocalIp, NetIp = model.NetIp, State = "success", EquipmentType = model.EquipmentType, LoginType = model.LoginType });
                    }
                    catch (Exception exp)
                    {
                        LogHelper.WriteErrorLog("SaveLoginLog登录", "错误信息：" + exp.Message + "--堆栈信息：" + exp.StackTrace);
                    }
                    result.Result = true;
                    result.Msg = "登录成功！";
                }
                else
                {
                    try
                    {
                        SaveLoginLog(new Sys_UserLoginLog() { LogUsername = model.UserName, LogId = Guid.NewGuid().ToString(), LogDateTime = DateTime.Now, AppVersion = model.AppVersion, MachineCode = model.MachineCode, MacAddress = model.MacAddress, LocalIp = model.LocalIp, NetIp = model.NetIp, State = "error", EquipmentType = model.EquipmentType, LoginType = model.LoginType });
                    }
                    catch (Exception exp)
                    {
                        LogHelper.WriteErrorLog("SaveLoginLog登录", "错误信息：" + exp.Message + "--堆栈信息：" + exp.StackTrace);
                    }
                    result.Result = false;
                    result.Msg = "用户密码错误， 请重试！";
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.Result = false;
                result.Msg = "异常信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace;
                LogHelper.WriteErrorLog("SaveLoginLog登录", "错误信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return result;
        }

        public ExcuteModel LoginOutLog(Sys_UserLoginLog model)
        {
            return SaveLoginLog(model);
        }
        /// <summary>
        /// 【内部方法】记录登录日志
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveLoginLog(Sys_UserLoginLog model)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                db.Sys_UserLoginLog.InsertOnSubmit(model);
                db.SubmitChanges();
                result.Result = true;
            }
            catch (Exception exp)
            {
                result.Msg = exp.ToString();
                result.Result = false;
                LogHelper.WriteErrorLog("SaveLoginLog登录", "错误信息：" + exp.Message + "--堆栈信息：" + exp.StackTrace);
            }
            return result;
        }
        /// <summary>
        /// 获取登录日志
        /// </summary>
        /// <returns></returns>
        public List<Sys_UserLoginLog> GetLoginLogList()
        {
            return db.Sys_UserLoginLog.ToList();
        }

        public List<Sys_UserLoginLog> GetNewLoginLogList(int pageSize, int pageIndex, out int totalRecord, out int totalPage)
        {
            totalRecord = 0;
            totalPage = 0;
            DataSet ds = new DataSet();
            List<Sys_UserLoginLog> list = new List<Sys_UserLoginLog>();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["InsideRISConnectionString"].ToString()))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.Parameters.Add(new SqlParameter("@pageSize", SqlDbType.VarChar, 10));
                cmd.Parameters.Add(new SqlParameter("@pageIndex", SqlDbType.VarChar, 10));

                SqlParameter paramTotalRecord = new SqlParameter("@totalRecord", SqlDbType.Int);
                paramTotalRecord.Direction = ParameterDirection.Output;
                SqlParameter paramTotalPage = new SqlParameter("@TotalPage", SqlDbType.Int);
                paramTotalPage.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(paramTotalRecord);
                cmd.Parameters.Add(paramTotalPage);

                cmd.Parameters[0].Value = pageSize;
                cmd.Parameters[1].Value = pageIndex;

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Sys_UserLoginLogPageQuery";

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = cmd;

                adapter.Fill(ds);

                list = DataSetToList<Sys_UserLoginLog>(ds, 0);
                object objtotalRecord = cmd.Parameters["@totalRecord"].Value;
                object objTotalPage = cmd.Parameters["@TotalPage"].Value;
                totalRecord = (objtotalRecord == null || objtotalRecord == DBNull.Value) ? 0 : Convert.ToInt32(objtotalRecord);
                totalPage = (objTotalPage == null || objTotalPage == DBNull.Value) ? 0 : Convert.ToInt32(objTotalPage);
            }
            return list;
        }

        public List<T> DataSetToList<T>(DataSet ds, int tableIndext)
        {
            if (ds == null || ds.Tables.Count <= 0 || tableIndext < 0)
            {
                return null;
            }
            DataTable dt = ds.Tables[tableIndext];

            IList<T> list = new List<T>();
            PropertyInfo[] tMembersAll = typeof(T).GetProperties();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                T t = Activator.CreateInstance<T>();
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    foreach (PropertyInfo tMember in tMembersAll)
                    {
                        if (dt.Columns[j].ColumnName.ToUpper().Equals(tMember.Name.ToUpper()))
                        {
                            if (dt.Rows[i][j] != DBNull.Value)
                            {
                                tMember.SetValue(t, dt.Rows[i][j], null);
                            }
                            else
                            {
                                tMember.SetValue(t, null, null);
                            }
                            break;//注意这里的break是写在if语句里面的，意思就是说如果列名和属性名称相同并且已经赋值了，那么我就跳出foreach循环，进行j+1的下次循环  
                        }
                    }
                }
                list.Add(t);
            }
            return list.ToList();
        }

        public List<Sys_UserLoginLog> GetLoginLogListByName(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                return db.Sys_UserLoginLog.Where(s => s.LogUsername == name).ToList();
            }
            else
            {
                return db.Sys_UserLoginLog.ToList();
            }

        }
        #endregion

        #region 服务项目

        /// <summary>
        /// 根据站点id获取站点的服务项目
        /// </summary>
        /// <param name="SiteId"></param>
        /// <returns></returns>
        public List<Sys_DiagnoseService> GetDiagnoseServiceListBySiteId(string SiteId)
        {
            List<Sys_DiagnoseService> list = new List<Sys_DiagnoseService>();
            list = db.Sys_DiagnoseService.Where(t => t.HospitalId == SiteId).ToList();
            return list;
        }
        /// <summary>
        /// 根据服务项目id获取服务项目
        /// </summary>
        /// <param name="HospitalId"></param>
        /// <param name="ServiceItemId"></param>
        /// <returns></returns>
        public Sys_DiagnoseService GetDiagnoseServiceItemByServiceId(string ServiceId)
        {
            Sys_DiagnoseService model = new Sys_DiagnoseService();
            model = db.Sys_DiagnoseService.FirstOrDefault(t => t.ServiceId == ServiceId);
            return model;
        }

        /// <summary>
        /// 获取所有的服务项目
        /// </summary>
        /// <returns></returns>
        public List<Sys_DiagnoseService> GetDiagnoseServiceAllList()
        {
            List<Sys_DiagnoseService> list = new List<Sys_DiagnoseService>();
            list = db.Sys_DiagnoseService.ToList();
            return list;
        }

        /// <summary>
        /// 添加或修改服务项目 Param 1new，2update
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveDiagnoseService(Sys_DiagnoseService model)
        {
            ExcuteModel RetModel = new ExcuteModel();
            List<Sys_DiagnoseService> list = db.Sys_DiagnoseService.Where(t => t.ServiceId == model.ServiceId).ToList();
            if (list.Count < 1)
            {
                db.Sys_DiagnoseService.InsertOnSubmit(model);
                db.SubmitChanges();
                RetModel.Result = true;
                RetModel.Msg = "服务项目添加成功！";
            }
            else
            {
                Sys_DiagnoseService _model = list[0];
                if (!string.IsNullOrEmpty(model.ServiceName))
                {
                    _model.ServiceName = model.ServiceName;
                }
                if (model.ServiceCost >= 0)
                {
                    _model.ServiceCost = model.ServiceCost;
                }
                if (model.ServiceState > 0)
                {
                    _model.ServiceState = model.ServiceState;
                }
                if (model.ServiceType > 0)
                {
                    _model.ServiceType = model.ServiceType;
                }
                if (!string.IsNullOrEmpty(model.ConfigurationFormula))
                {
                    _model.ConfigurationFormula = model.ConfigurationFormula;
                }
                if (model.DisState > 0)
                {
                    _model.DisState = model.DisState;
                }
                if (model.DisAmount >= 0)
                {
                    _model.DisAmount = model.DisAmount;
                }
                if (model.ServiceFlag > 0)
                {
                    _model.ServiceFlag = model.ServiceFlag;
                }
                if (!string.IsNullOrEmpty(model.ServiceSubUID))
                {
                    _model.ServiceSubUID = model.ServiceSubUID;
                }
                if (model.FreeFlag > 0)
                {
                    //List<Sys_DiagnoseService> i = db.Sys_DiagnoseService.Where(o => o.ServiceName == model.ServiceName && o.HospitalId == model.HospitalId).ToList();
                    //if (i.Count == 1)
                    //{
                    //    _model.FreeFlag = model.FreeFlag;
                    //}
                    //else if (i.Count > 1 && model.FreeFlag == 2)
                    //{
                    //    _model.FreeFlag = model.FreeFlag;
                    //}
                    //else if (i.Count > 1 && i.Where(o => o.FreeFlag == 1).Count() == 0)
                    //{
                    //    _model.FreeFlag = model.FreeFlag;
                    //}
                    //else
                    //{
                    //    strMark = "【服务项目相同只能有一个为免费】";
                    //}
                    _model.FreeFlag = model.FreeFlag;
                }
                _model.UpdatePeople = model.UpdatePeople;
                _model.UpdateTime = model.UpdateTime;
                db.SubmitChanges();
                RetModel.Result = true;
                RetModel.Msg = "服务项目更新成功！";
            }
            return RetModel;
        }

        /// <summary>
        /// 启用1，禁用2服务项目
        /// </summary>
        /// <param name="ServiceId"></param>
        /// <param name="State"></param>
        /// <returns></returns>
        public ExcuteModel SetEnableServiceItem(string ServiceId, int State)
        {
            ExcuteModel RetModel = new ExcuteModel();
            Sys_DiagnoseService model = db.Sys_DiagnoseService.FirstOrDefault(t => t.ServiceId == ServiceId);
            if (model == null)
            {
                RetModel.Result = false;
                RetModel.Msg = "未找到该服务项目，请核对！";
            }
            else
            {//  2已通过,正常,3 已禁用,1已申请
                model.ServiceState = State;
                db.SubmitChanges();
                RetModel.Result = true;
                if (State == 1)
                {
                    RetModel.Msg = "服务项目申请成功！";
                }
                else if (State == 2)
                {
                    RetModel.Msg = "服务项目启用成功！";
                }
                else if (State == 3)
                {
                    RetModel.Msg = "服务项目禁用成功！";
                }
            }
            return RetModel;
        }

        /// <summary>
        /// 添加套餐
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveDiagnoseServiceSub(Sys_DiagnoseServiceSub model)
        {
            ExcuteModel RetModel = new ExcuteModel();
            List<Sys_DiagnoseServiceSub> list = db.Sys_DiagnoseServiceSub.Where(t => t.ServiceSubUID == model.ServiceSubUID).ToList();
            if (list.Count < 1)
            {
                db.Sys_DiagnoseServiceSub.InsertOnSubmit(model);
                db.SubmitChanges();
                RetModel.Result = true;
                RetModel.Msg = "套餐添加成功！";
            }
            else
            {
                Sys_DiagnoseServiceSub modelSub = list[0];
                if (model.SubCount > 0)
                {
                    modelSub.SubCount = model.SubCount;
                }
                if (model.Amount != null)
                {
                    modelSub.Amount = model.Amount;
                }
                if (!string.IsNullOrEmpty(model.ServiceContent))
                {
                    modelSub.ServiceContent = model.ServiceContent;
                }
                if (model.ServiceState > 0)
                {
                    modelSub.ServiceState = model.ServiceState;
                }
                if (model.ServiceOrderBy > 0)
                {
                    modelSub.ServiceOrderBy = model.ServiceOrderBy;
                }
                if (!string.IsNullOrEmpty(model.ServiceSubName))
                {
                    modelSub.ServiceSubName = model.ServiceSubName;
                }
                modelSub.ServiceSubUID = model.ServiceSubUID;
                modelSub.UpdateTime = model.UpdateTime;
                db.SubmitChanges();
                RetModel.Result = true;
                RetModel.Msg = "套餐更新成功！";
            }
            if (RetModel.Result == true)
            {
                Sys_DiagnoseServiceSubRecord modelRecord = new Sys_DiagnoseServiceSubRecord();
                if (model.Amount != null)
                {
                    modelRecord.Amount = model.Amount;
                }
                if (!string.IsNullOrEmpty(model.ServiceContent))
                {
                    modelRecord.ServiceContent = model.ServiceContent;
                }
                if (model.SubCount > 0)
                {
                    modelRecord.SubCount = model.SubCount;
                }
                modelRecord.ServiceSubRecordUID = Guid.NewGuid().ToString();
                modelRecord.ServiceSubUID = model.ServiceSubUID;
                modelRecord.CreateTime = DateTime.Now;
                db.Sys_DiagnoseServiceSubRecord.InsertOnSubmit(modelRecord);
                db.SubmitChanges();
            }
            return RetModel;
        }

        /// <summary>
        /// 获取套餐列表
        /// </summary>
        /// <returns></returns>
        public List<Sys_DiagnoseServiceSub> GetDiagnoseServiceSubList()
        {
            List<Sys_DiagnoseServiceSub> list = new List<Sys_DiagnoseServiceSub>();
            list = db.Sys_DiagnoseServiceSub.OrderBy(z => z.ServiceOrderBy).ToList();
            return list;
        }
        #endregion

        #region 数据字典

        /// <summary>
        /// 添加字典项
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveDictionary(Sys_Dictionary model)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                List<Sys_Dictionary> list = db.Sys_Dictionary.Where(t => t.GUID == model.GUID).ToList();
                if (model.Param == "1")
                {
                    model.Param = "";
                    if (list.Count > 0)
                    {
                        result.Result = false;
                        result.Msg = "字典项保存异常！";
                        return result;
                    }
                    else
                    {
                        list = db.Sys_Dictionary.Where(t => t.Code == model.Code && t.Type == model.Type).ToList();
                        if (list.Count > 0)
                        {
                            result.Result = true;
                            result.Msg = "该代码及父类已存在，请核对！";
                            return result;
                        }
                        db.Sys_Dictionary.InsertOnSubmit(model);
                        db.SubmitChanges();
                        result.Result = true;
                        result.Msg = "字典项保存成功！";
                    }
                }
                else if (model.Param == "2")
                {
                    if (list.Count < 1)
                    {
                        result.Result = false;
                        result.Msg = "字典项更新异常，请联系管理员！";
                        return result;
                    }
                    else
                    {
                        Sys_Dictionary _model = list[0];
                        _model.Code = model.Code;
                        _model.Description = model.Description;
                        _model.Short = model.Short;
                        _model.Type = model.Type;
                        _model.Orderby = model.Orderby;
                        db.SubmitChanges();
                        result.Result = true;
                        result.Msg = "字典项更新成功！";
                    }
                }
            }
            catch (Exception exp) { result.Msg = exp.ToString(); }
            return result;
        }
        /// <summary>
        /// 获取字典列表
        /// </summary>
        /// <returns></returns
        public List<Sys_Dictionary> GetDictionaryListByType(string Type)
        {
            return db.Sys_Dictionary.Where(t => t.Type == Type).OrderBy(t => t.Orderby).ToList();
        }

        /// <summary>
        /// 根据代码获取字典对象
        /// </summary>
        /// <param name="Code"></param>
        /// <returns></returns>
        public Sys_Dictionary GetDictionaryDescriptionByTypeCode(string Type, string Code)
        {
            return db.Sys_Dictionary.FirstOrDefault(t => t.Code == Code && t.Type == Type);
        }

        /// <summary>
        /// 根据唯一号获取字典对象
        /// </summary>
        /// <param name="GUID"></param>
        /// <returns></returns>
        public Sys_Dictionary GetDictionaryByTypeGUID(string GUID)
        {
            return db.Sys_Dictionary.FirstOrDefault(t => t.GUID == GUID);
        }
        #endregion

        //dt
        #region 系统配置
        /// <summary>
        /// 获取系统配置
        /// </summary>
        /// <returns></returns>
        public Sys_Settings GetSysValueByKey(string Key)
        {
            return db.Sys_Settings.FirstOrDefault(t => t.SysKey == Key);
        }


        public List<Sys_Settings> GetSysList()
        {
            return db.Sys_Settings.ToList();
        }

        public List<Sys_Settings> GetSysSettingList(string key)
        {
            return db.Sys_Settings.Where(s => s.SysKey == key).ToList();
        }

        public List<Sys_Settings> GetSysSettingsListBySysType(string sysType)
        {
            return db.Sys_Settings.Where(s => s.SysType == sysType).ToList();
        }

        /// <summary>
        /// 更新配置
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateSysSetting(Sys_Settings model)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                ExcuteModel RetModel = new ExcuteModel();
                Sys_Settings modelInfo = db.Sys_Settings.Where(t => t.SysKey == model.SysKey).FirstOrDefault();
                modelInfo.SysValue = model.SysValue;
                db.SubmitChanges();
                RetModel.Result = true;
                RetModel.Msg = "配置更新成功！";
                return RetModel;
            }
            catch (Exception exp)
            {
                result.Result = false; result.Msg = exp.ToString();
            }
            return result;
        }

        /// <summary>
        /// save model Param -1: new else 2 update
        /// </summary>
        /// <param name="model"></param>
        /// <returns>bool true,false</returns>
        public ExcuteModel SaveSys_Settings(Sys_Settings model)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {

                ExcuteModel RetModel = new ExcuteModel();
                List<Sys_Settings> list = db.Sys_Settings.Where(t => t.GUID == model.GUID).ToList();
                if (list.Count < 1)
                {
                    db.Sys_Settings.InsertOnSubmit(model);
                    db.SubmitChanges();
                    RetModel.Result = true;
                    RetModel.Msg = "新的配置成功！";
                }
                else
                {
                    Sys_Settings _model = list[0];
                    _model.SysType = model.SysType;
                    _model.SysKey = model.SysKey;
                    _model.SysValue = model.SysValue;
                    _model.Remark = model.Remark;
                    _model.Grade = model.Grade;
                    db.SubmitChanges();
                    RetModel.Result = true;
                    RetModel.Msg = "配置更新成功！";
                }
                return RetModel;

            }
            catch (Exception exp) { result.Result = false; result.Msg = exp.ToString(); }
            return result;
        }


        public ExcuteModel DeleteSys_Settings(Sys_Settings model)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                //string strSql = string.Format(@"delete from  [Sys_RoleGroup] where UserName='{0}' and roleId='{1}'", model.UserId, model.RoleId);
                Sys_Settings _mo = new Sys_Settings();
                _mo = db.Sys_Settings.Where(u => u.GUID == model.GUID).FirstOrDefault();
                db.Sys_Settings.DeleteOnSubmit(_mo);
                // db.ExecuteCommand(strSql);
                db.SubmitChanges();
                result.Result = true;
                result.Msg = "移除成功！";
            }
            catch (Exception exp) { result.Msg = exp.ToString(); }
            return result;
        }


        #endregion

        //dt
        #region 系统初始化
        /// <summary>
        /// 获取系统初始化的文件夹
        /// </summary>
        /// <param name="SysCode"></param>
        /// <returns></returns>
        public List<Sys_Settings> GetInitFolder(string SysCode)
        {
            List<Sys_Settings> list = new List<Sys_Settings>();
            list = db.Sys_Settings.Where(t => t.SysType == "SystemInit").ToList();
            return list;
        }
        #endregion

        #region 部门
        public Sys_Dept GetDetpByDetpId(string DeptId)
        {
            return db.Sys_Dept.FirstOrDefault(t => t.DeptId == DeptId);
        }
        public List<Sys_Dept> GetAllDetps()
        {
            return db.Sys_Dept.ToList();
        }


        public ExcuteModel del_SubHospital(SubHospital model)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                string strSql = string.Format(@"delete from  [RemoteDianose].[dbo].[SubHospital] where SubHospitalId='{0}' ", model.SubHospitalId);
                // db.Sys_RoleGroup.DeleteOnSubmit(model);
                db.ExecuteCommand(strSql);
                db.SubmitChanges();
                result.Result = true;
                result.Msg = "医院关联移除成功！";
            }
            catch (Exception exp) { result.Msg = exp.ToString(); }
            return result;
        }



        public List<Sys_Dept> GetAllDetps_All(string Deptid)
        {
            db = new SystemManageDataContext();
            string StrSql = " SELECT TOP 1000 [DeptId],[DeptDisplayName],[Province],[DeptPhone] ,[Param]  ";
            StrSql += "FROM Sys_Dept  as Sys_Dept  where  Sys_Dept.DeptId not in (SELECT  [SubHospitalId] ";
            StrSql += "FROM [RemoteDianose].[dbo].[SubHospital] where PHospitalId='" + Deptid + "'   union   select '" + Deptid + "' as [SubHospitalId]) ";
            List<Sys_Dept> List = db.ExecuteQuery<Sys_Dept>(StrSql).ToList();
            return List;
        }

        public List<AllDept> GetAllDetpsAndSub()
        {
            db = new SystemManageDataContext();
            string StrSql = "SELECT  [SubHospitalId]   as DeptId   ,[SHname]     as DeptDisplayName,2 as Htype FROM [RemoteDianose].[dbo].[SubHospital] ";
            StrSql += " union ";
            StrSql += " SELECT  [DeptId]  as DeptId,[DeptDisplayName]    as DeptDisplayName  ,1 as Htype FROM Sys_Dept ";
            List<AllDept> List = db.ExecuteQuery<AllDept>(StrSql).ToList();
            return List;
        }

        public List<AllDept> GetAllDetpsRelevance(string Deptid)
        {
            db = new SystemManageDataContext();
            string StrSql = " with ALLDept as (  ";
            StrSql += "SELECT  [SubHospitalId]   as DeptId   ,[SHname]     as DeptDisplayName,2 as Htype FROM [RemoteDianose].[dbo].[SubHospital] ";
            StrSql += " union ";
            StrSql += " SELECT  [DeptId]  as DeptId,[DeptDisplayName]    as DeptDisplayName  ,1 as Htype FROM Sys_Dept ";
            StrSql += " ) select * from ALLDept   where ALLDept.DeptId not in (SELECT [SubHospitalId]  ";
            StrSql += " FROM [RemoteDianose].[dbo].[SubHospital] WHERE PHospitalId='" + Deptid + "' ";
            StrSql += " union select'" + Deptid + "' as SubHospitalId ) ";
            List<AllDept> List = db.ExecuteQuery<AllDept>(StrSql).ToList();
            return List;
        }
        //GetAllDetpsRelevance
        public ExcuteModel SaveDept(Sys_Dept model)
        {
            ExcuteModel ret = new ExcuteModel();
            var _model = db.Sys_Dept.Where(t => t.DeptId == model.DeptId).ToList();
            if (_model.Count > 0)
            {
                var t = _model.Where(o => o.DeptId == model.DeptId).ToList();
                if (t.Count > 1)
                {
                    ret.Result = false;
                    ret.Msg = "机构编码重复！";
                }
                else if (_model.Where(o => o.DeptDisplayName == model.DeptDisplayName).ToList().Count > 1)
                {
                    ret.Result = false;
                    ret.Msg = "机构名称重复！";
                }
                else
                {
                    _model[0].DeptDisplayName = model.DeptDisplayName;
                    _model[0].DeptPhone = model.DeptPhone;
                    _model[0].Province = model.Province;
                    _model[0].DepType = model.DepType;
                    db.SubmitChanges();
                    ret.Result = true;
                    ret.Msg = "机构更新成功！";
                }
            }
            else
            {
                if (db.Sys_Dept.Where(t => t.DeptId == model.DeptId).ToList().Count > 0)
                {
                    ret.Result = false;
                    ret.Msg = "机构编码重复！";
                }
                else if (db.Sys_Dept.Where(t => t.DeptDisplayName == model.DeptDisplayName).ToList().Count > 0)
                {
                    ret.Result = false;
                    ret.Msg = "机构名称重复！";
                }
                else
                {
                    db.Sys_Dept.InsertOnSubmit(model);
                    db.SubmitChanges();
                    ret.Result = true;
                    ret.Msg = "机构新增成功！";
                }
            }
            return ret;
        }
        #endregion

        /// <summary>
        /// 创建用户钱包
        /// </summary>
        /// <param name="objCustAccount"></param>
        /// <returns></returns>
        public ExcuteModel AddUserAccount(CustAccount objCustAccount)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                dbContext.CustAccount.InsertOnSubmit(objCustAccount);
                dbContext.SubmitChanges();
                result.Result = true;
            }
            catch (Exception ex)
            {
                result.Msg = ex.ToString();
            }
            return result;
        }

        /// <summary>
        /// 添加信息
        /// </summary>
        /// <param name="objDoctorInfo"></param>
        /// <returns></returns>
        public ExcuteModel AddDoctorInfo(DoctorInfo objDoctorInfo)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                dbContext.DoctorInfo.InsertOnSubmit(objDoctorInfo);
                dbContext.SubmitChanges();
                result.Result = true;
            }
            catch (Exception ex)
            {
                result.Msg = ex.ToString();
            }
            return result;
        }

        /// <summary>
        /// 更新专家机构信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateDoctorInfo(DoctorInfo model)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                DoctorInfo di = dbContext.DoctorInfo.Where(o => o.DoctorId == model.DoctorId).FirstOrDefault();
                if (di != null)
                {
                    di.Dept = model.Dept;
                    dbContext.SubmitChanges();
                }
                result.Result = true;
            }
            catch (Exception ex)
            {
                result.Msg = ex.ToString();
            }
            return result;
        }

        /// <summary>
        /// 更新专家状态
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateDoctorInfoState(DoctorInfo model)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                DoctorInfo di = dbContext.DoctorInfo.Where(o => o.DoctorId == model.DoctorId).FirstOrDefault();
                if (di != null)
                {
                    di.AuditUserId = model.AuditUserId;
                    di.AuditDate = DateTime.Now;
                    di.Remark = model.Remark;
                    di.State = model.State;
                    dbContext.SubmitChanges();
                }
                result.Result = true;
            }
            catch (Exception ex)
            {
                result.Msg = ex.ToString();
            }
            return result;
        }

        /// <summary>
        /// 获取医生
        /// </summary>
        /// <returns></returns>
        public List<DoctorInfo> GetDoctorInfo()
        {
            return dbContext.DoctorInfo.ToList();
        }

        public DoctorInfo GetDoctorInfoById(string doctorId)
        {
            return dbContext.DoctorInfo.FirstOrDefault(d => d.DoctorId == doctorId);
        }

        /// <summary>
        /// 保存并更新医生信息
        /// </summary>
        /// <param name="objDoctorInfo"></param>
        /// <returns></returns>
        public ExcuteModel SaveDoctorInfo(DoctorInfo objDoctorInfo)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                DoctorInfo objDoc = dbContext.DoctorInfo.Where(d => d.DoctorId == objDoctorInfo.DoctorId).FirstOrDefault();
                if (objDoc != null)
                {
                    objDoc.DoctorName = objDoctorInfo.DoctorName;
                    objDoc.DoctorIdentityType = objDoctorInfo.DoctorIdentityType;
                    objDoc.DoctorIdentityNum = objDoctorInfo.DoctorIdentityNum;
                    objDoc.DoctorTel = objDoctorInfo.DoctorTel;
                    objDoc.DoctorAdress = objDoctorInfo.DoctorAdress;
                    if (objDoctorInfo.DoctorGrade > 0)
                    {
                        objDoc.DoctorGrade = objDoctorInfo.DoctorGrade;
                    }
                    objDoc.GoodAt = objDoctorInfo.GoodAt;
                    objDoc.HospitalDept = objDoctorInfo.HospitalDept;
                    objDoc.Dept = objDoctorInfo.Dept;
                    objDoc.UserName = objDoctorInfo.UserName;
                    objDoc.DoctorOrderBy = objDoctorInfo.DoctorOrderBy;
                    objDoc.IntrodutionURL = objDoctorInfo.IntrodutionURL;
                    if (!string.IsNullOrEmpty(objDoctorInfo.DiagnosisType))
                    {
                        objDoc.DiagnosisType = objDoctorInfo.DiagnosisType;
                    }
                    objDoc.DiagnosisPrice = objDoctorInfo.DiagnosisPrice;
                    objDoc.DoctorAdress = objDoctorInfo.DoctorAdress;
                    objDoc.Introduction = objDoctorInfo.Introduction;
                    objDoc.DoctorTitle = objDoctorInfo.DoctorTitle;
                    dbContext.SubmitChanges();
                    result.Result = true;
                }
                else
                {
                    dbContext.DoctorInfo.InsertOnSubmit(objDoctorInfo);
                    dbContext.SubmitChanges();
                    result.Result = true;
                }
            }
            catch (Exception ex)
            {
                result.Result = false;
                result.Msg = ex.ToString();
            }
            return result;
        }

        public int? GetMaxOrderValue()
        {
            return dbContext.DoctorInfo.Max(o => o.DoctorOrderBy);
        }

        public Sys_Users GetSysUserById(string userId)
        {
            return db.Sys_Users.Where(s => s.UserId == userId).FirstOrDefault();
        }
        /// <summary>
        /// 获取用户组名
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<string> GetSysUserName(string userId)
        {
            string groupName = string.Empty;
            List<string> groupNameList = new List<string>();
            var userGroup = dbContext.UserGroup.Where(u => u.UserId == userId).ToList();
            if (userGroup.Count != 0)
            {
                for (int i = 0; i < userGroup.Count; i++)
                {
                    groupName = userGroup[i].GroupName;
                    groupNameList.Add(groupName);
                }
            }
            return groupNameList;
        }

        /// <summary>
        /// 获取部门名称
        /// </summary>
        /// <param name="deptId"></param>
        /// <returns></returns>
        public string GetDepartmentName(string deptId)
        {
            string deptName = string.Empty;
            var deptList = db.Sys_Dept.Where(d => d.DeptId == deptId).FirstOrDefault();
            if (deptList != null)
            {
                deptName = deptList.DeptDisplayName;
            }
            return deptName;
        }

        public string GetIDTypeName(string IdNum)
        {
            var IdTypeList = db.Sys_Dictionary.Where(i => i.Code == IdNum).FirstOrDefault();
            string idName = string.Empty;
            if (IdTypeList != null)
            {
                idName = IdTypeList.Description;
            }
            return idName;
        }

        public ExcuteModel SaveSysUserInfo(Sys_Users objSysUser)
        {
            ExcuteModel objExcute = new ExcuteModel();
            try
            {
                Sys_Users objUpdateUsers = db.Sys_Users.Where(s => s.UserId == objSysUser.UserId).FirstOrDefault();
                if (objUpdateUsers != null)
                {
                    if (!string.IsNullOrEmpty(objSysUser.UserName))
                    {
                        objUpdateUsers.UserName = objSysUser.UserName;
                    }
                    if (!string.IsNullOrEmpty(objSysUser.Name))
                    {
                        objUpdateUsers.Name = objSysUser.Name;
                    }
                    if (!string.IsNullOrEmpty(objSysUser.Mobile))
                    {
                        objUpdateUsers.Mobile = objSysUser.Mobile;
                    }
                    if (!string.IsNullOrEmpty(objSysUser.Dept))
                    {
                        objUpdateUsers.Dept = objSysUser.Dept;
                    }
                    if (!string.IsNullOrEmpty(objSysUser.IntroductionURL))
                    {
                        objUpdateUsers.IntroductionURL = objSysUser.IntroductionURL;
                    }
                    if (!string.IsNullOrEmpty(objSysUser.UserImgURL))
                    {
                        objUpdateUsers.UserImgURL = objSysUser.UserImgURL;
                    }
                    if (!string.IsNullOrEmpty(objSysUser.DiagnosisType))
                    {
                        objUpdateUsers.DiagnosisType = objSysUser.DiagnosisType;
                    }
                    if (!string.IsNullOrEmpty(objSysUser.Introduction))
                    {
                        objUpdateUsers.Introduction = objSysUser.Introduction;
                    }
                    if (!string.IsNullOrEmpty(objSysUser.IsLeader))
                    {
                        objUpdateUsers.IsLeader = objSysUser.IsLeader;
                    }
                    if (!string.IsNullOrEmpty(objSysUser.Email))
                    {
                        objUpdateUsers.Email = objSysUser.Email;
                    }
                    if (!string.IsNullOrEmpty(objSysUser.Sex))
                    {
                        objUpdateUsers.Sex = objSysUser.Sex;
                    }
                    if (objSysUser.Age > 0)
                    {
                        objUpdateUsers.Age = objSysUser.Age;
                    }
                    if (objSysUser.State > 0)
                    {
                        objUpdateUsers.State = objSysUser.State;
                    }
                    if (objSysUser.UserType > 0)
                    {
                        objUpdateUsers.UserType = objSysUser.UserType;
                    }
                    db.SubmitChanges();
                    objExcute.Result = true;
                }
            }
            catch (Exception ex)
            {
                objExcute.Result = false;
                objExcute.Msg = ex.ToString();
            }
            return objExcute;
        }

        #region 省份
        /// <summary>
        /// add by bob 2017-07-09
        /// 获取省份
        /// </summary>
        /// <returns></returns>
        public List<Base_Area> GetBaseArea()
        {
            List<Base_Area> list = new List<Base_Area>();
            list = db.Base_Area.Where(o => o.parentid == "0").ToList();
            return list;
        }
        #endregion

        #region 服务类型
        /// <summary>
        /// 获取服务类型数据
        /// </summary>
        /// <returns></returns>
        public List<Sys_DiagnoseProgram> GetDiagnoseProgramList()
        {
            List<Sys_DiagnoseProgram> list = new List<Sys_DiagnoseProgram>();
            list = db.Sys_DiagnoseProgram.ToList();
            return list;
        }

        /// <summary>
        /// 查询服务类型数据
        /// </summary>
        /// <param name="ServiceID"></param>
        /// <returns></returns>
        public Sys_DiagnoseProgram GetDiagnoseProgramModel(int ServiceID)
        {
            Sys_DiagnoseProgram model = new Sys_DiagnoseProgram();
            return model = db.Sys_DiagnoseProgram.Where(o => o.ServiceID == ServiceID).FirstOrDefault();
        }

        /// <summary>
        /// 添加服务类型
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel AddServiceProgram(Sys_DiagnoseProgram param)
        {
            ExcuteModel em = new ExcuteModel();
            try
            {
                Sys_DiagnoseProgram modelName = db.Sys_DiagnoseProgram.Where(o => o.ServiceName == param.ServiceName).FirstOrDefault();
                if (modelName == null)
                {
                    param.ServiceCreate = DateTime.Now;
                    param.ServiceUpdate = DateTime.Now;
                    db.Sys_DiagnoseProgram.InsertOnSubmit(param);
                    db.SubmitChanges();
                    em.Result = true;
                    em.Msg = "添加服务类型成功！";
                }
                else
                {
                    em.Result = false;
                    em.Msg = "服务类型已存在！";
                }
            }
            catch (Exception ex)
            {
                em.Result = false;
                em.Msg = ex.Message;
            }
            return em;
        }

        /// <summary>
        /// 更新服务类型
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel UpdateServiceProgram(Sys_DiagnoseProgram param)
        {
            ExcuteModel em = new ExcuteModel();
            try
            {
                Sys_DiagnoseProgram modelParam = db.Sys_DiagnoseProgram.Where(o => o.ServiceID == param.ServiceID).FirstOrDefault();
                if (modelParam != null)
                {
                    Sys_DiagnoseProgram modelName = db.Sys_DiagnoseProgram.Where(o => o.ServiceName == param.ServiceName).FirstOrDefault();
                    if (modelName == null)
                    {
                        modelParam.ServiceName = param.ServiceName;
                        modelParam.ServiceRemark = param.ServiceRemark;
                        modelParam.ServiceUpdate = DateTime.Now;
                    }
                    else
                    {
                        modelParam.ServiceRemark = param.ServiceRemark;
                        modelParam.ServiceUpdate = DateTime.Now;
                    }
                    db.SubmitChanges();
                    em.Result = true;
                    em.Msg = "更新服务类型成功！";
                }
            }
            catch (Exception ex)
            {
                em.Result = false;
                em.Msg = ex.Message;
            }
            return em;
        }

        /// <summary>
        /// 更新状态成功
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel UpdateServiceProgramState(Sys_DiagnoseProgram param)
        {
            ExcuteModel em = new ExcuteModel();
            try
            {
                Sys_DiagnoseProgram modelParam = db.Sys_DiagnoseProgram.Where(o => o.ServiceID == param.ServiceID).FirstOrDefault();
                if (modelParam != null)
                {
                    modelParam.ServiceState = param.ServiceState;
                    modelParam.ServiceUpdate = DateTime.Now;
                    db.SubmitChanges();
                    em.Result = true;
                    em.Msg = "更新服务状态成功！";
                }
            }
            catch (Exception ex)
            {
                em.Result = false;
                em.Msg = ex.Message;
            }
            return em;
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel UpdateServiceProgramDel(Sys_DiagnoseProgram param)
        {
            ExcuteModel em = new ExcuteModel();
            try
            {
                Sys_DiagnoseProgram modelParam = db.Sys_DiagnoseProgram.Where(o => o.ServiceID == param.ServiceID).FirstOrDefault();
                if (modelParam != null)
                {
                    modelParam.ServiceDelState = param.ServiceDelState;
                    modelParam.ServiceUpdate = DateTime.Now;
                    db.SubmitChanges();
                    em.Result = true;
                    em.Msg = "更新服务状态成功！";
                }
            }
            catch (Exception ex)
            {
                em.Result = false;
                em.Msg = ex.Message;
            }
            return em;
        }
        #endregion

        #region 用户组权限维护
        public List<ThePrivilegesOfTheUser> GetUserPrivileges(string userId)
        {
            List<ThePrivilegesOfTheUser> privilegesList = new List<ThePrivilegesOfTheUser>();
            string sql = "select UGroupId,UserId,Groups.GroupId,GroupsName from UserGroup";
            sql += " inner join Groups on UserGroup.GroupName=Groups.GroupId";
            sql += " where UserId='{0}'";
            sql = string.Format(sql, userId);
            privilegesList = db.ExecuteQuery<ThePrivilegesOfTheUser>(sql).ToList();
            return privilegesList;
        }

        public Sys_Settings GetSys_SettingsInfo(string sysKey)
        {
            Sys_Settings sys_Settings = db.Sys_Settings.Where(s => s.SysKey == sysKey).FirstOrDefault();
            return sys_Settings;
        }
        #endregion

        #region 工作量统计
        public List<Sys_Users> GetSysUsersByDept(string dept)
        {
            return db.Sys_Users.Where(s => s.Dept == dept).ToList();
        }
        #endregion

        #region 消息维护

        public ExcuteModel AddAppUpdateLog(AppUpdateLog appUpdateLog)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                List<AppUpdateLog> list = db.AppUpdateLog.Where(t => t.ID == appUpdateLog.ID).ToList();
                if (list.Count > 0)
                {
                    result.Msg = "该消息已存在，请核实！";
                    result.Result = false;
                    return result;
                }
                {
                    db.AppUpdateLog.InsertOnSubmit(appUpdateLog);
                    db.SubmitChanges();
                    result.Msg = "消息维护添加成功！";
                    result.Result = true;
                }
            }
            catch (Exception exp)
            {
                result.Msg = exp.ToString();
                result.Result = false;
                return result;
            }
            return result;
        }

        public ExcuteModel SaveAppUpdateLog(AppUpdateLog model)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                AppUpdateLog _appUpdateLog = db.AppUpdateLog.FirstOrDefault(t => t.ID == model.ID);
                if (_appUpdateLog != null)
                {
                    if (!string.IsNullOrEmpty(model.Title))
                    {
                        _appUpdateLog.Title = model.Title;
                    }
                    if (!string.IsNullOrEmpty(model.Explain))
                    {
                        _appUpdateLog.Explain = model.Explain;
                    }
                    if (!string.IsNullOrEmpty(model.Description))
                    {
                        _appUpdateLog.Description = model.Description;
                    }
                    if (!string.IsNullOrEmpty(model.Version))
                    {
                        _appUpdateLog.Version = model.Version;
                    }
                    if (!string.IsNullOrEmpty(model.UpdateType))
                    {
                        _appUpdateLog.UpdateType = model.UpdateType;
                    }
                    if (!string.IsNullOrEmpty(model.UploadFileName))
                    {
                        _appUpdateLog.UploadFileName = model.UploadFileName;
                    }
                    _appUpdateLog.IsUpdate = model.IsUpdate;
                    _appUpdateLog.DelState = model.DelState;
                }
                else
                {
                    db.AppUpdateLog.InsertOnSubmit(model);
                }
                db.SubmitChanges();
                RetModel.Result = true;
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        public ExcuteModel DelAppUpdateLog(AppUpdateLog model)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                AppUpdateLog list = db.AppUpdateLog.Where(t => t.ID == model.ID).FirstOrDefault();
                if (list != null)
                {
                    if (list.DelState == 1)
                    {
                        list.DelState = 0;
                    }
                    result.Result = true;
                    return result;
                }
            }
            catch (Exception exp)
            {
                result.Msg = exp.ToString();
                result.Result = false;
                return result;
            }
            return result;
        }

        public AppUpdateLog GetAppUpdateLogById(string id)
        {
            AppUpdateLog model = new AppUpdateLog();
            model = db.AppUpdateLog.FirstOrDefault(t => t.ID == id);
            return model;
        }

        public List<AppUpdateLog> GetAppUpdateLog()
        {
            return db.AppUpdateLog.Where(s => s.DelState == 1).ToList();
        }

        public List<AppUpdateLog> GetNewAppUpdateLogList(int pageSize, int pageIndex, out int totalRecord, out int totalPage)
        {
            totalRecord = 0;
            totalPage = 0;
            DataSet ds = new DataSet();
            List<AppUpdateLog> list = new List<AppUpdateLog>();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["InsideRISConnectionString"].ToString()))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.Parameters.Add(new SqlParameter("@pageSize", SqlDbType.VarChar, 10));
                cmd.Parameters.Add(new SqlParameter("@pageIndex", SqlDbType.VarChar, 10));

                SqlParameter paramTotalRecord = new SqlParameter("@totalRecord", SqlDbType.Int);
                paramTotalRecord.Direction = ParameterDirection.Output;
                SqlParameter paramTotalPage = new SqlParameter("@TotalPage", SqlDbType.Int);
                paramTotalPage.Direction = ParameterDirection.Output;

                cmd.Parameters.Add(paramTotalRecord);
                cmd.Parameters.Add(paramTotalPage);

                cmd.Parameters[0].Value = pageSize;
                cmd.Parameters[1].Value = pageIndex;

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "AppUpdateLogPageQuery";

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = cmd;

                adapter.Fill(ds);

                list = DataSetToList<AppUpdateLog>(ds, 0);
                object objtotalRecord = cmd.Parameters["@totalRecord"].Value;
                object objTotalPage = cmd.Parameters["@TotalPage"].Value;
                totalRecord = (objtotalRecord == null || objtotalRecord == DBNull.Value) ? 0 : Convert.ToInt32(objtotalRecord);
                totalPage = (objTotalPage == null || objTotalPage == DBNull.Value) ? 0 : Convert.ToInt32(objTotalPage);
            }
            return list;
        }

        public List<AppUpdateLog> GetAppUpdateLogByDateTime(DateTime datetime)
        {
            return db.AppUpdateLog.Where(s => s.UpdateDate == datetime).ToList();
        }
        #endregion

        #region 科室类型维护
        /// <summary>
        /// 添加科室
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel AddDepartment(Sys_Department model)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                Sys_Department info = db.Sys_Department.Where(t => t.DepartmentName == model.DepartmentName).FirstOrDefault();
                if (info != null)
                {
                    result.Msg = "科室已存在，请核实！";
                    result.Result = false;
                }
                else
                {
                    model.CreateTime = DateTime.Now;
                    db.Sys_Department.InsertOnSubmit(model);
                    db.SubmitChanges();
                    result.Msg = "科室保存成功！";
                    result.Result = true;
                }
            }
            catch (Exception exp)
            {
                result.Msg = exp.ToString();
                result.Result = false;
                return result;
            }
            return result;
        }
        /// <summary>
        /// 更新科室表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateDepartment(Sys_Department model)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                Sys_Department info = db.Sys_Department.Where(t => t.DepartmentId == model.DepartmentId).FirstOrDefault();
                if (info != null)
                {
                    var varIsExist = db.Sys_Department.Where(t => t.DepartmentName == model.DepartmentName).FirstOrDefault();
                    if (varIsExist == null)
                    {
                        info.DepartmentId = model.DepartmentId;
                        info.DepartmentName = model.DepartmentName;
                        info.Description = model.Description;
                        db.SubmitChanges();
                        result.Msg = "科室修改成功！";
                        result.Result = true;
                    }
                    else if (varIsExist != null && varIsExist.DepartmentName == model.DepartmentName)
                    {
                        info.DepartmentId = model.DepartmentId;
                        info.Description = model.Description;
                        db.SubmitChanges();
                        result.Msg = "科室修改成功！";
                        result.Result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Msg = ex.Message;
                result.Result = false;
            }
            return result;
        }

        /// <summary>
        /// 获取科室类型
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public List<Sys_Department> GetDepartmentList(Sys_Department model)
        {
            List<Sys_Department> list = new List<Sys_Department>();
            string StrSql = string.Empty;
            string Condition = string.Empty;
            if (model.DepartmentId > 0)
            {
                Condition = @" AND DepartmentId=" + model.DepartmentId;
            }
            if (!string.IsNullOrEmpty(model.DepartmentName))
            {
                Condition += @" AND DepartmentName='" + model.DepartmentName + "'";
            }
            StrSql = @"SELECT * FROM Sys_Department WHERE 1=1" + Condition;
            list = db.ExecuteQuery<Sys_Department>(StrSql).ToList();
            return list;
        }
        #endregion

        #region 设备类型维护
        /// <summary>
        /// 添加设备类型
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel AddDevice(Sys_Device model)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                Sys_Device info = db.Sys_Device.Where(t => t.DepartmentId == model.DepartmentId && t.DeviceType == model.DeviceType && (t.DeviceName == model.DeviceName || t.MachineRoom == model.MachineRoom)).FirstOrDefault();
                if (info != null)
                {
                    result.Msg = "设备已存在，请核实！";
                    result.Result = false;
                }
                else
                {
                    model.CreateTime = DateTime.Now;
                    db.Sys_Device.InsertOnSubmit(model);
                    db.SubmitChanges();
                    result.Msg = "设备保存成功！";
                    result.Result = true;
                }
            }
            catch (Exception exp)
            {
                result.Msg = exp.ToString();
                result.Result = false;
                return result;
            }
            return result;
        }

        public ExcuteModel UpdateDevice(Sys_Device model)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                Sys_Device info = db.Sys_Device.Where(t => t.DeviceId == model.DeviceId).FirstOrDefault();
                if (info != null)
                {
                    var isExist = db.Sys_Device.Where(t => t.DeviceName == model.DeviceName && t.MachineRoom == model.MachineRoom).FirstOrDefault();
                    if (isExist == null)
                    {
                        info.DeviceId = model.DeviceId;
                        info.DeviceType = model.DeviceType;
                        if (!string.IsNullOrEmpty(model.DeviceName))
                        {
                            info.DeviceName = model.DeviceName;
                        }
                        if (!string.IsNullOrEmpty(model.DeviceAE))
                        {
                            info.DeviceAE = model.DeviceAE;
                        }
                        if (!string.IsNullOrEmpty(model.MachineRoom))
                        {
                            info.MachineRoom = model.MachineRoom;
                        }
                        info.DepartmentId = model.DepartmentId;
                        info.Description = model.Description;
                        db.SubmitChanges();
                        result.Msg = "设备修改成功！";
                        result.Result = true;
                    }
                    else if (isExist != null && isExist.MachineRoom == model.MachineRoom && isExist.DeviceName == model.DeviceName)
                    {
                        info.DeviceId = model.DeviceId;
                        info.DeviceType = model.DeviceType;
                        info.DepartmentId = model.DepartmentId;
                        info.Description = model.Description;
                        if (!string.IsNullOrEmpty(model.DeviceAE))
                        {
                            info.DeviceAE = model.DeviceAE;
                        }
                        db.SubmitChanges();
                        result.Msg = "设备修改成功！";
                        result.Result = true;
                    }
                    else
                    {
                        result.Msg = "设备已存在，修改失败...";
                        result.Result = false;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Msg = ex.Message;
                result.Result = false;
            }
            return result;
        }

        /// <summary>
        /// 获取设备类型
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public List<OutDevice> GetDeviceList(OutDevice model)
        {
            List<OutDevice> list = new List<OutDevice>();
            string StrSql = string.Empty;
            string Condition = string.Empty;
            if (model.DepartmentId > 0)
            {
                Condition = @" AND DepartmentId=" + model.DepartmentId;
            }
            if (!string.IsNullOrEmpty(model.DeviceName))
            {
                Condition += @" AND DeviceName='" + model.DeviceName + "'";
            }
            if (!string.IsNullOrEmpty(model.DeviceType))
            {
                Condition += @" AND DeviceType='" + model.DeviceType + "'";
            }
            if (!string.IsNullOrEmpty(model.MachineRoom))
            {
                Condition += @" AND MachineRoom='" + model.MachineRoom + "'";
            }
            StrSql = @"SELECT * FROM Sys_Device WHERE 1=1" + Condition;
            list = db.ExecuteQuery<OutDevice>(StrSql).ToList();
            return list;
        }

        /// <summary>
        /// 根据检查科室获取设备类型
        /// </summary>
        /// <param name="DepartmentID"></param>
        /// <returns></returns>
        public List<DiagnoseType> GetDiagnoseTypeData(int DepartmentID)
        {
            List<DiagnoseType> list = new List<DiagnoseType>();
            string StrSql = @"SELECT * FROM DiagnoseType WHERE DiagnoseTypeName IN(SELECT DeviceType FROM Sys_Device WHERE DepartmentId=" + DepartmentID + " GROUP BY DeviceType)";
            list = db.ExecuteQuery<DiagnoseType>(StrSql).ToList();
            return list;
        }
        #endregion

        #region 胶片规格维护
        /// <summary>
        /// 添加规格
        /// </summary>
        /// <param name="model"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public ExcuteModel AddFilmSpeci(Sys_FilmSpeci model)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                Sys_FilmSpeci info = db.Sys_FilmSpeci.Where(t => t.FilmName == model.FilmName).FirstOrDefault();
                if (info != null)
                {
                    result.Msg = "信息已存在，请核实！";
                    result.Result = false;
                }
                else
                {
                    model.CreateTime = DateTime.Now;
                    db.Sys_FilmSpeci.InsertOnSubmit(model);
                    db.SubmitChanges();
                    result.Msg = "保存成功！";
                    result.Result = true;
                }
            }
            catch (Exception exp)
            {
                result.Msg = exp.ToString();
                result.Result = false;
                return result;
            }
            return result;
        }

        public ExcuteModel UpdateFilmSpeci(Sys_FilmSpeci model)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                Sys_FilmSpeci info = db.Sys_FilmSpeci.Where(t => t.FilmId == model.FilmId).FirstOrDefault();
                if (info != null)
                {
                    var IsExist = db.Sys_FilmSpeci.Where(t => t.FilmName == model.FilmName).FirstOrDefault();
                    if (IsExist == null)
                    {
                        info.FilmId = model.FilmId;
                        info.FilmName = model.FilmName;
                        info.Description = model.Description;
                        db.SubmitChanges();
                        result.Msg = "修改成功！";
                        result.Result = true;
                    }
                    else if (IsExist != null && IsExist.FilmName == model.FilmName)
                    {
                        info.FilmId = model.FilmId;
                        info.Description = model.Description;
                        db.SubmitChanges();
                        result.Msg = "修改成功！";
                        result.Result = true;
                    }
                }
                else
                {
                    result.Msg = "信息已存在！";
                    result.Result = false;
                }
            }
            catch (Exception ex)
            {
                result.Msg = ex.Message;
                result.Result = true;
            }
            return result;
        }

        /// <summary>
        /// 获取所有规格
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<Sys_FilmSpeci> GetFilmSpeciList(Sys_FilmSpeci model)
        {
            List<Sys_FilmSpeci> list = new List<Sys_FilmSpeci>();
            string StrSql = string.Empty;
            string Condition = string.Empty;
            if (!string.IsNullOrEmpty(model.FilmName))
            {
                Condition = @" AND FilmName='" + model.FilmName + "'";
            }
            StrSql = @"SELECT * FROM Sys_FilmSpeci WHERE 1=1" + Condition;
            list = db.ExecuteQuery<Sys_FilmSpeci>(StrSql).ToList();
            return list;
        }
        #endregion

        #region 患者影像号累计
        /// <summary>
        /// 批量创建影像号
        /// </summary>
        /// <returns></returns>
        public ExcuteModel CreatePatientNumber()
        {
            ExcuteModel em = new ExcuteModel();
            try
            {
                string count = db.Sys_PatientNumber.Where(x => x.Flag == null).Max(o => o.PatientId);
                long patientID = 0;
                if (!string.IsNullOrEmpty(count))
                {
                    patientID = long.Parse(count);
                }
                for (int i = 1; i <= 20000; i++)
                {
                    Sys_PatientNumber model = new Sys_PatientNumber { PatientId = (patientID + i).ToString().PadLeft(8, '0'), CreateTime = DateTime.Now };
                    db.Sys_PatientNumber.InsertOnSubmit(model);
                    db.SubmitChanges();
                }
            }
            catch (Exception ex)
            {
                em.Result = false;
                em.Msg = ex.Message;
            }
            return em;
        }
        /// <summary>
        /// 获取影像号
        /// 同时判断，影像号的数量
        /// </summary>
        /// <returns></returns>
        public string GetPatientNumber()
        {
            string strPatientNumber = db.Sys_PatientNumber.Where(z => z.Flag == null).Min(o => o.PatientId);
            if (!string.IsNullOrEmpty(strPatientNumber))
            {
                var modelInfo = db.Sys_PatientNumber.Where(o => o.PatientId == strPatientNumber).FirstOrDefault();
                if (modelInfo != null)
                {
                    modelInfo.Flag = 1;
                    db.SubmitChanges();
                }
            }
            int numberCount = db.Sys_PatientNumber.ToList().Count;
            if (numberCount < 10)
            {
                Thread th = new Thread(() =>
                {
                    CreatePatientNumber();
                });
                th.Start();
            }
            return strPatientNumber;
        }
        /// <summary>
        /// 删除影像号
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel DeletePatientNumber(string param)
        {
            ExcuteModel em = new ExcuteModel();
            try
            {
                if (!string.IsNullOrEmpty(param))
                {
                    Sys_PatientNumber item = db.Sys_PatientNumber.Where(o => o.PatientId == param).FirstOrDefault();
                    db.Sys_PatientNumber.DeleteOnSubmit(item);
                    db.SubmitChanges();
                    em.Result = true;
                }
                else
                {
                    em.Result = false;
                    em.Msg = "参数为空";
                }
            }
            catch (Exception ex)
            {
                em.Result = false;
                em.Msg = ex.Message;
            }
            return em;
        }
        /// <summary>
        /// 修改字段值
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel UpdatePatientNumber(string param)
        {
            ExcuteModel em = new ExcuteModel();
            try
            {
                if (!string.IsNullOrEmpty(param))
                {
                    Sys_PatientNumber item = db.Sys_PatientNumber.Where(o => o.PatientId == param).FirstOrDefault();
                    item.Flag = null;
                    db.SubmitChanges();
                    em.Result = true;
                }
                else
                {
                    em.Result = true;
                    em.Msg = "参数为空";
                }
            }
            catch (Exception ex)
            {
                em.Result = false;
                em.Msg = ex.Message;
            }
            return em;
        }
        #endregion
    }
}