﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WcfService.DB_Server.SystemManage;
using WcfService.Models;
using System.Text;

namespace WcfService.DB_Server.ImageServer
{
    public class ImageServerContext
    {
        public ImageServerContext() { }
        ImageServerDataContext DBContext = new ImageServerDataContext();
        SystemManageDataContext db = new SystemManageDataContext();




        #region   clearcanvas


        public List<string> StudyFolder()
        {
            string StrSql = "select (f.FilesystemPath+'\\'+s.PartitionFolder) as t_Path from Filesystem f ,ServerPartition s  ";
            List<string> list = DBContext.ExecuteQuery<string>(StrSql).ToList();
            return list;
        }

        /// <summary>
        /// 获取日期文件夹
        /// </summary>
        /// <param name="patientid"></param>
        /// <returns></returns>
        public List<string> GetStudyFolder(string patientid,string studyInstanceUid)
        {
            List<string> list = new List<string>();
            try
            {
                // where StudyStorageGUID in (select StudyStorageGUID from dbo.Study where PatientId='" + patientid + "')";
                //string StrSql = @"select StudyFolder from [ImageServer].[dbo].[FilesystemStudyStorage] 
                string StrSql = @"SELECT StudyFolder FROM FilesystemStudyStorage fs
                                LEFT JOIN Study s ON fs.StudyStorageGUID=s.StudyStorageGUID WHERE 1=1";
                if (!string.IsNullOrEmpty(patientid))
                { 
                  StrSql+=" AND s.PatientId ='"+ patientid +"'";
                }
                if (!string.IsNullOrEmpty(studyInstanceUid))
                {
                    StrSql += "AND s.StudyInstanceUid='" + studyInstanceUid + "'";
                }
                LogHelper.WriteInfoLog("GetStudyFolder获取文件夹："+StrSql);
                list = DBContext.ExecuteQuery<string>(StrSql).ToList();
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("GetStudyFolder", "异常信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return list;
        }

        public string GetRoot()
        {
            string StrSql = "select (f.FilesystemPath+s.PartitionFolder) as t_Path from Filesystem f ,ServerPartition s  ";

            int list = DBContext.ExecuteCommand(StrSql);
            return list.ToString();
        }

        public List<Study> GetStudy()
        {
            List<Study> list = new List<Study>();
            try
            {
                //string StrSql = "select top 300 * from Study ";
                //StrSql += "order by StudyDate desc";
                //add by bob 把ReferringPhysiciansName替换为检查方法
                string StrSql = @"WITH TEMPS AS (SELECT ROW_NUMBER() OVER(PARTITION BY StudyGUID ORDER BY SeriesNumber) AS ROWS,StudyGUID, Modality FROM [ImageServer].[dbo].[Series]) 
                               SELECT TOP 300 [GUID],[ServerPartitionGUID] ,[StudyStorageGUID],[PatientGUID] ,[SpecificCharacterSet]
                             ,[StudyInstanceUid],[PatientsName],[PatientId],[IssuerOfPatientId],[PatientsBirthDate],[PatientsAge],[PatientsSex]
                             ,[StudyDate],[StudyTime],[AccessionNumber],[StudyId],[StudyDescription],[NumberOfStudyRelatedSeries]
                             ,[NumberOfStudyRelatedInstances],[StudySizeInKB],[ResponsiblePerson],[ResponsibleOrganization]
                             ,[QueryXml],[QCStatusEnum],[QCOutput],[QCUpdateTimeUtc]
                             ,Se.Modality AS [ReferringPhysiciansName] FROM [ImageServer].[dbo].[Study] AS S
                             LEFT JOIN TEMPS AS Se ON S.GUID=Se.StudyGUID AND Se.ROWS=1 ORDER BY StudyDate DESC";
               list = DBContext.ExecuteQuery<Study>(StrSql).ToList();
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("GetStudy", "异常信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return list;
        }

        /// <summary>
        /// 获取病人的信息
        /// </summary>
        /// <param name="patientId"></param>
        /// <returns></returns>
        public List<Study> GetStudyByPatientId(string patientId, string studyInstanceUID)
        {
            List<Study> list = new List<Study>();
            try
            {
                string StrSql = "SELECT * FROM Study WHERE 1=1";
                if (!string.IsNullOrEmpty(patientId))
                {
                    StrSql += " AND PatientId='" + patientId + "'";
                }
                if (!string.IsNullOrEmpty(studyInstanceUID))
                {
                    StrSql += " AND StudyInstanceUid='" + studyInstanceUID + "'";
                }
                list = DBContext.ExecuteQuery<Study>(StrSql).ToList();
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("GetStudyByPatientId", "异常信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return list;
        }

        public List<Patient_requestDetail> GetPatient_requestDetail(string patientId, string HospitalId, string tB__SHname_txt, string D_SH_type_txt)
        {//AccessionNumber
            int D_SH_type_txt_int = Convert.ToInt32(D_SH_type_txt.ToString() == "" ? 0 : Convert.ToInt32(D_SH_type_txt.ToString()));
            string StrSql = @"SELECT ";
            StrSql += "RequestInfo.RequestId";
            StrSql += ",PatientInfo.PatientName";
            StrSql += ",PatientInfo.CPatientName";
            StrSql += ",PatientInfo.PatientAge";
            StrSql += ",PatientInfo.PatientSex";
            StrSql += ",ISNULL(RequestInfo.ReviewStatus,0) as ReviewStatus";
            StrSql += ",RequestInfo.AccessionNumber";
            StrSql += ",RequestInfo.DiagnoseDoctor";
            StrSql += ",ISNULL(RequestInfo.RequestJDSJ,'')as RequestJDSJ ";
            StrSql += "FROM [PatientInfo]  as PatientInfo ";
            StrSql += "inner join [RequestInfo] as RequestInfo ";
            StrSql += "on  PatientInfo.PatientId=RequestInfo.PatientId ";
            StrSql += "where RequestInfo.Diagnosticstate='Diagnose' AND RequestState=15 ";
            if (HospitalId.Trim() != "") { StrSql += "and  RequestInfo.HospitalId='" + HospitalId + "'"; }
            if (patientId.Trim() != "") { StrSql += "and  PatientInfo.PatientId='" + patientId + "'"; }
            if (D_SH_type_txt.Trim() != "") { StrSql += "and  RequestInfo.ReviewStatus='" + D_SH_type_txt_int + "'"; }
            if (tB__SHname_txt.Trim() != "") { StrSql += "and PatientInfo.PatientName like '%" + tB__SHname_txt + "%'  or PatientInfo.CPatientName like '%" + tB__SHname_txt + "%'"; }
            StrSql += " ORDER BY DiagnoseDate DESC ";
            List<Patient_requestDetail> list = DBContext.ExecuteQuery<Patient_requestDetail>(StrSql).ToList();
            return list;
        }

        public List<Study> GetSearchStudy(string name, DateTime dtStart, DateTime dtEnd, string patientId)
        {
            string StrSql = @"WITH TEMPS AS (SELECT ROW_NUMBER() OVER(PARTITION BY StudyGUID ORDER BY SeriesNumber) AS ROWS,StudyGUID, Modality FROM [ImageServer].[dbo].[Series]) 
                               SELECT S.[GUID],S.[ServerPartitionGUID] ,[StudyStorageGUID],[PatientGUID] ,[SpecificCharacterSet]
                             ,S.[StudyInstanceUid],[PatientsName],[PatientId],[IssuerOfPatientId],[PatientsBirthDate],[PatientsAge],[PatientsSex]
                             ,[StudyDate],[StudyTime],[AccessionNumber],[StudyId],[StudyDescription],[NumberOfStudyRelatedSeries]
                             ,[NumberOfStudyRelatedInstances],[StudySizeInKB],[ResponsiblePerson],[ResponsibleOrganization]
                             ,[QueryXml],[QCStatusEnum],[QCOutput],[QCUpdateTimeUtc]
                             ,Se.Modality AS [ReferringPhysiciansName] FROM [ImageServer].[dbo].[Study] AS S
                             LEFT JOIN [ImageServer].[dbo].[StudyStorage] SS ON S.StudyInstanceUid=SS.StudyInstanceUid
                             LEFT JOIN TEMPS AS Se ON S.GUID=Se.StudyGUID AND Se.ROWS=1 WHERE  S.StudySizeInKB IS NOT NULL AND SS.QueueStudyStateEnum='101' ";
            if (dtStart != Convert.ToDateTime("1900-01-01"))
            {
                StrSql += " AND  SS.InsertTime>='" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "'";
            }
            if (dtEnd != Convert.ToDateTime("1900-01-01"))
            {
                StrSql += " AND  SS.InsertTime<='" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "'";
            }
            if (name.Trim() != "")
            {
                StrSql += " AND  PatientsName LIKE '%" + name + "%'";
            }
            if (patientId.Trim() != "")
            {
                StrSql += " AND PatientId like '%" + patientId + "%'";
            }
            StrSql += "ORDER BY StudyDate DESC";
            LogHelper.WriteInfoLog("查询GetSearchStudy数据，SQL：" + StrSql);
            List<Study> list = DBContext.ExecuteQuery<Study>(StrSql).ToList();
            return list;
        }


        /// <summary>
        /// 同步数据请勿修改
        /// add by bob 2017-4-18
        /// </summary>
        /// <param name="strPatientId"></param>
        /// <param name="strAccessNumber"></param>
        /// <param name="strStudyInstanceUid"></param>
        /// <returns></returns>
        public List<Study> SearchStudyList (string strPatientId, string strAccessNumber,string strStudyInstanceUid)
        {
            List<Study> list = new List<Study>();
            StringBuilder strAdd = new StringBuilder();
            strAdd.Append(@"SELECT * FROM [ImageServer].[dbo].[Study] as Study WHERE 1=1");
            if (!string.IsNullOrEmpty(strPatientId))
            {
                strAdd.Append(" AND PatientId ='" + strPatientId + "'");
            }
            if (!string.IsNullOrEmpty(strAccessNumber))
            {
                strAdd.Append(" AND AccessionNumber ='" + strAccessNumber + "'");
            }
            if (!string.IsNullOrEmpty(strStudyInstanceUid))
            {
                strAdd.Append(" AND StudyInstanceUid ='" + strStudyInstanceUid + "'");
            }
            list = DBContext.ExecuteQuery<Study>(strAdd.ToString()).ToList();
            return list;
        }

        /// <summary>
        /// 手机app 接口
        /// </summary>
        /// <param name="name"></param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <param name="patientId"></param>
        /// <returns></returns>
        public List<Study> GetSearchStudyAPP(string name, DateTime dtStart, DateTime dtEnd, string patientId, string HospitalId)
        {
            string StrSql = @"SELECT *";
            StrSql += "FROM [ImageServer].[dbo].[Study] as Study ";
            StrSql += "WHERE  1=1 ";
            if (dtStart != Convert.ToDateTime("1900-01-01"))
            {
                StrSql += " AND  convert(datetime,StudyDate)>='" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "'";
            }
            if (dtEnd != Convert.ToDateTime("1900-01-01"))
            {
                StrSql += " AND  convert(datetime,StudyDate)<='" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "'";
            }
            if (name.Trim() != "")
            {
                StrSql += " AND  PatientsName like '%" + name + "%'";
            }
            if (patientId.Trim() != "")
            {
                StrSql += " AND PatientId like '%" + patientId + "%'";
            }
            if (HospitalId.Trim() != "")
            {
                StrSql += " AND AccessionNumber like '%." + HospitalId + "'";
            }
            StrSql += "order by StudyDate desc";
            List<Study> list = DBContext.ExecuteQuery<Study>(StrSql).ToList();
            return list;
        }



        #endregion
    }
}