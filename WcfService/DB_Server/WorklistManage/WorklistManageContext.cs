﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService.DB_Server.WorklistManage
{
    public class WorklistManageContext
    {
        WorklistManageDataContext db = new WorklistManageDataContext();

        #region worklist数据操作
        /// <summary>
        /// 添加数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel AddWorklistInfo(Worklist model)
        {
            ExcuteModel em = new ExcuteModel();
            try
            {
                Worklist info = db.Worklist.Where(t => t.study_instance_uid == model.study_instance_uid).FirstOrDefault();
                if (info != null)
                {
                    //em.Msg = "序列号已存在";
                    //em.Result = false;
                    info.study_instance_uid = model.study_instance_uid;
                    if (!string.IsNullOrEmpty(model.patient_id))
                    {
                        info.patient_id = model.patient_id;
                    }
                    if (!string.IsNullOrEmpty(model.patient_name))
                    {
                        info.patient_name = model.patient_name;
                    }
                    if (!string.IsNullOrEmpty(model.patient_sex))
                    {
                        info.patient_sex = model.patient_sex;
                    }
                    if (!string.IsNullOrEmpty(model.modality))
                    {
                        info.modality = model.modality;
                    }
                    em.Result = true;
                    em.Msg = "修改成功";
                }
                else
                {
                    model.sched_start_date = DateTime.Now.ToString("yyyyMMdd");
                    model.sched_start_time = DateTime.Now.ToString("HHmmss");
                    db.Worklist.InsertOnSubmit(model);
                    em.Msg = "保存成功";
                    em.Result = true;
                }
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                em.Msg = ex.Message;
                em.Result = false;
            }
            return em;
        }

        /// <summary>
        /// 获取数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<Worklist> GetWorklistInfo(Worklist model)
        {
            List<Worklist> list = new List<Worklist>();
            string StrSql = string.Empty;
            string Condition = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(model.study_instance_uid))
                {
                    Condition = @" AND  study_instance_uid='" + model.study_instance_uid + "'";
                }
                if (!string.IsNullOrEmpty(model.patient_id))
                {
                    Condition += @"AND patient_id='" + model.patient_id + "'";
                }
                if (!string.IsNullOrEmpty(model.patient_sex))
                {
                    Condition += @"AND patient_sex='" + model.patient_sex + "'";
                }
                if (!string.IsNullOrEmpty(model.station_aet))
                {
                    Condition += @"AND station_aet='" + model.station_aet + "'";
                }
                if (!string.IsNullOrEmpty(model.perfrmd_aet))
                {
                    Condition += @"AND perfrmd_aet='" + model.perfrmd_aet + "'";
                }
                if (!string.IsNullOrEmpty(model.req_proc_id))
                {
                    Condition += @"AND req_proc_id='" + model.req_proc_id + "'";
                }
                if (!string.IsNullOrEmpty(model.req_proc_desc))
                {
                    Condition += @"AND req_proc_desc='" + model.req_proc_desc + "'";
                }
                if (!string.IsNullOrEmpty(model.sched_proc_id))
                {
                    Condition += @"AND sched_proc_id='" + model.sched_proc_id + "'";
                }
                if (!string.IsNullOrEmpty(model.sched_proc_desc))
                {
                    Condition += @"AND sched_proc_desc='" + model.sched_proc_desc + "'";
                }
                if (!string.IsNullOrEmpty(model.accession_num))
                {
                    Condition += @"AND accession_num='" + model.accession_num + "'";
                }
                StrSql = @"SELECT * FROM Worklist WHERE 1=1 " + Condition;
                list = db.ExecuteQuery<Worklist>(StrSql).ToList();
            }
            catch (Exception ex)
            {
                string strex = ex.Message;
            }
            return list;
        }

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateWorklistInfo(Worklist model)
        {
            ExcuteModel em = new ExcuteModel();
            try
            {
                Worklist info = db.Worklist.Where(o => o.study_instance_uid == model.study_instance_uid).FirstOrDefault();
                if (info != null)
                {
                    #region  更新字段
                    info.study_instance_uid = model.study_instance_uid;
                    if (!string.IsNullOrEmpty(model.patient_id))
                    {
                        info.patient_id = model.patient_id;
                    }
                    if (!string.IsNullOrEmpty(model.patient_name))
                    {
                        info.patient_name = model.patient_name;
                    }
                    if (!string.IsNullOrEmpty(model.patient_birth))
                    {
                        info.patient_birth = model.patient_birth;
                    }
                    if (!string.IsNullOrEmpty(model.patient_sex))
                    {
                        info.patient_sex = model.patient_sex;
                    }
                    if (model.patient_size != null)
                    {
                        info.patient_size = model.patient_size;
                    }
                    if (model.patient_weight != null)
                    {
                        info.patient_weight = model.patient_weight;
                    }
                    if (!string.IsNullOrEmpty(model.patient_location))
                    {
                        info.patient_location = model.patient_location;
                    }
                    if (!string.IsNullOrEmpty(model.patient_state))
                    {
                        info.patient_state = model.patient_state;
                    }
                    if (!string.IsNullOrEmpty(model.patient_transport))
                    {
                        info.patient_transport = model.patient_transport;
                    }
                    if (!string.IsNullOrEmpty(model.medical_alert))
                    {
                        info.medical_alert = model.medical_alert;
                    }
                    if (!string.IsNullOrEmpty(model.contrast_allergy))
                    {
                        info.contrast_allergy = model.contrast_allergy;
                    }
                    if (!string.IsNullOrEmpty(model.occupation))
                    {
                        info.occupation = model.occupation;
                    }
                    if (model.pregnancy_status != null)
                    {
                        info.pregnancy_status = model.pregnancy_status;
                    }
                    if (!string.IsNullOrEmpty(model.modality))
                    {
                        info.modality = model.modality;
                    }
                    if (!string.IsNullOrEmpty(model.sched_start_date))
                    {
                        info.sched_start_date = model.sched_start_date;
                    }
                    if (!string.IsNullOrEmpty(model.sched_start_time))
                    {
                        info.sched_start_time = model.sched_start_time;
                    }
                    if (!string.IsNullOrEmpty(model.perform_physician))
                    {
                        info.perform_physician = model.perform_physician;
                    }
                    if (!string.IsNullOrEmpty(model.request_physician))
                    {
                        info.request_physician = model.request_physician;
                    }
                    if (!string.IsNullOrEmpty(model.refer_physician))
                    {
                        info.refer_physician = model.refer_physician;
                    }
                    if (!string.IsNullOrEmpty(model.accession_num))
                    {
                        info.accession_num = model.accession_num;
                    }
                    if (!string.IsNullOrEmpty(model.sched_proc_id))
                    {
                        info.sched_proc_id = model.sched_proc_id;
                    }
                    if (!string.IsNullOrEmpty(model.sched_proc_desc))
                    {
                        info.sched_proc_desc = model.sched_proc_desc;
                    }
                    if (!string.IsNullOrEmpty(model.sched_proc_loc))
                    {
                        info.sched_proc_loc = model.sched_proc_loc;
                    }
                    if (!string.IsNullOrEmpty(model.req_proc_id))
                    {
                        info.req_proc_id = model.req_proc_id;
                    }
                    if (!string.IsNullOrEmpty(model.req_proc_desc))
                    {
                        info.req_proc_desc = model.req_proc_desc;
                    }
                    if (!string.IsNullOrEmpty(model.req_proc_location))
                    {
                        info.req_proc_location = model.req_proc_location;
                    }
                    if (!string.IsNullOrEmpty(model.req_proc_priority))
                    {
                        info.req_proc_priority = model.req_proc_priority;
                    }
                    if (!string.IsNullOrEmpty(model.station_name))
                    {
                        info.station_name = model.station_name;
                    }
                    if (!string.IsNullOrEmpty(model.station_aet))
                    {
                        info.station_aet = model.station_aet;
                    }
                    if (!string.IsNullOrEmpty(model.pre_medication))
                    {
                        info.pre_medication = model.pre_medication;
                    }
                    if (!string.IsNullOrEmpty(model.admission_id))
                    {
                        info.admission_id = model.admission_id;
                    }
                    if (!string.IsNullOrEmpty(model.special_needs))
                    {
                        info.special_needs = model.special_needs;
                    }
                    if (!string.IsNullOrEmpty(model.confidentiality))
                    {
                        info.confidentiality = model.confidentiality;
                    }
                    if (!string.IsNullOrEmpty(model.perfrmd_aet))
                    {
                        info.perfrmd_aet = model.perfrmd_aet;
                    }
                    if (!string.IsNullOrEmpty(model.perfrmd_start_date))
                    {
                        info.perfrmd_start_date = model.perfrmd_start_date;
                    }
                    if (!string.IsNullOrEmpty(model.perfrmd_start_time))
                    {
                        info.perfrmd_start_time = model.perfrmd_start_time;
                    }
                    if (!string.IsNullOrEmpty(model.perfrmd_end_date))
                    {
                        info.perfrmd_end_date = model.perfrmd_end_date;
                    }
                    if (!string.IsNullOrEmpty(model.perfrmd_end_time))
                    {
                        info.perfrmd_end_time = model.perfrmd_end_time;
                    }
                    if (!string.IsNullOrEmpty(model.perfrmd_status))
                    {
                        info.perfrmd_status = model.perfrmd_status;
                    }
                    if (!string.IsNullOrEmpty(model.perfrmd_proc_id))
                    {
                        info.perfrmd_proc_id = model.perfrmd_proc_id;
                    }
                    if (!string.IsNullOrEmpty(model.mpps_sop_uid))
                    {
                        info.mpps_sop_uid = model.mpps_sop_uid;
                    }
                    if (!string.IsNullOrEmpty(model.character_set))
                    {
                        info.character_set = model.character_set;
                    }
                    if (!string.IsNullOrEmpty(model.service))
                    {
                        info.service = model.service;
                    }
                    if (!string.IsNullOrEmpty(model.residence))
                    {
                        info.residence = model.residence;
                    }
                    if (!string.IsNullOrEmpty(model.code_value))
                    {
                        info.code_value = model.code_value;
                    }
                    if (!string.IsNullOrEmpty(model.code_designator))
                    {
                        info.code_designator = model.code_designator;
                    }
                    if (!string.IsNullOrEmpty(model.code_meaning))
                    {
                        info.code_meaning = model.code_meaning;
                    }
                    #endregion
                    em.Result = true;
                }
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                em.Result = false;
                em.Msg = ex.Message;
            }
            return em;
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel DeleteWorklistInfo(string param)
        {
            ExcuteModel em = new ExcuteModel();
            try
            {
                Worklist info = db.Worklist.First(t => t.study_instance_uid == param);
                db.Worklist.DeleteOnSubmit(info);
                db.SubmitChanges();
                em.Result = true;
            }
            catch (Exception ex)
            {
                em.Result = false;
                em.Msg = ex.Message;
            }
            return em;
        }
        #endregion
    }
}