﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Transactions;
using System.Web;
using WcfService.DB_Server.SystemManage;
using WcfService.Models;

namespace WcfService.DB_Server
{
    public class RemoteDianoseContext
    {
        private string connString = ConfigurationManager.ConnectionStrings["InsideRISConnectionString"].ToString();
        RemoteDianoseDataContext db = new RemoteDianoseDataContext();
        SystemManageDataContext DbDataContext = new SystemManageDataContext();
        //ImageServerContext ImageServerContext = new ImageServerContext();
        //ImageServerDataContext DBContext = new ImageServerDataContext();

        #region    班次表设置 SchedulShift
        public List<SchedulTempListJoinSys_Users> GetSchedulRecord_query(SchedulRecord model, string Currentdatetime, int Stype)
        {
            List<SchedulTempListJoinSys_Users> List = new List<SchedulTempListJoinSys_Users>();
            try
            {
                string StrSql = @"SELECT  [SchedulTemplateId],Sys_Users.[UserId], [SchedulingID] ,[Monday] ,[Tuesday] , ";
                StrSql += "[Wednesday] , [Thursday] ,[Friday] , [Saturday] ,[Sunday], ";
                StrSql += "(case when Type=1  then '第一周'  when Type=2  then '第二周'  when Type=3  then '第三周'  else '第四周' end) as Type, ";
                StrSql += "[CreatePeople], [CreateTime] ,[UpdatePeople] ,[UpdateTime] ,[Standby] ,[Standby1], Sys_Users.Name ";
                StrSql += " from [RemoteDianose].[dbo].[SchedulRecord] as SchedulTempList inner join [PatientVisit].[dbo].[Sys_Users] as Sys_Users ";
                StrSql += "on SchedulTempList.UserId=Sys_Users.UserId  where 1=1 ";
                if (!string.IsNullOrEmpty(model.Standby.ToString()))
                {
                    StrSql += "and  Standby='" + model.Standby.ToString() + "' ";
                }
                else
                {
                    StrSql += "and  Standby=0 ";
                }

                if (model != null)
                {
                    if (!string.IsNullOrEmpty(model.CreateTime.ToString()))
                    {
                        StrSql += " AND SchedulTempList.CreateTime ='" + model.CreateTime.ToString() + "' ";
                    }
                    if (!string.IsNullOrEmpty(model.UserId))
                    {
                        StrSql += " AND SchedulTempList.UserId ='" + model.UserId.ToString() + "' ";
                    }
                    if (!string.IsNullOrEmpty(model.SchedulTemplateId))
                    {
                        StrSql += " AND SchedulTempList.SchedulTemplateId ='" + model.SchedulTemplateId.ToString() + "' ";
                    }
                    if (!string.IsNullOrEmpty(model.Type))
                    {
                        if (Convert.ToInt32(model.Type) != 0)
                        {
                            StrSql += " AND SchedulTempList.Type ='" + model.Type.ToString() + "' ";
                        }

                    }

                    if (!string.IsNullOrEmpty(Stype.ToString()))
                    {

                        if (Stype == 1)
                        {
                            DateTime dtStart = Convert.ToDateTime(Convert.ToDateTime(Currentdatetime).AddDays(-7).ToString("yyyy/MM/dd"));
                            DateTime dtEnd = Convert.ToDateTime(Currentdatetime);

                            if (dtStart != Convert.ToDateTime("1900-01-01"))
                            {
                                StrSql += " AND  convert(datetime,CreateTime)>='" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                            }
                            if (dtEnd != Convert.ToDateTime("1900-01-01"))
                            {
                                StrSql += " AND  convert(datetime,CreateTime)<='" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                            }
                        }
                        else if (Stype == 2)
                        {
                            DateTime dtStart = Convert.ToDateTime(Currentdatetime);
                            DateTime dtEnd = Convert.ToDateTime(Convert.ToDateTime(Currentdatetime).AddDays(-7).ToString("yyyy/MM/dd"));
                            if (dtStart != Convert.ToDateTime("1900-01-01"))
                            {
                                StrSql += " AND  convert(datetime,CreateTime)>='" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                            }
                            if (dtEnd != Convert.ToDateTime("1900-01-01"))
                            {
                                StrSql += " AND  convert(datetime,CreateTime)<='" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(Currentdatetime))
                            {
                                DateTime dt = Convert.ToDateTime(Currentdatetime);
                                string Year = dt.Year.ToString();//2005
                                string Month = dt.Month.ToString();//11
                                int day = dt.AddDays(1 - dt.Day).AddMonths(1).AddDays(-1).Day;
                                string NewTimebegin = Year + "-" + Month + "-1";
                                string NewTimeEnd = Year + "-" + Month + "-" + day;
                                DateTime dtStart = Convert.ToDateTime(NewTimebegin);
                                DateTime dtEnd = Convert.ToDateTime(NewTimeEnd);
                                if (dtStart != Convert.ToDateTime("1900-01-01"))
                                {
                                    StrSql += " AND  convert(datetime,CreateTime)>='" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                                }
                                if (dtEnd != Convert.ToDateTime("1900-01-01"))
                                {
                                    StrSql += " AND  convert(datetime,CreateTime)<='" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                                }
                            }
                        }

                    }
                }
                List = db.ExecuteQuery<SchedulTempListJoinSys_Users>(StrSql).ToList();
            }
            catch (Exception exp)
            {
            }
            return List;
        }

        public List<SchedulTempListJoinSys_Users> GetSchedulTempList_query(SchedulTempList model, string Currentdatetime, int Stype)
        {
            List<SchedulTempListJoinSys_Users> List = new List<SchedulTempListJoinSys_Users>();
            try
            {
                db = new RemoteDianoseDataContext();
                string StrSql = @"SELECT  ";
                StrSql += " [SchedulTemplateId],";
                StrSql += " Sys_Users.[UserId], ";
                StrSql += " [STemplateName], ";
                StrSql += " [SchedulingID] , ";
                StrSql += " [Monday] , ";
                StrSql += " [Tuesday] , ";
                StrSql += " [Wednesday] , ";
                StrSql += " [Thursday] , ";
                StrSql += " [Friday] , ";
                StrSql += " [Saturday] , ";
                StrSql += " [Sunday] , ";
                StrSql += " (case when Type=1  then '第一周'  when Type=2  then '第二周'  when Type=3  then '第三周'  else '第四周' end) as Type  , ";
                StrSql += " [CreatePeople], ";
                StrSql += " [CreateTime] , ";
                StrSql += " [UpdatePeople] , ";
                StrSql += " [UpdateTime] , ";
                StrSql += " [Standby] , ";
                StrSql += " [Standby1], ";
                StrSql += " Sys_Users.Name ";
                StrSql += " from [RemoteDianose].[dbo].[SchedulTempList] as SchedulTempList ";
                StrSql += " inner join [PatientVisit].[dbo].[Sys_Users] as Sys_Users  ";
                StrSql += " on SchedulTempList.UserId=Sys_Users.UserId  ";
                StrSql += "where 1=1 ";
                if (!string.IsNullOrEmpty(model.Standby.ToString()))
                {
                    StrSql += "and  Standby='" + model.Standby.ToString() + "' ";
                }
                else
                {
                    StrSql += "and  Standby=0 ";
                }

                if (model != null)
                {
                    if (!string.IsNullOrEmpty(model.CreateTime.ToString()))
                    {
                        StrSql += " AND SchedulTempList.CreateTime ='" + model.CreateTime.ToString() + "' ";
                    }
                    if (!string.IsNullOrEmpty(model.UserId))
                    {
                        StrSql += " AND SchedulTempList.UserId ='" + model.UserId.ToString() + "' ";
                    }
                    if (!string.IsNullOrEmpty(model.SchedulTemplateId))
                    {
                        StrSql += " AND SchedulTempList.SchedulTemplateId ='" + model.SchedulTemplateId.ToString() + "' ";
                    }
                    if (!string.IsNullOrEmpty(model.Type))
                    {
                        StrSql += " AND SchedulTempList.Type ='" + model.Type.ToString() + "' ";
                    }

                    if (!string.IsNullOrEmpty(Stype.ToString()))
                    {

                        if (Stype == 1)
                        {
                            DateTime dtStart = Convert.ToDateTime(Convert.ToDateTime(Currentdatetime).AddDays(-7).ToString("yyyy/MM/dd"));
                            DateTime dtEnd = Convert.ToDateTime(Currentdatetime);

                            if (dtStart != Convert.ToDateTime("1900-01-01"))
                            {
                                StrSql += " AND  convert(datetime,CreateTime)>='" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                            }
                            if (dtEnd != Convert.ToDateTime("1900-01-01"))
                            {
                                StrSql += " AND  convert(datetime,CreateTime)<='" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                            }
                        }
                        else if (Stype == 2)
                        {
                            DateTime dtStart = Convert.ToDateTime(Currentdatetime);
                            DateTime dtEnd = Convert.ToDateTime(Convert.ToDateTime(Currentdatetime).AddDays(-7).ToString("yyyy/MM/dd"));
                            if (dtStart != Convert.ToDateTime("1900-01-01"))
                            {
                                StrSql += " AND  convert(datetime,CreateTime)>='" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                            }
                            if (dtEnd != Convert.ToDateTime("1900-01-01"))
                            {
                                StrSql += " AND  convert(datetime,CreateTime)<='" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(Currentdatetime))
                            {
                                DateTime dt = Convert.ToDateTime(Currentdatetime);
                                string Year = dt.Year.ToString();//2005
                                string Month = dt.Month.ToString();//11
                                int day = dt.AddDays(1 - dt.Day).AddMonths(1).AddDays(-1).Day;
                                string NewTimebegin = Year + "-" + Month + "-1";
                                string NewTimeEnd = Year + "-" + Month + "-" + day;
                                DateTime dtStart = Convert.ToDateTime(NewTimebegin);
                                DateTime dtEnd = Convert.ToDateTime(NewTimeEnd);
                                if (dtStart != Convert.ToDateTime("1900-01-01"))
                                {
                                    StrSql += " AND  convert(datetime,CreateTime)>='" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                                }
                                if (dtEnd != Convert.ToDateTime("1900-01-01"))
                                {
                                    StrSql += " AND  convert(datetime,CreateTime)<='" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                                }
                            }
                        }

                    }
                }
                List = db.ExecuteQuery<SchedulTempListJoinSys_Users>(StrSql).ToList();
            }
            catch (Exception exp)
            {
            }
            return List;
        }

        public ExcuteModel UpdateSchedulTempList(SchedulTempList model)
        {
            List<SchedulTempList> loglist = new List<SchedulTempList>();
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                loglist = db.SchedulTempList.Where(t => t.SchedulingID == model.SchedulingID).ToList();
                foreach (var item in loglist)
                {
                    SchedulTempList _modelExist = db.SchedulTempList.FirstOrDefault(t => t.SchedulTemplateId == item.SchedulTemplateId);
                    if (_modelExist != null)
                    {
                        _modelExist.Standby = model.Standby;
                        _modelExist.Standby1 = model.Standby1;
                        _modelExist.CreatePeople = model.CreatePeople;
                        _modelExist.CreateTime = model.CreateTime;
                        _modelExist.UpdatePeople = model.UpdatePeople;
                        _modelExist.UpdateTime = model.UpdateTime;
                        RetModel.RetValue = _modelExist.SchedulingID.ToString();
                    }
                    else
                    {
                        RetModel.RetValue = model.SchedulTemplateId.ToString();
                        db.SchedulTempList.InsertOnSubmit(model);
                    }
                    db.SubmitChanges();
                    RetModel.Result = true;
                }
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }

            return RetModel;
        }

        #endregion

        #region    班次表设置 SchedulShift

        public ExcuteModel SaveSchedulShift(SchedulShift model)
        {
            List<SchedulShift> loglist = new List<SchedulShift>();
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                SchedulShift _modelExist = db.SchedulShift.FirstOrDefault(t => t.SchedulingID == model.SchedulingID);
                if (_modelExist != null)
                {
                    _modelExist.SchedulingID = model.SchedulingID;
                    _modelExist.JobsID = model.JobsID;
                    _modelExist.SName = model.SName;
                    _modelExist.SUserType = model.SUserType;
                    _modelExist.STypeID = model.STypeID;
                    _modelExist.ShiftType = model.ShiftType;

                    _modelExist.DefineCategory = model.DefineCategory;
                    _modelExist.SchedulBeginTime = model.SchedulBeginTime;
                    _modelExist.SchedulEndTime = model.SchedulEndTime;
                    _modelExist.Note = model.Note;
                    _modelExist.Standby = model.Standby;
                    _modelExist.Standby1 = model.Standby1;
                    _modelExist.CreatePeople = model.CreatePeople;
                    _modelExist.CreateTime = model.CreateTime;
                    _modelExist.UpdatePeople = model.UpdatePeople;
                    _modelExist.UpdateTime = model.UpdateTime;
                    RetModel.RetValue = _modelExist.SchedulingID.ToString();
                }
                else
                {
                    RetModel.RetValue = model.SchedulingID.ToString();
                    db.SchedulShift.InsertOnSubmit(model);
                }
                db.SubmitChanges();
                RetModel.Result = true;
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }

            return RetModel;
        }


        public List<SchedulShift> GetSchedulShiftList(SchedulShift model)
        {
            List<SchedulShift> List = new List<SchedulShift>();
            try
            {
                db = new RemoteDianoseDataContext();

                string StrSql = @"SELECT  ";
                StrSql += "  [SchedulingID], ";
                StrSql += "  [HospitalID], ";
                StrSql += "  [JobsID], ";
                StrSql += "  [SName], ";
                StrSql += "  (case when ShiftType=0  then '工作'  when ShiftType=1  then '非工作' else '其他' end) as ShiftType , ";
                StrSql += "  [SUserType], ";
                StrSql += "  [STypeID], ";
                StrSql += "  [DefineCategory], ";
                StrSql += "  [SchedulBeginTime], ";
                StrSql += "  [SchedulEndTime], ";
                StrSql += "  [Schedulcolor], ";
                StrSql += "  [CreatePeople], ";
                StrSql += "  [CreateTime], ";
                StrSql += "  [UpdatePeople], ";
                StrSql += "  [UpdateTime], ";
                StrSql += "  [Standby], ";
                StrSql += " cast(floor(datediff(minute,SchedulBeginTime,SchedulEndTime) / 1440) as varchar)+'天'+ ";
                StrSql += "  cast(floor((datediff(minute,SchedulBeginTime,SchedulEndTime) % 1440)/60) as varchar)+'小时'+  ";
                StrSql += " cast(((datediff(minute,SchedulBeginTime,SchedulEndTime))- ";
                StrSql += "  (floor(datediff(minute,SchedulBeginTime,SchedulEndTime) / 1440) *1440)- ";
                StrSql += " (floor((datediff(minute,SchedulBeginTime,SchedulEndTime) % 1440)/60)*60)) as varchar)+'分' as[Standby1]";
                StrSql += " from [RemoteDianose].[dbo].[SchedulShift] ";
                StrSql += "where 1=1 ";
                StrSql += " AND Standby =0 ";
                if (model != null)
                {
                    if (!string.IsNullOrEmpty(model.HospitalID))
                    {
                        StrSql += " AND HospitalID ='" + model.HospitalID.ToString() + "' ";
                    }
                    if (!string.IsNullOrEmpty(model.SchedulingID))
                    {
                        StrSql += " AND SchedulingID ='" + model.SchedulingID.ToString() + "' ";
                    }
                    if (!string.IsNullOrEmpty(model.JobsID))
                    {
                        StrSql += " AND JobsID ='" + model.JobsID.ToString() + "' ";
                    }
                    if (!string.IsNullOrEmpty(model.STypeID))
                    {
                        StrSql += " AND STypeID ='" + model.STypeID.ToString() + "' ";
                    }
                    if (!string.IsNullOrEmpty(model.SUserType))
                    {
                        StrSql += " AND SUserType ='" + model.SUserType.ToString() + "' ";
                    }
                    if (!string.IsNullOrEmpty(model.SName))
                    {
                        StrSql += " AND SName ='" + model.SName.ToString() + "' ";
                    }
                    if (!string.IsNullOrEmpty(model.DefineCategory))
                    {
                        StrSql += " AND DefineCategory ='" + model.DefineCategory.ToString() + "' ";
                    }

                }
                List = db.ExecuteQuery<SchedulShift>(StrSql).ToList();
            }
            catch (Exception exp)
            {
            }
            return List;
        }

        #endregion

        #region 写报告给医生余额转账
        public ExcuteModel SaveSettleAccountsMain(SettleAccountsMain model)
        {
            List<SettleAccountsMain> loglist = new List<SettleAccountsMain>();
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                SettleAccountsMain _modelExist = db.SettleAccountsMain.FirstOrDefault(t => t.SNMId == model.SNMId);
                if (_modelExist != null)
                {
                    _modelExist.SNMId = model.SNMId;
                    _modelExist.HospitalID = model.HospitalID;
                    _modelExist.UserId = model.UserId;
                    _modelExist.RequestId = model.RequestId;
                    _modelExist.ServiceId = model.ServiceId;
                    _modelExist.Diagnosisdoctor = model.Diagnosisdoctor;
                    _modelExist.DiagnosisTime = model.DiagnosisTime;
                    _modelExist.ApplyDoctor = model.ApplyDoctor;
                    _modelExist.ApplyTime = model.ApplyTime;
                    _modelExist.ApplyTime = model.ApplyTime;
                    _modelExist.CreatePeople = model.CreatePeople;
                    _modelExist.CreateTime = model.CreateTime;
                    _modelExist.UpdatePeople = model.UpdatePeople;
                    _modelExist.UpdateTime = model.UpdateTime;
                    RetModel.RetValue = _modelExist.SNMId.ToString();
                }
                else
                {
                    RetModel.RetValue = model.SNMId.ToString();
                    db.SettleAccountsMain.InsertOnSubmit(model);
                }
                db.SubmitChanges();
                RetModel.Result = true;
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        /// <summary>
        /// 获取结算主表信息
        /// </summary>
        /// <param name="RequestID"></param>
        /// <returns></returns>
        public List<SettleAccountsMain> GetSettleAccountsMain(string RequestID)
        {
            string strName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            List<SettleAccountsMain> List = new List<SettleAccountsMain>();
            try
            {
                List = db.SettleAccountsMain.Where(u => u.RequestId == RequestID).ToList();
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog(strName, "异常信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return List;
        }

        public ExcuteModel SaveSettleAccounts(SettleAccounts model)
        {
            List<SettleAccounts> loglist = new List<SettleAccounts>();
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                SettleAccounts _modelExist = db.SettleAccounts.FirstOrDefault(t => t.SN == model.SN);
                if (_modelExist != null)
                {
                    _modelExist.SNMId = model.SNMId;
                    _modelExist.HospitalID = model.HospitalID;
                    _modelExist.UserId = model.UserId;
                    _modelExist.RequestId = model.RequestId;
                    _modelExist.ServiceId = model.ServiceId;
                    _modelExist.AccountID = model.AccountID;
                    _modelExist.Type = model.Type;
                    _modelExist.ShareMoney = model.ShareMoney;
                    _modelExist.AccountBalance = model.AccountBalance;
                    _modelExist.State = model.State;
                    _modelExist.CheckValue = model.CheckValue;
                    _modelExist.CreatePeople = model.CreatePeople;
                    _modelExist.CreateTime = model.CreateTime;
                    _modelExist.UpdatePeople = model.UpdatePeople;
                    _modelExist.UpdateTime = model.UpdateTime;
                    RetModel.RetValue = _modelExist.SNMId.ToString();
                }
                else
                {
                    RetModel.RetValue = model.SN.ToString();
                    db.SettleAccounts.InsertOnSubmit(model);
                }
                db.SubmitChanges();
                RetModel.Result = true;
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        public ExcuteModel UpdateCustSettleAccountsState(SettleAccounts model)
        {
            List<SettleAccounts> loglist = new List<SettleAccounts>();
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    CustAccount CustAccountModel = db.CustAccount.FirstOrDefault(u => u.AccountID == model.AccountID);
                    if (CustAccountModel != null)
                    {
                        try
                        {
                            if (CustAccountModel.Balance != null && model.ShareMoney != null)
                            {
                                CustAccountModel.Balance = CustAccountModel.Balance + model.ShareMoney;
                            }
                            if (CustAccountModel.AmountMonery != null && model.ShareMoney != null)
                            {
                                CustAccountModel.AmountMonery = CustAccountModel.AmountMonery + model.ShareMoney;
                            }
                            db.SubmitChanges();
                            RetModel.Result = true;
                        }
                        catch (Exception)
                        {
                            //Decimal monery_Balance = Convert.ToDecimal(CustAccount_model_List.Balance) + Convert.ToDecimal(model.ShareMoney);
                            //CustAccount_model_List.Balance = monery_Balance;
                            //Decimal monery = Convert.ToDecimal(CustAccount_model_List.AmountMonery) + Convert.ToDecimal(model.ShareMoney);
                            //CustAccount_model_List.AmountMonery = monery;
                        }
                    }
                    else
                    {
                        RetModel.Msg = "提现失败，资金回滚更新失败";
                        RetModel.Result = false;
                    }
                    if (RetModel.Result == true)
                    {
                        SettleAccounts SettleAccounts_model = db.SettleAccounts.FirstOrDefault(u => u.SN == model.SN);
                        if (SettleAccounts_model != null)
                        {
                            try
                            {
                                CustAccount CA_Model = db.CustAccount.FirstOrDefault(u => u.AccountID == model.AccountID);
                                if (CA_Model != null)
                                {
                                    try
                                    {
                                        if (model.AccountBalance != null && SettleAccounts_model.ShareMoney != null)
                                        {
                                            SettleAccounts_model.AccountBalance = model.AccountBalance + SettleAccounts_model.ShareMoney;
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        //Decimal monery = Convert.ToDecimal(CA_Model.Balance) + Convert.ToDecimal(SettleAccounts_model.ShareMoney);
                                        //SettleAccounts_model.AccountBalance = monery;
                                    }
                                    SettleAccounts_model.State = "2";
                                    SettleAccounts_model.UpdateTime = DateTime.Now;
                                    SettleAccounts_model.UpdatePeople = model.UpdatePeople;
                                }
                                else
                                {
                                    RetModel.Msg = "不存在当前账户";
                                    RetModel.Result = false;
                                }
                            }
                            catch (Exception)
                            {
                                RetModel.Msg = "提现失败，资金回滚更新失败";
                                RetModel.Result = false;
                            }
                        }
                        else
                        {
                            RetModel.Msg = "没有找到对应的缴费记录";
                            RetModel.Result = false;
                        }
                        db.SubmitChanges();
                        RetModel.Result = true;
                        scope.Complete();
                        return RetModel;
                    }
                    else
                    {
                        RetModel.Result = false;
                    }
                }
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        public List<SettleAccountsList> GetSettleAccountsList(SettleAccountsList model)
        {
            List<SettleAccountsList> List = new List<SettleAccountsList>();
            try
            {
                try
                {
                    db = new RemoteDianoseDataContext();
                    string StrSql = @"SELECT * ";
                    StrSql += " from [RemoteDianose].[dbo].[SettleAccountsMain] AS Main";
                    StrSql += " inner join [RemoteDianose].[dbo].[SettleAccounts] as Sub";
                    StrSql += " on Main.SNMId==Sub.SNMId";
                    StrSql += " where 1=1 ";
                    StrSql += " and Standby =0 ";
                    List = db.ExecuteQuery<SettleAccountsList>(StrSql).ToList();
                }
                catch (Exception)
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
            return List;
        }

        /// <summary>
        /// 结算平分
        /// </summary>
        /// <param name="paramRequestInfo"></param>
        /// <param name="paramUsers"></param>
        /// <returns></returns>
        public ExcuteModel SettlementBisection(RequestInfo paramRequestInfo, string paramUserID)
        {
            string strName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ExcuteModel em = new ExcuteModel();
            try
            {
                var paramUsers = DbDataContext.Sys_Users.FirstOrDefault(t => t.UserId == paramUserID);
                if (paramUsers == null)
                {
                    em.Result = false;
                    em.Msg = "专家用户不存在";
                    LogHelper.WriteInfoLog(strName + "--专家用户不存在");
                    return em;
                }
                //根据requestInfo表中的服务项目，加载相关信息
                Sys_DiagnoseService diagnoseServiceModel = DbDataContext.Sys_DiagnoseService.FirstOrDefault(t => t.ServiceId == paramRequestInfo.ServeiceType);
                if (diagnoseServiceModel == null)
                {
                    em.Result = false;
                    em.Msg = "服务项目未配置或服务金额不大于0";
                    LogHelper.WriteInfoLog(strName + "--服务项目未配置");
                    return em;
                }
                if (string.IsNullOrEmpty(diagnoseServiceModel.ConfigurationFormula) || diagnoseServiceModel.ServiceCost <= 0)
                {
                    em.Result = false;
                    em.Msg = "服务项目未配置或服务金额不大于0";
                    LogHelper.WriteInfoLog(strName + "--服务项目未配置");
                    return em;
                }
                SettleAccountsMain SAmodel = new SettleAccountsMain()
                {
                    SNMId = System.DateTime.Now.ToString("yyyyMMddHHmmss") + Guid.NewGuid().ToString().Substring(0, 8).ToString(),
                    UserId = paramUsers.UserId,
                    HospitalID = paramUsers.Dept,
                    RequestId = paramRequestInfo.RequestId,
                    ServiceId = paramRequestInfo.ServeiceType,
                    ApplyDoctor = paramRequestInfo.RequestDoctor,
                    ApplyTime = paramRequestInfo.RequestDate,
                    Diagnosisdoctor = paramRequestInfo.DiagnoseDoctor,
                    DiagnosisTime = paramRequestInfo.DiagnoseDate,
                    CreatePeople = paramUsers.UserName,
                    CreateTime = DateTime.Now,
                    UpdatePeople = paramUsers.UserName,
                    UpdateTime = DateTime.Now
                };
                var RESULT = SaveSettleAccountsMain(SAmodel);
                if (RESULT.Result == true)
                {
                    em.Result = true;
                    var Cresult = Newtonsoft.Json.JsonConvert.DeserializeObject<DistributionPattern>(diagnoseServiceModel.ConfigurationFormula.ToString());
                    Dictionary<string, string> SettlementFormula = new Dictionary<string, string>();
                    SettlementFormula.Add(SettlementType.ApplyDoctor.ToString(), Cresult.ApplyDoctor);
                    SettlementFormula.Add(SettlementType.Platform.ToString(), Cresult.Platform);
                    SettlementFormula.Add(SettlementType.Channel.ToString(), Cresult.Channel);
                    SettlementFormula.Add(SettlementType.Experts.ToString(), Cresult.Experts);

                    foreach (var item in SettlementFormula)
                    {
                        string ShareMoney_ = "0.00001";
                        Double ShareMoney_D = Convert.ToDouble(item.Value);
                        decimal ShareMoney = Convert.ToDecimal(ShareMoney_);
                        string Userid = "";
                        string Type_Service = "Platform";
                        switch (item.Key.ToString())
                        {
                            case "ApplyDoctor":
                                Type_Service = "6";
                                if (!string.IsNullOrEmpty(paramRequestInfo.RequestDoctor))
                                {
                                    Sys_Users sysUserRequestDoctor = DbDataContext.Sys_Users.FirstOrDefault(t => t.UserName == paramRequestInfo.RequestDoctor || t.Mobile == paramRequestInfo.RequestDoctor);
                                    if (sysUserRequestDoctor != null)
                                    {
                                        Userid = sysUserRequestDoctor.UserId;
                                        ShareMoney = Convert.ToDecimal(diagnoseServiceModel.ServiceCost) * Convert.ToDecimal(ShareMoney_D.ToString("0.00000")) / 100;
                                    }
                                    else
                                    {
                                        em.Msg = strName + "--1未找到申请医生";
                                    }
                                }
                                else
                                {
                                    em.Msg = strName + "--2未找到申请医生";
                                }
                                break;
                            case "Platform":
                                Type_Service = "9";
                                Userid = "20170101000000XR";
                                ShareMoney = Convert.ToDecimal(diagnoseServiceModel.ServiceCost) * Convert.ToDecimal(ShareMoney_D.ToString("0.00000")) / 100;
                                break;
                            case "Experts":
                                Type_Service = "7";
                                if (!string.IsNullOrEmpty(paramRequestInfo.DiagnoseDoctor))
                                {
                                    Sys_Users sysUserDiagnose = DbDataContext.Sys_Users.FirstOrDefault(t => t.UserName == paramRequestInfo.DiagnoseDoctor || t.Mobile == paramRequestInfo.DiagnoseDoctor);
                                    if (sysUserDiagnose != null)
                                    {
                                        Userid = sysUserDiagnose.UserId;
                                        ShareMoney = Convert.ToDecimal(diagnoseServiceModel.ServiceCost) * Convert.ToDecimal(ShareMoney_D.ToString("0.00000")) / 100;
                                    }
                                    else
                                    {
                                        em.Msg = strName + "--1未找到专家";
                                    }
                                }
                                else
                                {
                                    em.Msg = strName + "--2未找到专家";
                                }
                                break;
                            default:
                                //Type_Service = "8";
                                //Sys_Users sysUserChnnel = DbDataContext.Sys_Users.FirstOrDefault(t => t.UserName == paramRequestInfo.RequestDoctor || t.Mobile == paramRequestInfo.RequestDoctor);
                                //if (sysUserChnnel != null)
                                //{
                                //    Userid = sysUserChnnel.UserId;
                                //    ShareMoney = Convert.ToDecimal(diagnoseServiceModel.ServiceCost) * Convert.ToDecimal(ShareMoney_D.ToString("0.00")) / 100;
                                //}
                                break;
                        }
                        if (!string.IsNullOrEmpty(Userid))
                        {
                            CustAccount CAModel = GetCustAccountList(new CustAccount { UserId = Userid }).FirstOrDefault();
                            if (CAModel != null)
                            {
                                SettleAccounts SLModel = new SettleAccounts()
                                {
                                    SN = System.DateTime.Now.ToString("yyyyMMddHHmmss") + Guid.NewGuid().ToString().Substring(0, 8).ToString(),
                                    SNMId = RESULT.RetValue,
                                    UserId = Userid,
                                    AccountID = CAModel.AccountID,
                                    RequestId = paramRequestInfo.RequestId,
                                    HospitalID = paramUsers.Dept.ToString(),
                                    ServiceId = paramRequestInfo.ServeiceType,
                                    ShareMoney = ShareMoney,
                                    AccountBalance = CAModel.Balance,
                                    State = "1",
                                    Type = Type_Service,
                                    CreatePeople = paramUsers.UserName,
                                    CreateTime = DateTime.Now,
                                    UpdatePeople = paramUsers.UserName,
                                    UpdateTime = DateTime.Now
                                };
                                var SLRESULT = SaveSettleAccounts(SLModel);
                                if (SLRESULT.Result == true)
                                {
                                    var CSLRESULT = UpdateCustSettleAccountsState(SLModel);
                                }
                            }
                            else
                            {
                                em.Msg = strName + "--Userid:" + Userid + "--对应的账户为开通";
                            }
                        }
                    }

                }
                em.Msg = "结算成功！";
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog(strName, "异常信息：" + ex.Message + "-堆栈信息：" + ex.StackTrace);
                em.Result = false;
                em.Msg = "异常请查看日志";
                em.ErrorMessage = "异常信息：" + ex.Message + "-堆栈信息：" + ex.StackTrace;
            }
            return em;
        }
        #endregion



        #region   开通个人账户

        public ExcuteModel OpenCustAccount(CustAccount model)
        {
            List<CustAccount> loglist = new List<CustAccount>();
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                CustAccount _modelExist = db.CustAccount.FirstOrDefault(t => t.AccountID == model.AccountID);
                if (_modelExist != null)
                {
                    _modelExist.AccountID = model.AccountID;
                    _modelExist.HospitalID = model.HospitalID;
                    _modelExist.Account = model.Account;
                    _modelExist.CustId = model.CustId;
                    _modelExist.Type = model.Type;
                    _modelExist.PayTradeID = model.PayTradeID;
                    _modelExist.UserId = model.UserId;
                    _modelExist.Name = model.Name;
                    _modelExist.NickName = model.NickName;
                    _modelExist.Subaccount_type = model.Subaccount_type;
                    _modelExist.AmountMonery = model.AmountMonery;
                    _modelExist.Balance = model.Balance;
                    _modelExist.Password = model.Password;
                    _modelExist.DPassword = model.DPassword;
                    _modelExist.CashAmount = model.CashAmount;
                    _modelExist.UncashAmount = model.UncashAmount;
                    _modelExist.FreezeCashAmount = model.FreezeCashAmount;
                    _modelExist.Property = model.Property;
                    _modelExist.State = model.State;
                    _modelExist.CreditRating = model.CreditRating;
                    _modelExist.LastTermAmount = model.LastTermAmount;
                    _modelExist.LastUpdateTime = model.LastUpdateTime;
                    _modelExist.CheckValue = model.CheckValue;
                    _modelExist.BindQQ = model.BindQQ;
                    _modelExist.BindPhone = model.BindPhone;
                    _modelExist.BindWeChat = model.BindWeChat;
                    _modelExist.IDCard = model.IDCard;
                    _modelExist.ISCheck = model.ISCheck;
                    _modelExist.CreatePeople = model.CreatePeople;
                    _modelExist.CreateTime = model.CreateTime;
                    _modelExist.UpdatePeople = model.UpdatePeople;
                    _modelExist.UpdateTime = model.UpdateTime;
                    RetModel.RetValue = _modelExist.AccountID.ToString();
                }
                else
                {
                    db.CustAccount.InsertOnSubmit(model);
                }
                db.SubmitChanges();
                RetModel.Result = true;
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        public ExcuteModel JudgeCustAccount(CustAccount model)
        {
            List<CustAccount> loglist = new List<CustAccount>();
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                CustAccount _modelExist = db.CustAccount.FirstOrDefault(t => t.AccountID == model.AccountID && t.Account == model.Account);
                if (_modelExist != null)
                {
                    RetModel.Result = false;
                    RetModel.Msg = _modelExist.AccountID.ToString();
                }
                else
                {
                    RetModel.Result = true;
                }
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        public List<CustAccount> GetCustAccountList(CustAccount model)
        {
            List<CustAccount> List = new List<CustAccount>();
            try
            {
                db = new RemoteDianoseDataContext();
                //string str = @"SELECT    [PaymentID],   [HospitalID],   [PayAccount],   [Paypassword],   [PayModeNode],   [PayTypeNode],   [OrganPhone],   [OrganContact],   [StartUsing],   [CreatePeople],   [CreateTime],   [UpdatePeople],   [UpdateTime],   [Standby],   [Standby1],   [Standby2],   [Standby3],   [Standby4],    PayType,   [PayMode],   CASE [SMSNotification] WHEN '0'   THEN '否' WHEN '1'   THEN '是' WHEN '2'   THEN '' WHEN '3'   THEN ''ELSE '其他' END as [SMSNotification]   from [RemoteDianose].[dbo].[OrganPayAccount] where 1=1  AND Standby =0  ";
                string StrSql = @"SELECT  ";
                StrSql += "  [AccountID], ";
                StrSql += "  [Account], ";
                StrSql += "  [CustId], ";
                StrSql += "  [Type], ";
                StrSql += "  [PayTradeID], ";
                StrSql += "  [UserId], ";
                StrSql += "  [HospitalID], ";
                StrSql += "  [Name], ";
                StrSql += "  [NickName], ";
                StrSql += "  [Subaccount_type], ";
                StrSql += "  [AmountMonery], ";
                StrSql += "  [Balance], ";
                StrSql += "  [Password], ";
                StrSql += "  [DPassword], ";
                StrSql += "  [CashAmount], ";
                StrSql += "  [UncashAmount], ";
                StrSql += "  [FreezeCashAmount], ";
                StrSql += "  [Property], ";
                StrSql += "  [State], ";
                StrSql += "  [CreditRating], ";
                StrSql += "  [LastTermAmount], ";
                StrSql += "  [LastUpdateTime], ";
                StrSql += "  [CheckValue], ";
                StrSql += "  [BindQQ], ";
                StrSql += "  [BindPhone], ";
                StrSql += "  [BindWeChat], ";
                StrSql += "  [IDCard], ";
                StrSql += "  [ISCheck], ";
                StrSql += "  [CreatePeople], ";
                StrSql += "  [CreateTime], ";
                StrSql += "  [UpdatePeople], ";
                StrSql += "  [UpdateTime] ";
                StrSql += " from [RemoteDianose].[dbo].[CustAccount] ";
                StrSql += "where 1=1 ";

                if (model != null)
                {
                    if (!string.IsNullOrEmpty(model.Account))
                    {
                        StrSql += " AND Account ='" + model.Account.ToString() + "' ";
                    }
                    if (!string.IsNullOrEmpty(model.HospitalID))
                    {
                        StrSql += " AND HospitalID ='" + model.HospitalID.ToString() + "' ";
                    }
                    if (!string.IsNullOrEmpty(model.AccountID))
                    {
                        StrSql += " AND AccountID ='" + model.AccountID.ToString() + "' ";
                    }
                    if (!string.IsNullOrEmpty(model.UserId))
                    {
                        StrSql += " AND UserId ='" + model.UserId.ToString() + "' ";
                    }
                    //if (!string.IsNullOrEmpty(model.Type))
                    //{
                    //    if (Convert.ToInt32(model.Type) != 0)
                    //    {
                    //        StrSql += " AND Type ='" + model.Type.ToString() + "' ";
                    //    }
                    //}
                    if (!string.IsNullOrEmpty(model.PayTradeID))
                    {
                        StrSql += " AND PayTradeID ='" + model.PayTradeID.ToString() + "' ";
                    }
                    if (!string.IsNullOrEmpty(model.CustId))
                    {
                        StrSql += " AND CustId ='" + model.CustId.ToString() + "' ";
                    }
                }
                StrSql = ReplaceInstance(StrSql, "GetCustAccountList：");
                List = db.ExecuteQuery<CustAccount>(StrSql).ToList();
            }
            catch (Exception)
            {
                return null;
            }
            return List;
        }

        #endregion

        #region   充值

        /// <summary>
        /// 用户提现或充值记录
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <returns></returns>
        public List<RechargeWithDrawBILL> GetRechargeWithDrawBILLList(RechargeWithDrawBILL Model, DateTime? dtStart = null, DateTime? dtEnd = null)
        {
            List<RechargeWithDrawBILL> model_List = new List<RechargeWithDrawBILL>();
            try
            {
                db = new RemoteDianoseDataContext();
                string StrSql = @"SELECT * FROM  RechargeWithDrawBILL WHERE 1=1 ";
                if (!string.IsNullOrEmpty(Model.AccountID))
                {
                    StrSql += " AND AccountID='" + Model.AccountID + "'";
                }
                if (dtStart != null && dtStart != Convert.ToDateTime("1900-01-01"))
                {
                    StrSql += " AND  WORKDATE>=CONVERT(varchar(100),'" + Convert.ToDateTime(dtStart).ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                }
                if (dtEnd != null && dtEnd != Convert.ToDateTime("1900-01-01"))
                {
                    StrSql += " AND  WORKDATE<=CONVERT(varchar(100),'" + Convert.ToDateTime(dtEnd).ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                }
                StrSql += " ORDER BY CreateTime DESC";
                StrSql = ReplaceInstance(StrSql, "GetRechargeWithDrawBILLList");
                model_List = db.ExecuteQuery<RechargeWithDrawBILL>(StrSql).ToList();
            }
            catch (Exception exp)
            {
                model_List = new List<DB_Server.RechargeWithDrawBILL>();
                LogHelper.WriteErrorLog("查询用户提现或充值记录信息", "异常信息：" + exp.Message + "--堆栈信息：" + exp.StackTrace);
            }
            return model_List;
        }

        public ExcuteModel UpdateRechargeWithDrawBILLState(RechargeWithDrawBILL model, CustAccountModel CustAccount_model)
        {
            List<RechargeWithDrawBILL> loglist = new List<RechargeWithDrawBILL>();
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                if (CustAccount_model.ResultState.Equals("SUCCESS"))
                {
                    using (TransactionScope scope = new TransactionScope())
                    {
                        try
                        {
                            RechargeWithDrawBILL RechargeWithDrawBILL_model = db.RechargeWithDrawBILL.FirstOrDefault(u => u.PayTradeID == CustAccount_model.PayTradeID_);
                            if (RechargeWithDrawBILL_model != null)
                            {
                                RechargeWithDrawBILL_model.State = "2";
                                RechargeWithDrawBILL_model.UpdateTime = DateTime.Now;
                                RechargeWithDrawBILL_model.UpdatePeople = CustAccount_model.Login_Name;
                            }
                            else
                            {
                                RetModel.Msg = "没有找到对应的缴费记录";
                                RetModel.Result = false;
                            }
                            db.SubmitChanges();
                            RetModel.Result = true;
                            scope.Complete();
                            return RetModel;
                        }
                        catch (Exception)
                        {
                            RetModel.Result = false;
                            return RetModel;
                        }
                    }
                }
                else if (CustAccount_model.ResultState.Equals("FAIL"))
                {
                    using (TransactionScope scope = new TransactionScope())
                    {
                        CustAccount CustAccount_model_List = db.CustAccount.FirstOrDefault(u => u.AccountID == CustAccount_model.AccountID);
                        if (CustAccount_model_List != null)
                        {
                            try
                            {
                                CustAccount_model_List.Balance = CustAccount_model_List.Balance + model.Amount;
                                CustAccount_model_List.AmountMonery = CustAccount_model_List.AmountMonery + model.Amount;
                            }
                            catch (Exception)
                            {
                                Decimal monery_Balance = Convert.ToDecimal(CustAccount_model_List.Balance) + Convert.ToDecimal(model.Amount);
                                CustAccount_model_List.Balance = monery_Balance;
                                Decimal monery = Convert.ToDecimal(CustAccount_model_List.AmountMonery) + Convert.ToDecimal(model.Amount);
                                CustAccount_model_List.AmountMonery = monery;
                            }
                        }
                        else
                        {
                            RetModel.Msg = "提现失败，资金回滚更新失败";
                            RetModel.Result = false;
                        }
                        db.SubmitChanges();
                        RetModel.Result = true;
                        if (RetModel.Result == true)
                        {
                            RechargeWithDrawBILL RechargeWithDrawBILL_model = db.RechargeWithDrawBILL.FirstOrDefault(u => u.PayTradeID == CustAccount_model.PayTradeID_);
                            if (RechargeWithDrawBILL_model != null)
                            {
                                try
                                {
                                    try
                                    {
                                        RechargeWithDrawBILL_model.AccountBalance = RechargeWithDrawBILL_model.AccountBalance + RechargeWithDrawBILL_model.Amount;
                                    }
                                    catch (Exception)
                                    {
                                        Decimal monery = Convert.ToDecimal(RechargeWithDrawBILL_model.AccountBalance) + Convert.ToDecimal(RechargeWithDrawBILL_model.Amount);
                                        RechargeWithDrawBILL_model.AccountBalance = monery;
                                    }
                                    RechargeWithDrawBILL_model.State = "3";
                                    RechargeWithDrawBILL_model.UpdateTime = DateTime.Now;
                                    RechargeWithDrawBILL_model.UpdatePeople = CustAccount_model.Login_Name;
                                }
                                catch (Exception)
                                {
                                    RetModel.Msg = "提现失败，资金回滚更新失败";
                                    RetModel.Result = false;
                                }
                            }
                            else
                            {
                                RetModel.Msg = "没有找到对应的缴费记录";
                                RetModel.Result = false;
                            }
                            db.SubmitChanges();
                            RetModel.Result = true;
                            scope.Complete();
                            return RetModel;
                        }
                        else
                        {
                            RetModel.Result = false;
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        public ExcuteModel RechargeWithDrawBILL_Log(RechargeWithDrawBILL model, CustAccountModel CustAccount_model)
        {
            List<RechargeWithDrawBILL> loglist = new List<RechargeWithDrawBILL>();
            ExcuteModel RetModel = new ExcuteModel();
            Decimal Amount_Monery = Convert.ToDecimal("0.001");
            DateTime birthDate = new DateTime(1900, 1, 1);
            Double M = Convert.ToDouble(model.Amount.ToString());
            if (M.ToString("0.00").Equals("0.00"))
            {
                RetModel.Result = false;
                RetModel.Msg = "充值金额必须大于0.01元";
                return RetModel;
            }
            else
            {
                if (model.Amount != null)
                {
                    Amount_Monery = Convert.ToDecimal(model.Amount);
                }
                else
                {
                    RetModel.Result = false;
                    RetModel.Msg = "充值金额必须大于0.01元";
                    return RetModel;
                }
            }
            try
            {
                String DBConnStr;
                DataSet MyDataSet = new DataSet();
                System.Data.SqlClient.SqlDataAdapter DataAdapter = new System.Data.SqlClient.SqlDataAdapter();
                DBConnStr = System.Configuration.ConfigurationManager.ConnectionStrings["InsideRISConnectionString"].ConnectionString;
                System.Data.SqlClient.SqlConnection myConnection = new System.Data.SqlClient.SqlConnection(DBConnStr);
                SqlParameter[] paras = new SqlParameter[] {
                    new SqlParameter("@name",SqlDbType.VarChar),
                    new SqlParameter("@type",SqlDbType.VarChar),
                    new SqlParameter("@DPassword",SqlDbType.VarChar),
                    new SqlParameter("@Cust_Id",SqlDbType.VarChar),
                    new SqlParameter("@AccountID",SqlDbType.NVarChar),
                    new SqlParameter("@SubAccount_type",SqlDbType.VarChar),
                    new SqlParameter("@Cust_Name",SqlDbType.VarChar),
                    new SqlParameter("@RechargeType",SqlDbType.VarChar),
                    new SqlParameter("@TransferState",SqlDbType.VarChar),
                    new SqlParameter("@TransferType",SqlDbType.VarChar),
                    new SqlParameter("@TransferMode",SqlDbType.VarChar),
                    new SqlParameter("@Body",SqlDbType.NVarChar),
                    new SqlParameter("@Bank_Type",SqlDbType.VarChar),
                    new SqlParameter("@Bank_Code",SqlDbType.VarChar),
                    new SqlParameter("@Bank_Name",SqlDbType.VarChar),
                    new SqlParameter("@BankAddrNo",SqlDbType.VarChar),
                    new SqlParameter("@BankCardNo",SqlDbType.VarChar),
                    new SqlParameter("@BankCardName",SqlDbType.VarChar),
                    new SqlParameter("@Amount",SqlDbType.Decimal),
                    new SqlParameter("@AccountBalance",SqlDbType.Decimal),
                    new SqlParameter("@Note",SqlDbType.VarChar),
                    new SqlParameter("@ResultCode",SqlDbType.VarChar),
                    new SqlParameter("@ResultNote",SqlDbType.VarChar),
                    new SqlParameter("@State",SqlDbType.VarChar),
                    new SqlParameter("@Check_State",SqlDbType.VarChar),
                    new SqlParameter("@CreatePeople",SqlDbType.VarChar),
                    new SqlParameter("@UpdatePeople",SqlDbType.VarChar),
                    new SqlParameter("@Standby",SqlDbType.NVarChar),
                    new SqlParameter("@Standby1",SqlDbType.NVarChar),
                    new SqlParameter("@PayTradeID",SqlDbType.NVarChar)
                };
                paras[0].Value = CustAccount_model.Login_Name;
                paras[1].Value = CustAccount_model.TransactionType;
                paras[2].Value = CustAccount_model.PayPassword;
                paras[3].Value = model.Cust_Id;
                paras[4].Value = CustAccount_model.AccountID;
                paras[5].Value = model.SubAccount_type == null ? "" : model.SubAccount_type;
                paras[6].Value = model.Cust_Name == null ? "" : model.Cust_Name;
                paras[7].Value = model.RechargeType == null ? "" : model.RechargeType.ToString();
                paras[8].Value = model.TransferState == null ? "" : model.TransferState;
                paras[9].Value = model.TransferType == null ? "" : model.TransferType;
                paras[10].Value = model.TransferMode == null ? "" : model.TransferMode;
                paras[11].Value = model.Body == null ? "" : model.Body;
                paras[12].Value = model.Bank_Type == null ? "" : model.Bank_Type;
                paras[13].Value = model.Bank_Code == null ? "" : model.Bank_Code;
                paras[14].Value = model.Bank_Name == null ? "" : model.Bank_Name;
                paras[15].Value = model.BankAddrNo == null ? "" : model.BankAddrNo;
                paras[16].Value = model.BankCardNo == null ? "" : model.BankCardNo;
                paras[17].Value = model.BankCardName == null ? "" : model.BankCardName;
                paras[18].Value = Amount_Monery;
                paras[19].Value = Amount_Monery;
                paras[20].Value = model.Note == null ? "" : model.Note;
                paras[21].Value = model.ResultCode == null ? "" : model.ResultCode;
                paras[22].Value = model.ResultNote == null ? "" : model.ResultNote;
                paras[23].Value = model.State == null ? "" : model.State.ToString();
                paras[24].Value = model.Check_State == null ? "" : model.Check_State;
                paras[25].Value = model.CreatePeople == null ? "" : model.CreatePeople;
                paras[26].Value = model.UpdatePeople == null ? "" : model.UpdatePeople.ToString();
                paras[27].Value = model.Standby == null ? "" : model.Standby.ToString();
                paras[28].Value = model.Standby1 == null ? "" : model.Standby1.ToString();
                paras[29].Value = CustAccount_model.PayTradeID_ == null ? "" : CustAccount_model.PayTradeID_;
                //paras[29].Value = ParameterDirection.Output;
                using (SqlConnection conn = new SqlConnection(DBConnStr))
                {
                    try
                    {
                        SqlCommand myCommand = new SqlCommand("InsertRechargeWithDrawBILL_Log", conn);
                        myCommand.CommandType = CommandType.StoredProcedure;
                        foreach (SqlParameter parameter in paras)
                        {
                            myCommand.Parameters.Add(parameter); //存款用户姓名
                        }
                        SqlParameter para1 = new SqlParameter("@ErrorType", SqlDbType.Int, 20);
                        para1.Direction = ParameterDirection.Output;
                        myCommand.Parameters.Add(para1);
                        conn.Open();
                        try
                        {
                            myCommand.ExecuteNonQuery();
                            var RValue = myCommand.Parameters["@ErrorType"].Value.ToString();///此处将存储过程的返回参数
                            switch (Convert.ToInt32(RValue))
                            {
                                case 15:
                                    RetModel.Result = false;
                                    RetModel.RetValue = "15";
                                    RetModel.Msg = "密码错误";
                                    break;
                                case 16:
                                    RetModel.Result = false;
                                    RetModel.RetValue = "16";
                                    RetModel.Msg = "交易失败！余额不足";
                                    break;
                                default:
                                    RetModel.Result = true;
                                    RetModel.Msg = "成功";
                                    break;
                            }
                        }
                        catch (Exception exp)
                        {
                            RetModel.Result = false;
                            if (exp.Message.Contains("密码错误"))
                            {
                                RetModel.Result = false;
                                RetModel.RetValue = "15";
                                RetModel.Msg = "密码错误";
                                if (exp.Message.Contains("交易失败"))
                                {
                                    RetModel.Result = false;
                                    RetModel.RetValue = "16";
                                    RetModel.Msg = "交易失败！余额不足";
                                }
                            }
                            else if (exp.Message.Contains("交易失败"))
                            {
                                RetModel.Result = false;
                                RetModel.RetValue = "16";
                                RetModel.Msg = "交易失败！余额不足";
                            }
                        }
                    }
                    catch (System.Exception exp)
                    {
                        RetModel.Result = false;
                        RetModel.Msg = exp.ToString();
                    }
                }
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        public ExcuteModel RechargeWithDrawBILL(RechargeWithDrawBILL model, CustAccountModel CustAccount_model)
        {
            string strName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            List<RechargeWithDrawBILL> loglist = new List<RechargeWithDrawBILL>();
            ExcuteModel RetModel = new ExcuteModel();
            Decimal Amount_Monery = Convert.ToDecimal("0.001");
            DateTime birthDate = new DateTime(1900, 1, 1);
            Double M = Convert.ToDouble(model.Amount.ToString());
            if (M.ToString("0.00").Equals("0.00"))
            {
                RetModel.Result = false;
                RetModel.Msg = "充值金额必须大于0.01元";
                return RetModel;
            }
            else
            {
                if (model.Amount != null)
                {
                    Amount_Monery = Convert.ToDecimal(model.Amount);
                }
                else
                {
                    RetModel.Result = false;
                    RetModel.Msg = "充值金额必须大于0.01元";
                    return RetModel;
                }
            }
            try
            {
                String DBConnStr;
                DataSet MyDataSet = new DataSet();
                System.Data.SqlClient.SqlDataAdapter DataAdapter = new System.Data.SqlClient.SqlDataAdapter();
                DBConnStr = System.Configuration.ConfigurationManager.ConnectionStrings["InsideRISConnectionString"].ConnectionString;
                System.Data.SqlClient.SqlConnection myConnection = new System.Data.SqlClient.SqlConnection(DBConnStr);
                SqlParameter[] paras = new SqlParameter[] {
                    new SqlParameter("@name",SqlDbType.VarChar),
                    new SqlParameter("@type",SqlDbType.VarChar),
                    new SqlParameter("@DPassword",SqlDbType.VarChar),
                    new SqlParameter("@Cust_Id",SqlDbType.VarChar),
                    new SqlParameter("@AccountID",SqlDbType.NVarChar),
                    new SqlParameter("@SubAccount_type",SqlDbType.VarChar),
                    new SqlParameter("@Cust_Name",SqlDbType.VarChar),
                    new SqlParameter("@RechargeType",SqlDbType.VarChar),
                    new SqlParameter("@TransferState",SqlDbType.VarChar),
                    new SqlParameter("@TransferType",SqlDbType.VarChar),
                    new SqlParameter("@TransferMode",SqlDbType.VarChar),
                    new SqlParameter("@Body",SqlDbType.NVarChar),
                    new SqlParameter("@Bank_Type",SqlDbType.VarChar),
                    new SqlParameter("@Bank_Code",SqlDbType.VarChar),
                    new SqlParameter("@Bank_Name",SqlDbType.VarChar),
                    new SqlParameter("@BankAddrNo",SqlDbType.VarChar),
                    new SqlParameter("@BankCardNo",SqlDbType.VarChar),
                    new SqlParameter("@BankCardName",SqlDbType.VarChar),
                    new SqlParameter("@Amount",SqlDbType.Decimal),
                    new SqlParameter("@AccountBalance",SqlDbType.Decimal),
                    new SqlParameter("@Note",SqlDbType.VarChar),
                    new SqlParameter("@ResultCode",SqlDbType.VarChar),
                    new SqlParameter("@ResultNote",SqlDbType.VarChar),
                    new SqlParameter("@State",SqlDbType.VarChar),
                    new SqlParameter("@Check_State",SqlDbType.VarChar),
                    new SqlParameter("@CreatePeople",SqlDbType.VarChar),
                    new SqlParameter("@UpdatePeople",SqlDbType.VarChar),
                    new SqlParameter("@Standby",SqlDbType.NVarChar),
                    new SqlParameter("@Standby1",SqlDbType.NVarChar),
                    new SqlParameter("@PayTradeID",SqlDbType.NVarChar)
                };
                paras[0].Value = CustAccount_model.Login_Name;
                paras[1].Value = CustAccount_model.TransactionType;
                paras[2].Value = CustAccount_model.PayPassword;
                paras[3].Value = model.Cust_Id;
                paras[4].Value = CustAccount_model.AccountID;
                paras[5].Value = model.SubAccount_type == null ? "" : model.SubAccount_type;
                paras[6].Value = model.Cust_Name == null ? "" : model.Cust_Name;
                paras[7].Value = model.RechargeType == null ? "" : model.RechargeType;
                paras[8].Value = model.TransferState == null ? "" : model.TransferState;
                paras[9].Value = model.TransferType == null ? "" : model.TransferType;
                paras[10].Value = model.TransferMode == null ? "" : model.TransferMode;
                paras[11].Value = model.Body == null ? "" : model.Body;
                paras[12].Value = model.Bank_Type == null ? "" : model.Bank_Type;
                paras[13].Value = model.Bank_Code == null ? "" : model.Bank_Code;
                paras[14].Value = model.Bank_Name == null ? "" : model.Bank_Name;
                paras[15].Value = model.BankAddrNo == null ? "" : model.BankAddrNo;
                paras[16].Value = model.BankCardNo == null ? "" : model.BankCardNo;
                paras[17].Value = model.BankCardName == null ? "" : model.BankCardName;
                paras[18].Value = Amount_Monery;
                paras[19].Value = Amount_Monery;
                paras[20].Value = model.Note == null ? "" : model.Note;
                paras[21].Value = model.ResultCode == null ? "" : model.ResultCode;
                paras[22].Value = model.ResultNote == null ? "" : model.ResultNote;
                paras[23].Value = model.State == null ? "" : model.State.ToString();
                paras[24].Value = model.Check_State == null ? "" : model.Check_State;
                paras[25].Value = model.CreatePeople == null ? "" : model.CreatePeople;
                paras[26].Value = model.UpdatePeople == null ? "" : model.UpdatePeople;
                paras[27].Value = model.Standby == null ? "" : model.Standby;
                paras[28].Value = model.Standby1 == null ? "" : model.Standby1;
                paras[29].Value = CustAccount_model.PayTradeID_ == null ? "" : CustAccount_model.PayTradeID_;

                using (SqlConnection conn = new SqlConnection(DBConnStr))
                {
                    try
                    {
                        SqlCommand myCommand = new SqlCommand("OperationMoney", conn);
                        myCommand.CommandType = CommandType.StoredProcedure;
                        foreach (SqlParameter parameter in paras)
                        {
                            myCommand.Parameters.Add(parameter); //存款用户姓名
                        }
                        SqlParameter para1 = new SqlParameter("@ErrorType", SqlDbType.Int, 20);
                        para1.Direction = ParameterDirection.Output;
                        myCommand.Parameters.Add(para1);
                        SqlParameter Snum = new SqlParameter("@Snum", SqlDbType.NVarChar, 50);
                        Snum.Direction = ParameterDirection.Output;
                        myCommand.Parameters.Add(Snum);
                        conn.Open();
                        try
                        {
                            myCommand.ExecuteNonQuery();
                            var RValue = myCommand.Parameters["@ErrorType"].Value.ToString();///此处将存储过程的返回参数
                            var SnumValue = myCommand.Parameters["@Snum"].Value.ToString();///此处将存储过程的返回参数
                            switch (Convert.ToInt32(RValue))
                            {
                                case 15:
                                    RetModel.Result = false;
                                    RetModel.RetValue = "15";
                                    RetModel.Msg = "密码错误";
                                    break;
                                case 16:
                                    RetModel.Result = false;
                                    RetModel.RetValue = "16";
                                    RetModel.Msg = "交易失败！余额不足";
                                    break;
                                default:
                                    RetModel.Result = true;
                                    RetModel.WcfMessage = SnumValue;
                                    RetModel.Msg = "成功";
                                    break;
                            }
                        }
                        catch (Exception exp)
                        {
                            RetModel.Result = false;
                            if (exp.Message.Contains("密码错误"))
                            {
                                RetModel.RetValue = "15";
                                RetModel.Msg = "密码错误";
                                if (exp.Message.Contains("交易失败"))
                                {
                                    RetModel.RetValue = "16";
                                    RetModel.Msg = "交易失败！余额不足";
                                    LogHelper.WriteErrorLog(strName, "15 16交易失败！余额不足 Cust_Id:" + model.Cust_Id);
                                }
                                LogHelper.WriteErrorLog(strName, "交易失败！密码错误 Cust_Id:" + model.Cust_Id);
                            }
                            else if (exp.Message.Contains("交易失败"))
                            {
                                RetModel.RetValue = "16";
                                RetModel.Msg = "交易失败！余额不足";
                                LogHelper.WriteErrorLog(strName, "交易失败！余额不足 Cust_Id:" + model.Cust_Id);
                            }
                            else
                            {
                                RetModel.Msg = exp.Message.ToString();
                                LogHelper.WriteErrorLog(strName, "Cust_Id:" + model.Cust_Id);
                            }
                        }
                    }
                    catch (System.Exception exp)
                    {
                        RetModel.Result = false;
                        RetModel.Msg = exp.ToString();
                        LogHelper.WriteErrorLog(strName, "Cust_Id:" + model.Cust_Id);
                    }
                }
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }
        #endregion

        #region   修改列表
        public ExcuteModel UpdateRequestInfo(string RequestId)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                RequestInfo _modelExist = db.RequestInfo.FirstOrDefault(t => t.RequestId == RequestId && t.RequestState == 3);
                if (_modelExist != null)
                {
                    _modelExist.RequestState = 1;
                }
                db.SubmitChanges();
                RetModel.Result = true;
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        #endregion

        #region   退费接口

        public ExcuteModel RefundInterface(string RequestId, string refund_reason, string Amount, string people)
        {
            ExcuteModel RetModel = new ExcuteModel();
            RequestInfo RequestInfo_model = db.RequestInfo.Where(u => u.RequestId == RequestId).FirstOrDefault();
            if (RequestInfo_model != null)
            {
                //诊断开始退费
                RequestInfo RequestInfo_ChargeRecord = new RequestInfo();
                RequestInfo RequestInfo_model_ = db.RequestInfo.FirstOrDefault(t => t.RequestId == RequestId);
                if (RequestInfo_model != null)
                {
                    RequestInfo_model.RequestState = 5; //已退费
                    RequestInfo_model.FeeState = 1;  //未交费状态
                }
                else
                {
                    RetModel.Msg = "没有找到对应的缴费记录";
                    RetModel.Result = false;
                }
                db.SubmitChanges();
                RetModel.Result = true;
                if (RetModel.Result == true)
                {
                    db.SubmitChanges();
                    RetModel.Result = true;
                }
            }
            return RetModel;
        }

        #endregion

        #region 支付宝支付订单记录表

        public ExcuteModel SavePayTransaction_State(PayTransaction model)
        {
            List<PayTransaction> loglist = new List<PayTransaction>();
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                RequestInfo RequestInfo_ChargeRecord = new RequestInfo();
                PayTransaction _modelExist = db.PayTransaction.FirstOrDefault(t => t.PayTradeID == model.PayTradeID);
                if (_modelExist != null)
                {
                    if (model.HospitalID == null)
                    {
                        _modelExist.PayState = model.PayState;
                        _modelExist.Type = model.Type;
                        _modelExist.PayCount = model.PayCount;
                        _modelExist.Standby3 = model.Standby3;
                        _modelExist.WhatOrder = model.WhatOrder;
                    }
                    else
                    {
                        _modelExist.PayTradeID = model.PayTradeID;
                        _modelExist.HospitalID = model.HospitalID;
                        _modelExist.PatientID = model.PatientID;
                        _modelExist.PayState = model.PayState;
                        _modelExist.Body = model.Body;
                        _modelExist.Type = model.Type;
                        _modelExist.GoodsId = model.GoodsId;
                        _modelExist.GoodsName = model.GoodsName;
                        _modelExist.Quantity = model.Quantity;
                        _modelExist.Subject = model.Subject;
                        _modelExist.TotalAmount = model.TotalAmount;
                        _modelExist.StoreId = model.StoreId;
                        _modelExist.SellerId = model.SellerId;
                        _modelExist.CreatePeople = model.CreatePeople;
                        _modelExist.CreateTime = model.CreateTime;
                        _modelExist.UpdatePeople = model.UpdatePeople;
                        _modelExist.UpdateTime = model.UpdateTime;
                        _modelExist.Standby = model.Standby;
                        _modelExist.Standby1 = model.Standby1;
                        _modelExist.Standby2 = model.Standby2;
                        _modelExist.Standby3 = model.Standby3;
                        _modelExist.Standby4 = model.Standby4;
                    }
                    RetModel.Msg = _modelExist.PayTradeID.ToString();
                    RetModel.RetValue = _modelExist.PayTradeID.ToString();
                }
                else
                {
                    RetModel.Msg = model.PayTradeID.ToString();
                    db.PayTransaction.InsertOnSubmit(model);
                }
                db.SubmitChanges();
                RetModel.Result = true;
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        public ExcuteModel SavePayTransactionLog(PayTransactionLog model)
        {
            List<PayTransactionLog> loglist = new List<PayTransactionLog>();
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                RequestInfo RequestInfoChargeRecord = new RequestInfo();
                PayTransactionLog _modelExist = db.PayTransactionLog.FirstOrDefault(t => t.PayTradeID == model.PayTradeID);
                if (_modelExist != null)
                {
                    if (model.HospitalID == null)
                    {
                        _modelExist.PayState = model.PayState;
                        _modelExist.Type = model.Type;
                        _modelExist.PayCount = model.PayCount;
                        _modelExist.Standby3 = model.Standby3;
                        _modelExist.WhatOrder = model.WhatOrder;
                    }
                    else
                    {
                        _modelExist.PayTradeID = model.PayTradeID;
                        _modelExist.HospitalID = model.HospitalID;
                        _modelExist.PatientID = model.PatientID;
                        _modelExist.PayState = model.PayState;
                        _modelExist.Body = model.Body;
                        _modelExist.Type = model.Type;
                        _modelExist.GoodsId = model.GoodsId;
                        _modelExist.GoodsName = model.GoodsName;
                        _modelExist.Quantity = model.Quantity;
                        _modelExist.Subject = model.Subject;
                        _modelExist.TotalAmount = model.TotalAmount;
                        _modelExist.StoreId = model.StoreId;
                        _modelExist.SellerId = model.SellerId;
                        _modelExist.CreatePeople = model.CreatePeople;
                        _modelExist.CreateTime = model.CreateTime;
                        _modelExist.UpdatePeople = model.UpdatePeople;
                        _modelExist.UpdateTime = model.UpdateTime;
                        _modelExist.Standby = model.Standby;
                        _modelExist.Standby1 = model.Standby1;
                        _modelExist.Standby2 = model.Standby2;
                        _modelExist.Standby3 = model.Standby3;
                        _modelExist.Standby4 = model.Standby4;
                    }
                    RetModel.Msg = _modelExist.PayTradeID.ToString();
                    RetModel.RetValue = _modelExist.PayTradeID.ToString();
                }
                else
                {
                    RetModel.Msg = model.PayTradeID.ToString();
                    db.PayTransactionLog.InsertOnSubmit(model);
                }
                db.SubmitChanges();
                RetModel.Result = true;
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        public ExcuteModel SavePayTransaction_Recharge(PayTransaction model)
        {
            List<PayTransaction> loglist = new List<PayTransaction>();
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                PayTransaction _modelExist = db.PayTransaction.FirstOrDefault(t => t.PayTradeID == model.PayTradeID);
                if (_modelExist != null)
                {
                    if (model.HospitalID == null)
                    {
                        _modelExist.PayState = model.PayState;
                        _modelExist.Type = model.Type;
                        _modelExist.PayCount = model.PayCount;
                        _modelExist.Standby3 = model.Standby3;
                        _modelExist.UpdateTime = model.UpdateTime;
                    }
                    else
                    {
                        _modelExist.PayTradeID = model.PayTradeID;
                        _modelExist.HospitalID = model.HospitalID;
                        _modelExist.PatientID = model.PatientID;
                        _modelExist.PayState = model.PayState;
                        _modelExist.Body = model.Body;
                        _modelExist.Type = model.Type;
                        _modelExist.GoodsId = model.GoodsId;
                        _modelExist.GoodsName = model.GoodsName;
                        _modelExist.Quantity = model.Quantity;
                        _modelExist.Subject = model.Subject;
                        _modelExist.TotalAmount = model.TotalAmount;
                        _modelExist.StoreId = model.StoreId;
                        _modelExist.SellerId = model.SellerId;
                        _modelExist.CreatePeople = model.CreatePeople;
                        _modelExist.CreateTime = model.CreateTime;
                        _modelExist.UpdatePeople = model.UpdatePeople;
                        _modelExist.UpdateTime = model.UpdateTime;
                        _modelExist.Standby = model.Standby;
                        _modelExist.Standby1 = model.Standby1;
                        _modelExist.Standby2 = model.Standby2;
                        _modelExist.Standby3 = model.Standby3;
                        _modelExist.Standby4 = model.Standby4;
                    }
                    RetModel.Msg = _modelExist.PayTradeID.ToString();
                    RetModel.RetValue = _modelExist.PayTradeID.ToString();
                }
                else
                {
                    RetModel.Msg = model.PayTradeID.ToString();
                    db.PayTransaction.InsertOnSubmit(model);
                }
                db.SubmitChanges();
                RetModel.Result = true;
                if (RetModel.Result == true)
                {
                    RetModel.Result = true;
                    RetModel.Msg = model.PayTradeID.ToString();
                }
                else
                {
                    LogHelper.WriteInfoLog("信息错误");
                }
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        public ExcuteModel SavePayTransactionLog_Recharge(PayTransactionLog model)
        {
            List<PayTransactionLog> loglist = new List<PayTransactionLog>();
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                PayTransactionLog _modelExist = db.PayTransactionLog.FirstOrDefault(t => t.PayTradeID == model.PayTradeID);
                if (_modelExist != null)
                {
                    if (model.HospitalID == null)
                    {
                        _modelExist.PayState = model.PayState;
                        _modelExist.Type = model.Type;
                        _modelExist.PayCount = model.PayCount;
                        _modelExist.Standby3 = model.Standby3;
                        _modelExist.UpdateTime = model.UpdateTime;
                    }
                    else
                    {
                        _modelExist.PayTradeID = model.PayTradeID;
                        _modelExist.HospitalID = model.HospitalID;
                        _modelExist.PatientID = model.PatientID;
                        _modelExist.PayState = model.PayState;
                        _modelExist.Body = model.Body;
                        _modelExist.Type = model.Type;
                        _modelExist.GoodsId = model.GoodsId;
                        _modelExist.GoodsName = model.GoodsName;
                        _modelExist.Quantity = model.Quantity;
                        _modelExist.Subject = model.Subject;
                        _modelExist.TotalAmount = model.TotalAmount;
                        _modelExist.StoreId = model.StoreId;
                        _modelExist.SellerId = model.SellerId;
                        _modelExist.CreatePeople = model.CreatePeople;
                        _modelExist.CreateTime = model.CreateTime;
                        _modelExist.UpdatePeople = model.UpdatePeople;
                        _modelExist.UpdateTime = model.UpdateTime;
                        _modelExist.Standby = model.Standby;
                        _modelExist.Standby1 = model.Standby1;
                        _modelExist.Standby2 = model.Standby2;
                        _modelExist.Standby3 = model.Standby3;
                        _modelExist.Standby4 = model.Standby4;
                    }
                    RetModel.Msg = _modelExist.PayTradeID.ToString();
                    RetModel.RetValue = _modelExist.PayTradeID.ToString();
                }
                else
                {
                    RetModel.Msg = model.PayTradeID.ToString();
                    db.PayTransactionLog.InsertOnSubmit(model);
                }
                db.SubmitChanges();
                RetModel.Result = true;
                if (RetModel.Result == true)
                {
                    RetModel.Result = true;
                    RetModel.Msg = model.PayTradeID.ToString();
                }
                else
                {
                    LogHelper.WriteInfoLog("信息错误");
                }
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        public ExcuteModel SavePayTransaction(PayTransaction model)
        {
            List<PayTransaction> loglist = new List<PayTransaction>();
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                //ChargeRecord ChargeRecord_ChargeRecord = new ChargeRecord();
                //RequestInfo RequestInfo_ChargeRecord = new RequestInfo();
                PayTransaction _modelExist = db.PayTransaction.FirstOrDefault(t => t.PayTradeID == model.PayTradeID);
                //PayTransaction _modelExist = db.PayTransaction.FirstOrDefault(t => t.RequestId == model.RequestId);
                if (_modelExist != null)
                {
                    if (model.HospitalID == null)
                    {
                        if (!string.IsNullOrEmpty(model.PayState))
                        {
                            _modelExist.PayState = model.PayState;
                        }
                        if (!string.IsNullOrEmpty(model.Type))
                        {
                            _modelExist.Type = model.Type;
                        }
                        if (!string.IsNullOrEmpty(model.PayCount))
                        {
                            _modelExist.PayCount = model.PayCount;
                        }
                        if (string.IsNullOrEmpty(_modelExist.PayTradeID))
                        {
                            _modelExist.PayTradeID = model.PayTradeID;
                        }
                        _modelExist.Standby3 = model.Standby3;
                        _modelExist.UpdateTime = model.UpdateTime;
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(_modelExist.PayTradeID))
                        {
                            _modelExist.PayTradeID = model.PayTradeID;
                        }
                        else
                        {
                            string strSql = string.Format(@"update PayTransaction set PayTradeID='{0}' where RequestId ='{1}'", model.PayTradeID, model.RequestId);
                            db.ExecuteCommand(strSql);
                        }
                        _modelExist.HospitalID = model.HospitalID;
                        _modelExist.PatientID = model.PatientID;
                        _modelExist.PayState = model.PayState;
                        _modelExist.Body = model.Body;
                        _modelExist.Type = model.Type;
                        _modelExist.GoodsId = model.GoodsId;
                        _modelExist.GoodsName = model.GoodsName;
                        _modelExist.Quantity = model.Quantity;
                        _modelExist.Subject = model.Subject;
                        _modelExist.TotalAmount = model.TotalAmount;
                        _modelExist.StoreId = model.StoreId;
                        _modelExist.SellerId = model.SellerId;
                        _modelExist.CreatePeople = model.CreatePeople;
                        _modelExist.CreateTime = model.CreateTime;
                        _modelExist.UpdatePeople = model.UpdatePeople;
                        _modelExist.UpdateTime = model.UpdateTime;
                        _modelExist.Standby = model.Standby;
                        _modelExist.Standby1 = model.Standby1;
                        _modelExist.Standby2 = model.Standby2;
                        _modelExist.Standby3 = model.Standby3;
                        _modelExist.Standby4 = model.Standby4;
                    }
                    RetModel.Msg = model.PayTradeID;
                    RetModel.RetValue = model.PayTradeID;
                }
                else
                {
                    RetModel.Msg = model.PayTradeID.ToString();
                    db.PayTransaction.InsertOnSubmit(model);
                }
                db.SubmitChanges();
                if (model.HospitalID == null)
                {
                    try
                    {
                        if (_modelExist != null)
                        {
                            if (!string.IsNullOrEmpty(_modelExist.RequestId))
                            {
                                RequestInfo _model = db.RequestInfo.FirstOrDefault(t => t.RequestId == _modelExist.RequestId);
                                if (_model != null)
                                {
                                    if (_model.Diagnosticstate == "Consultation")
                                    {
                                        if (!string.IsNullOrEmpty(_modelExist.PayState))
                                        {
                                            if (_modelExist.PayState == "2")
                                            {
                                                _model.Cost = model.TotalAmount;
                                                _model.FeeState = 2;
                                                _model.RequestState = 3;
                                                db.SubmitChanges();
                                                RetModel.Result = true;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    RetModel.Msg = "申请单不存在，请联系管理员！";
                                    RetModel.Result = false;
                                }
                            }
                        }
                    }
                    catch (Exception exp)
                    {
                        RetModel.Result = false;
                        RetModel.Msg = exp.ToString();
                    }
                }
                else
                {
                    //不能添加次此信息，因为返回过去，会把Msg做为保存费用的主键
                    //RetModel.Msg = "保存交易记录失败，请联系管理员！";
                    RetModel.Result = true;
                }
            }
            catch (Exception exp)
            {
                RetModel.Msg = "保存交易记录异常，请联系管理员！";
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        /// <summary>
        /// 针对平台支付
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SavePayTransactionQRCode(PayTransaction model)
        {
            List<PayTransaction> loglist = new List<PayTransaction>();
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                PayTransaction _modelExist = db.PayTransaction.FirstOrDefault(t => t.PayTradeID == model.PayTradeID);
                if (_modelExist != null)
                {
                    if (model.HospitalID == null)
                    {
                        if (!string.IsNullOrEmpty(model.PayState))
                        {
                            _modelExist.PayState = model.PayState;
                        }
                        if (!string.IsNullOrEmpty(model.Type))
                        {
                            _modelExist.Type = model.Type;
                        }
                        if (!string.IsNullOrEmpty(model.PayCount))
                        {
                            _modelExist.PayCount = model.PayCount;
                        }
                        if (string.IsNullOrEmpty(_modelExist.PayTradeID))
                        {
                            _modelExist.PayTradeID = model.PayTradeID;
                        }
                        _modelExist.Standby3 = model.Standby3;
                        _modelExist.UpdateTime = model.UpdateTime;
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(_modelExist.PayTradeID))
                        {
                            _modelExist.PayTradeID = model.PayTradeID;
                        }
                        else
                        {
                            string strSql = string.Format(@"update PayTransaction set PayTradeID='{0}' where RequestId ='{1}'", model.PayTradeID, model.RequestId);
                            db.ExecuteCommand(strSql);
                        }
                        _modelExist.HospitalID = model.HospitalID;
                        _modelExist.PatientID = model.PatientID;
                        _modelExist.PayState = model.PayState;
                        _modelExist.Body = model.Body;
                        _modelExist.Type = model.Type;
                        _modelExist.GoodsId = model.GoodsId;
                        _modelExist.GoodsName = model.GoodsName;
                        _modelExist.Quantity = model.Quantity;
                        _modelExist.Subject = model.Subject;
                        _modelExist.TotalAmount = model.TotalAmount;
                        _modelExist.StoreId = model.StoreId;
                        _modelExist.SellerId = model.SellerId;
                        _modelExist.CreatePeople = model.CreatePeople;
                        _modelExist.CreateTime = model.CreateTime;
                        _modelExist.UpdatePeople = model.UpdatePeople;
                        _modelExist.UpdateTime = model.UpdateTime;
                        _modelExist.Standby = model.Standby;
                        _modelExist.Standby1 = model.Standby1;
                        _modelExist.Standby2 = model.Standby2;
                        _modelExist.Standby3 = model.Standby3;
                        _modelExist.Standby4 = model.Standby4;
                    }
                    RetModel.Msg = model.PayTradeID;
                    RetModel.RetValue = model.PayTradeID;
                }
                else
                {
                    RetModel.Msg = model.PayTradeID.ToString();
                    db.PayTransaction.InsertOnSubmit(model);
                }
                db.SubmitChanges();
                RetModel.Result = true;
                //if (model.HospitalID == null)
                //{
                //    try
                //    {
                //        if (_modelExist != null)
                //        {
                //            if (!string.IsNullOrEmpty(_modelExist.RequestId))
                //            {
                //                RequestInfo _model = db.RequestInfo.FirstOrDefault(t => t.RequestId == _modelExist.RequestId);
                //                if (_model != null)
                //                {
                //                    if (_model.Diagnosticstate == "Consultation")
                //                    {
                //                        if (!string.IsNullOrEmpty(_modelExist.PayState))
                //                        {
                //                            if (_modelExist.PayState == "2")
                //                            {
                //                                _model.Cost = model.TotalAmount;
                //                                _model.FeeState = 2;
                //                                _model.RequestState = 3;
                //                                db.SubmitChanges();
                //                                RetModel.Result = true;
                //                            }
                //                        }
                //                    }
                //                }
                //                else
                //                {
                //                    RetModel.Msg = "申请单不存在，请联系管理员！";
                //                    RetModel.Result = false;
                //                }
                //            }
                //        }
                //    }
                //    catch (Exception exp)
                //    {
                //        RetModel.Result = false;
                //        RetModel.Msg = exp.ToString();
                //    }
                //}
                //else
                //{
                //    //不能添加次此信息，因为返回过去，会把Msg做为保存费用的主键
                //    //RetModel.Msg = "保存交易记录失败，请联系管理员！";
                //    RetModel.Result = true;
                //}
            }
            catch (Exception exp)
            {
                RetModel.Msg = "保存交易记录异常，请联系管理员！";
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        public List<PayTransaction> GetPayTransactionDetail(string PayTradeID, string RequestId)
        {
            try
            {
                if (!string.IsNullOrEmpty(RequestId))
                {
                    List<PayTransaction> List = new List<PayTransaction>();
                    List = db.PayTransaction.Where(t => t.RequestId == RequestId).ToList().OrderByDescending(u => u.CreateTime).ToList();
                    if (List == null)
                    {
                        return null;
                    }
                    return List;
                }
                if (!string.IsNullOrEmpty(PayTradeID))
                {
                    List<PayTransaction> List = new List<PayTransaction>();
                    List = db.PayTransaction.Where(t => t.PayTradeID == PayTradeID).ToList();
                    if (List == null)
                    {
                        return null;
                    }
                    return List;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// 根据时间查询订单信息
        /// </summary>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <returns></returns>
        public List<PayTransaction> SearchPayTransactionList(DateTime dtStart, DateTime dtEnd)
        {
            List<PayTransaction> list = new List<PayTransaction>();
            try
            {
                StringBuilder strAppend = new StringBuilder();
                strAppend.Append("SELECT * FROM [RemoteDianose].[dbo].[PayTransaction] WHERE 1=1");
                if (dtStart != Convert.ToDateTime("1900-01-01"))
                {
                    strAppend.Append(" AND CreateTime >= CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)");
                }
                if (dtEnd != Convert.ToDateTime("1900-01-01"))
                {
                    strAppend.Append(" AND CreateTime<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)");
                }

                list = db.ExecuteQuery<PayTransaction>(strAppend.ToString()).ToList();
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("SearchPayTransactionList", "异常信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return list;
        }

        /// <summary>
        /// 获取申请单状态
        /// </summary>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        public int GetPayTransactionState(string RequestId)
        {
            PayTransaction _model = db.PayTransaction.FirstOrDefault(t => t.RequestId == RequestId);
            int _RetState = 1;
            if (_model != null)
            {
                _RetState = int.Parse(_model.PayState.ToString());
            }
            return _RetState;
        }

        /// <summary>
        /// 修改PayTransaction中的状态
        /// </summary>
        /// <param name="parma"></param>
        /// <returns></returns>
        public ExcuteModel ChargePayTransactionState(PayTransaction param)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                if (param != null && !string.IsNullOrEmpty(param.PayTradeID))
                {
                    PayTransaction resultModel = db.PayTransaction.FirstOrDefault(t => t.PayTradeID == param.PayTradeID);
                    if (resultModel != null)
                    {
                        resultModel.PayState = param.PayState;
                        db.SubmitChanges();
                        RetModel.Result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                RetModel.Result = false;
                RetModel.ErrorMessage = "异常信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace;
                LogHelper.WriteErrorLog("ChargePayTransactionState", "异常信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return RetModel;
        }
        #endregion

        #region  机构 支付账号维护
        public ExcuteModel SaveOrganPayAccounts(OrganPayAccount model)
        {
            //List<OrganPayAccount> loglist = new List<OrganPayAccount>();
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                OrganPayAccount _modelExist = db.OrganPayAccount.FirstOrDefault(t => t.PaymentID == model.PaymentID && t.PayAccount == model.PayAccount);
                if (_modelExist != null)
                {
                    _modelExist.PaymentID = model.PaymentID;
                    _modelExist.HospitalID = model.HospitalID;
                    _modelExist.PayAccount = model.PayAccount;
                    _modelExist.Paypassword = model.Paypassword;
                    _modelExist.PayMode = model.PayMode;
                    _modelExist.PayModeNode = model.PayModeNode;
                    _modelExist.PayType = model.PayType;
                    _modelExist.PayTypeNode = model.PayTypeNode;
                    _modelExist.SMSNotification = model.SMSNotification;
                    _modelExist.OrganPhone = model.OrganPhone;
                    _modelExist.OrganContact = model.OrganContact;
                    _modelExist.StartUsing = model.StartUsing;
                    _modelExist.CreatePeople = model.CreatePeople;
                    _modelExist.CreateTime = model.CreateTime;
                    _modelExist.UpdatePeople = model.UpdatePeople;
                    _modelExist.UpdateTime = model.UpdateTime;
                    _modelExist.Standby = model.Standby;
                    _modelExist.Standby1 = model.Standby1;
                    _modelExist.Standby2 = model.Standby2;
                    _modelExist.Standby3 = model.Standby3;
                    _modelExist.Standby4 = model.Standby4;
                    _modelExist.UserID = model.UserID;
                    _modelExist.MDkEY = model.MDkEY;
                    RetModel.RetValue = _modelExist.PaymentID.ToString();
                }
                else
                {
                    db.OrganPayAccount.InsertOnSubmit(model);
                }
                db.SubmitChanges();
                RetModel.Result = true;
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        public List<OrganPayAccount> GetOrganPayAccountList(OrganPayAccount model)
        {
            List<OrganPayAccount> List = new List<OrganPayAccount>();
            try
            {
                db = new RemoteDianoseDataContext();
                string StrSql = @"SELECT  ";
                StrSql += "  [PaymentID], ";
                StrSql += "  [HospitalID], ";
                StrSql += "  [PayAccount], ";
                StrSql += "  [Paypassword], ";
                StrSql += "  [PayModeNode], ";
                StrSql += "  [PayTypeNode], ";
                StrSql += "  [OrganPhone], ";
                StrSql += "  [OrganContact], ";
                StrSql += "  [StartUsing], ";
                StrSql += "  [CreatePeople], ";
                StrSql += "  [CreateTime], ";
                StrSql += "  [UpdatePeople], ";
                StrSql += "  [UpdateTime], ";
                StrSql += "  [Standby], ";
                StrSql += "  [Standby1], ";
                StrSql += "  [Standby2], ";
                StrSql += "  [Standby3], ";
                StrSql += "  [Standby4], ";
                StrSql += "  CASE PayType WHEN '0'   THEN '' WHEN '1'   THEN '微信' WHEN '2'   THEN '支付宝' WHEN '3'   THEN '银联'  ELSE '其他' END as PayType, ";
                StrSql += "  CASE [PayMode] WHEN '0'   THEN '' WHEN '1'   THEN '自动转账' WHEN '2'   THEN '不是自动转账' WHEN '3'   THEN '' ELSE '其他' END as [PayMode], ";
                StrSql += "  CASE [SMSNotification] WHEN '0'   THEN '否' WHEN '1'   THEN '是' WHEN '2'   THEN '' WHEN '3'   THEN ''ELSE '其他' END as [SMSNotification],  ";
                StrSql += " UserID";
                StrSql += " from OrganPayAccount ";
                StrSql += "where 1=1 ";
                if (model != null)
                {
                    if (!string.IsNullOrEmpty(model.HospitalID))
                    {
                        StrSql += " AND HospitalID ='" + model.HospitalID.ToString() + "' ";
                    }
                    if (!string.IsNullOrEmpty(model.PayAccount))
                    {
                        StrSql += " AND PayAccount ='" + model.PayAccount.ToString() + "' ";
                    }
                    if (!string.IsNullOrEmpty(model.PayMode) && model.PayMode != "0")
                    {
                        StrSql += " AND PayMode ='" + model.PayMode + "' ";
                    }
                    if (!string.IsNullOrEmpty(model.PayType) && model.PayType != "0")
                    {
                        StrSql += " AND PayType ='" + model.PayType.ToString() + "' ";
                    }
                    if (!string.IsNullOrEmpty(model.OrganPhone))
                    {
                        StrSql += " AND OrganPhone ='" + model.OrganPhone.ToString() + "' ";
                    }
                    if (!string.IsNullOrEmpty(model.OrganContact))
                    {
                        StrSql += " AND OrganContact ='" + model.OrganContact.ToString() + "' ";
                    }
                    if (!string.IsNullOrEmpty(model.SMSNotification))
                    {
                        StrSql += " AND SMSNotification ='" + model.SMSNotification.ToString() + "' ";
                    }
                    if (!string.IsNullOrEmpty(model.UserID))
                    {
                        StrSql += " AND UserID ='" + model.UserID.ToString() + "' ";
                    }
                    StrSql += " ORDER BY CreateTime DESC";
                }
                List = db.ExecuteQuery<OrganPayAccount>(StrSql).ToList();
            }
            catch (Exception)
            {
            }
            return List;
        }

        public List<OrganPayAccount> GetOneOrganPayAccount(string PaymentID, string HospitalID)
        {
            List<OrganPayAccount> List = new List<OrganPayAccount>();
            try
            {
                db = new RemoteDianoseDataContext();
                string StrSql = @"SELECT *  FROM OrganPayAccount WHERE 1=1 ";
                if (PaymentID != "")
                {
                    StrSql += " AND  PayAccount='" + PaymentID + "'";
                }
                if (HospitalID != "")
                {
                    StrSql += " AND  HospitalID='" + HospitalID + "'";
                }
                List = db.ExecuteQuery<OrganPayAccount>(StrSql).ToList();
            }
            catch (Exception)
            {
            }
            return List;
        }

        /// <summary>
        /// 删除账号
        /// </summary>
        /// <param name="PaymentID"></param>
        /// <returns></returns>
        public int DeleteOrganPayAccount(string PaymentID)
        {
            int result = 0;
            try
            {
                db = new RemoteDianoseDataContext();
                string StrSql = "DELETE FROM OrganPayAccount WHERE PaymentID='" + PaymentID + "'";
                result = db.ExecuteCommand(StrSql);
            }
            catch (Exception)
            {
            }
            return result;
        }
        #endregion

        #region  审核记录表

        public ExcuteModel SaveAuditRecord(AuditRecord Model)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                AuditRecord _model = db.AuditRecord.FirstOrDefault(t => t.RequestId == Model.RequestId && t.DiagnoseDoctor == Model.DiagnoseDoctor && t.ReportConClusion == Model.ReportConClusion && t.ReportDescription == Model.ReportDescription && t.ReportSuggestion == Model.ReportSuggestion);
                if (_model == null)
                {
                    db.AuditRecord.InsertOnSubmit(Model);
                    db.SubmitChanges();
                }
                else
                {

                }
                RetModel.Result = true;
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        public List<AuditRecord> GetAuditRecordByRequestId(string RequestId)
        {
            return db.AuditRecord.Where(t => t.RequestId == RequestId).OrderByDescending(x => x.DiagnoseDate).ToList();
        }
        #endregion

        #region  操作权限组   用户组 维护

        public void ProcessRequest(HttpContext context)
        {
            string DBConnStr = "";
            context.Response.ContentType = "JSON";
            int index = Convert.ToInt32(context.Request["pageIndex"]);
            DBConnStr = System.Configuration.ConfigurationManager.ConnectionStrings["InsideRISConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(DBConnStr);
            con.Open();
            string sql = string.Format("select * from NS_Shop_Discuss");
            string namepro = "xp_GetPage ";
            SqlParameter apa1 = new SqlParameter("@sql", SqlDbType.VarChar, 255);
            apa1.Value = sql;
            SqlParameter apa2 = new SqlParameter("@page", SqlDbType.Int, 64);
            apa2.Value = index;
            SqlParameter apa3 = new SqlParameter("@pageSize", SqlDbType.Int, 64);
            apa3.Value = 5;
            SqlParameter apa4 = new SqlParameter("@totalcount", SqlDbType.Int, 64);
            apa4.Direction = ParameterDirection.Output;
            SqlCommand com = new SqlCommand(namepro, con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add(apa1);
            com.Parameters.Add(apa2);
            com.Parameters.Add(apa3);
            com.Parameters.Add(apa4);
            SqlDataAdapter ad = new SqlDataAdapter();
            ad.SelectCommand = com;
            DataSet da = new DataSet();
            ad.Fill(da);
            DataTable table = new DataTable();
            table = da.Tables[0];
        }

        public List<Operationlimits> GetAllOlimits(string UserID)
        {
            List<Operationlimits> List = new List<Operationlimits>();
            try
            {
                String DBConnStr;
                DataSet MyDataSet = new DataSet();
                System.Data.SqlClient.SqlDataAdapter DataAdapter = new System.Data.SqlClient.SqlDataAdapter();
                DBConnStr = System.Configuration.ConfigurationManager.ConnectionStrings["InsideRISConnectionString"].ConnectionString;
                System.Data.SqlClient.SqlConnection myConnection = new System.Data.SqlClient.SqlConnection(DBConnStr);
                if (myConnection.State != ConnectionState.Open)
                {
                    myConnection.Open();
                }
                System.Data.SqlClient.SqlCommand myCommand = new System.Data.SqlClient.SqlCommand("Olimits", myConnection);
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.Parameters.Add("@UserId", SqlDbType.VarChar);
                myCommand.Parameters["@UserId"].Value = UserID;
                SqlDataAdapter ad = new SqlDataAdapter();
                ad.SelectCommand = myCommand;
                DataSet da = new DataSet();
                ad.Fill(da);
                DataTable table = new DataTable();
                table = da.Tables[0];
                DataSetToList DataSetToList = new DataSetToList();
                List = DataSetToList.DataTableToList<Operationlimits>(table);

                myCommand.ExecuteNonQuery();
                DataAdapter.SelectCommand = myCommand;
                if (MyDataSet != null)
                {
                    DataAdapter.Fill(MyDataSet, "table");
                }

                if (myConnection.State == ConnectionState.Open)
                {
                    myConnection.Close();
                }
            }
            catch (Exception)
            {
                return null;
            }
            return List;
        }

        /// <summary>
        /// 获取用户对报告的操作权限
        /// </summary>
        /// <param name="UserID">用户ID</param>
        /// <param name="GroupID">权限组ID</param>
        /// <returns></returns>
        public List<string> GetAllOlimitsData(string UserID, string GroupID)
        {
            List<string> list = new List<string>();
            List<string> tempList = new List<string>();
            string strTemp = @"SELECT OlimitsId FROM UserGroup ug LEFT JOIN OperationGroup og ON ug.GroupName=og.UGroupId WHERE ug.UserId='{0}'";
            if (!string.IsNullOrEmpty(GroupID))
            {
                strTemp += " AND og.UGroupId='{1}'";
            }
            string strSQL = string.Format(strTemp, UserID, GroupID);
            tempList = db.ExecuteQuery<string>(strSQL).ToList();
            if (tempList.Count > 0)
            {
                list = tempList.FirstOrDefault().Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            }
            strSQL = ReplaceInstance(strSQL, "GetDiagnoseReportList::" + list.Count);
            return list;
        }

        public ExcuteModel SaveOperationlimits(Operationlimits model)
        {
            List<Operationlimits> loglist = new List<Operationlimits>();
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                Operationlimits _modelExist = db.Operationlimits.FirstOrDefault(t => t.OlimitsId == model.OlimitsId);
                if (_modelExist != null)
                {
                    _modelExist.OlimitsId = model.OlimitsId;
                    _modelExist.OName = model.OName;
                    _modelExist.State = model.State;
                    _modelExist.Standby = model.Standby;
                    _modelExist.Remark = model.Remark;
                    RetModel.RetValue = _modelExist.OlimitsId;
                }
                else
                {
                    db.Operationlimits.InsertOnSubmit(model);
                }
                db.SubmitChanges();
                RetModel.Result = true;
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        public List<Operationlimits> GetOperationlimitsList(string Type)
        {
            List<Operationlimits> List = new List<Operationlimits>();
            try
            {
                string[] strTypes = Type.Split(',');
                string types = "";
                for (int k = 0; k < strTypes.Length; k++)
                {
                    types += "'" + strTypes[k] + "',";
                }
                types = types.Substring(0, types.Length - 1);

                db = new RemoteDianoseDataContext();
                string StrSql = @"SELECT * ";
                StrSql += " from  [Operationlimits] ";
                StrSql += "where 1=1 ";
                List = db.ExecuteQuery<Operationlimits>(StrSql).ToList();
            }
            catch (Exception)
            {
                return null;
            }
            return List;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveGroups(Groups model)
        {
            List<Groups> loglist = new List<Groups>();
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                Groups _modelExist = db.Groups.FirstOrDefault(t => t.GroupId == model.GroupId);
                if (_modelExist != null)
                {
                    _modelExist.GroupId = model.GroupId;
                    _modelExist.GroupsName = model.GroupsName;
                    _modelExist.Standby = model.Standby;
                    _modelExist.Standby1 = model.Standby1;
                    RetModel.RetValue = _modelExist.GroupId;
                }
                else
                {
                    db.Groups.InsertOnSubmit(model);
                }
                db.SubmitChanges();
                RetModel.Result = true;
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        public ExcuteModel SaveGroups_ArrayList(Groups model, string ArrayList)
        {
            List<Groups> loglist = new List<Groups>();
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                Groups _modelExist = db.Groups.FirstOrDefault(t => t.GroupId == model.GroupId);
                if (_modelExist != null)
                {
                    _modelExist.GroupId = model.GroupId;
                    _modelExist.GroupsName = model.GroupsName;
                    _modelExist.StartTime = model.StartTime;
                    _modelExist.EndTime = model.EndTime;
                    _modelExist.Standby = model.Standby;
                    _modelExist.Standby1 = model.Standby1;
                    RetModel.RetValue = _modelExist.GroupId;
                }
                else
                {
                    db.Groups.InsertOnSubmit(model);
                }
                db.SubmitChanges();
                RetModel.Result = true;

                string updateSql = @"UPDATE Groups SET Standby1=0 WHERE GroupId !='" + model.GroupId + "'";
                db.ExecuteCommand(updateSql);
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            try
            {
                OperationGroup OperationGroup_List = db.OperationGroup.FirstOrDefault(u => u.UGroupId == model.GroupId);  //当前组的全部权限               
                try
                {
                    OperationGroup _modelExist = db.OperationGroup.FirstOrDefault(t => t.UGroupId == model.GroupId);
                    if (_modelExist != null)
                    {
                        _modelExist.OlimitsId = ArrayList.ToString();
                    }
                    else
                    {
                        OperationGroup mo = new OperationGroup() { OGroupId = Guid.NewGuid().ToString(), OlimitsId = ArrayList.ToString(), UGroupId = model.GroupId };
                        db.OperationGroup.InsertOnSubmit(mo);
                    }
                    db.SubmitChanges();
                    RetModel.Result = true;
                }
                catch (Exception exp)
                {
                    RetModel.Result = false;
                    RetModel.Msg = exp.ToString();
                }
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        public List<Groups> GetGroupsList(string Type)
        {
            List<Groups> List = new List<Groups>();
            try
            {
                string[] strTypes = Type.Split(',');
                string types = "";
                for (int k = 0; k < strTypes.Length; k++)
                {
                    types += "'" + strTypes[k] + "',";
                }
                types = types.Substring(0, types.Length - 1);

                db = new RemoteDianoseDataContext();
                string StrSql = @"SELECT * ";
                StrSql += " from [Groups] ";
                StrSql += "where 1=1 ";
                List = db.ExecuteQuery<Groups>(StrSql).ToList();
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("GetGroupsList", "错误信息：" + ex.Message + "堆栈信息：" + ex.StackTrace);
            }
            return List;
        }

        public ExcuteModel SaveUserGroup(UserGroup model)
        {
            List<UserGroup> loglist = new List<UserGroup>();
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                UserGroup _modelExist = db.UserGroup.FirstOrDefault(t => t.UGroupId == model.UGroupId);
                if (_modelExist != null)
                {
                    _modelExist.UGroupId = model.UGroupId;
                    if (!string.IsNullOrEmpty(model.UserId))
                    {
                        _modelExist.UserId = model.UserId;
                    }
                    if (!string.IsNullOrEmpty(model.GroupName))
                    {
                        _modelExist.GroupName = model.GroupName;
                    }
                    if (!string.IsNullOrEmpty(model.Standby))
                    {
                        _modelExist.Standby = model.Standby;
                    }
                    if (!string.IsNullOrEmpty(model.Describe))
                    {
                        _modelExist.Describe = model.Describe;
                    }
                    if (!string.IsNullOrEmpty(model.Remark))
                    {
                        _modelExist.Remark = model.Remark;
                    }
                    RetModel.RetValue = _modelExist.UGroupId;
                }
                else
                {
                    db.UserGroup.InsertOnSubmit(model);
                }
                db.SubmitChanges();
                RetModel.Result = true;
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        public List<UserGroup> GetUserGroupInfo(string userID)
        {
            return db.UserGroup.Where(o => o.UserId == userID).ToList();
        }

        public List<UserGroup> GetUserGroup_List(string Type)
        {
            List<UserGroup> List = new List<UserGroup>();
            try
            {
                string[] strTypes = Type.Split(',');
                string types = "";
                for (int k = 0; k < strTypes.Length; k++)
                {
                    types += "'" + strTypes[k] + "',";
                }
                types = types.Substring(0, types.Length - 1);

                db = new RemoteDianoseDataContext();
                string StrSql = @"SELECT * ";
                StrSql += " from [UserGroup] ";
                StrSql += "where 1=1 ";
                List = db.ExecuteQuery<UserGroup>(StrSql).ToList();
            }
            catch (Exception)
            {
                return null;
            }
            return List;
        }

        public ExcuteModel SaveOperationGroup(OperationGroup model)
        {
            List<OperationGroup> loglist = new List<OperationGroup>();
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                OperationGroup _modelExist = db.OperationGroup.FirstOrDefault(t => t.OGroupId == model.OGroupId);
                if (_modelExist != null)
                {
                    _modelExist.OGroupId = model.OGroupId;
                    _modelExist.UGroupId = model.UGroupId;
                    _modelExist.OlimitsId = model.OlimitsId;
                    _modelExist.Standby = model.Standby;
                    _modelExist.Standby1 = model.Standby1;
                    _modelExist.Remark = model.Remark;
                    RetModel.RetValue = _modelExist.OGroupId;
                }
                else
                {
                    db.OperationGroup.InsertOnSubmit(model);
                }
                db.SubmitChanges();
                RetModel.Result = true;
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }
        public List<OperationGroup> GetOperationGroupList(string Type)
        {
            List<OperationGroup> List = new List<OperationGroup>();
            try
            {
                string[] strTypes = Type.Split(',');
                string types = "";
                for (int k = 0; k < strTypes.Length; k++)
                {
                    types += "'" + strTypes[k] + "',";
                }
                types = types.Substring(0, types.Length - 1);

                db = new RemoteDianoseDataContext();
                string StrSql = @"SELECT * ";
                StrSql += " from [OperationGroup] ";
                StrSql += "where 1=1 ";
                List = db.ExecuteQuery<OperationGroup>(StrSql).ToList();
            }
            catch (Exception)
            {
                return null;
            }
            return List;
        }

        public List<Operationlimits> GetOneOperationlimits(string Groupid, string olimitid)
        {
            List<Operationlimits> List = new List<Operationlimits>();
            try
            {
                db = new RemoteDianoseDataContext();
                string StrSql = @"select *  ";
                StrSql += " FROM [Operationlimits]  ";
                StrSql += " where 1=1 ";
                StrSql += " AND OlimitsId= '" + olimitid + "' ";
                List = db.ExecuteQuery<Operationlimits>(StrSql).ToList();
            }
            catch (Exception)
            {
                return null;
            }
            return List;
        }

        public List<GroupOperation> GetOneGroupOperation(string Groupid, string olimitid)
        {
            List<GroupOperation> List = new List<GroupOperation>();
            try
            {
                db = new RemoteDianoseDataContext();
                string StrSql = @"SELECT g.GroupId,g.GroupsName,g.[Standby],g.Standby1,g.StartTime,g.EndTime,og.OlimitsId ";
                StrSql += " FROM [Groups]  AS g ";
                StrSql += " INNER JOIN [OperationGroup] AS og ";
                StrSql += " ON og.UGroupId=g.GroupId  ";
                StrSql += " WHERE 1=1 ";
                StrSql += " AND GroupId= '" + Groupid + "' ";
                List = db.ExecuteQuery<GroupOperation>(StrSql).ToList();
            }
            catch (Exception)
            {
                return null;
            }
            return List;
        }

        public List<UserGroup> GetOneusergroup(string Groupid, string UserId)
        {
            List<UserGroup> List = new List<UserGroup>();
            try
            {
                db = new RemoteDianoseDataContext();
                string StrSql = @"select *  ";
                StrSql += "  from [UserGroup]  ";
                StrSql += " where 1=1 ";
                StrSql += " AND UserId= '" + UserId + "' ";
                List = db.ExecuteQuery<UserGroup>(StrSql).ToList();
            }
            catch (Exception)
            {
                return null;
            }
            return List;
        }
        #endregion


        #region     //获取当前医院的全部信息

        public List<SubHospital> GetAllSubHospital(string PHospitalId, string name, string txt_name)
        {
            List<SubHospital> ExpertConsultationRecord_model;
            try
            {
                SystemManageDataContext db = new SystemManageDataContext();
                List<SubHospital> List = new List<SubHospital>();
                string StrSql = " select SubHospitalId ";
                StrSql += " ,PHospitalId ";
                StrSql += " ,SHname ";
                StrSql += " ,SHaddr ";
                StrSql += " ,SHphone ";
                StrSql += " ,SHurl ";
                StrSql += " ,SHcontact ";
                StrSql += ", SHprovince ";
                StrSql += " ,SHlogo ";
                StrSql += " ,SHdescription ";
                StrSql += " ,Standby ";
                StrSql += " ,Standby1 ";
                StrSql += " ,Standby2 ";
                StrSql += " ,Standby3 ";
                StrSql += " ,Standby4 ";
                StrSql += " ,Standby5 ";
                StrSql += " ,Standby6 ";
                StrSql += " ,Standby7 ";
                StrSql += " ,CreateTime ";
                if (name == "1")
                {
                    StrSql += " FROM SubHospital as SubHospital inner join Sys_Dept  as Sys_Dept ";
                    StrSql += " on SubHospital.PHospitalId=Sys_Dept.DeptId ";
                }
                else if (name == "2")
                {
                    StrSql += " FROM SubHospital";
                }
                else
                {
                    StrSql += " FROM SubHospital";
                }
                StrSql += " where  1=1 ";

                if (name == "2")
                {
                    if (PHospitalId.Trim() != "")
                    {
                        StrSql += "and SubHospitalId ='" + PHospitalId + "'";
                    }
                }
                else
                {
                    if (PHospitalId.Trim() != "")
                    {
                        StrSql += "and PHospitalId ='" + PHospitalId + "'";
                    }
                }
                if (txt_name.Trim() != "")
                {
                    StrSql += "and SHname like '%" + txt_name + "%'  or SubHospitalId like '%" + txt_name + "%'  or SHcontact like '%" + txt_name + "%'";
                }
                ExpertConsultationRecord_model = db.ExecuteQuery<SubHospital>(StrSql).ToList();
                return ExpertConsultationRecord_model;
            }
            catch (Exception)
            {
                return null;
            }
        }

        #endregion

        #region   SubHospital  子医院维护
        /// <summary>
        /// 保存 PatientInfo
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveSubHospital_relevance(SubHospital model)
        {
            List<SubHospital> loglist = new List<SubHospital>();
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                SubHospital _modelExist = db.SubHospital.FirstOrDefault(t => t.SubHospitalId == model.SubHospitalId);
                if (_modelExist != null)
                {
                    _modelExist.PHospitalId = model.PHospitalId;
                    RetModel.RetValue = _modelExist.PHospitalId;
                }
                else
                {
                    db.SubHospital.InsertOnSubmit(model);
                }
                db.SubmitChanges();
                RetModel.Result = true;
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        /// <summary>
        /// 保存 PatientInfo
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveSubHospital(SubHospital model)
        {
            List<SubHospital> loglist = new List<SubHospital>();
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                SubHospital _modelExist = db.SubHospital.FirstOrDefault(t => t.SubHospitalId == model.SubHospitalId);
                if (_modelExist != null)
                {
                    _modelExist.PHospitalId = model.PHospitalId;
                    _modelExist.SHname = model.SHname;
                    _modelExist.SHaddr = model.SHaddr;
                    _modelExist.SHcontact = model.SHcontact;
                    _modelExist.SHdescription = model.SHdescription;
                    _modelExist.SHlogo = model.SHlogo;
                    _modelExist.SHphone = model.SHphone;
                    _modelExist.SHprovince = model.SHprovince;
                    _modelExist.SHurl = model.SHurl;
                    _modelExist.Standby = model.Standby;
                    _modelExist.Standby1 = model.Standby1;
                    _modelExist.Standby2 = model.Standby2;
                    _modelExist.Standby3 = model.Standby3;
                    _modelExist.Standby4 = model.Standby4;
                    _modelExist.Standby5 = model.Standby5;
                    _modelExist.Standby6 = model.Standby6;
                    _modelExist.Standby7 = model.Standby7;
                    RetModel.RetValue = _modelExist.PHospitalId;
                }
                else
                {
                    db.SubHospital.InsertOnSubmit(model);
                }
                db.SubmitChanges();
                RetModel.Result = true;
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        /// <summary>
        /// 根据医院获取相关中心医院
        /// </summary>
        /// <param name="strHospitalID"></param>
        /// <returns></returns>
        public List<SubHospital> GetSubHospitalList(string strHospitalID)
        {
            List<SubHospital> list = new List<SubHospital>();
            list = db.SubHospital.Where(o => o.SubHospitalId == strHospitalID).ToList();
            return list;
        }

        #endregion

        #region  clearcanvas

        //public List<string> StudyFolder()
        //{
        //    return ImageServerContext.StudyFolder();
        //}

        //public List<string> GetStudyFolder(string paatientid, string studyInstanceUid)
        //{
        //    return ImageServerContext.GetStudyFolder(paatientid, studyInstanceUid);
        //}
        //public string GetRoot()
        //{
        //    return ImageServerContext.GetRoot();
        //}

        //public List<Study> GetStudy()
        //{
        //    return ImageServerContext.GetStudy();
        //}

        ///// <summary>
        ///// 获取病人的信息
        ///// </summary>
        ///// <param name="patientId"></param>
        ///// <returns></returns>
        //public List<Study> GetStudyByPatientId(string patientId, string studyInstanceUID)
        //{
        //    return ImageServerContext.GetStudyByPatientId(patientId, studyInstanceUID);
        //}

        //public List<Patient_requestDetail> GetPatient_requestDetail(string patientId, string HospitalId, string tB__SHname_txt, string D_SH_type_txt)
        //{
        //    return ImageServerContext.GetPatient_requestDetail(patientId, HospitalId, tB__SHname_txt, D_SH_type_txt);
        //}

        //查询当前医生已审核的记录
        public List<Patient_requestDetail> GetDoctors_AuditRecords(string patientId, string HospitalId, string ReviewStatus, string CPatientName, string DiagnoseDoctor)
        {
            db = new RemoteDianoseDataContext();
            int D_SH_type_txt_int = Convert.ToInt32(ReviewStatus.ToString() == "" ? 0 : Convert.ToInt32(ReviewStatus.ToString()));
            string StrSql = @"SELECT ";
            StrSql += "RequestInfo.RequestId";
            StrSql += ",PatientInfo.PatientName";
            StrSql += ",PatientInfo.CPatientName";
            StrSql += ",PatientInfo.PatientAge";
            StrSql += ",PatientInfo.PatientSex";
            StrSql += ",ISNULL(RequestInfo.ReviewStatus,0) as ReviewStatus";
            StrSql += ",RequestInfo.AccessionNumber";
            StrSql += ",RequestInfo.DiagnoseDoctor";
            StrSql += ",ISNULL(RequestInfo.RequestJDSJ,'')as RequestJDSJ ";
            StrSql += ",Sys_Dept.DeptDisplayName ";
            StrSql += "FROM [RemoteDianose].[dbo].[PatientInfo]  as PatientInfo ";
            StrSql += "inner join [RemoteDianose].[dbo].[RequestInfo] as RequestInfo ";
            StrSql += "on  PatientInfo.PatientId=RequestInfo.PatientId ";
            StrSql += "inner join  [RemoteDianose].[dbo].[RefusedRecord] as RefusedRecord On ";
            StrSql += "RefusedRecord.RequestId=RequestInfo.RequestId ";
            StrSql += "INNER JOIN [RemoteDianose].[dbo].[Sys_Dept]  AS Sys_Dept ";
            StrSql += "ON Sys_Dept.DeptId=RequestInfo.HospitalId";
            StrSql += " where DiagnoseDoctor='" + DiagnoseDoctor + "' ";
            if (HospitalId.Trim() != "")
            {
                StrSql += "and  RequestInfo.HospitalId='" + HospitalId + "'";
            }
            if (patientId.Trim() != "")
            {
                StrSql += "and  PatientInfo.PatientId LIKE '%" + patientId + "%'";
            }
            if (ReviewStatus.Trim() != "")
            {
                StrSql += "and  RequestInfo.ReviewStatus='" + D_SH_type_txt_int + "'";
            }
            if (CPatientName.Trim() != "")
            {
                StrSql += "and PatientInfo.PatientName like '%" + CPatientName + "%'  or PatientInfo.CPatientName like '%" + CPatientName + "%'";
            }
            StrSql = ReplaceInstance(StrSql, "GetDoctors_AuditRecords：");
            List<Patient_requestDetail> list = db.ExecuteQuery<Patient_requestDetail>(StrSql).ToList();
            return list;
        }

        // //已写报告 待审核
        public List<Patient_requestDetail> Getwrite_Report(string patientId, string HospitalId, string ReviewStatus, string CPatientName, string DiagnoseDoctor)
        {
            db = new RemoteDianoseDataContext();
            int D_SH_type_txt_int = Convert.ToInt32(ReviewStatus.ToString() == "" ? 0 : Convert.ToInt32(ReviewStatus.ToString()));
            string StrSql = @"SELECT ";
            StrSql += "RequestInfo.RequestId";
            StrSql += ",PatientInfo.PatientName";
            StrSql += ",PatientInfo.CPatientName";
            StrSql += ",PatientInfo.PatientAge";
            StrSql += ",PatientInfo.PatientSex";
            StrSql += ",ISNULL(RequestInfo.ReviewStatus,0) as ReviewStatus";
            StrSql += ",RequestInfo.AccessionNumber";
            StrSql += ",RequestInfo.DiagnoseDoctor";
            StrSql += ",ISNULL(RequestInfo.RequestJDSJ,'')as RequestJDSJ ";
            StrSql += ",Diagnose.DiagnoseDoctor";
            StrSql += ",Diagnose.DiagnoseDate  ";
            StrSql += "FROM [PatientInfo]  as PatientInfo ";
            StrSql += "inner join [RequestInfo] as RequestInfo ";
            StrSql += "on  PatientInfo.PatientId=RequestInfo.PatientId ";
            StrSql += "inner join [Diagnose] as [Diagnose]";
            StrSql += "on Diagnose.RequestId=RequestInfo.RequestId ";
            StrSql += "where RequestState='15'  and RequestInfo.ReviewStatus=0   ";
            if (HospitalId.Trim() != "")
            {
                StrSql += "and  RequestInfo.HospitalId='" + HospitalId + "'";
            }
            if (patientId.Trim() != "")
            {
                StrSql += "and  PatientInfo.PatientId LIKE '%" + patientId + "%'";
            }
            if (ReviewStatus.Trim() != "")
            {
                StrSql += "and  RequestInfo.ReviewStatus='" + D_SH_type_txt_int + "'";
            }
            if (CPatientName.Trim() != "")
            {
                StrSql += "and PatientInfo.PatientName like '%" + CPatientName + "%'  or PatientInfo.CPatientName like '%" + CPatientName + "%'";
            }
            List<Patient_requestDetail> list = db.ExecuteQuery<Patient_requestDetail>(StrSql).ToList();
            return list;
        }

        // //已写报告 待审核
        public List<Patient_requestDetail> Alreadypaycost_Nowrite_Report(string patientId, string HospitalId, string ReviewStatus, string CPatientName, string DiagnoseDoctor)
        {
            db = new RemoteDianoseDataContext();
            int D_SH_type_txt_int = Convert.ToInt32(ReviewStatus.ToString() == "" ? 0 : Convert.ToInt32(ReviewStatus.ToString()));
            string StrSql = @"SELECT ";
            StrSql += "RequestInfo.RequestId";
            StrSql += ",PatientInfo.PatientName";
            StrSql += ",PatientInfo.CPatientName";
            StrSql += ",PatientInfo.PatientAge";
            StrSql += ",PatientInfo.PatientSex";
            StrSql += ",ISNULL(RequestInfo.ReviewStatus,0) as ReviewStatus";
            StrSql += ",RequestInfo.AccessionNumber";
            StrSql += ",RequestInfo.DiagnoseDoctor";
            StrSql += ",ISNULL(RequestInfo.RequestJDSJ,'')as RequestJDSJ ";
            StrSql += "FROM [PatientInfo]  as PatientInfo  ";
            StrSql += "inner join [RequestInfo] as RequestInfo ";
            StrSql += "on  PatientInfo.PatientId=RequestInfo.PatientId ";
            StrSql += "inner join [ChargeRecord] as  [ChargeRecord] on ";
            StrSql += " ChargeRecord.RequestId=RequestInfo.RequestId ";
            StrSql += "where ChargeRecord.ChargeState='1'    and  RequestState='11' ";
            if (HospitalId.Trim() != "")
            {
                StrSql += "and  RequestInfo.HospitalId='" + HospitalId + "'";
            }
            if (patientId.Trim() != "")
            {
                StrSql += "and  PatientInfo.PatientId LIKE '%" + patientId + "%'";
            }
            if (ReviewStatus.Trim() != "")
            {
                StrSql += "and  RequestInfo.ReviewStatus='" + D_SH_type_txt_int + "'";
            }
            if (CPatientName.Trim() != "")
            {
                StrSql += "and PatientInfo.PatientName like '%" + CPatientName + "%'  or PatientInfo.CPatientName like '%" + CPatientName + "%'";
            }
            List<Patient_requestDetail> list = db.ExecuteQuery<Patient_requestDetail>(StrSql).ToList();
            return list;
        }

        /// <summary>
        /// 查询未缴费患者列表，外网环境下使用
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <param name="name"></param>
        /// <param name="patientId"></param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        public List<RequestStudy> GetRequestStudyList(string hospitalID, string name, string patientId, int? costState, DateTime dtStart, DateTime dtEnd)
        {
            List<RequestStudy> list = new List<RequestStudy>();
            try
            {
                string StrSql = @"SELECT [GUID],[HospitalID],[ServerPartitionGUID] ,[StudyStorageGUID],[PatientGUID] ,[SpecificCharacterSet]
                             ,[StudyInstanceUid],[PatientsName],[PatientId],[IssuerOfPatientId],[PatientsBirthDate],[PatientsAge],[PatientsSex]
                             ,[StudyDate],[StudyTime],[AccessionNumber],[StudyId],[StudyDescription],[NumberOfStudyRelatedSeries]
                             ,[NumberOfStudyRelatedInstances],[StudySizeInKB],[ResponsiblePerson],[ResponsibleOrganization]
                             ,[QueryXml],[QCStatusEnum],[QCOutput],[QCUpdateTimeUtc]
                             ,[CostState] FROM [RemoteDianose].[dbo].[RequestStudy] AS S  WHERE  [StudySizeInKB] IS NOT NULL ";
                if (dtStart != Convert.ToDateTime("1900-01-01"))
                {
                    StrSql += " AND  CONVERT(datetime,StudyDate)>='" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                }
                if (dtEnd != Convert.ToDateTime("1900-01-01"))
                {
                    StrSql += " AND  CONVERT(datetime,StudyDate)<='" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                }
                if (!string.IsNullOrEmpty(name))
                {
                    StrSql += " AND  PatientsName LIKE '%" + name + "%'";
                }
                if (!string.IsNullOrEmpty(patientId))
                {
                    StrSql += " AND PatientId LIKE '%" + patientId + "%'";
                }
                if (!string.IsNullOrEmpty(hospitalID))
                {
                    StrSql += " AND HospitalID = '" + hospitalID + "'";
                }
                if (costState != null)
                {
                    StrSql += " AND CostState = " + costState;
                }
                StrSql += "ORDER BY StudyDate DESC";
                StrSql = ReplaceInstance(StrSql, "GetRequestStudyList");
                list = db.ExecuteQuery<RequestStudy>(StrSql).ToList();
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("查询GetRequestStudyList列表异常", "异常信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return list;
        }

        public PatientInfo GetOnePatientInfo(string PatientId)
        {
            db = new RemoteDianoseDataContext();
            PatientInfo model = new PatientInfo();
            model = db.PatientInfo.FirstOrDefault(t => t.PatientId == PatientId);
            return model;
        }

        #endregion

        #region   模板下载

        public ExcuteModel Setdefault_template(string userid, string dept_id, string downloadT_ID, string template_type)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                SystemManageDataContext db = new SystemManageDataContext();
                string StrSql_two = "update [PatientVisit].[dbo].[Download_Template] set Default_T=0 where [PatientVisit].[dbo].[Download_Template].Standby = '" + template_type + "'";
                StrSql_two += " and Dept_ID='" + dept_id + "'";
                int num1 = db.ExecuteCommand(StrSql_two);
                string StrSql = "update  [PatientVisit].[dbo].[Download_Template] set Default_T=1 where Template_ID='" + downloadT_ID + "' ";
                StrSql += " and Dept_ID='" + dept_id + "' ";
                StrSql += " and [PatientVisit].[dbo].[Download_Template].Standby='" + template_type + "' ";
                int num = db.ExecuteCommand(StrSql);
                if (num != 0)
                {
                    RetModel.Result = true;
                }
                else
                {
                    RetModel.Result = false;
                }
            }
            catch (Exception exp)
            {
                RetModel.Result = false; RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        //得到单个数据详情
        public Download_Template GetOneDownloadTemplate(string Template_ID)
        {
            int DiagnoseTip_IDINT = Convert.ToInt32(Template_ID);
            db = new RemoteDianoseDataContext();
            Download_Template model = new Download_Template();
            model = DbDataContext.Download_Template.FirstOrDefault(t => t.Template_ID == DiagnoseTip_IDINT);
            return model;
        }

        public Download_Template GetONEDownload_Template_model(Download_Template _model)
        {

            db = new RemoteDianoseDataContext();
            Download_Template model = new Download_Template();
            model = DbDataContext.Download_Template.FirstOrDefault(t => t.downloadT_ID == _model.downloadT_ID);
            return model;
        }

        //查询
        public List<Download_Template> GetAllDownload_TemplateList_model(string userid, string dept_id)
        {
            db = new RemoteDianoseDataContext();
            List<Download_Template> list = new List<Download_Template>();
            list = DbDataContext.Download_Template.Where(u => u.UserId == userid && u.Dept_ID == dept_id).ToList();
            return list;
        }

        //查询
        public List<Download_Template> GetAllDownload_TemplateList()
        {
            db = new RemoteDianoseDataContext();
            List<Download_Template> list = new List<Download_Template>();
            list = DbDataContext.Download_Template.ToList();
            return list;
        }

        //保存
        public ExcuteModel SaveDownload_Template(Download_Template model)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                Download_Template _model = DbDataContext.Download_Template.FirstOrDefault(t => t.downloadT_ID == model.downloadT_ID);
                if (_model != null)
                {
                    _model.UserId = model.UserId;
                    _model.Dept_ID = model.Dept_ID;
                    _model.Template_ID = model.Template_ID;
                    _model.Default_T = model.Default_T;
                    _model.Down_time = model.Down_time;
                    _model.Standby1 = model.Standby1;
                    _model.Standby2 = model.Standby2;
                    _model.Down_operation = model.Down_operation;
                }
                else
                {
                    DbDataContext.Download_Template.InsertOnSubmit(model);
                }
                DbDataContext.SubmitChanges();
                RetModel.Result = true;
            }
            catch (Exception exp)
            {
                RetModel.Result = false; RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        #endregion

        #region  专家会诊记录
        public List<ExpertConsultationRecord> ExpertRecord(string ServiceId, string dept_id, string DiagnoseDoctor, string txtname)
        {
            List<ExpertConsultationRecord> ExpertConsultationRecord_model;
            try
            {
                SystemManageDataContext db = new SystemManageDataContext();
                List<ExpertConsultationRecord> List = new List<ExpertConsultationRecord>();
                string StrSql = " select ";
                StrSql += "  RequestInfo.RequestId, ";
                StrSql += "  ISNULL(RequestInfo.RequestDate,'') as RequestDate, ";
                StrSql += "  PatientInfo.PatientName, ";
                StrSql += "  PatientInfo.CPatientName, ";
                StrSql += "  case PatientInfo.PatientSex when 'M' THEN '男' WHEN 'F'  THEN '女' ELSE '其他' END  AS  PatientSex, ";
                StrSql += "  PatientInfo.PatientAge, ";
                StrSql += "  RequestInfo.Cost, ";
                StrSql += "  Sys_DiagnoseService.ServiceName, ";
                StrSql += "  Sys_Dept.DeptDisplayName, ";
                StrSql += "  Diagnose.ReportDescription, ";
                StrSql += "  Diagnose.ReportConClusion, ";
                StrSql += "  Diagnose.ReportSuggestion, ";
                StrSql += "  Diagnose.DiagnoseDate ";
                StrSql += " from  [RemoteDianose].[dbo].[PatientInfo] as PatientInfo  inner join  [RemoteDianose].[dbo].[RequestInfo] as RequestInfo ";
                StrSql += " on  PatientInfo.PatientId=RequestInfo.PatientId   inner join [RemoteDianose].[dbo].[Sys_Dept]  as  Sys_Dept ";
                StrSql += " on  Sys_Dept.DeptId= RequestInfo.HospitalId  inner join  [RemoteDianose].[dbo].[Diagnose] as Diagnose ";
                StrSql += " on   Diagnose.RequestId=RequestInfo.RequestId  ";
                StrSql += " left join  [RemoteDianose].[dbo].[ChargeRecord]  as ChargeRecord  ";
                StrSql += " on ChargeRecord.RequestId=RequestInfo.RequestId   left join  [RemoteDianose].[dbo].[Sys_DiagnoseService] as Sys_DiagnoseService  ";
                StrSql += " on Sys_DiagnoseService.ServiceId=ChargeRecord.ServeiceType ";

                StrSql += " where  RequestInfo.DiagnoseDoctor='" + DiagnoseDoctor + "' ";
                StrSql += " and RequestInfo.RequestState='15' ";
                if (ServiceId.Trim() != "")
                {
                    StrSql += "and Sys_DiagnoseService.ServiceId ='" + ServiceId + "'";
                }
                if (dept_id.Trim() != "")
                {
                    StrSql += "and Sys_DiagnoseService.HospitalId ='" + dept_id + "'";
                }
                if (txtname.Trim() != "")
                {
                    StrSql += "and PatientInfo.PatientName LIKE '%" + txtname + "%'";
                }
                StrSql += "Order By Diagnose.DiagnoseDate Desc";
                StrSql = ReplaceInstance(StrSql, "ExpertRecord：");
                ExpertConsultationRecord_model = db.ExecuteQuery<ExpertConsultationRecord>(StrSql).ToList();
                return ExpertConsultationRecord_model;
            }
            catch (Exception)
            {
                return null;
            }
        }
        #endregion

        #region   目录菜单维护
        /// <summary>
        /// 查询
        /// </summary>
        /// <returns></returns>
        public List<DiagnoseType> GetAllDiagnoseType()
        {
            db = new RemoteDianoseDataContext();
            List<DiagnoseType> list = new List<DiagnoseType>();
            list = db.DiagnoseType.ToList();
            return list;
        }

        public DiagnoseType GetONEDiagnoseType(string DiagnoseType_ID)
        {
            int DiagnoseType_IDINT = Convert.ToInt32(DiagnoseType_ID);
            db = new RemoteDianoseDataContext();
            DiagnoseType model = new DiagnoseType();
            model = db.DiagnoseType.FirstOrDefault(t => t.ID == DiagnoseType_IDINT);
            return model;
        }

        public ExcuteModel DelDiagnoseTypeName(DiagnoseType model)
        {
            string model_ID = model.ID.ToString();
            ExcuteModel retModel = new ExcuteModel();
            try
            {
                DiagnoseType delmodel = db.DiagnoseType.First(t => t.ID == model.ID);
                db.DiagnoseType.DeleteOnSubmit(delmodel);
                db.SubmitChanges();
                retModel.Result = true;
            }
            catch (Exception exp)
            {
                retModel.Result = false;
                retModel.Msg = exp.ToString();
            }
            return retModel;
        }

        /// <summary>
        /// 保存 RequestInfo
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveDiagnoseTypeName(DiagnoseType model)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                DiagnoseType _model = db.DiagnoseType.FirstOrDefault(t => t.ID == model.ID);
                if (_model != null)
                {
                    _model.ID = model.ID;
                    var isExist = db.DiagnoseType.Where(t => t.DiagnoseTypeName == model.DiagnoseTypeName).FirstOrDefault();
                    if (isExist == null)
                    {
                        if (!string.IsNullOrEmpty(model.DiagnoseTypeName))
                        {
                            _model.DiagnoseTypeName = model.DiagnoseTypeName;
                        }
                        if (!string.IsNullOrEmpty(model.Param))
                        {
                            _model.Param = model.Param;
                        }
                        if (!string.IsNullOrEmpty(model.description))
                        {
                            _model.description = model.description;
                        }
                    }
                    else if (isExist != null && isExist.DiagnoseTypeName == model.DiagnoseTypeName)
                    {
                        _model.Param = model.Param;
                        _model.description = model.description;
                    }
                }
                else
                {
                    model.CreateTime = DateTime.Now;
                    db.DiagnoseType.InsertOnSubmit(model);
                }
                db.SubmitChanges();
                RetModel.Result = true;
            }
            catch (Exception exp)
            {
                RetModel.Result = false; RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        public ExcuteModel JudgeDiagnoseTypeName(DiagnoseType model)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                DiagnoseType _model = db.DiagnoseType.FirstOrDefault(t => t.DiagnoseTypeName == model.DiagnoseTypeName);
                if (_model != null)
                {
                    RetModel.Result = true;
                }
                else
                {
                    RetModel.Result = false;
                }
            }
            catch (Exception exp)
            {
                RetModel.Result = false; RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        #region 报告模板维护
        /// <summary>
        /// 模板操作
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel SaveDiagnoseTemp(DiagnoseTemp param)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                DiagnoseTemp dtModel = db.DiagnoseTemp.FirstOrDefault(t => t.RootID == param.RootID);
                if (dtModel != null)
                {
                    if (!string.IsNullOrEmpty(param.TemplateMC))
                    {
                        dtModel.TemplateMC = param.TemplateMC;
                    }
                    if (param.NodeTypes > 0)
                    {
                        dtModel.NodeTypes = param.NodeTypes;
                    }
                    if (!string.IsNullOrEmpty(param.Remark))
                    {
                        dtModel.Remark = param.Remark;
                    }
                    if (param.Orderbys > 0)
                    {
                        dtModel.Orderbys = param.Orderbys;
                    }
                    if (param.NodeOrder > 0)
                    {
                        dtModel.NodeOrder = param.NodeOrder;
                    }
                    if (param.ParentID == -1 || param.ParentID > 0)
                    {
                        dtModel.ParentID = param.ParentID;
                    }
                    RetModel.Msg = "修改成功！";
                }
                else
                {
                    param.DelNode = 0;
                    db.DiagnoseTemp.InsertOnSubmit(param);
                    RetModel.Msg = "添加成功！";
                }
                db.SubmitChanges();
                RetModel.Result = true;
            }
            catch (Exception ex)
            {
                RetModel.Result = false;
                RetModel.Msg = ex.ToString();
            }
            return RetModel;
        }

        /// <summary>
        /// 保存模板内容信息
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel SaveDiagnoseTempContent(DiagnoseTemp param)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                DiagnoseTemp dtModel = db.DiagnoseTemp.FirstOrDefault(t => t.RootID == param.RootID);
                if (dtModel != null)
                {
                    if (!string.IsNullOrEmpty(param.RadioActivity))
                    {
                        dtModel.RadioActivity = param.RadioActivity;
                    }
                    if (!string.IsNullOrEmpty(param.RadioDiagnose))
                    {
                        dtModel.RadioDiagnose = param.RadioDiagnose;
                    }
                    if (!string.IsNullOrEmpty(param.TemplateMC))
                    {
                        dtModel.TemplateMC = param.TemplateMC;
                    }
                    db.SubmitChanges();
                    RetModel.Result = true;
                    RetModel.Msg = "保存成功！";
                }
            }
            catch (Exception ex)
            {
                RetModel.Result = false;
                RetModel.Msg = ex.Message + "--堆栈信息：" + ex.StackTrace;
                LogHelper.WriteErrorLog("", "错误信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return RetModel;
        }

        /// <summary>
        /// 删除节点
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel DelDiagnoseTempNode(DiagnoseTemp param)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                DiagnoseTemp dtModel = db.DiagnoseTemp.FirstOrDefault(t => t.RootID == param.RootID);
                if (dtModel != null)
                {
                    dtModel.DelNode = param.DelNode;
                    if (param.DelNode == 1)
                    {
                        RetModel.Msg = "节点已删除";
                    }
                    else
                    {
                        RetModel.Msg = "节点已开启";
                    }
                    RetModel.Result = true;
                }
                else
                {
                    RetModel.Result = false;
                    RetModel.Msg = "未找到节点";
                }
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("DelDiagnoseTempNode", "错误信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return RetModel;
        }

        /// <summary>
        /// 获取单个模板内容
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public DiagnoseTemp GetDiagnoseTemp(DiagnoseTemp param)
        {
            DiagnoseTemp di = db.DiagnoseTemp.Where(o => o.RootID == param.RootID).FirstOrDefault();
            return di;
        }

        /// <summary>
        /// 根据父节点，计算相应排序最大值
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public int GetParentOrderbyValue(int param, string strName)
        {
            int getValue = 0;
            if (string.IsNullOrEmpty(strName))
            {
                int typeValue = db.DiagnoseTemp.Where(o => o.ParentID == param && o.CreatePeople == null).Count();
                if (typeValue > 0)
                {
                    getValue = db.DiagnoseTemp.Where(o => o.ParentID == param && o.CreatePeople == null).Max(z => z.Orderbys);
                }
            }
            else
            {
                int typeValue = db.DiagnoseTemp.Where(o => o.ParentID == param && o.CreatePeople == strName).Count();
                if (typeValue > 0)
                {
                    getValue = db.DiagnoseTemp.Where(o => o.ParentID == param && o.CreatePeople == strName).Max(z => z.Orderbys);
                }
            }
            return getValue + 1;
        }

        /// <summary>
        /// 获取父节点值
        /// </summary>
        /// <returns></returns>
        public int GetPatientIdValue()
        {
            int patientID = 0;
            patientID = db.DiagnoseTemp.Max(o => o.ParentID);
            return patientID + 1;
        }

        /// <summary>
        /// 判断是否有根节点
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public int GetPatientNodeCout(int param)
        {
            int patientCount = 0;
            patientCount = db.DiagnoseTemp.Where(o => o.ParentID == param).Count();
            return patientCount;
        }

        /// <summary>
        /// 查询主节点和二级节点，排序为降序的公共模板
        /// </summary>
        /// <returns></returns>
        public List<DiagnoseTemp> GetDiagnoseTempPatient(int isPublic, string userName)
        {
            List<DiagnoseTemp> list = new List<DiagnoseTemp>();
            string StrSql = @"SELECT RootID,ParentID,TemplateMC,NodeTypes,NodeOrder,Orderbys FROM DiagnoseTemp
                              WHERE DelNode=0   AND ISPublic=" + isPublic;
            if (!string.IsNullOrEmpty(userName))
            {
                StrSql += " AND CreatePeople='" + userName + "'";
            }
            StrSql += " ORDER BY Orderbys ASC";
            list = db.ExecuteQuery<DiagnoseTemp>(StrSql).ToList();
            return list;
        }

        /// <summary>
        ///  获取单个节点数据
        /// </summary>
        /// <param name="param">选中节点</param>
        /// <param name="nodeTypeParam">节点类型</param>
        /// <returns></returns>
        public List<DiagnoseTemp> GetDiagnoseTempNodeData(int param, int nodeTypeParam, int nodeOrderParam, int isPublic, string userName)
        {
            List<DiagnoseTemp> list = new List<DiagnoseTemp>();
            List<DiagnoseTemp> liste = new List<DiagnoseTemp>();
            string StrSqls = @"SELECT RootID,ParentID,TemplateMC,NodeTypes,NodeOrder,Orderbys FROM DiagnoseTemp
                              WHERE DelNode=0 AND ISPublic=" + isPublic + " AND NodeTypes>1 AND NodeTypes =" + (nodeTypeParam + 1) +
                              " AND ParentID=" + param;
            if (!string.IsNullOrEmpty(userName))
            {
                StrSqls += " AND CreatePeople='" + userName + "'";
            }
            StrSqls += " ORDER BY Orderbys ASC";

            list = db.ExecuteQuery<DiagnoseTemp>(StrSqls).ToList();

            string StrSqle = @"SELECT * FROM (SELECT RootID,ParentID,TemplateMC,NodeTypes,NodeOrder,Orderbys, ROW_NUMBER() OVER(PARTITION BY ParentID ORDER BY RootID) AS DD FROM DiagnoseTemp
                              WHERE DelNode=0 AND ISPublic=" + isPublic + "  AND NodeTypes =" + (nodeTypeParam + 2);
            if (!string.IsNullOrEmpty(userName))
            {
                StrSqle += " AND CreatePeople='" + userName + "'";
            }
            StrSqle += " AND ParentID IN (SELECT RootID FROM DiagnoseTemp WHERE NodeTypes>1 AND NodeTypes =" + (nodeTypeParam + 1) + " AND ParentID=" + param + ") ) AA WHERE DD=1 ORDER BY Orderbys ASC";
            liste = db.ExecuteQuery<DiagnoseTemp>(StrSqle).ToList();
            list.AddRange(liste);
            return list;
        }

        /// <summary>
        /// 查询选中节点下的后两级
        /// </summary>
        /// <param name="paramList"></param>
        /// <param name="param"></param>
        /// <param name="temp"></param>
        /// <returns></returns>
        public List<DiagnoseTemp> RepeatNode(List<DiagnoseTemp> paramList, int param, int nodeTypeParam, List<DiagnoseTemp> temp)
        {
            List<DiagnoseTemp> parentList = new List<DiagnoseTemp>();
            List<DiagnoseTemp> getValue = paramList.Where(o => o.ParentID == param).ToList();
            //if (getValue.Count > 0)
            //{
            //    foreach (DiagnoseTemp item in getValue)
            //    {
            //        if (item.NodeTypes != 1 && item.NodeTypes <= nodeTypeParam + 2)
            //        {
            //            temp.Add(item);
            //        }
            //        RepeatNode(paramList, item.RootID, nodeTypeParam, temp);
            //    }
            //}
            getValue.ForEach(o =>
            {
                parentList.Add(o);
                temp = parentList;
            });
            parentList.ForEach(z =>
            {
                var model = paramList.Where(o => o.ParentID == z.RootID).FirstOrDefault();
                if (model != null)
                {
                    temp.Add(model);
                }
            });
            return temp;
        }

        /// <summary>
        /// 获取自动编号
        /// </summary>
        /// <returns></returns>
        public int GetDiagnoseTempRootID()
        {
            int intRootID = 0;
            intRootID = db.DiagnoseTemp.Max(o => o.RootID);
            return intRootID;
        }

        /// <summary>
        /// 获取父节点排序值，为查询做准备
        /// </summary>
        /// <returns></returns>
        public int GetParentNodeOrderValue()
        {
            int intNodeOrder = 0;
            var temp = db.DiagnoseTemp.Where(o => o.ParentID == -1).ToList();
            if (temp != null && temp.Count > 0)
            {
                intNodeOrder = temp.Max(z => z.NodeOrder);
            }
            return intNodeOrder;
        }

        /// <summary>
        /// 获取DiagnsoeTemp表数据总数
        /// </summary>
        /// <returns></returns>
        public int GetDiagnoseTempCount()
        {
            int dtCount = 0;
            dtCount = db.DiagnoseTemp.Count();
            return dtCount;
        }

        /// <summary>
        /// 拖动节点修改值
        /// </summary>
        /// <param name="rootID"></param>
        /// <param name="nodeTypes"></param>
        /// <param name="pRootID"></param>
        /// <param name="pNodeTypes"></param>
        /// <param name="pNodeOrder"></param>
        /// <param name="strTempName"></param>
        public void UpdateDiagnoseTemp(int rootID, int nodeTypes, int pRootID, int pNodeTypes, int pNodeOrder, string strTempName)
        {
            int orderbyValue = GetParentOrderbyValue(pRootID, strTempName);
            SaveDiagnoseTemp(new DiagnoseTemp
            {
                RootID = rootID,
                ParentID = pRootID,
                NodeTypes = pNodeTypes + 1,
                Orderbys = orderbyValue,
                NodeOrder = pNodeOrder
            });

            if (pNodeTypes + 1 != nodeTypes)
            {
                GetChildNodes(rootID, pNodeTypes + 1, pRootID, pNodeOrder, strTempName);
            }
        }

        private void changNodeValue(int dID, int dOrder, int pID, int pOrder)
        {
            SaveDiagnoseTemp(new DiagnoseTemp
            {
                RootID = dID,
                Orderbys = dOrder,
            });
            SaveDiagnoseTemp(new DiagnoseTemp
            {
                RootID = pID,
                Orderbys = pOrder,
            });
        }

        private void GetChildNodes(int rootID, int nodeTypes, int pRootID, int nodeOrder, string strTempName)
        {
            List<DiagnoseTemp> parentNode = db.DiagnoseTemp.Where(o => o.ParentID == rootID).ToList();
            foreach (DiagnoseTemp node in parentNode)
            {
                int rootId = node.RootID;
                String t = node.TemplateMC;
                int tempNodeTypes = node.NodeTypes;
                int orderbyValue = GetParentOrderbyValue(rootID, strTempName);
                SaveDiagnoseTemp(new DiagnoseTemp
                {
                    NodeTypes = nodeTypes + 1,
                    RootID = node.RootID,
                    Orderbys = orderbyValue + 1,
                    NodeOrder = nodeOrder,
                });

                if (nodeTypes + 1 < tempNodeTypes)
                {
                    tempNodeTypes = tempNodeTypes - 1;
                }
                else
                {
                    tempNodeTypes = tempNodeTypes + 1;
                }
                GetChildNodes(rootId, tempNodeTypes, pRootID, nodeOrder, strTempName);
            }
        }
        #endregion

        #endregion

        /// <summary>
        /// 获取申请单列表
        /// </summary>
        /// <param name="Type">1：已申请，2已诊断</param>
        /// <returns></returns>
        public List<OutRqeustInfo> GetRequestList(string Type, string HospitalId, string FeeState, DateTime dtStart, DateTime dtEnd, string PatientName, string PatientId, int smallAge, int bigAge, string Sex, string accessionNumber, string RequestDoctor, string requestJDR, string PatientType, string ExamineType)
        {
            List<OutRqeustInfo> List = new List<OutRqeustInfo>();
            try
            {
                string[] FeeTypes = FeeState.Split(',');
                string[] strTypes = Type.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                string types = "";
                string feeTypes = "";
                if (strTypes.Length > 0)
                {
                    for (int k = 0; k < strTypes.Length; k++)
                    {
                        types += "'" + strTypes[k] + "',";
                    }
                    types = types.Substring(0, types.Length - 1);
                }

                for (int k = 0; k < FeeTypes.Length; k++)
                {
                    feeTypes += "'" + FeeTypes[k] + "',";
                }
                feeTypes = feeTypes.Substring(0, feeTypes.Length - 1);
                db = new RemoteDianoseDataContext();
                string StrSql = @"SELECT b.PatientAge,a.PatientId,CASE
                                    WHEN a.HospitalId IS NULL THEN
	                                    a.PatientId
                                    ELSE
	                                    replace(
		                                    a.PatientId,
		                                    '.' + a.HospitalId,
		                                    ''
	                                    )
                                    END AS NewPatientId,b.PatientMobile,ISNULL(a.FeeState,0)as FeeState, ISNULL(a.RequestState,0) as RequestState,a.RequestId,
                                  a.RequestJDSJ,b.PatientIdentityType,RequestJDR,RequestDoctor,
                                  CONVERT(varchar(100),a.RequestDate,20) AS RequestDate,a.ExaminePart,a.ExamineMethod,b.OutPatientNum,b.RegisterNum,
                                  b.PatientSex,PatientBirth,BirthTime,PatientWeight,LXRDZ, c.DeptDisplayName AS HospitalName,b.PatientName,b.CPatientName,
                                  ISNULL(SD.Name,'') AS SDDoctor,ISNULL(SR.Name,'') AS SRDoctor,ISNULL(SRP.Name,'') AS SRPDoctor,
                                  a.AccessionNumber,a.ExamineDate,a.DiagnoseDoctor,
                                  CONVERT(varchar(100),a.DiagnoseDate, 20) AS DiagnoseDate, ISNULL(d.UpLoadFlag,0) AS UpLoadFlag,
                                  ISNULL(d.UpLoadCount,0) AS UpLoadCount,ISNULL(d.DownFlag,0) AS DownFlag,
                                  ISNULL(d.DownCount,0) AS DownCount,ISNULL(d.UpLoadPerson,'') AS UpLoadPerson,
                                  ISNULL(d.DownPerson,'') AS DownPerson,a.Diagnosticstate,ISNULL(a.ReviewStatus,0) AS ReviewStatus,
                                  a.ReportState,a.ReportPerson,StudyInstanceUid, a.RequestPurpose,a.ServeiceType as ServiceItem,b.PatientIdentityNum,
                                  ISNULL(a.NumberOfStudyRelatedSeries,0) as NumberOfStudyRelatedSeries,ISNULL(a.NumberOfStudyRelatedInstances,0) as NumberOfStudyRelatedInstances,
                                  a.StudySizeInKB,a.PatientType,a.UrgentType,a.Remark,ISNULL(a.UploadWay,0) as UploadWay,ExamineType,ISNULL(a.IsPrint,0) as IsPrint,
                                  DiseaseSummary,ClinicalDiagnose,a.DeviceId,DE.DeviceName
                                  FROM  RequestInfo AS a
                                  LEFT JOIN  PatientInfo AS b ON a.PatientId=b.PatientId 
                                  LEFT JOIN Sys_Dept  AS c ON a.HospitalId=c.DeptId 
                                  LEFT JOIN FileUpDownRecord AS d ON a.RequestId=d.RequestID  
                                  LEFT JOIN Sys_Users AS SR ON SR.UserName=a.RequestDoctor
                                  LEFT JOIN Sys_Users AS SD ON SD.UserName=a.DiagnoseDoctor
                                  LEFT JOIN Sys_Users AS SRP ON SRP.UserName=a.ReportPerson
                                  LEFT JOIN Sys_Device DE ON DE.DeviceId=A.DeviceId
                                  WHERE 1=1 "; //  LEFT JOIN ChargeRecord AS e ON a.RequestId=e.RequestId    CONVERT(varchar(16),d.ChargeDate, 25)as ChargeDate,  left join  ChargeRecord as d on a.RequestId=d.RequestId
                if (!string.IsNullOrEmpty(types))
                {
                    StrSql += " AND  a.RequestState in(" + types + ")";
                }
                if (PatientName.Trim() != "")
                {
                    StrSql += " AND  (b.CPatientName like'%" + PatientName + "%' OR  b.PatientName like'%" + PatientName + "%') ";
                }
                StrSql += " AND  a.FeeState in(" + feeTypes + ")";
                if (HospitalId != "")
                {
                    StrSql += " AND  a.HospitalId='" + HospitalId + "'";
                }
                if (PatientId != "")
                {
                    StrSql += " AND  a.PatientId LIKE '%" + PatientId + "%'";
                }
                if (smallAge > 0)
                {
                    StrSql += " AND b.PatientAge>=" + smallAge;
                }
                if (bigAge > 0)
                {
                    StrSql += " AND b.PatientAge<=" + bigAge;
                }
                if (!string.IsNullOrEmpty(accessionNumber))
                {
                    StrSql += " AND a.AccessionNumber LIKE '%" + accessionNumber + "%'";
                }
                if (!string.IsNullOrEmpty(requestJDR))
                {
                    StrSql += " AND (a.RequestJDR='" + requestJDR + "')";
                }
                if (!string.IsNullOrEmpty(RequestDoctor))
                {
                    StrSql += " AND (a.RequestDoctor='" + RequestDoctor + "'OR a.RequestDoctor IS NULL)";
                }
                if (!string.IsNullOrEmpty(Sex) && (Sex != "qb" && Sex != "qxz"))
                {
                    StrSql += " And b.PatientSex='" + Sex + "'";
                }
                if (!string.IsNullOrEmpty(ExamineType) && ExamineType != "全部")
                {
                    StrSql += " AND a.ExamineType='" + ExamineType + "'";
                }
                if (!string.IsNullOrEmpty(PatientType) && PatientType != "全部")
                {
                    StrSql += " AND a.PatientType='" + PatientType + "'";
                }
                if (Type.Length == 1 && Type.Contains("9"))
                {
                    if (dtStart != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSql += " AND  a.RefuseDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                    }
                    if (dtEnd != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSql += " AND  a.RefuseDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                    }
                }
                else
                {
                    if (dtStart != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSql += " AND  a.RequestJDSJ>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                    }
                    if (dtEnd != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSql += " AND  a.RequestJDSJ<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                    }
                }
                StrSql = ReplaceInstance(StrSql, "GetRequestList");
                List = db.ExecuteQuery<OutRqeustInfo>(StrSql).ToList();
            }
            catch (Exception exp)
            {
                LogHelper.WriteErrorLog("查询申请端信息GetRequestList", "异常信息：" + exp.Message + "--堆栈信息：" + exp.StackTrace);
            }
            return List;
        }

        /// <summary>
        /// 当前医生已审核的列表
        /// </summary>
        /// <param name="Type">1：已申请，2已诊断</param>
        /// <returns></returns>
        public List<OutRqeustInfo> GetRequestList_YSH(string Type, string HospitalId, string FeeState, DateTime dtStart, DateTime dtEnd, string PatientName, string PatientId, string Doctor_Name)
        {
            List<OutRqeustInfo> List = new List<OutRqeustInfo>();
            try
            {
                string[] FeeTypes = FeeState.Split(',');
                string[] strTypes = Type.Split(',');
                string types = "";
                string feeTypes = "";
                for (int k = 0; k < strTypes.Length; k++)
                {
                    types += "'" + strTypes[k] + "',";
                }
                types = types.Substring(0, types.Length - 1);

                for (int k = 0; k < FeeTypes.Length; k++)
                {
                    feeTypes += "'" + FeeTypes[k] + "',";
                }
                feeTypes = feeTypes.Substring(0, feeTypes.Length - 1);
                db = new RemoteDianoseDataContext();
                string StrSql = @"SELECT b.PatientAge,a.PatientId,CASE
                                    WHEN a.HospitalId IS NULL THEN
	                                    a.PatientId
                                    ELSE
	                                    replace(
		                                    a.PatientId,
		                                    '.' + a.HospitalId,
		                                    ''
	                                    )
                                    END AS NewPatientId,ISNULL(a.FeeState,0)as FeeState, ISNULL(a.RequestState,0) as RequestState,a.RequestId,a.RequestJDSJ,
                                  b.PatientSex,CONVERT(varchar(16),b.PatientBirth, 23) AS PatientBirth, c.DeptDisplayName AS HospitalName,b.PatientName,b.CPatientName,
                                  a.AccessionNumber,a.ExamineDate,
                                  CONVERT(varchar(16),a.DiagnoseDate, 20) AS DiagnoseDate, ISNULL(d.UpLoadFlag,0) AS UpLoadFlag,
                                  ISNULL(d.UpLoadCount,0) AS UpLoadCount,ISNULL(d.DownFlag,0) AS DownFlag,
                                  ISNULL(d.DownCount,0) AS DownCount,ISNULL(d.UpLoadPerson,'') AS UpLoadPerson,
                                  ISNULL(d.DownPerson,'') AS DownPerson,a.Diagnosticstate,ISNULL(a.ReviewStatus,0) AS ReviewStatus,
                                  a.ReportState,a.ReportPerson
                                  FROM  RequestInfo AS a
                                  left join  PatientInfo AS b ON a.PatientId=b.PatientId 
                                  left join Sys_Dept  AS c ON a.HospitalId=c.DeptId 
                                  LEFT JOIN FileUpDownRecord AS d ON a.RequestId=d.RequestID  "; // CONVERT(varchar(16),d.ChargeDate, 25)as ChargeDate,  left join  ChargeRecord as d on a.RequestId=d.RequestId
                StrSql += " inner join RefusedRecord as [RefusedRecord] on [RefusedRecord].RequestId=a.RequestId ";
                StrSql += " WHERE 1=1 ";
                StrSql += " and  a.RequestState in(" + types + ")";
                StrSql += " and  a.FeeState in(" + feeTypes + ")";
                StrSql += " and  DiagnoseDoctor = '" + Doctor_Name + "' ";
                if (HospitalId != "")
                {
                    StrSql += "and  a.HospitalId='" + HospitalId + "'";
                }
                if (PatientId != "")
                {
                    StrSql += "and  a.PatientId LIKE '%" + PatientId + "%'";
                }
                if (dtStart != Convert.ToDateTime("1900-01-01"))
                {
                    StrSql += "and  a.RequestJDSJ>='" + dtStart + "'";
                }
                if (dtEnd != Convert.ToDateTime("1900-01-01"))
                {
                    StrSql += "and  a.RequestJDSJ<='" + dtEnd + "'";
                }
                if (PatientName.Trim() != "")
                {
                    StrSql += "and  b.CPatientName like'%" + PatientName + "%' or  b.PatientName like'%" + PatientName + "%' ";
                }
                StrSql = ReplaceInstance(StrSql, "GetRequestList_YSH:");
                List = db.ExecuteQuery<OutRqeustInfo>(StrSql).ToList();
            }
            catch (Exception)
            {
            }
            return List;
        }

        /// <summary>
        /// 除自己的报告以外       待审核的    审核的列表
        /// </summary>
        /// <param name="Type">1：已申请，2已诊断</param>
        /// <returns></returns>
        public List<OutRqeustInfo> GetRequestList_DSH(string Type, string HospitalId, string FeeState, DateTime dtStart, DateTime dtEnd, string PatientName, string PatientId, string Doctor_Name)
        {
            List<OutRqeustInfo> List = new List<OutRqeustInfo>();
            try
            {
                string[] FeeTypes = FeeState.Split(',');
                string[] strTypes = Type.Split(',');
                string types = "";
                string feeTypes = "";
                for (int k = 0; k < strTypes.Length; k++)
                {
                    types += "'" + strTypes[k] + "',";
                }
                types = types.Substring(0, types.Length - 1);

                for (int k = 0; k < FeeTypes.Length; k++)
                {
                    feeTypes += "'" + FeeTypes[k] + "',";
                }
                feeTypes = feeTypes.Substring(0, feeTypes.Length - 1);
                db = new RemoteDianoseDataContext();
                string StrSql = @"SELECT b.PatientAge,a.PatientId,CASE
                                    WHEN a.HospitalId IS NULL THEN
	                                    a.PatientId
                                    ELSE
	                                    replace(
		                                    a.PatientId,
		                                    '.' + a.HospitalId,
		                                    ''
	                                    )
                                    END AS NewPatientId,ISNULL(a.FeeState,0)as FeeState, ISNULL(a.RequestState,0) as RequestState,a.RequestId,a.RequestJDSJ,
                                  b.PatientSex,CONVERT(varchar(16),b.PatientBirth, 23) AS PatientBirth, c.DeptDisplayName AS HospitalName,b.PatientName,b.CPatientName,
                                  a.AccessionNumber,a.ExamineDate,
                                  CONVERT(varchar(16),a.DiagnoseDate, 20) AS DiagnoseDate, ISNULL(d.UpLoadFlag,0) AS UpLoadFlag,
                                  ISNULL(d.UpLoadCount,0) AS UpLoadCount,ISNULL(d.DownFlag,0) AS DownFlag,
                                  ISNULL(d.DownCount,0) AS DownCount,ISNULL(d.UpLoadPerson,'') AS UpLoadPerson,
                                  ISNULL(d.DownPerson,'') AS DownPerson,a.Diagnosticstate,ISNULL(a.ReviewStatus,0) AS ReviewStatus,
                                  a.ReportState,a.ReportPerson
                                  FROM  RequestInfo AS a
                                  left join  PatientInfo AS b ON a.PatientId=b.PatientId 
                                  left join [RemoteDianose].[dbo].[Sys_Dept]  AS c ON a.HospitalId=c.DeptId 
                                  LEFT JOIN FileUpDownRecord AS d ON a.RequestId=d.RequestID  ";
                StrSql += " inner join  [RemoteDianose].[dbo].[Diagnose]   as [Diagnose] on [Diagnose].RequestId=a.RequestId";
                StrSql += " WHERE 1=1 ";
                StrSql += " and   ReviewStatus='0'  and RequestState='15' ";
                StrSql += " and  a.RequestState in(" + types + ")";
                StrSql += " and  a.FeeState in(" + feeTypes + ")";
                if (HospitalId != "")
                {
                    StrSql += "and  a.HospitalId='" + HospitalId + "'";
                }
                if (PatientId != "")
                {
                    StrSql += "and  a.PatientId LIKE '%" + PatientId + "%'";
                }
                if (dtStart != Convert.ToDateTime("1900-01-01"))
                {
                    StrSql += "and  a.RequestJDSJ>='" + dtStart + "'";
                }
                if (dtEnd != Convert.ToDateTime("1900-01-01"))
                {
                    StrSql += "and  a.RequestJDSJ<='" + dtEnd + "'";
                }
                if (PatientName.Trim() != "")
                {
                    StrSql += "and  b.CPatientName like'%" + PatientName + "%' or  b.PatientName like'%" + PatientName + "%' ";
                }
                StrSql = ReplaceInstance(StrSql, "GetRequestList_DSH::");
                List = db.ExecuteQuery<OutRqeustInfo>(StrSql).ToList();
            }
            catch (Exception)
            {
            }
            return List;
        }

        public List<OutRqeustInfo> GetPayInfoList_alllist(string Type, string HospitalId, string FeeState, DateTime dtStart, DateTime dtEnd, string PatientName, string PatientId)
        {
            List<OutRqeustInfo> List = new List<OutRqeustInfo>();
            try
            {
                string[] FeeTypes = FeeState.Split(',');
                string[] strTypes = Type.Split(',');
                string types = "";
                string feeTypes = "";
                for (int k = 0; k < strTypes.Length; k++)
                {
                    types += "'" + strTypes[k] + "',";
                }
                types = types.Substring(0, types.Length - 1);

                for (int k = 0; k < FeeTypes.Length; k++)
                {
                    feeTypes += "'" + FeeTypes[k] + "',";
                }
                feeTypes = feeTypes.Substring(0, feeTypes.Length - 1);
                db = new RemoteDianoseDataContext();
                string StrSql = @"SELECT P.PatientAge,R.PatientId,CASE
                                    WHEN R.HospitalId IS NULL THEN
	                                    R.PatientId
                                    ELSE
	                                    replace(
		                                    R.PatientId,
		                                    '.' + R.HospitalId,
		                                    ''
	                                    )
                                    END AS NewPatientId,P.PatientMobile,R.RequestId,P.PatientSex,R.DiagnoseDoctor,S.DeptDisplayName AS HospitalName,CONVERT(varchar(16),R.RequestDate, 20) AS RequestDate,
                                   P.PatientName,P.CPatientName,R.AccessionNumber,R.ReportState,R.ReportPerson,R.StudyInstanceUid,
                                   ISNULL(R.FeeState,0) AS FeeState,ISNULL(R.RequestState,0) AS RequestState,R.RequestPurpose,
                                   ISNULL(R.ReviewStatus,0) AS ReviewStatus,R.Diagnosticstate,ISNULL(SRP.Name,'') AS SRPDoctor, ISNULL(SRD.Name,'') AS SRDoctor,CONVERT(varchar(16),P.PatientBirth,23) AS PatientBirth,
                                   CONVERT(varchar(16),R.DiagnoseDate,20) AS DiagnoseDate,R.ExamineDate,R.ExaminePart,R.ExamineMethod                        
                                   ,P.PatientName,P.CPatientName,P.PatientAge,R.PatientId,R.RequestId,P.PatientSex,P.PatientIdentityNum,
                                    ISNULL(R.FeeState,0)as FeeState, ISNULL(R.RequestState,0) as RequestState,
                                    ISNULL(R.ReviewStatus,0) AS ReviewStatus,R.HospitalId,
                                    R.Serveicetype AS ServiceItem,R.RequestJDSJ                  
                                   FROM RequestInfo AS R
                                   LEFT JOIN  PatientInfo P ON R.PatientId=P.PatientId
                                   LEFT JOIN Sys_Dept S ON R.HospitalId=S.DeptId
                                   LEFT JOIN Sys_Users AS SRP ON SRP.UserName=R.ReportPerson
                                   LEFT JOIN Sys_Users AS SRD ON SRD.UserName=R.RequestDoctor
                                   WHERE R.Diagnosticstate='Consultation' ";
                if (Type != "")
                {
                    StrSql += " AND  R.RequestState in(" + types + ")";
                }
                if (HospitalId != "")
                {
                    StrSql += " AND R.HospitalId='" + HospitalId + "'";
                }
                if (dtStart != Convert.ToDateTime("1900-01-01"))
                {
                    StrSql += " AND  R.RequestJDSJ>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                }
                if (dtEnd != Convert.ToDateTime("1900-01-01"))
                {
                    StrSql += " AND  R.RequestJDSJ<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                }
                if (PatientName.Trim() != "")
                {
                    StrSql += " AND  P.CPatientName like'%" + PatientName + "%' OR P.PatientName like'%" + PatientName + "%' ";
                }
                StrSql += " ORDER BY R.RequestJDSJ ASC";
                StrSql = ReplaceInstance(StrSql, "GetPayInfoList_alllist::");
                List = db.ExecuteQuery<OutRqeustInfo>(StrSql).ToList();
            }
            catch (Exception)
            {
            }
            return List;
        }

        /// <summary>
        /// 收费数据
        /// </summary>
        /// <param name="Type"></param>
        /// <param name="HospitalId"></param>
        /// <param name="FeeState"></param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <param name="PatientName"></param>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        public List<OutPayInfo> GetPayInfoList(string Type, string HospitalId, string FeeState, DateTime dtStart, DateTime dtEnd, string PatientName, string PatientId)
        {
            List<OutPayInfo> List = new List<OutPayInfo>();
            try
            {
                string[] FeeTypes = FeeState.Split(',');
                string[] strTypes = Type.Split(',');
                string types = "";
                string feeTypes = "";
                for (int k = 0; k < strTypes.Length; k++)
                {
                    types += "'" + strTypes[k] + "',";
                }
                types = types.Substring(0, types.Length - 1);

                for (int k = 0; k < FeeTypes.Length; k++)
                {
                    feeTypes += "'" + FeeTypes[k] + "',";
                }
                feeTypes = feeTypes.Substring(0, feeTypes.Length - 1);

                db = new RemoteDianoseDataContext();
                string StrSql = @"SELECT b.PatientName,b.CPatientName,b.PatientAge,a.PatientId,a.RequestId,b.PatientSex,b.PatientIdentityNum,
                                  ISNULL(a.FeeState,0)as FeeState, ISNULL(a.RequestState,0) as RequestState,ISNULL(a.UploadWay,0) as UploadWay,
                                  ISNULL(a.ReviewStatus,0) AS ReviewStatus,a.HospitalId,StudyInstanceUid,
                                  a.Serveicetype AS ServiceItem, CONVERT(varchar(16),a.RequestJDSJ,20) AS RequestJDSJ
                                  FROM  RequestInfo AS a
                                  LEFT JOIN  PatientInfo AS b ON a.PatientId=b.PatientId 
                                  WHERE 1=1 ";
                StrSql += " AND  a.RequestState in(" + types + ")";
                StrSql += " AND  a.FeeState in(" + feeTypes + ")";
                if (HospitalId != "") { StrSql += " AND  a.HospitalId='" + HospitalId + "'"; }
                if (dtStart != Convert.ToDateTime("1900-01-01")) { StrSql += " AND  a.RequestJDSJ>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)"; }
                if (dtEnd != Convert.ToDateTime("1900-01-01")) { StrSql += " AND  a.RequestJDSJ<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)"; }
                if (PatientName.Trim() != "") { StrSql += " AND  b.CPatientName like'%" + PatientName + "%' OR  b.PatientName like'%" + PatientName + "%' "; }
                StrSql += " ORDER BY a.RequestJDSJ DESC";
                StrSql = ReplaceInstance(StrSql, "GetPayInfoList::");
                List = db.ExecuteQuery<OutPayInfo>(StrSql).ToList();
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("GetPayInfoList", "错误信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return List;
        }

        /// <summary>
        /// 添加血管神经病学科数据查询
        /// </summary>
        /// <param name="Type"></param>
        /// <param name="HospitalId"></param>
        /// <param name="FeeState"></param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <param name="PatientName"></param>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        public List<OutRqeustInfo> GetConDiscussionInfoList(string Type, string HospitalId, string FeeState, string dtStart, string dtEnd, string PatientName, string PatientId, string strUserID = "", string ExaminePosition = "", string ExamineMethod = "", string IdentityNum = "", string Sex = "")
        {
            List<OutRqeustInfo> List = new List<OutRqeustInfo>();
            try
            {
                string[] FeeTypes = FeeState.Split(',');
                string[] strTypes = Type.Split(',');
                string types = "";
                string feeTypes = "";
                string HospitalIdmap = string.Empty;
                for (int k = 0; k < strTypes.Length; k++)
                {
                    types += "'" + strTypes[k] + "',";
                }
                types = types.Substring(0, types.Length - 1);

                for (int k = 0; k < FeeTypes.Length; k++)
                {
                    feeTypes += "'" + FeeTypes[k] + "',";
                }
                feeTypes = feeTypes.Substring(0, feeTypes.Length - 1);
                db = new RemoteDianoseDataContext();
                string StrSql = @"SELECT 
	                                    CASE
                                    WHEN a.HospitalId IS NULL THEN
	                                    a.PatientId
                                    ELSE
	                                    replace(
		                                    a.PatientId,
		                                    '.' + a.HospitalId,
		                                    ''
	                                    )
                                    END AS PatientId,
                                     b.PatientName,
                                     b.CPatientName,
                                     b.PatientAge,
                                     a.PatientId as NewPatientId,
                                     a.RequestId,
                                     b.PatientSex,
                                     b.PatientIdentityNum,
                                     ISNULL(a.FeeState, 0) AS FeeState,
                                     ISNULL(a.RequestState, 0) AS RequestState,
                                     ISNULL(a.ReviewStatus, 0) AS ReviewStatus,
                                     a.HospitalId,
                                     ExamineMethod,
                                     ExaminePart,
                                     RequestJDSJ,
                                     a.StudyInstanceUid AS StudyInstanceUid,
                                     a.ExamineDate,
                                     sd.DeptDisplayName AS HospitalName
                                    FROM
	                                    RequestInfo AS a
                                    LEFT JOIN PatientInfo AS b ON a.PatientId = b.PatientId
                                    LEFT JOIN Sys_Dept sd ON a.HospitalId = sd.deptID
                                    WHERE
	                                    Diagnosticstate = 'Neuro' ";
                if (string.IsNullOrEmpty(types))
                {
                    StrSql += " AND  a.RequestState in(" + types + ")";
                }
                if (string.IsNullOrEmpty(feeTypes))
                {
                    StrSql += " AND  a.FeeState in(" + feeTypes + ")";
                }
                if (!string.IsNullOrEmpty(IdentityNum))
                {
                    StrSql += " AND b.PatientIdentityNum LIKE '%" + IdentityNum + "%' ";
                }
                if (!string.IsNullOrEmpty(Sex))
                {
                    StrSql += " AND b.PatientSex ='" + Sex + "' ";
                }
                if (!string.IsNullOrEmpty(HospitalId))
                {
                    var list = db.SubHospital.Where(u => u.PHospitalId == HospitalId).ToList();
                    if (list != null)
                    {
                        foreach (var u in list)
                        {
                            HospitalIdmap += "'" + u.SubHospitalId.ToString() + "',";
                        }
                        HospitalIdmap = HospitalIdmap + "'" + HospitalId + "'";
                    }
                    else
                    {
                        HospitalIdmap = "'" + HospitalId + "'";
                    }
                }
                if (!string.IsNullOrEmpty(strUserID))
                {
                    string strTemp = string.Empty;
                    strTemp = GetStringHospital(strUserID);
                    if (!string.IsNullOrEmpty(strTemp))
                    {
                        HospitalIdmap = strTemp;
                    }
                }
                if (!string.IsNullOrEmpty(HospitalIdmap))
                {
                    StrSql += " AND  a.HospitalId in(" + HospitalIdmap + ")";
                }
                if (!string.IsNullOrEmpty(dtStart))
                {
                    StrSql += " AND  a.RequestJDSJ>=CONVERT(varchar(100),'" + dtStart + "', 23)";
                }
                if (!string.IsNullOrEmpty(dtEnd))
                {
                    StrSql += " AND  a.RequestJDSJ<=CONVERT(varchar(100),'" + dtEnd + " 23:59:59',23)";
                }
                if (PatientName.Trim() != "")
                {
                    StrSql += " AND b.CPatientName like'%" + PatientName + "%'";
                }
                if (!string.IsNullOrEmpty(ExaminePosition))
                {
                    StrSql += " AND a.ExaminePart='" + ExaminePosition + "'";
                }
                if (!string.IsNullOrEmpty(ExamineMethod))
                {
                    StrSql += " AND  a.ExamineMethod ='" + ExamineMethod + "'";
                }
                StrSql += " ORDER BY a.RequestJDSJ DESC";
                StrSql = ReplaceInstance(StrSql, "GetConDiscussionInfoList::");
                List = db.ExecuteQuery<OutRqeustInfo>(StrSql).ToList();
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("GetConDiscussionInfoList", "异常信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return List;
        }

        /// <summary>
        ///  添加血管神经病学科数据查询（使用存储过程分页）
        /// </summary>
        /// <param name="RequestStates"></param>
        /// <param name="HospitalId"></param>
        /// <param name="FeeState"></param>
        /// <param name="PageIndex"></param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <param name="PatientName"></param>
        /// <param name="PatientId"></param>
        /// <param name="pageCount"></param>
        /// <param name="patientCount"></param>
        /// <returns></returns>
        public List<OutRqeustInfo> GetConDiscussionPatientList(string RequestStates, string HospitalId, string FeeState, string strUserID, int PageIndex, string dtStart, string dtEnd, string PatientName, string PatientId, string strSex, int intAge, out int pageCount, out int patientCount)
        {
            List<OutRqeustInfo> List = new List<OutRqeustInfo>();
            pageCount = 0;
            patientCount = 0;
            string StrSql = string.Empty;
            string HospitalIdmap = string.Empty;
            try
            {
                string strField = string.Empty;
                string strTable = string.Empty;
                string strCondition = string.Empty;
                string strRequestState = string.Empty;
                string strFeeState = string.Empty;
                string[] RequestStateArr = RequestStates.Split(',');
                string[] FeeTypes = FeeState.Split(',');
                int pageSize = 20;
                string orderByType = string.Empty;
                for (int k = 0; k < RequestStateArr.Length; k++)
                {
                    strRequestState += "'" + RequestStateArr[k] + "',";
                }
                strRequestState = strRequestState.Substring(0, strRequestState.Length - 1);

                for (int k = 0; k < FeeTypes.Length; k++)
                {
                    strFeeState += "'" + FeeTypes[k] + "',";
                }
                strFeeState = strFeeState.Substring(0, strFeeState.Length - 1);
                #region 参数
                if (!string.IsNullOrEmpty(strRequestState))
                {
                    strCondition += " AND  a.RequestState in(" + strRequestState + ")";
                }
                if (!string.IsNullOrEmpty(strFeeState))
                {
                    strCondition += " AND  a.FeeState in(" + strFeeState + ")";
                }
                if (!string.IsNullOrEmpty(HospitalId))
                {
                    var list = db.SubHospital.Where(u => u.PHospitalId == HospitalId).ToList();
                    if (list != null)
                    {
                        foreach (var u in list)
                        {
                            HospitalIdmap += "'" + u.SubHospitalId.ToString() + "',";
                        }
                        HospitalIdmap = HospitalIdmap + "'" + HospitalId + "'";
                    }
                    else
                    {
                        HospitalIdmap = "'" + HospitalId + "'";
                    }
                }
                if (!string.IsNullOrEmpty(strUserID))
                {
                    string strTemp = string.Empty;
                    strTemp = GetStringHospital(strUserID);
                    if (!string.IsNullOrEmpty(strTemp))
                    {
                        HospitalIdmap = strTemp;
                    }
                }
                if (!string.IsNullOrEmpty(HospitalIdmap))
                {
                    strCondition += " AND  a.HospitalId in(" + HospitalIdmap + ")";
                }
                if (!string.IsNullOrEmpty(dtStart))
                {
                    strCondition += " AND  CONVERT(varchar(100),a.RequestJDSJ, 23)>='" + dtStart + "'";
                }
                if (!string.IsNullOrEmpty(dtEnd))
                {
                    strCondition += " AND  CONVERT(varchar(100),a.RequestJDSJ, 23)<='" + dtEnd + "'";
                }
                if (!string.IsNullOrEmpty(PatientName))
                {
                    strCondition += " AND  (b.CPatientName like'%" + PatientName.Trim() + "%' OR  b.PatientName like'%" + PatientName.Trim() + "%') ";
                }
                if (!string.IsNullOrEmpty(PatientId))
                {
                    strCondition += " AND  b.PatientId like'%" + PatientId.Trim() + "%'";
                }
                if (!string.IsNullOrEmpty(strSex))
                {
                    strCondition += " AND  b.PatientSex= '" + strSex.Trim() + "'";
                }
                if (intAge > 0)
                {
                    strCondition += " AND b.PatientAge=" + intAge;
                }
                orderByType += " a.RequestJDSJ";
                LogHelper.WriteInfoLog("GetConDiscussionPatientList，条件：" + strCondition + "--orderByType:" + orderByType);
                #endregion
                DataSet ds = new DataSet();
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["InsideRISConnectionString"].ToString()))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.Parameters.Add(new SqlParameter("@Condition", SqlDbType.VarChar, 1000));
                    cmd.Parameters.Add(new SqlParameter("@OrderFile", SqlDbType.VarChar, 50));
                    cmd.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.VarChar, 10));
                    cmd.Parameters.Add(new SqlParameter("@PageIndex", SqlDbType.VarChar, 10));

                    SqlParameter paramPageCount = new SqlParameter("@PageCount", SqlDbType.Int);
                    SqlParameter paramPateitnCount = new SqlParameter("@PatientCount", SqlDbType.Int);
                    paramPageCount.Direction = ParameterDirection.Output;
                    paramPateitnCount.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(paramPageCount);
                    cmd.Parameters.Add(paramPateitnCount);

                    cmd.Parameters[0].Value = strCondition;
                    cmd.Parameters[1].Value = orderByType;
                    cmd.Parameters[2].Value = pageSize;
                    cmd.Parameters[3].Value = PageIndex;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "DiscussionPatientList";

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;

                    adapter.Fill(ds);

                    List = DataSetToList<OutRqeustInfo>(ds, 0);

                    object objPageCount = cmd.Parameters["@PageCount"].Value;
                    object objPatientCount = cmd.Parameters["@PatientCount"].Value;
                    pageCount = (objPageCount == null || objPageCount == DBNull.Value) ? 0 : Convert.ToInt32(objPageCount);
                    patientCount = (objPatientCount == null || objPatientCount == DBNull.Value) ? 0 : Convert.ToInt32(objPatientCount);
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("GetConDiscussionPatientList", "错误信息：" + ex.Message + "堆栈信息：" + ex.StackTrace);
            }
            return List;
        }

        public string GetStringHospital(string userID)
        {
            string strHospitalID = string.Empty;
            string StrSql = string.Empty;
            try
            {
                var masterHospital = db.RelateHospital.Where(u => u.UserID == userID).ToList();
                if (masterHospital.Count > 0)
                {
                    foreach (RelateHospital item in masterHospital)
                    {
                        if (string.IsNullOrEmpty(strHospitalID))
                        {
                            strHospitalID = "'" + item.SubHospitalID + "'";
                        }
                        else
                        {
                            strHospitalID += ",'" + item.SubHospitalID + "'";
                        }
                    }
                }
                StrSql = @"SELECT * FROM SubHospital WHERE PHospitalId in (SELECT SubHospitalID FROM RelateHospital WHERE UserID='" + userID + "')";
                List<SubHospital> list = db.ExecuteQuery<SubHospital>(StrSql).ToList();
                if (list.Count > 0)
                {
                    foreach (SubHospital item in list)
                    {
                        if (string.IsNullOrEmpty(strHospitalID))
                        {
                            strHospitalID = "'" + item.SubHospitalId + "'";
                        }
                        else
                        {
                            strHospitalID += ",'" + item.SubHospitalId + "'";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("GetStringHospital", "错误信息：" + ex.Message + "堆栈信息：" + ex.StackTrace);
            }
            return strHospitalID;
        }

        /// <summary>
        /// 诊断
        /// 查询已审核或未审核的病人
        /// </summary>
        /// <param name="HospitalId"></param>
        /// <param name="PatientName"></param>
        /// <param name="PatientId"></param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <returns></returns>
        /// 
        public List<OutRqeustInfo> GetPatient_CheckingDetail_bg(string Type, string HospitalId, string PatientName, string PatientId, int ReviwState, DateTime dtStart, DateTime dtEnd, int Age = 0, string Sex = "", string roleType = "", string RequestDcotor = "")
        {
            List<OutRqeustInfo> List = new List<OutRqeustInfo>();
            try
            {
                StringBuilder strBuilder = new StringBuilder();
                string strField = string.Empty;
                string strTable = string.Empty;
                string strCondition = string.Empty;
                string[] strTypes = Type.Split(',');
                string types = "";
                for (int k = 0; k < strTypes.Length; k++)
                {
                    types += "'" + strTypes[k] + "',";
                }
                types = types.Substring(0, types.Length - 1);
                if (!string.IsNullOrEmpty(types) && types == "16")
                {
                    types = "'15','16'";
                }
                strCondition += " ";
                string roleTypes_types = "";
                if (roleType != "")
                {
                    string[] roleTypes = roleType.Split(',');
                    for (int k = 0; k < roleTypes.Length; k++)
                    {
                        roleTypes_types += "'" + roleTypes[k] + "',";
                    }
                    roleTypes_types = roleTypes_types.Substring(0, roleTypes_types.Length - 1);
                    strCondition += " AND R.Diagnosticstate  in (" + roleTypes_types + " )";
                }
                if (!string.IsNullOrEmpty(RequestDcotor))
                {
                    strCondition += " AND RequestDoctor='" + RequestDcotor + "' ";
                }
                if (HospitalId != "")
                {
                    strCondition += " AND  R.HospitalId='" + HospitalId + "'";
                }
                if (PatientId != "")
                {
                    strCondition += " AND  R.PatientId LIKE '%" + PatientId + "%'";
                }
                if (PatientName.Trim() != "")
                {
                    strCondition += " AND  (P.CPatientName LIKE'%" + PatientName + "%') ";
                }
                if (Age > 0)
                {
                    strCondition += " AND PatientAge=" + Age;
                }
                if (!string.IsNullOrEmpty(Sex) && (Sex != "qb" && Sex != "qxz"))
                {
                    strCondition += " And PatientSex='" + Sex + "'";
                }
                if (ReviwState > -1)
                {
                    //待审核、已审核由诊断时间查询
                    if (dtStart != Convert.ToDateTime("1900-01-01"))
                    {
                        strCondition += " AND  R.DiagnoseDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                    }
                    if (dtEnd != Convert.ToDateTime("1900-01-01"))
                    {
                        strCondition += " AND  R.DiagnoseDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                    }
                    strCondition += " AND ReviewStatus=" + ReviwState;
                    if (ReviwState == 0)
                    {
                        strCondition += " ORDER BY DiagnoseDate DESC";
                    }
                    else if (ReviwState == 1)
                    {
                        strCondition += " ORDER BY RefusedDate DESC";
                    }
                }
                else
                {
                    //待书写 由申请时间查询
                    if (dtStart != Convert.ToDateTime("1900-01-01"))
                    {
                        strCondition += " AND  R.RequestDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                    }
                    if (dtEnd != Convert.ToDateTime("1900-01-01"))
                    {
                        strCondition += " AND  R.RequestDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                    }
                    strCondition += " ORDER BY RequestDate DESC ";
                }
                db = new RemoteDianoseDataContext();
                string StrSql = string.Format(@"with tabs as (select ROW_NUMBER() over(partition by RequestId   order by RefusedDate) as rows,RequestId,RefusedPerson, RefusedId,RefusedDate 
                                                from [RemoteDianose].[dbo].[RefusedRecord])
                                                SELECT R.RequestId ,CASE
                                                WHEN R.HospitalId IS NULL THEN
	                                                R.PatientId
                                                ELSE
	                                                replace(
		                                                R.PatientId,
		                                                '.' + R.HospitalId,
		                                                ''
	                                                )
                                                END AS NewPatientId,P.PatientName,P.PatientId,
                                                P.CPatientName,P.PatientAge,P.PatientSex,R.ReportState,
                                                S.DeptDisplayName AS HospitalName,R.Diagnosticstate,
                                                R.ExaminePart,R.ExamineMethod,R.RequestPurpose,R.ReportPerson,
                                                ISNULL(SD.Name,'') AS SDDoctor,ISNULL(SR.Name,'') AS SRDoctor, ISNULL(SP.Name,'') AS RefusedPerson,ISNULL(SRP.Name,'') AS SRPDoctor,
                                                ISNULL(R.ReviewStatus,0) AS ReviewStatus,R.AccessionNumber,R.DiagnoseDoctor,R.RequestState,R.StudyInstanceUid,
                                                CONVERT(varchar(16),DiagnoseDate,20) AS DiagnoseDate,ExamineDate,
                                                CONVERT(varchar(16),RE.RefusedDate,20) AS RefusedDate, CONVERT(varchar(16),R.RequestDate,20) AS RequestDate
                                                FROM [RemoteDianose].[dbo].[RequestInfo]  AS R
                                                LEFT JOIN [RemoteDianose].[dbo].[PatientInfo] AS P ON  P.PatientId=R.PatientId 
                                                LEFT JOIN [RemoteDianose].[dbo].[Sys_Dept]  AS S ON R.HospitalId=S.DeptId 
                                                LEFT JOIN [RemoteDianose].[dbo].ChargeRecord AS C ON R.RequestId=C.RequestId
                                                LEFT JOIN tabs AS RE ON R.RequestId=RE.RequestId
                                                LEFT JOIN [RemoteDianose].[dbo].[Sys_Users] AS SR ON SR.UserName=R.RequestDoctor
                                                LEFT JOIN [RemoteDianose].[dbo].[Sys_Users] AS SD ON SD.UserName=R.DiagnoseDoctor
                                                LEFT JOIN [RemoteDianose].[dbo].[Sys_Users] AS SP ON SP.UserName=RE.RefusedPerson
                                                LEFT JOIN [RemoteDianose].[dbo].[Sys_Users] AS SRP ON SRP.UserName=R.ReportPerson
                                                WHERE RequestState in({0}) {1} ", types, strCondition);

                StrSql = ReplaceInstance(StrSql, "GetPatient_CheckingDetail_bg::");
                List = db.ExecuteQuery<OutRqeustInfo>(StrSql).ToList();
            }
            catch (Exception)
            {
            }
            return List;
        }

        public List<OutRqeustInfo> GetPatient_CheckingDetail(string Type, string HospitalId, string PatientName, string PatientId, int ReviwState, DateTime dtStart, DateTime dtEnd, int Age = 0, string Sex = "", string roleType = "", string RequestDcotor = "")
        {
            List<OutRqeustInfo> List = new List<OutRqeustInfo>();
            try
            {
                StringBuilder strBuilder = new StringBuilder();
                string strField = string.Empty;
                string strTable = string.Empty;
                string strCondition = string.Empty;
                string[] strTypes = Type.Split(',');
                string types = "";
                for (int k = 0; k < strTypes.Length; k++)
                {
                    types += "'" + strTypes[k] + "',";
                }
                types = types.Substring(0, types.Length - 1);
                strCondition += " ";
                if (roleType != "")
                {
                    strCondition += " AND R.Diagnosticstate='Diagnose' ";
                }
                if (!string.IsNullOrEmpty(RequestDcotor))
                {
                    strCondition += " AND RequestDoctor='" + RequestDcotor + "' ";
                }
                string HospitalIdmap = "";
                if (HospitalId != "")
                {
                    var list = db.SubHospital.Where(u => u.PHospitalId == HospitalId).ToList();
                    if (list != null)
                    {

                        foreach (var u in list)
                        {
                            HospitalIdmap += "'" + u.SubHospitalId.ToString() + "',";
                        }
                        HospitalIdmap = HospitalIdmap + "'" + HospitalId + "'";
                    }
                    strCondition += " AND  R.HospitalId in(" + HospitalIdmap + ")";
                }
                if (PatientId != "")
                {
                    strCondition += " AND  R.PatientId LIKE '%" + PatientId + "%'";
                }
                if (PatientName.Trim() != "")
                {
                    strCondition += " AND  (P.CPatientName LIKE'%" + PatientName + "%' OR  P.PatientName like'%" + PatientName + "%') ";
                }
                if (Age > 0)
                {
                    strCondition += " AND PatientAge=" + Age;
                }
                if (!string.IsNullOrEmpty(Sex) && (Sex != "qb" && Sex != "qxz"))
                {
                    strCondition += " And PatientSex='" + Sex + "'";
                }
                if (ReviwState > -1)
                {
                    //待审核、已审核由诊断时间查询
                    if (dtStart != Convert.ToDateTime("1900-01-01"))
                    {
                        strCondition += " AND  R.DiagnoseDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                    }
                    if (dtEnd != Convert.ToDateTime("1900-01-01"))
                    {
                        strCondition += " AND  R.DiagnoseDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                    }
                    strCondition += " AND ReviewStatus=" + ReviwState;
                    if (ReviwState == 0)
                    {
                        strCondition += " ORDER BY DiagnoseDate DESC";
                    }
                    else if (ReviwState == 1)
                    {
                        strCondition += " ORDER BY RefusedDate DESC";
                    }
                }
                else
                {
                    //待书写 由申请时间查询
                    if (dtStart != Convert.ToDateTime("1900-01-01"))
                    {
                        strCondition += " AND  R.RequestDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                    }
                    if (dtEnd != Convert.ToDateTime("1900-01-01"))
                    {
                        strCondition += " AND  R.RequestDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                    }
                }
                db = new RemoteDianoseDataContext();
                string StrSql = string.Format(@"
                                                SELECT top 30 R.RequestId,CASE
                                                WHEN R.HospitalId IS NULL THEN
	                                                R.PatientId
                                                ELSE
	                                                replace(
		                                                R.PatientId,
		                                                '.' + R.HospitalId,
		                                                ''
	                                                )
                                                END AS NewPatientId,P.PatientName,P.PatientId,
                                                P.CPatientName,P.PatientAge,P.PatientSex,R.ReportState,
                                                S.DeptDisplayName AS HospitalName,R.Diagnosticstate,
                                                R.ExaminePart,R.ExamineMethod,R.RequestPurpose,R.ReportPerson,

                                                 (select SR.Name from [RemoteDianose].[dbo].[Sys_Users] AS SR where SR.UserName = R.RequestDoctor ) as SRDoctor,
                                                 (select SR.Name from [RemoteDianose].[dbo].[Sys_Users] AS SR where SR.UserName = R.DiagnoseDoctor ) as SDDoctor,
                                                 (select SR.Name from [RemoteDianose].[dbo].[Sys_Users] AS SR where SR.UserName = RE.RefusedPerson ) as RefusedPerson,
                                                 (select SR.Name from [RemoteDianose].[dbo].[Sys_Users] AS SR where SR.UserName = R.ReportPerson ) as SRPDoctor,

                                                ISNULL(R.ReviewStatus,0) AS ReviewStatus,R.AccessionNumber,R.DiagnoseDoctor,R.RequestState,R.StudyInstanceUid,
                                                CONVERT(varchar(16),DiagnoseDate,20) AS DiagnoseDate,ExamineDate,
                                                CONVERT(varchar(16),RE.RefusedDate,20) AS RefusedDate, CONVERT(varchar(16),R.RequestDate,20) AS RequestDate
                                                FROM [RemoteDianose].[dbo].[RequestInfo]  AS R
                                                LEFT JOIN [RemoteDianose].[dbo].[PatientInfo] AS P ON  P.PatientId=R.PatientId 
                                                LEFT JOIN [RemoteDianose].[dbo].[Sys_Dept]  AS S ON R.HospitalId=S.DeptId 
                                                LEFT JOIN [RemoteDianose].[dbo].ChargeRecord AS C ON R.RequestId=C.RequestId
                                                LEFT JOIN [dbo].[RefusedRecord] AS RE ON R.RequestId=RE.RequestId                                                
                                                WHERE RequestState in({0}) {1} ", types, strCondition);

                StrSql = "WITH abc AS (" + StrSql + @") select * from abc as a ORDER BY a.RequestDate DESC";
                StrSql = ReplaceInstance(StrSql, "GetPatient_CheckingDetail::");
                List = db.ExecuteQuery<OutRqeustInfo>(StrSql).ToList();
            }
            catch (Exception)
            {
            }
            return List;
        }

        public List<OutRqeustInfo> GetPatient_ConsultationDetail_allList(string state, string PatientName, string PatientId, int ChooseType, string strDoctor, DateTime dtStart, DateTime dtEnd, int Age = 0, string Sex = null)
        {
            List<OutRqeustInfo> List = new List<OutRqeustInfo>();
            string StrSQL = string.Empty;
            try
            {
                StrSQL = @"SELECT P.PatientAge,R.PatientId,CASE
                            WHEN R.HospitalId IS NULL THEN
	                            R.PatientId
                            ELSE
	                            replace(
		                            R.PatientId,
		                            '.' + R.HospitalId,
		                            '')
                           END AS NewPatientId,R.RequestId,P.PatientSex,R.DiagnoseDoctor,S.DeptDisplayName AS HospitalName,CONVERT(varchar(16),R.RequestDate, 20) AS RequestDate,
                           P.PatientName,P.CPatientName,R.AccessionNumber,R.ReportState,R.ReportPerson,R.StudyInstanceUid,
                           ISNULL(R.FeeState,0) AS FeeState,ISNULL(R.RequestState,0) AS RequestState,R.RequestPurpose,
                           ISNULL(R.ReviewStatus,0) AS ReviewStatus,R.Diagnosticstate,ISNULL(SRP.Name,'') AS SRPDoctor, ISNULL(SRD.Name,'') AS SRDoctor,
                           CONVERT(varchar(16),P.PatientBirth,23) AS PatientBirth,
                           CONVERT(varchar(16),R.DiagnoseDate,20) AS DiagnoseDate,R.ExamineDate,R.ExaminePart,R.ExamineMethod
                           FROM  RequestInfo AS R
                           LEFT JOIN PatientInfo P ON R.PatientId=P.PatientId
                           LEFT JOIN Sys_Dept S ON R.HospitalId=S.DeptId
                           LEFT JOIN Sys_Users AS SRP ON SRP.UserName=R.ReportPerson
                           LEFT JOIN Sys_Users AS SRD ON SRD.UserName=R.RequestDoctor
                           WHERE R.Diagnosticstate='Consultation'";
                if (PatientName.Trim() != "")
                {
                    StrSQL += " AND  (P.CPatientName LIKE'%" + PatientName + "%' OR  P.PatientName like'%" + PatientName + "%') ";
                }
                if (PatientId != "")
                {
                    StrSQL += " AND  R.PatientId LIKE '%" + PatientId + "%' ";
                }
                if (Age > 0)
                {
                    StrSQL += " AND P.PatientAge=" + Age;
                }
                if (!string.IsNullOrEmpty(Sex) && (Sex != "qb" && Sex != "qxz"))
                {
                    StrSQL += " And PatientSex='" + Sex + "'";
                }

                if (ChooseType == 21)
                {
                    //由指定专家和申请时间查询
                    if (dtStart != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.RequestDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                    }
                    if (dtEnd != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.RequestDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                    }
                    StrSQL += " AND R.DiagnoseDoctor='" + strDoctor + "' AND R.RequestState=11   ORDER BY RequestDate DESC";
                }
                else if (ChooseType == 22)
                {
                    //已写报告 由诊断医生和诊断时间查询
                    if (dtStart != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.DiagnoseDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                    }
                    if (dtEnd != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.DiagnoseDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                    }
                    StrSQL += " AND R.DiagnoseDoctor='" + strDoctor + "' AND R.RequestState=15 ORDER BY R.DiagnoseDate";
                }
                else
                {
                    //中心资源 由诊断医生为NULL的所有会诊患者
                    if (dtStart != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.RequestDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                    }
                    if (dtEnd != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.RequestDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                    }
                    if (state.Trim() != "")
                    {
                        StrSQL += " AND RequestState=" + state + "";
                    }
                    StrSQL += "AND (R.DiagnoseDoctor='' OR R.DiagnoseDoctor IS NULL)  ORDER BY RequestDate DESC";
                }
                StrSQL = ReplaceInstance(StrSQL, "GetPatient_ConsultationDetail_allList::");
                List = db.ExecuteQuery<OutRqeustInfo>(StrSQL).ToList();
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("会诊查询异常GetPatient_ConsultationDetail_allList：" + StrSQL, ex.Message);
            }
            return List;
        }

        /// <summary>
        /// 获取诊断患者信息列表(存储过程)
        /// </summary>
        /// <param name="RequestStateID"></param>
        /// <param name="HospitalId"></param>
        /// <param name="PatientName"></param>
        /// <param name="PatientId"></param>
        /// <param name="ReviwState"></param>
        /// <param name="PageIndex"></param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <param name="Age"></param>
        /// <param name="Sex"></param>
        /// <param name="RequestDcotor"></param>
        /// <param name="pageCount"></param>
        /// <param name="patientCount"></param>
        /// <returns></returns>
        public List<OutRqeustInfo> GetDiagnosePatientInfoList(string RequestStateID, string HospitalId, string PatientName, string PatientId, int ReviwState, int PageIndex, DateTime dtStart, DateTime dtEnd, int smallAge, int bigAge, string Sex, string RequestDcotor, out int pageCount, out int patientCount)
        {
            string strName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            List<OutRqeustInfo> listInfo = new List<OutRqeustInfo>();
            pageCount = 0;
            patientCount = 0;
            try
            {
                string strField = string.Empty;
                string strTable = string.Empty;
                string strCondition = string.Empty;
                string[] RequestStateArr = RequestStateID.Split(',');
                string strRequestStateID = "";
                int pageSize = 20;
                string orderByType = string.Empty;
                #region 参数
                for (int k = 0; k < RequestStateArr.Length; k++)
                {
                    strRequestStateID += "'" + RequestStateArr[k] + "',";
                }
                strRequestStateID = strRequestStateID.Substring(0, strRequestStateID.Length - 1);
                if (!string.IsNullOrEmpty(strRequestStateID))
                {
                    strCondition += " WHERE RequestState in(" + strRequestStateID + ")";
                }
                strCondition += " ";
                if (!string.IsNullOrEmpty(RequestDcotor))
                {
                    strCondition += " AND RequestDoctor='" + RequestDcotor + "' ";
                }
                if (HospitalId != "")
                {
                    strCondition += " AND R.Diagnosticstate='Diagnose' AND (R.HospitalId IN(SELECT SH.SubHospitalId FROM SubHospital SH WHERE SH.PHospitalId='" + HospitalId + "') OR  R.HospitalId ='" + HospitalId + "')";
                }
                if (PatientId != "")
                {
                    strCondition += " AND  R.PatientId LIKE '%" + PatientId + "%'";
                }
                if (PatientName.Trim() != "")
                {
                    strCondition += " AND  (P.CPatientName LIKE'%" + PatientName + "%') ";
                }
                if (smallAge > 0)
                {
                    strCondition += " AND PatientAge>=" + smallAge;
                }
                if (bigAge > 0)
                {
                    strCondition += " AND PatientAge<=" + bigAge;
                }
                if (!string.IsNullOrEmpty(Sex) && (Sex != "qb" && Sex != "qxz"))
                {
                    strCondition += " And PatientSex='" + Sex + "'";
                }
                if (ReviwState > -1)
                {
                    if (ReviwState == 0)
                    {
                        //待审核
                        if (dtStart != Convert.ToDateTime("1900-01-01"))
                        {
                            strCondition += " AND  R.DiagnoseDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                        }
                        if (dtEnd != Convert.ToDateTime("1900-01-01"))
                        {
                            strCondition += " AND  R.DiagnoseDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                        }
                        orderByType = "DiagnoseDate";
                    }
                    else if (ReviwState == 1)
                    {
                        //已审核
                        if (dtStart != Convert.ToDateTime("1900-01-01"))
                        {
                            strCondition += " AND  RE.RefusedDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                        }
                        if (dtEnd != Convert.ToDateTime("1900-01-01"))
                        {
                            strCondition += " AND  RE.RefusedDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                        }
                        orderByType = "RE.RefusedDate";
                    }
                    strCondition += " AND ReviewStatus=" + ReviwState;
                }
                else
                {
                    //待书写 由申请时间查询
                    if (dtStart != Convert.ToDateTime("1900-01-01"))
                    {
                        strCondition += " AND  R.RequestDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                    }
                    if (dtEnd != Convert.ToDateTime("1900-01-01"))
                    {
                        strCondition += " AND  R.RequestDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                    }
                    orderByType = "RequestDate";
                }

                LogHelper.WriteWarningLog(strName + "--条件" + strCondition + "--排序：" + orderByType);
                #endregion
                DataSet ds = new DataSet();
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["InsideRISConnectionString"].ToString()))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.Parameters.Add(new SqlParameter("@Condition", SqlDbType.VarChar, 1000));
                    cmd.Parameters.Add(new SqlParameter("@OrderFile", SqlDbType.VarChar, 50));
                    cmd.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.VarChar, 10));
                    cmd.Parameters.Add(new SqlParameter("@PageIndex", SqlDbType.VarChar, 10));

                    SqlParameter paramPageCount = new SqlParameter("@PageCount", SqlDbType.Int);
                    SqlParameter paramPateitnCount = new SqlParameter("@PatientCount", SqlDbType.Int);
                    paramPageCount.Direction = ParameterDirection.Output;
                    paramPateitnCount.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(paramPageCount);
                    cmd.Parameters.Add(paramPateitnCount);

                    cmd.Parameters[0].Value = strCondition;
                    cmd.Parameters[1].Value = orderByType;
                    cmd.Parameters[2].Value = pageSize;
                    cmd.Parameters[3].Value = PageIndex;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "DiagnosePatientList";
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds);

                    listInfo = DataSetToList<OutRqeustInfo>(ds, 0);

                    object objPageCount = cmd.Parameters["@PageCount"].Value;
                    object objPatientCount = cmd.Parameters["@PatientCount"].Value;
                    pageCount = (objPageCount == null || objPageCount == DBNull.Value) ? 0 : Convert.ToInt32(objPageCount);
                    patientCount = (objPatientCount == null || objPatientCount == DBNull.Value) ? 0 : Convert.ToInt32(objPatientCount);
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog(strName, "异常信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return listInfo;
        }

        /// <summary>
        /// 获取诊断患者数据
        /// </summary>
        /// <param name="RequestState"></param>
        /// <param name="HospitalId"></param>
        /// <param name="PatientName"></param>
        /// <param name="PatientId"></param>
        /// <param name="ReviwState"></param>
        /// <param name="PageIndex"></param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <param name="smallAge"></param>
        /// <param name="bigAge"></param>
        /// <param name="Sex"></param>
        /// <param name="RequestDcotor"></param>
        /// <param name="DiagnoseDoctor"></param>
        /// <param name="pageCount"></param>
        /// <param name="patientCount"></param>
        /// <returns></returns>
        public List<OutRqeustInfo> GetDiagnosePatientBaseList(string requestState, string hospitalId, string hospitalSon, string patientName, string patientId, int reviwState, int pageIndex, DateTime dtStart, DateTime dtEnd, int smallAge, int bigAge, string Sex, string requestDcotor, string diagnoseDoctor, string examineType, out int pageCount, out int patientCount)
        {
            string strName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            List<OutRqeustInfo> listInfo = new List<OutRqeustInfo>();
            pageCount = 0;
            patientCount = 0;
            try
            {
                string strField = string.Empty;
                string strTable = string.Empty;
                string strCondition = string.Empty;
                string[] RequestStateArr = requestState.Split(',');
                string strRequestStateID = "";
                int pageSize = 20;
                string orderByType = string.Empty;
                #region 参数
                for (int k = 0; k < RequestStateArr.Length; k++)
                {
                    strRequestStateID += "'" + RequestStateArr[k] + "',";
                }
                strRequestStateID = strRequestStateID.Substring(0, strRequestStateID.Length - 1);
                if (!string.IsNullOrEmpty(strRequestStateID))
                {
                    if (strRequestStateID.Contains("16"))
                    {
                        strRequestStateID = "'15','16'";//暂时查询，版本稳定，更新数据库数据，即可去掉
                    }
                    strCondition += " WHERE RequestState in(" + strRequestStateID + ")";
                }
                strCondition += " ";
                if (!string.IsNullOrEmpty(requestDcotor))
                {
                    strCondition += " AND RequestDoctor='" + requestDcotor + "' ";
                }
                if (!string.IsNullOrEmpty(diagnoseDoctor))
                {
                    strCondition += " AND DiagnoseDoctor='" + diagnoseDoctor + "' AND R.Diagnosticstate='Multi' ";
                }
                //else
                //{
                //    if ((strRequestStateID.Contains("16") || strRequestStateID.Contains("15")) && reviwState == 1)
                //    {
                //        strCondition += " AND R.Diagnosticstate in('Diagnose','Multi') ";
                //    }
                //    else
                //    {
                //        strCondition += " AND R.Diagnosticstate='Diagnose' ";
                //    }
                //}
                if (hospitalId != "")
                {
                    if (!string.IsNullOrEmpty(hospitalSon) && hospitalSon != "0")
                    {
                        strCondition += " AND R.HospitalId='" + hospitalSon + "' ";
                    }
                    else
                    {
                        strCondition += " AND (R.HospitalId IN(SELECT SH.SubHospitalId FROM SubHospital SH WHERE SH.PHospitalId='" + hospitalId + "') OR  R.HospitalId ='" + hospitalId + "')";
                    }
                }
                if (patientId != "")
                {
                    strCondition += " AND  R.PatientId LIKE '%" + patientId + "%'";
                }
                if (!string.IsNullOrEmpty(examineType) && examineType != "0")
                {
                    strCondition += " AND R.ExamineType='" + examineType + "' ";
                }
                if (patientName.Trim() != "")
                {
                    strCondition += " AND  (P.CPatientName LIKE'%" + patientName.Trim() + "%') ";
                }
                if (smallAge > 0)
                {
                    strCondition += " AND PatientAge>=" + smallAge;
                }
                if (bigAge > 0)
                {
                    strCondition += " AND PatientAge<=" + bigAge;
                }
                if (!string.IsNullOrEmpty(Sex) && (Sex != "qb" && Sex != "qxz"))
                {
                    strCondition += " And PatientSex='" + Sex + "'";
                }
                if (reviwState > -1)
                {
                    if (reviwState == 0)
                    {
                        //待审核
                        if (dtStart != Convert.ToDateTime("1900-01-01"))
                        {
                            strCondition += " AND  R.DiagnoseDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                        }
                        if (dtEnd != Convert.ToDateTime("1900-01-01"))
                        {
                            strCondition += " AND  R.DiagnoseDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                        }
                        orderByType = "DiagnoseDate";
                    }
                    else if (reviwState == 1)
                    {
                        //已审核
                        if (dtStart != Convert.ToDateTime("1900-01-01"))
                        {
                            strCondition += " AND  RE.RefusedDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                        }
                        if (dtEnd != Convert.ToDateTime("1900-01-01"))
                        {
                            strCondition += " AND  RE.RefusedDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                        }
                        orderByType = "RE.RefusedDate";
                    }
                    strCondition += " AND ReviewStatus=" + reviwState;
                }
                else
                {
                    //待书写 由申请时间查询
                    if (dtStart != Convert.ToDateTime("1900-01-01"))
                    {
                        strCondition += " AND  R.RequestDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                    }
                    if (dtEnd != Convert.ToDateTime("1900-01-01"))
                    {
                        strCondition += " AND  R.RequestDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                    }
                    orderByType = "RequestDate";
                }

                LogHelper.WriteWarningLog(strName + "--条件" + strCondition + "--排序：" + orderByType);
                #endregion
                DataSet ds = new DataSet();
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["InsideRISConnectionString"].ToString()))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.Parameters.Add(new SqlParameter("@Condition", SqlDbType.VarChar, 1000));
                    cmd.Parameters.Add(new SqlParameter("@OrderFile", SqlDbType.VarChar, 50));
                    cmd.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.VarChar, 10));
                    cmd.Parameters.Add(new SqlParameter("@PageIndex", SqlDbType.VarChar, 10));

                    SqlParameter paramPageCount = new SqlParameter("@PageCount", SqlDbType.Int);
                    SqlParameter paramPateitnCount = new SqlParameter("@PatientCount", SqlDbType.Int);
                    paramPageCount.Direction = ParameterDirection.Output;
                    paramPateitnCount.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(paramPageCount);
                    cmd.Parameters.Add(paramPateitnCount);

                    cmd.Parameters[0].Value = strCondition;
                    cmd.Parameters[1].Value = orderByType;
                    cmd.Parameters[2].Value = pageSize;
                    cmd.Parameters[3].Value = pageIndex;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "DiagnosePatientList";
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds);

                    listInfo = DataSetToList<OutRqeustInfo>(ds, 0);

                    object objPageCount = cmd.Parameters["@PageCount"].Value;
                    object objPatientCount = cmd.Parameters["@PatientCount"].Value;
                    pageCount = (objPageCount == null || objPageCount == DBNull.Value) ? 0 : Convert.ToInt32(objPageCount);
                    patientCount = (objPatientCount == null || objPatientCount == DBNull.Value) ? 0 : Convert.ToInt32(objPatientCount);
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog(strName, "异常信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return listInfo;
        }

        /// <summary>
        /// 获取会诊患者信息列表(存储过程)
        /// </summary>
        /// <param name="PatientName"></param>
        /// <param name="PatientId"></param>
        /// <param name="HospitalID"></param>
        /// <param name="PageIndex"></param>
        /// <param name="ChooseType"></param>
        /// <param name="strDoctor"></param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <param name="Age"></param>
        /// <param name="Sex"></param>
        /// <param name="pageCount"></param>
        /// <param name="patientCount"></param>
        /// <returns></returns>
        public List<OutRqeustInfo> GetConsulationPatientInfoList(string PatientName, string PatientId, string HospitalID, int PageIndex, int ChooseType, string strDoctor, DateTime dtStart, DateTime dtEnd, int smallAge, int bigAge, string Sex, out int pageCount, out int patientCount)
        {
            string strName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            List<OutRqeustInfo> List = new List<OutRqeustInfo>();
            pageCount = 0;
            patientCount = 0;
            try
            {
                string StrSQL = string.Empty;
                string OrderByType = string.Empty;
                int pageSize = 20;

                #region 查询条件
                if (PatientName.Trim() != "")
                {
                    StrSQL += " AND  (P.CPatientName LIKE'%" + PatientName + "%' OR  P.PatientName like'%" + PatientName + "%') ";
                }
                if (PatientId != "")
                {
                    StrSQL += " AND  R.PatientId LIKE '%" + PatientId + "%' ";
                }
                if (smallAge > 0)
                {
                    StrSQL += " AND P.PatientAge>=" + smallAge;
                }
                if (bigAge > 0)
                {
                    StrSQL += " AND P.PatientAge<=" + bigAge;
                }
                if (!string.IsNullOrEmpty(Sex) && (Sex != "qb" && Sex != "qxz"))
                {
                    StrSQL += " AND P.PatientSex='" + Sex + "'";
                }
                if (!string.IsNullOrEmpty(HospitalID))
                {
                    StrSQL += " AND HospitalId='" + HospitalID + "'";
                }
                if (ChooseType == 20)
                {
                    pageSize = 10;
                    //申请未完成
                    if (dtStart != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.RequestJDSJ>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                    }
                    if (dtEnd != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.RequestJDSJ<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                    }
                    StrSQL += " AND R.RequestJDR='" + strDoctor + "' AND R.RequestState < 11 ";
                    OrderByType = "R.RequestJDSJ";
                }
                else if (ChooseType == 25)
                {
                    pageSize = 10;
                    //已申请未书写
                    if (dtStart != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.RequestDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                    }
                    if (dtEnd != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.RequestDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                    }
                    StrSQL += " AND R.RequestJDR='" + strDoctor + "' AND R.RequestState >= 11 AND R.RequestState<15";
                    OrderByType = "R.RequestDate";
                }
                else if (ChooseType == 15)
                {
                    pageSize = 10;
                    //该用户申请已完成的
                    if (dtStart != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.RequestDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                    }
                    if (dtEnd != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.RequestDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                    }
                    StrSQL += " AND R.RequestJDR='" + strDoctor + "' AND R.RequestState = 15 ";
                    OrderByType = "R.RequestDate";
                }
                else if (ChooseType == 21)
                {
                    //由指定专家和申请时间查询
                    if (dtStart != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.RequestDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                    }
                    if (dtEnd != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.RequestDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                    }
                    StrSQL += " AND R.DiagnoseDoctor='" + strDoctor + "' AND R.RequestState=11 ";
                    OrderByType = "R.RequestDate";
                }
                else if (ChooseType == 22)
                {
                    //已写报告 由诊断医生和诊断时间查询
                    if (dtStart != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.DiagnoseDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                    }
                    if (dtEnd != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.DiagnoseDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                    }
                    StrSQL += " AND R.DiagnoseDoctor='" + strDoctor + "' AND R.RequestState=15";
                    OrderByType = "R.DiagnoseDate";
                }
                else if (ChooseType == 23)
                {
                    //总览报告
                    if (dtStart != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.DiagnoseDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                    }
                    if (dtEnd != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.DiagnoseDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                    }
                    StrSQL += " AND R.RequestState=15";
                    OrderByType = "R.DiagnoseDate";
                }
                else if (ChooseType == 24)//查询所有全部
                {
                    if (dtStart != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.RequestDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                    }
                    if (dtEnd != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.RequestDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                    }
                    StrSQL += " AND (R.RequestState=11 OR R.RequestState=15) ";
                    OrderByType = "R.RequestDate";
                }
                else
                {
                    //中心资源 由诊断医生为NULL的所有会诊患者
                    if (dtStart != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.RequestDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                    }
                    if (dtEnd != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.RequestDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                    }
                    StrSQL += "AND (R.DiagnoseDoctor='' OR R.DiagnoseDoctor IS NULL) AND RequestState=11";
                    OrderByType = "R.RequestDate";
                }

                LogHelper.WriteInfoLog("会诊参数：" + StrSQL + "--分组参数：" + OrderByType);
                #endregion

                DataSet ds = new DataSet();
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["InsideRISConnectionString"].ToString()))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.Parameters.Add(new SqlParameter("@Condition", SqlDbType.VarChar, 1000));
                    cmd.Parameters.Add(new SqlParameter("@OrderFile", SqlDbType.VarChar, 50));
                    cmd.Parameters.Add(new SqlParameter("@PageSize", SqlDbType.VarChar, 10));
                    cmd.Parameters.Add(new SqlParameter("@PageIndex", SqlDbType.VarChar, 10));

                    SqlParameter paramPageCount = new SqlParameter("@PageCount", SqlDbType.Int);
                    SqlParameter paramPateitnCount = new SqlParameter("@PatientCount", SqlDbType.Int);
                    paramPageCount.Direction = ParameterDirection.Output;
                    paramPateitnCount.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(paramPageCount);
                    cmd.Parameters.Add(paramPateitnCount);

                    cmd.Parameters[0].Value = StrSQL;
                    cmd.Parameters[1].Value = OrderByType;
                    cmd.Parameters[2].Value = pageSize;
                    cmd.Parameters[3].Value = PageIndex;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "ConsulationPatientList";

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;

                    adapter.Fill(ds);

                    List = DataSetToList<OutRqeustInfo>(ds, 0);

                    List.ForEach(o =>
                    {
                        //List<StudyRecordInfo> recordItem = new List<StudyRecordInfo>();
                        //recordItem.Add(new StudyRecordInfo { RequestId = o.RequestId });
                        //o.CollectionStudy = recordItem;
                        o.ExamineCount = db.RequestStudyRecord.Where(x => x.RequestId == o.RequestId).ToList().Count;
                    });

                    object objPageCount = cmd.Parameters["@PageCount"].Value;
                    object objPatientCount = cmd.Parameters["@PatientCount"].Value;
                    pageCount = (objPageCount == null || objPageCount == DBNull.Value) ? 0 : Convert.ToInt32(objPageCount);
                    patientCount = (objPatientCount == null || objPatientCount == DBNull.Value) ? 0 : Convert.ToInt32(objPatientCount);

                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog(strName, "异常信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return List;
        }

        /// <summary>
        /// 报告端数据查询
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<OutRqeustInfo> GetDiagnoseBaseList(InBaseParam param)
        {
            string strName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            List<OutRqeustInfo> listInfo = new List<OutRqeustInfo>();

            try
            {
                string strCondition = string.Empty;
                string[] RequestStateArr = param.RequestState.Split(',');
                string strRequestStateID = "";
                string orderByType = string.Empty;
                #region 参数
                for (int k = 0; k < RequestStateArr.Length; k++)
                {
                    strRequestStateID += "'" + RequestStateArr[k] + "',";
                }
                strRequestStateID = strRequestStateID.Substring(0, strRequestStateID.Length - 1);

                if (!string.IsNullOrEmpty(strRequestStateID))
                {
                    strCondition += " WHERE RequestState in(" + strRequestStateID + ")";
                }
                strCondition += " ";
                if (!string.IsNullOrEmpty(param.DiagnoseDoctor))//指定会诊
                {
                    strCondition += " AND DiagnoseDoctor='" + param.DiagnoseDoctor + "'";
                }
                //else
                //{
                //    if (param.RequestState == "16")
                //    {
                //        strCondition += " AND R.Diagnosticstate in('Diagnose','Multi') ";
                //    }
                //    else
                //    {
                //        strCondition += " AND R.Diagnosticstate='Diagnose' ";
                //    }
                //}
                //if (!string.IsNullOrEmpty(param.HospitalId))
                //{
                //    if (!string.IsNullOrEmpty(param.HospitalSon))
                //    {
                //        strCondition += " AND R.HospitalId='" + param.HospitalSon + "' ";
                //    }
                //    else
                //    {
                //        strCondition += " AND (R.HospitalId IN(SELECT SH.SubHospitalId FROM SubHospital SH WHERE SH.PHospitalId='" + param.HospitalId + "') OR  R.HospitalId ='" + param.HospitalId + "')";
                //    }
                //}

                if (!string.IsNullOrEmpty(param.PatientId))
                {
                    strCondition += " AND  R.PatientId LIKE '%" + param.PatientId + "%'";
                }
                if (!string.IsNullOrEmpty(param.ExamineType))
                {

                    strCondition += " AND R.ExamineType IN (" + param.ExamineType + ")";
                }
                if (!string.IsNullOrEmpty(param.PatientType))
                {

                    strCondition += " AND R.PatientType IN (" + param.PatientType + ")";
                }
                if (!string.IsNullOrEmpty(param.PatientName))
                {
                    strCondition += " AND  (P.CPatientName LIKE'%" + param.PatientName.Trim() + "%') ";
                }
                if (param.SmallAge > 0)
                {
                    strCondition += " AND PatientAge>=" + param.SmallAge;
                }
                if (param.BigAge > 0)
                {
                    strCondition += " AND PatientAge<=" + param.BigAge;
                }
                if (!string.IsNullOrEmpty(param.Sex) && (param.Sex != "qxz"))
                {
                    strCondition += " And PatientSex='" + param.Sex + "'";
                }
                if (param.RequestState == "15")
                {
                    //待审核
                    if (param.DateTimeBegin != Convert.ToDateTime("1900-01-01"))
                    {
                        strCondition += " AND  R.DiagnoseDate>=CONVERT(varchar(100),'" + param.DateTimeBegin.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                    }
                    if (param.DateTimeEnd != Convert.ToDateTime("1900-01-01"))
                    {
                        strCondition += " AND  R.DiagnoseDate<=CONVERT(varchar(100),'" + param.DateTimeEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                    }
                    orderByType = "DiagnoseDate";
                }
                else if (param.RequestState == "16")
                {
                    //已审核
                    if (param.DateTimeBegin != Convert.ToDateTime("1900-01-01"))
                    {
                        strCondition += " AND  RE.RefusedDate>=CONVERT(varchar(100),'" + param.DateTimeBegin.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                    }
                    if (param.DateTimeEnd != Convert.ToDateTime("1900-01-01"))
                    {
                        strCondition += " AND  RE.RefusedDate<=CONVERT(varchar(100),'" + param.DateTimeEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                    }
                    orderByType = "RE.RefusedDate";
                }
                else if (param.RequestState.Contains("11") || param.RequestState.Contains("20"))
                {
                    //待书写 由申请时间查询
                    if (param.DateTimeBegin != Convert.ToDateTime("1900-01-01"))
                    {
                        strCondition += " AND  R.RequestDate>=CONVERT(varchar(100),'" + param.DateTimeBegin.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                    }
                    if (param.DateTimeEnd != Convert.ToDateTime("1900-01-01"))
                    {
                        strCondition += " AND  R.RequestDate<=CONVERT(varchar(100),'" + param.DateTimeEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                    }
                    orderByType = "RequestDate";
                }

                LogHelper.WriteWarningLog(strName + "--条件" + strCondition + "--排序：" + orderByType);
                #endregion
                DataSet ds = new DataSet();
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["InsideRISConnectionString"].ToString()))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.Parameters.Add(new SqlParameter("@Condition", SqlDbType.VarChar, 1000));
                    cmd.Parameters.Add(new SqlParameter("@OrderFile", SqlDbType.VarChar, 50));
                    cmd.Parameters[0].Value = strCondition;
                    cmd.Parameters[1].Value = orderByType;

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "DiagnoseList";
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds);

                    listInfo = DataSetToList<OutRqeustInfo>(ds, 0);
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog(strName, "异常信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return listInfo;
        }

        /// <summary>
        /// 获取子表记录信息
        /// </summary>
        /// <param name="requestidParam"></param>
        /// <returns></returns>
        public List<StudyRecordInfo> GetStudyRecordInfoList(string requestidParam)
        {
            List<StudyRecordInfo> list = new List<StudyRecordInfo>();
            StringBuilder strAppend = new StringBuilder();
            strAppend.Append(@"SELECT  RequestId,NewPatientId,HospitalId,StudyInstanceUid,PatientId,PatientsName,PatientSex,PatientsAge,AccessionNumber,HospitalId,(Dates+' '+Times) AS ExamineDate,StudyDescription,NumberOfStudyRelatedSeries AS StudySeries,NumberOfStudyRelatedInstances AS StudyInstances ,StudySizeInKB,Modality,ExaminePart,IsMasterCheckRecord,IsUploadCase FROM
                               (SELECT *,CONVERT(VARCHAR(100),CONVERT(DATE,StudyDate,23),23) AS Dates, SUBSTRING(StudyTime,1,2)+':'+SUBSTRING(StudyTime,3,2)+':'+SUBSTRING(StudyTime,5,2) AS Times FROM RequestStudyRecord) vv
                               WHERE RequestId='" + requestidParam + "'");
            list = db.ExecuteQuery<StudyRecordInfo>(strAppend.ToString()).ToList();
            return list;
        }

        /// <summary>
        /// 查询子列表数据
        /// </summary>
        /// <param name="requestParam"></param>
        /// <returns></returns>
        public List<StudyRecordInfo> GetRequestStudyRecordAllList(string requestParam)
        {
            List<StudyRecordInfo> list = new List<StudyRecordInfo>();
            StringBuilder strAppend = new StringBuilder();
            strAppend.Append(@"SELECT  RequestId,NewPatientId,HospitalId,StudyInstanceUid,PatientId,PatientsName,PatientSex,PatientsAge,AccessionNumber,HospitalId,(StudyDate+' '+StudyTime) AS ExamineDate,StudyDescription,NumberOfStudyRelatedSeries AS StudySeries,NumberOfStudyRelatedInstances AS StudyInstances ,StudySizeInKB,Modality,ExaminePart,IsMasterCheckRecord,IsUploadCase FROM RequestStudyRecord WHERE RequestId='" + requestParam + "'");
            list = db.ExecuteQuery<StudyRecordInfo>(strAppend.ToString()).ToList();
            return list;
        }

        /// <summary>
        /// 会诊
        /// 查询中心推荐、指定专家、已写报告会诊
        /// </summary>
        /// <param name="PatientName">病人姓名</param>
        /// <param name="ChooseType">类型</param>
        /// <param name="strDoctor">专家</param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <returns></returns>
        public List<OutRqeustInfo> GetPatient_ConsultationDetail(string PatientName, string PatientId, int ChooseType, string strDoctor, DateTime dtStart, DateTime dtEnd, int Age = 0, string Sex = null)
        {
            List<OutRqeustInfo> List = new List<OutRqeustInfo>();
            string StrSQL = string.Empty;
            try
            {
                StrSQL = @"SELECT P.PatientAge,R.PatientId,CASE
                            WHEN R.HospitalId IS NULL THEN
	                            R.PatientId
                            ELSE
	                            replace(
		                            R.PatientId,
		                            '.' + R.HospitalId,
		                            ''
	                            )
                            END AS NewPatientId,R.RequestId,P.PatientSex,R.DiagnoseDoctor,S.DeptDisplayName AS HospitalName,CONVERT(varchar(16),R.RequestDate, 20) AS RequestDate,
                           P.PatientName,P.CPatientName,R.AccessionNumber,R.ReportState,R.ReportPerson,R.StudyInstanceUid,R.RequestJDSJ,
                           ISNULL(R.FeeState,0) AS FeeState,ISNULL(R.RequestState,0) AS RequestState,R.RequestPurpose,
                           ISNULL(R.ReviewStatus,0) AS ReviewStatus,R.Diagnosticstate,ISNULL(SRP.Name,'') AS SRPDoctor, ISNULL(SRD.Name,'') AS SRDoctor,
                           ISNULL(SRD.Mobile, '') as RequestMobile,
                           CONVERT(varchar(16),P.PatientBirth,23) AS PatientBirth,
                           CONVERT(varchar(16),R.DiagnoseDate,20) AS DiagnoseDate,R.ExamineDate,R.ExaminePart,R.ExamineMethod,
                           R.ServeiceType as ServiceItem,RequestDoctor,
                           R.PatientId
                           FROM  RequestInfo AS R
                           LEFT JOIN PatientInfo P ON R.PatientId=P.PatientId
                           LEFT JOIN Sys_Dept S ON R.HospitalId=S.DeptId
                           LEFT JOIN Sys_Users AS SRP ON SRP.UserName=R.ReportPerson
                           LEFT JOIN Sys_Users AS SRD ON SRD.UserName=R.RequestDoctor
                           WHERE R.Diagnosticstate='Consultation'";
                if (PatientName.Trim() != "")
                {
                    StrSQL += " AND  (P.CPatientName LIKE'%" + PatientName + "%' OR  P.PatientName like'%" + PatientName + "%') ";
                }
                if (PatientId != "")
                {
                    StrSQL += " AND  R.PatientId LIKE '%" + PatientId + "%' ";
                }
                if (Age > 0)
                {
                    StrSQL += " AND P.PatientAge=" + Age;
                }
                if (!string.IsNullOrEmpty(Sex) && (Sex != "qb" && Sex != "qxz"))
                {
                    StrSQL += " And PatientSex='" + Sex + "'";
                }
                if (ChooseType == 21)
                {
                    //由指定专家和申请时间查询
                    if (dtStart != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.RequestDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                    }
                    if (dtEnd != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.RequestDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                    }
                    StrSQL += " AND R.DiagnoseDoctor='" + strDoctor + "' AND R.RequestState=11   ORDER BY RequestDate DESC";
                }
                else if (ChooseType == 22)
                {
                    //已写报告 由诊断医生和诊断时间查询
                    if (dtStart != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.DiagnoseDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                    }
                    if (dtEnd != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.DiagnoseDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                    }
                    StrSQL += " AND R.DiagnoseDoctor='" + strDoctor + "' AND R.RequestState=15 ORDER BY R.DiagnoseDate";
                }
                else if (ChooseType == 23)
                {
                    //总览报告
                    if (dtStart != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.DiagnoseDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                    }
                    if (dtEnd != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.DiagnoseDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                    }
                    StrSQL += " AND R.RequestState=15  ORDER BY R.DiagnoseDate";
                }
                else
                {
                    //中心资源 由诊断医生为NULL的所有会诊患者
                    if (dtStart != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.RequestDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                    }
                    if (dtEnd != Convert.ToDateTime("1900-01-01"))
                    {
                        StrSQL += " AND  R.RequestDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                    }
                    StrSQL += "AND (R.DiagnoseDoctor='' OR R.DiagnoseDoctor IS NULL) AND RequestState=11 ORDER BY RequestDate DESC";
                }
                StrSQL = ReplaceInstance(StrSQL, "GetPatient_ConsultationDetail::");
                List = db.ExecuteQuery<OutRqeustInfo>(StrSQL).ToList();
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("会诊查询异常GetPatient_ConsultationDetail：" + StrSQL, ex.Message);
            }
            return List;
        }

        /// <summary>
        /// 统计会诊数据量
        /// </summary>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <returns></returns>
        public List<OutRqeustInfo> GetConsulationStatistics(string HospitalID, DateTime dtStart, DateTime dtEnd)
        {
            List<OutRqeustInfo> List = new List<OutRqeustInfo>();
            string strCon = string.Empty;
            try
            {
                if (dtStart != Convert.ToDateTime("1900-01-01"))
                {
                    strCon += " AND  ri.RequestDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                }
                if (dtEnd != Convert.ToDateTime("1900-01-01"))
                {
                    strCon += " AND  ri.RequestDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                }
                db = new RemoteDianoseDataContext();
                string StrSql = string.Format(@"SELECT su.Name AS RequestDoctor, sd.Name AS DiagnoseDoctor, p.CPatientName, p.PatientAge, 
                                CASE WHEN ri.HospitalId IS NULL THEN ri.PatientId ELSE REPLACE( ri.PatientId, '.' + ri.HospitalId, '') END AS PatientId,
                                CASE p.PatientSex WHEN 'M' THEN '男' WHEN 'F' THEN '女' ELSE '其他' END AS PatientSex,
                                ri.ExamineDate,CONVERT(varchar(19),ri.RequestDate,20) AS RequestDate,CONVERT(varchar(19),ri.DiagnoseDate,20) AS DiagnoseDate
                                FROM RequestInfo ri
                                LEFT JOIN PatientInfo p ON ri.PatientId=p.PatientId
                                LEFT JOIN Sys_Users su ON su.UserName=ri.RequestDoctor
                                LEFT JOIN Sys_Users sd on sd .UserName=ri.DiagnoseDoctor
                                WHERE ri.Diagnosticstate='Consultation' AND ri.HospitalId='{0}' AND ri.RequestState='15' {1} ", HospitalID, strCon);
                StrSql = ReplaceInstance(StrSql, "GetConsulationStatistics::");
                List = db.ExecuteQuery<OutRqeustInfo>(StrSql).ToList();
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("GetConsulationStatistics", "错误信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return List;
        }

        public List<T> DataSetToList<T>(DataSet ds, int tableIndext)
        {
            if (ds == null || ds.Tables.Count <= 0 || tableIndext < 0)
            {
                return null;
            }
            DataTable dt = ds.Tables[tableIndext];

            IList<T> list = new List<T>();
            PropertyInfo[] tMembersAll = typeof(T).GetProperties();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                T t = Activator.CreateInstance<T>();
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    foreach (PropertyInfo tMember in tMembersAll)
                    {
                        if (dt.Columns[j].ColumnName.ToUpper().Equals(tMember.Name.ToUpper()))
                        {
                            if (dt.Rows[i][j] != DBNull.Value)
                            {
                                tMember.SetValue(t, dt.Rows[i][j], null);
                            }
                            else
                            {
                                tMember.SetValue(t, null, null);
                            }
                            break;//注意这里的break是写在if语句里面的，意思就是说如果列名和属性名称相同并且已经赋值了，那么我就跳出foreach循环，进行j+1的下次循环  
                        }
                    }
                }
                list.Add(t);
            }
            return list.ToList();
        }

        /// <summary>
        /// 获取申请、诊断或会诊患者数量
        /// </summary>
        /// <param name="strDoctor"></param>
        /// <param name="strHospitalID"></param>
        /// <param name="strConsulation"></param>
        /// <returns></returns>
        public int GetPatientCounts(string strDoctor, string strHospitalID, string strConsulation)
        {
            int patientCount = 0;
            try
            {
                StringBuilder strAppend = new StringBuilder();
                strAppend.Append("SELECT RequestId FROM RequestInfo WHERE 1=1 ");
                if (!string.IsNullOrEmpty(strHospitalID))
                {
                    strAppend.Append(" AND HospitalId='" + strHospitalID + "'");
                }
                else
                {
                    strAppend.Append(" AND HospitalId='0000'");
                }
                if (!string.IsNullOrEmpty(strDoctor))
                {
                    strAppend.Append(" AND RequestDoctor='" + strDoctor + "'");
                }
                if (!string.IsNullOrEmpty(strConsulation))
                {
                    strAppend.Append(" AND Diagnosticstate='" + strConsulation + "'");
                    if (strConsulation == "Consultation")
                    {
                        strAppend.Append(" AND RequestState =15");
                    }
                    if (strConsulation == "Diagnose")
                    {
                        strAppend.Append(" AND RequestState =15 AND ReviewStatus IN(0,1)");
                    }
                }
                else
                {
                    strAppend.Append(" AND RequestState >=7");
                }
                List<string> listCount = db.ExecuteQuery<string>(strAppend.ToString()).ToList();
                return patientCount = listCount.Count;
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("", "异常信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return patientCount;
        }
        /// <summary>
        /// 正常关闭报告状态
        /// </summary>
        /// <param name="requestID"></param>
        /// <returns></returns>
        public void NormalRequestInfoReportState(string requestID)
        {
            string updateSql = @"UPDATE RequestInfo SET ReportState=0,ReportDate = GetDate() WHERE RequestId='" + requestID + "'";
            db.ExecuteCommand(updateSql);
        }

        /// <summary>
        /// 更新报告状态
        /// </summary>
        /// <param name="requestID"></param>
        /// <param name="reportPerson"></param>
        /// <returns></returns>
        public ExcuteModel UpdateRequestInfoReportState(string requestID, string reportPerson)
        {
            ExcuteModel exModel = new ExcuteModel();
            db = new RemoteDianoseDataContext();
            string strSearch = @"SELECT A.ReportState,A.RequestState,A.ReviewStatus,A.ReportPerson,A.DiagnoseDoctor FROM RequestInfo  A WHERE A.RequestId='" + requestID + "'";
            OutRqeustInfo temp = db.ExecuteQuery<OutRqeustInfo>(strSearch).FirstOrDefault();
            if (temp != null)
            {
                if (temp.RequestState == 15)
                {
                    //处理异常关闭
                    if (temp.ReportState == 1 && temp.ReportPerson == reportPerson)
                    {
                        exModel.TempViewValue = 2;
                        ExecuteUpdateReportState(db, requestID, reportPerson);
                        exModel.Result = true;
                    }
                    else if (temp.ReportState == 0)
                    {
                        exModel.TempViewValue = 2;
                        ExecuteUpdateReportState(db, requestID, reportPerson);
                        exModel.Result = true;
                    }
                    else
                    {
                        exModel.Result = false;
                    }
                }
                //如果申请状态为20（已保存待提交），其他医生不能修改此报告，只能查看
                else if (temp.RequestState == 20)
                {
                    if (temp.ReportState == 1)
                    {
                        //判断填写报告人与打开报告人和登录报告人是否为同一个人，排除异常
                        if (temp.ReportPerson == reportPerson)
                        {
                            exModel.TempViewValue = 2;
                            ExecuteUpdateReportState(db, requestID, reportPerson);
                            exModel.Result = true;
                        }
                        else
                        {
                            exModel.Result = false;
                        }
                    }
                    //如果医生已诊断
                    else if (temp.DiagnoseDoctor == reportPerson)
                    {
                        exModel.TempViewValue = 2;
                        ExecuteUpdateReportState(db, requestID, reportPerson);
                        exModel.Result = true;
                    }
                    //其他医生只能查看
                    else
                    {
                        exModel.TempViewValue = 1;
                        ExecuteUpdateReportState(db, requestID, reportPerson);
                        exModel.Result = true;
                    }
                }
                //处理当填写报告时异常关闭
                else if (temp.RequestState == 11 && temp.ReportState == 1 && temp.ReportPerson == reportPerson)
                {
                    exModel.TempViewValue = 2;
                    ExecuteUpdateReportState(db, requestID, reportPerson);
                    exModel.Result = true;
                }
                else if (temp.ReportState == 1)
                {
                    if (temp.ReportPerson == reportPerson)
                    {
                        exModel.TempViewValue = 2;
                        ExecuteUpdateReportState(db, requestID, reportPerson);
                        exModel.Result = true;
                    }
                    else
                    {
                        //报告已打开
                        exModel.Result = false;
                    }
                }
                else
                {
                    //可打开报告并编辑
                    exModel.TempViewValue = 2;
                    ExecuteUpdateReportState(db, requestID, reportPerson);
                    exModel.Result = true;
                }
            }
            return exModel;
        }

        /// <summary>
        /// 获取诊断报告列表信息
        /// </summary>
        /// <param name="PatientId"></param>
        /// <param name="RequestState"></param>
        /// <param name="ReviewState"></param>
        /// <param name="RequestId"></param>
        /// <param name="DtStart"></param>
        /// <param name="DtEnd"></param>
        /// <returns></returns>
        public List<OutDiagnoseReport> GetDiagnoseReportList(string PatientId, string HospitalId, string RequestId, string RequestState, int ReviewState, DateTime DtStart, DateTime DtEnd)
        {
            List<OutDiagnoseReport> List = new List<OutDiagnoseReport>();
            string strSQL = string.Empty;
            try
            {
                string[] strTypes = RequestState.Split(',');
                string types = "";
                string HospitalIdmap = string.Empty;
                for (int k = 0; k < strTypes.Length; k++)
                {
                    types += "'" + strTypes[k] + "',";
                }
                types = types.Substring(0, types.Length - 1);

                strSQL = @"WITH NT AS (SELECT ROW_NUMBER() OVER(PARTITION BY RequestId ORDER BY RefusedDate DESC) AS ROWS,RequestId,RefusedPerson, RefusedId,RefusedDate 
                              FROM [RemoteDianose].[dbo].[RefusedRecord])
                              SELECT RI.PatientId,RI.RequestId,D.ReportDescription,D.ReportConClusion,CONVERT(varchar(16),D.DiagnoseDate, 20) AS DiagnoseDate,
                              CONVERT(varchar(16),RR.RefusedDate, 20) AS RefusedDate,RR.RefusedPerson,D.DiagnoseDoctor
                              FROM [RemoteDianose].[dbo].[RequestInfo] RI
                              LEFT JOIN [RemoteDianose].[dbo].[Diagnose] D ON RI.RequestId =D.RequestId
                              LEFT JOIN NT RR ON RR.RequestId=RI.RequestId  AND RR.ROWS=1
                              LEFT JOIN PullPatient PP ON PP.PatientID=RI.PatientId AND PP.RequestID=RI.RequestId
                              WHERE RequestState in(" + types + ") AND ReviewStatus=" + ReviewState + " AND Diagnosticstate='Diagnose'";
                if (!string.IsNullOrEmpty(PatientId))
                {
                    strSQL += " AND RI.PatientId LIKE '%" + PatientId + "%' ";
                }
                if (!string.IsNullOrEmpty(RequestId))
                {
                    strSQL += " AND RI.RequestId='" + RequestId + "'";
                }
                if (DtStart != Convert.ToDateTime("1900-01-01"))
                {
                    strSQL += " AND RI.DiagnoseDate >= CONVERT(varchar(100),'" + DtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                }
                if (DtEnd != Convert.ToDateTime("1900-01-01"))
                {
                    strSQL += " AND RI.DiagnoseDate<=CONVERT(varchar(100),'" + DtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                }
                if (!string.IsNullOrEmpty(HospitalId))
                {
                    var list = db.SubHospital.Where(u => u.PHospitalId == HospitalId).ToList();
                    if (list != null)
                    {

                        foreach (var u in list)
                        {
                            HospitalIdmap += "'" + u.SubHospitalId.ToString() + "',";
                        }
                        HospitalIdmap = HospitalIdmap + "'" + HospitalId + "'";
                    }
                    strSQL += " AND  RI.HospitalId in(" + HospitalIdmap + ")";
                }
                strSQL += " AND PP.RequestId IS NULL ORDER BY RI.DiagnoseDate DESC";
                strSQL = ReplaceInstance(strSQL, "GetDiagnoseReportList::");
                List = db.ExecuteQuery<OutDiagnoseReport>(strSQL).ToList();
            }
            catch (Exception ex)
            {
                string str = ex.Message;
            }
            return List;
        }

        private void ExecuteUpdateReportState(RemoteDianoseDataContext db, string requestID, string reportPerson)
        {
            string updateSql = @"UPDATE RequestInfo SET ReportState=1,ReportDate = GetDate(), ReportPerson='" + reportPerson + "' WHERE RequestId='" + requestID + "'";
            db.ExecuteCommand(updateSql);
        }

        /// <summary>
        /// 修改会诊申请单状态
        /// </summary>
        /// <param name="requestID"></param>
        /// <param name="reportPerson"></param>
        private int ExecuteUpdateReportState(string requestID, string reportPerson)
        {
            int result = 0;
            try
            {
                string updateSql = @"UPDATE RequestInfo SET ReportState=1,ReportDate = GetDate(), ReportPerson='" + reportPerson + "' WHERE RequestId='" + requestID + "'";
                result = db.ExecuteCommand(updateSql);
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return result;
        }

        /// <summary>
        /// 根据PatientId 获取病人信息
        /// </summary>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        public PatientInfo GetPatientInfoByPatientId(string PatientId)
        {
            PatientInfo model = new PatientInfo();
            model = db.PatientInfo.FirstOrDefault(t => t.PatientId == PatientId);
            return model;
        }

        /// <summary>
        /// 根据RequestId获取申请单记录
        /// </summary>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        public RequestInfo GetRequestInfoByRequestId(string RequestId, string studyId = null)
        {
            string StrSql = "";
            StrSql = "select * from RequestInfo where 1=1 ";
            if (!string.IsNullOrEmpty(RequestId))
            {
                StrSql += "AND RequestId ='" + RequestId + "'";
            }
            if (!string.IsNullOrEmpty(studyId))
            {
                StrSql += "AND StudyInstanceUid ='" + studyId + "'";
            }
            RequestInfo model = new RequestInfo();
            if (string.IsNullOrEmpty(RequestId) && string.IsNullOrEmpty(studyId))
            {
                model = new RequestInfo();
            }
            else
            {
                model = db.ExecuteQuery<RequestInfo>(StrSql).FirstOrDefault();
            }
            return model;
        }

        public List<RequestInfoStatis> GetRequestStudyRecordByRequestId(string RequestId)
        {
            string StrSql = "";
            StrSql = "select RSR.PatientId,RSR.StudyInstanceUid,RSR.RequestId,RSR.NewPatientId,Flag from RequestInfo AS RI inner join RequestStudyRecord AS RSR on RI.RequestId=RSR.RequestId";
            StrSql += " where 1=1";
            if (!string.IsNullOrEmpty(RequestId))
            {
                StrSql += "AND RI.RequestId ='" + RequestId + "'";
            }
            List<RequestInfoStatis> list = new List<RequestInfoStatis>();
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(StrSql, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                list.Add(new RequestInfoStatis()
                {
                    PatientId = reader["PatientId"].ToString(),
                    StudyInstanceUid = reader["StudyInstanceUid"].ToString(),
                    RequestId = reader["RequestId"].ToString(),
                    NewPatientId = reader["NewPatientId"].ToString(),
                    Flag = Convert.ToInt32(reader["Flag"])
                });
            }
            conn.Close();
            return list;
        }

        /// <summary>
        /// 在保存患者信息时，判断同一患者是否连续上传
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<RequestInfo> GetRequestInfoListByStudyInstance(string hospital, string param)
        {
            List<RequestInfo> ResultModel = db.RequestInfo.Where(t => t.StudyInstanceUid == param && t.HospitalId == hospital).ToList();
            return ResultModel;
        }

        /// <summary>
        /// 获取同一个人的历史申请单
        /// </summary>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        public List<RequestInfo> GetRequestInfoByPatientId(string PatientId)
        {
            db = new RemoteDianoseDataContext();
            List<RequestInfo> list = new List<RequestInfo>();
            list = db.RequestInfo.Where(t => t.PatientId == PatientId && t.RequestState == 15).ToList();
            return list;
        }

        /// <summary>
        /// 保存 PatientInfo
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SavePatientInfo(PatientInfo model)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                PatientInfo _modelExist = db.PatientInfo.FirstOrDefault(t => t.PatientId == model.PatientId);
                if (_modelExist != null)
                {
                    if (!string.IsNullOrEmpty(model.PatientIdentityNum))
                    {
                        _modelExist.PatientIdentityNum = model.PatientIdentityNum;
                    }
                    if (!string.IsNullOrEmpty(model.PatientIdentityType))
                    {
                        _modelExist.PatientIdentityType = model.PatientIdentityType;
                    }
                    if (model.PatientBirth != null)
                    {
                        _modelExist.PatientBirth = model.PatientBirth;
                    }
                    if (!string.IsNullOrEmpty(model.PatientName))
                    {
                        _modelExist.PatientName = model.PatientName;
                    }
                    if (!string.IsNullOrEmpty(model.PatientSex))
                    {
                        _modelExist.PatientSex = model.PatientSex;
                    }
                    if (!string.IsNullOrEmpty(model.PatientAge))
                    {
                        _modelExist.PatientAge = model.PatientAge;
                    }
                    if (!string.IsNullOrEmpty(model.PatientMobile))
                    {
                        _modelExist.PatientMobile = model.PatientMobile;
                    }
                    if (!string.IsNullOrEmpty(model.PatientMarriageState))
                    {
                        _modelExist.PatientMarriageState = model.PatientMarriageState;
                    }
                    if (!string.IsNullOrEmpty(model.PatientMZ))
                    {
                        _modelExist.PatientMZ = model.PatientMZ;
                    }
                    if (!string.IsNullOrEmpty(model.PatientGJ))
                    {
                        _modelExist.PatientGJ = model.PatientGJ;
                    }
                    if (!string.IsNullOrEmpty(model.PatientTel))
                    {
                        _modelExist.PatientTel = model.PatientTel;
                    }
                    if (!string.IsNullOrEmpty(model.GZDWMC))
                    {
                        _modelExist.GZDWMC = model.GZDWMC;
                    }
                    if (!string.IsNullOrEmpty(model.GZDWDZ))
                    {
                        _modelExist.GZDWDZ = model.GZDWDZ;
                    }
                    if (!string.IsNullOrEmpty(model.JZDZ))
                    {
                        _modelExist.JZDZ = model.JZDZ;
                    }
                    if (!string.IsNullOrEmpty(model.HKDZ))
                    {
                        _modelExist.HKDZ = model.HKDZ;
                    }
                    if (!string.IsNullOrEmpty(model.HKDZYB))
                    {
                        _modelExist.HKDZYB = model.HKDZYB;
                    }
                    if (!string.IsNullOrEmpty(model.LXRXM))
                    {
                        _modelExist.LXRXM = model.LXRXM;
                    }
                    if (!string.IsNullOrEmpty(model.LXRGX))
                    {
                        _modelExist.LXRGX = model.LXRGX;
                    }
                    if (!string.IsNullOrEmpty(model.LXRDZ))
                    {
                        _modelExist.LXRDZ = model.LXRDZ;
                    }
                    if (!string.IsNullOrEmpty(model.LXRDH))
                    {
                        _modelExist.LXRDH = model.LXRDH;
                    }
                    if (!string.IsNullOrEmpty(model.CPatientName))
                    {
                        _modelExist.CPatientName = model.CPatientName;
                    }
                    if (!string.IsNullOrEmpty(model.OutPatientNum))
                    {
                        _modelExist.OutPatientNum = model.OutPatientNum;
                    }
                    if (!string.IsNullOrEmpty(model.RegisterNum))
                    {
                        _modelExist.RegisterNum = model.RegisterNum;
                    }
                    if (model.DataUpdateTime != null)
                    {
                        _modelExist.DataUpdateTime = DateTime.Now;
                    }
                    if (!string.IsNullOrEmpty(model.BirthTime))
                    {
                        _modelExist.BirthTime = model.BirthTime;
                    }
                    if (model.PatientWeight != null)
                    {
                        _modelExist.PatientWeight = model.PatientWeight;
                    }
                    RetModel.RetValue = _modelExist.PatientId;
                }
                else
                {
                    model.DataCreateTime = DateTime.Now;
                    model.DataUpdateTime = DateTime.Now;
                    db.PatientInfo.InsertOnSubmit(model);
                }
                db.SubmitChanges();
                RetModel.Result = true;
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        /// <summary>
        /// 修改患者基本信息
        /// 只做修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdatePatientInfo(PatientInfo model)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                PatientInfo _modelExist = db.PatientInfo.FirstOrDefault(t => t.PatientId == model.PatientId);
                if (_modelExist != null)
                {
                    _modelExist.PatientIdentityNum = model.PatientIdentityNum;
                    _modelExist.PatientIdentityType = model.PatientIdentityType;
                    _modelExist.PatientBirth = model.PatientBirth;
                    _modelExist.PatientName = model.PatientName;
                    _modelExist.PatientSex = model.PatientSex;
                    _modelExist.PatientAge = model.PatientAge;
                    _modelExist.PatientBirth = model.PatientBirth;
                    _modelExist.PatientMobile = model.PatientMobile;
                    _modelExist.PatientMarriageState = model.PatientMarriageState;
                    _modelExist.PatientMZ = model.PatientMZ;
                    _modelExist.PatientGJ = model.PatientGJ;
                    _modelExist.PatientTel = model.PatientTel;
                    _modelExist.GZDWMC = model.GZDWMC;
                    _modelExist.GZDWDZ = model.GZDWDZ;
                    _modelExist.JZDZ = model.JZDZ;
                    _modelExist.HKDZ = model.HKDZ;
                    _modelExist.HKDZYB = model.HKDZYB;
                    _modelExist.LXRXM = model.LXRXM;
                    _modelExist.LXRGX = model.LXRGX;
                    _modelExist.LXRDZ = model.LXRDZ;
                    _modelExist.LXRDH = model.LXRDH;
                    _modelExist.CPatientName = model.CPatientName;
                    _modelExist.DataUpdateTime = DateTime.Now;
                    _modelExist.OutPatientNum = model.OutPatientNum;
                    _modelExist.RegisterNum = model.RegisterNum;
                    RetModel.RetValue = _modelExist.PatientId;
                    db.SubmitChanges();
                    RetModel.Msg = "修改信息成功！";
                    RetModel.Result = true;
                }
                else
                {
                    RetModel.Result = false;
                    RetModel.Msg = "患者未存在，请先添加！";
                }
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        /// <summary>
        /// 根据证件类型，证件号码获取病人信息
        /// </summary>
        /// <param name="ZJLX"></param>
        /// <param name="ZJHM"></param>
        /// <returns></returns>
        public PatientInfo GetPatientInfoByZJHM(string ZJLX, string ZJHM)
        {
            db = new RemoteDianoseDataContext();
            PatientInfo model = db.PatientInfo.FirstOrDefault(t => t.PatientIdentityType == ZJLX && t.PatientIdentityNum == ZJHM);
            return model;
        }

        /// <summary>
        /// 保存 RequestInfo
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveRequestInfo(RequestInfo model)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                RequestInfo _model = db.RequestInfo.FirstOrDefault(t => t.RequestId == model.RequestId);
                if (_model != null)
                {
                    if (model.Cost > 0)
                    {
                        _model.Cost = model.Cost;
                    }
                    if (model.DiagnoseDate != null)
                    {
                        _model.DiagnoseDate = model.DiagnoseDate;
                    }
                    if (!string.IsNullOrEmpty(model.DiagnoseDoctor))
                    {
                        _model.DiagnoseDoctor = model.DiagnoseDoctor;
                    }
                    if (!string.IsNullOrEmpty(model.ClinicalDiagnose))
                    {
                        _model.ClinicalDiagnose = model.ClinicalDiagnose;
                    }
                    if (!string.IsNullOrEmpty(model.DiseaseSummary))
                    {
                        _model.DiseaseSummary = model.DiseaseSummary;
                    }
                    if (!string.IsNullOrEmpty(model.ExaminePurpose))
                    {
                        _model.ExaminePurpose = model.ExaminePurpose;
                    }
                    if (model.ExamineDate != null)
                    {
                        _model.ExamineDate = model.ExamineDate;
                    }
                    if (!string.IsNullOrEmpty(model.ExamineMethod))
                    {
                        _model.ExamineMethod = model.ExamineMethod;
                    }
                    if (!string.IsNullOrEmpty(model.ExaminePart))
                    {
                        _model.ExaminePart = model.ExaminePart;
                    }
                    if (model.FileCount > 0)
                    {
                        _model.FileCount = model.FileCount;
                    }
                    if (!string.IsNullOrEmpty(model.HospitalId))
                    {
                        _model.HospitalId = model.HospitalId;
                    }
                    if (!string.IsNullOrEmpty(model.PatientId))
                    {
                        _model.PatientId = model.PatientId;
                    }
                    if (model.RefuseDate != null)
                    {
                        _model.RefuseDate = model.RefuseDate;
                    }
                    if (!string.IsNullOrEmpty(model.RefuseReason))
                    {
                        _model.RefuseReason = model.RefuseReason;
                    }
                    if (!string.IsNullOrEmpty(model.Remark))
                    {
                        _model.Remark = model.Remark;
                    }
                    if (!string.IsNullOrEmpty(model.RequestJDR))
                    {
                        _model.RequestJDR = model.RequestJDR;
                    }
                    if (model.RequestJDSJ != null)
                    {
                        _model.RequestJDSJ = model.RequestJDSJ;
                    }
                    if (!string.IsNullOrEmpty(model.RequestId))
                    {
                        _model.RequestId = model.RequestId;
                        RetModel.RetValue = model.RequestId;
                    }
                    if (model.RequestState != null)
                    {
                        _model.RequestState = model.RequestState;
                    }
                    if (model.Flag != null)
                    {
                        _model.Flag = model.Flag;
                    }
                    //ReviewStatus，只需判断不为空，如果为0，修改时无法使用
                    if (model.ReviewStatus != null)
                    {
                        _model.ReviewStatus = model.ReviewStatus;
                    }
                    if (model.ReportState != null)
                    {
                        _model.ReportState = model.ReportState;
                    }
                    if (model.FeeState != null)
                    {
                        _model.FeeState = model.FeeState;
                    }
                    if (!string.IsNullOrEmpty(model.StudyInstanceUid))
                    {
                        _model.StudyInstanceUid = model.StudyInstanceUid;
                    }
                    if (!string.IsNullOrEmpty(model.ServeiceType))
                    {
                        _model.ServeiceType = model.ServeiceType;
                    }
                    if (!string.IsNullOrEmpty(model.RequestPurpose))
                    {
                        _model.RequestPurpose = model.RequestPurpose;
                    }
                    if (!string.IsNullOrEmpty(model.PatientType))
                    {
                        _model.PatientType = model.PatientType;
                    }
                    if (!string.IsNullOrEmpty(model.UrgentType))
                    {
                        _model.UrgentType = model.UrgentType;
                    }
                    if (!string.IsNullOrEmpty(model.ExamineType))
                    {
                        _model.ExamineType = model.ExamineType;
                    }
                    if (model.NumberOfStudyRelatedInstances > 0)
                    {
                        _model.NumberOfStudyRelatedInstances = model.NumberOfStudyRelatedInstances;
                    }
                    if (model.NumberOfStudyRelatedSeries > 0)
                    {
                        _model.NumberOfStudyRelatedSeries = model.NumberOfStudyRelatedSeries;
                    }
                    if (model.RequestDate != null)
                    {
                        _model.RequestDate = model.RequestDate;
                    }
                    if (model.DeviceId != null)
                    {
                        _model.DeviceId = model.DeviceId;
                    }
                }
                else
                {
                    model.RequestJDSJ = DateTime.Now;
                    model.ExamineDate = DateTime.Now;
                    model.RequestDate = DateTime.Now;
                    if (model.UploadWay == null)
                    {
                        model.UploadWay = 1;
                    }
                    if (model.ReportState == null)
                    {
                        model.ReportState = 0;
                    }
                    if (model.IsPrint == null)
                    {
                        model.IsPrint = 0;
                    }
                    db.RequestInfo.InsertOnSubmit(model);
                }
                db.SubmitChanges();
                RetModel.RetValue = model.RequestId;
                RetModel.Result = true;
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
                LogHelper.WriteErrorLog("SaveRequestInfo", "异常信息：" + exp.Message + "--堆栈信息：" + exp.StackTrace);
            }
            return RetModel;
        }

        /// <summary>
        /// 保存并更新诊断时间
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveUpdateRequestInfo(RequestInfo model)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                RequestInfo _model = db.RequestInfo.FirstOrDefault(t => t.RequestId == model.RequestId);
                if (_model != null)
                {
                    _model.ReviewStatus = model.ReviewStatus;
                    _model.DiagnoseDoctor = model.DiagnoseDoctor;
                    _model.DiagnoseDate = DateTime.Now;
                    db.SubmitChanges();
                    RetModel.Result = true;
                }
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        /// <summary>
        /// 修改打印状态
        /// </summary>
        /// <param name="RequestID"></param>
        /// <returns></returns>
        public ExcuteModel UpdateRequestPrintState(string RequestID)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                RequestInfo model = db.RequestInfo.Where(t => t.RequestId == RequestID).FirstOrDefault();
                if (model != null)
                {
                    model.IsPrint = 1;
                    db.SubmitChanges();
                    RetModel.Result = true;
                }
            }
            catch (Exception ex)
            {
                RetModel.Result = false;
                RetModel.Msg = ex.ToString();
            }
            return RetModel;
        }

        /// <summary>
        /// 保存 RequestInfo 更新申请单状态
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel ChangeRequestState(InRequestReport InModel)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                RequestInfo _model = db.RequestInfo.FirstOrDefault(t => t.RequestId == InModel.RequestId);
                if (_model != null)
                {
                    _model.RequestState = InModel.RequestState;
                    if (InModel.RequestState == 5)//退费
                    {
                        _model.FeeState = 3;
                    }
                    else if (InModel.RequestState == 3)//缴费
                    {
                        _model.FeeState = 2;
                    }
                    if (InModel.ReviewStatus != null)
                    {
                        _model.ReviewStatus = InModel.ReviewStatus;
                    }
                    db.SubmitChanges();
                    RetModel.Result = true;
                }
                else
                {
                    RetModel.Result = false;
                    RetModel.Msg = "信息不存在";
                }
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        /// <summary>
        /// 获取申请单状态
        /// </summary>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        public int GetRequestState(string RequestId)
        {
            RequestInfo _model = db.RequestInfo.FirstOrDefault(t => t.RequestId == RequestId);
            int _RetState = -99;
            if (_model != null)
            {
                _RetState = int.Parse(_model.RequestState.ToString());
            }
            return _RetState;
        }

        /// <summary>
        /// 保存报告新方法
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel SaveDiagnoseReportNew(InDianose param)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                Diagnose _model = db.Diagnose.FirstOrDefault(t => t.RequestId == param.RequestId);
                if (_model != null)
                {
                    _model.ReportDescription = param.ReportDescriptionHtml;
                    _model.ReportConClusion = param.ReportConClusionHtml;
                    _model.ReportSuggestion = param.ReportSuggestionHtml;
                    if (!string.IsNullOrEmpty(param.DiagnoseDoctor))
                    {
                        _model.DiagnoseDoctor = param.DiagnoseDoctor;
                    }
                    if (param.StateYinYang != null)
                    {
                        _model.YinYang = param.StateYinYang;
                    }
                    if (!string.IsNullOrEmpty(param.Crifically))
                    {
                        _model.Crifically = param.Crifically;
                    }
                }
                else
                {
                    Diagnose newModel = new Diagnose();
                    newModel.RequestId = param.RequestId;
                    newModel.ReportDescription = param.ReportDescriptionHtml;
                    newModel.ReportConClusion = param.ReportConClusionHtml;
                    newModel.ReportSuggestion = param.ReportSuggestionHtml;
                    newModel.DiagnoseState = 0;
                    newModel.DiagnoseDate = DateTime.Now;
                    newModel.Crifically = param.Crifically;
                    newModel.YinYang = param.StateYinYang;
                    if (!string.IsNullOrEmpty(param.DiagnoseDoctor))
                    {
                        newModel.DiagnoseDoctor = param.DiagnoseDoctor;
                    }
                    db.Diagnose.InsertOnSubmit(newModel);
                }
                db.SubmitChanges();

                AuditRecord ar = new AuditRecord
                {
                    SHrecordid = Guid.NewGuid().ToString(),
                    State = param.reportState,
                    RequestId = param.RequestId,
                    CheckPeople = param.CheckPeople,
                    ReportDescription = param.ReportDescriptionHtml,
                    ReportConClusion = param.ReportConClusionHtml,
                    ReportSuggestion = param.ReportSuggestionHtml,
                    DiagnoseDate = param.DiagnoseDate,
                    DiagnoseDoctor = param.DiagnoseDoctor,
                    CheckDatetime = DateTime.Now,
                    WriteReportPeople = param.WriteReportPeople
                };
                db.AuditRecord.InsertOnSubmit(ar);
                db.SubmitChanges();

                RequestInfo RequestInfomodel = db.RequestInfo.Where(t => t.RequestId == param.RequestId).FirstOrDefault();
                if (RequestInfomodel != null)
                {
                    if (param.submit_Param == 1)
                    {
                        RequestInfomodel.RequestState = 20;
                    }
                    else if (param.submit_Param == 2)
                    {
                        RequestInfomodel.RequestState = 16;
                        RequestInfomodel.ReviewStatus = 1;
                    }
                    else
                    {
                        RequestInfomodel.RequestState = 15;
                    }
                    RequestInfomodel.DiagnoseDate = DateTime.Now;
                    if (!string.IsNullOrEmpty(param.DiagnoseDoctor))
                    {
                        RequestInfomodel.DiagnoseDoctor = param.DiagnoseDoctor;
                    }
                    db.SubmitChanges();
                    RetModel.Result = true;
                }
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
                LogHelper.WriteErrorLog("SaveDiagnoseReportNew", "异常信息：" + exp.Message + "--堆栈信息：" + exp.StackTrace);
            }
            return RetModel;
        }

        /// <summary>
        /// 获取报告诊断
        /// </summary>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        public Diagnose GetDianoseByRequestId(string RequestId)
        {
            Diagnose Diagnose = new DB_Server.Diagnose();
            RequestInfo model = new RequestInfo();
            model = db.RequestInfo.FirstOrDefault(u => u.RequestId == RequestId);
            switch (Convert.ToInt32(model.ReviewStatus))
            {
                case 0:
                    return db.Diagnose.FirstOrDefault(t => t.RequestId == RequestId);
                case 1:
                    List<AuditRecord> AuditRecord_list = db.AuditRecord.Where(t => t.RequestId == RequestId && t.State == "1").ToList().OrderByDescending(u => u.CheckDatetime).ToList();
                    AuditRecord AuditRecord_one = AuditRecord_list.FirstOrDefault(t => t.RequestId == RequestId);
                    if (AuditRecord_one != null)
                    {
                        DateTime time;
                        try
                        {
                            time = Convert.ToDateTime(AuditRecord_one.DiagnoseDate);
                        }
                        catch (Exception)
                        {
                            time = DateTime.Now;
                        }
                        Diagnose = new Diagnose()
                        {
                            RequestId = AuditRecord_one.RequestId,
                            DiagnoseDate = time,
                            DiagnoseDoctor = AuditRecord_one.DiagnoseDoctor,
                            NewAutoID = Convert.ToInt32(AuditRecord_one.NewAutoID == "" ? 0 : Convert.ToInt32(AuditRecord_one.NewAutoID)),
                            ReportConClusion = AuditRecord_one.ReportConClusion,
                            ReportDescription = AuditRecord_one.ReportDescription,
                            ReportSuggestion = AuditRecord_one.ReportSuggestion
                        };
                    }
                    else
                    {
                        Diagnose = db.Diagnose.FirstOrDefault(t => t.RequestId == RequestId);
                    }
                    return Diagnose;
                case 2:
                    break;
            }
            return null;
        }

        /// <summary>
        /// 更新报告状态
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel ChangeDiagnoseState(Diagnose model)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                Diagnose _model = db.Diagnose.FirstOrDefault(t => t.RequestId == model.RequestId);
                if (_model != null)
                {
                    _model.DiagnoseState = model.DiagnoseState;
                    db.SubmitChanges();
                    RetModel.Result = true;
                }
                else
                {
                    RetModel.Result = false;
                    RetModel.Msg = "信息不存在";
                }
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        /// <summary>
        /// 获取诊断报告内容信息
        /// </summary>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        public Diagnose GetDiagnoseReportByRequesId(string RequestId)
        {
            Diagnose model = null;
            try
            {
                model = db.Diagnose.FirstOrDefault(t => t.RequestId == RequestId);
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("GetDiagnoseReportByRequesId", "获取报告内容异常：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return model;
        }

        /// <summary>
        /// 缴费
        /// </summary>
        /// <param name="InModel"></param>
        /// <returns></returns>
        public ExcuteModel Charge(InChage InModel)
        {
            ExcuteModel RetModel = new ExcuteModel();
            return RetModel;
        }

        /// <summary>
        /// 修改需要会诊的医生
        /// 手机端App保存服务项目和会诊专家时使用
        /// </summary>
        /// <param name="RequestId"></param>
        /// <param name="DoctorName"></param>
        /// <param name="ServiceId">服务项目ID可选填</param>
        /// <returns></returns>
        public ExcuteModel ChangeDianoseDoctor(string RequestId, string DoctorName, string ServiceId = "")
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                RequestInfo _model = db.RequestInfo.FirstOrDefault(t => t.RequestId == RequestId);
                if (_model != null)
                {
                    if (!string.IsNullOrEmpty(DoctorName))
                    {
                        _model.DiagnoseDoctor = DoctorName;
                    }
                    if (!string.IsNullOrEmpty(ServiceId))
                    {
                        _model.ServeiceType = ServiceId;
                    }
                    db.SubmitChanges();
                    RetModel.Result = true;
                }
                else
                {
                    RetModel.Result = false;
                }
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        /// <summary>
        /// 申请会诊
        /// </summary>
        /// <param name="InModel"></param>
        /// <returns></returns>
        public ExcuteModel RequestSubmit(InRequest InModel)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                RequestInfo _model = db.RequestInfo.FirstOrDefault(t => t.RequestId == InModel.RequestId);
                if (_model != null)
                {
                    if (!string.IsNullOrEmpty(InModel.RequestPurpose))
                    {
                        _model.RequestPurpose = InModel.RequestPurpose;
                    }
                    _model.RequestDate = DateTime.Now;
                    if (!string.IsNullOrEmpty(InModel.RequestDoctor))
                    {
                        _model.RequestDoctor = InModel.RequestDoctor;
                    }
                    if (InModel.RequestState != null)
                    {
                        _model.RequestState = InModel.RequestState;
                    }
                    if (!string.IsNullOrEmpty(InModel.DoctorName))
                    {
                        _model.DiagnoseDoctor = InModel.DoctorName;
                    }
                    if (!string.IsNullOrEmpty(InModel.ExaminePart))
                    {
                        _model.ExaminePart = InModel.ExaminePart;
                    }
                    if (!string.IsNullOrEmpty(InModel.ExamineMethod))
                    {
                        _model.ExamineMethod = InModel.ExamineMethod;
                    }
                    if (InModel.ExamineDate != null)
                    {
                        _model.ExamineDate = InModel.ExamineDate;
                    }
                    db.SubmitChanges();
                    RetModel.Result = true;
                }
                else
                {
                    RetModel.Result = false;
                }
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        #region 费用

        /// <summary>
        /// 保存会诊申请服务和费用信息
        /// 用于缴费
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveRequestServiceCost(RequestInfo model)
        {
            ExcuteModel RetModel = new ExcuteModel();
            RequestInfo riModel = db.RequestInfo.FirstOrDefault(t => t.RequestId == model.RequestId);
            if (riModel != null)
            {
                if (riModel.Diagnosticstate == "Consultation")
                {
                    if (riModel.FeeState == 2)
                    {
                        riModel.FeeState = 2;
                        riModel.RequestState = 3;
                    }
                    else
                    {
                        riModel.FeeState = 1;
                    }
                }
                else
                {
                    riModel.FeeState = 2;
                }
                riModel.Cost = model.Cost;
                riModel.ServeiceType = model.ServeiceType;
                db.SubmitChanges();
                RetModel.Result = true;
            }
            else
            {
                RetModel.Result = false;
                RetModel.Msg = "未查询到会诊申请";
            }
            return RetModel;
        }
        #endregion

        #region 病人文件

        public ExcuteModel SaveRequestFileRoot(RequestFileRoot model)
        {
            ExcuteModel retModel = new ExcuteModel();
            try
            {
                if (model != null)
                {
                    List<RequestFileRoot> list = db.RequestFileRoot.Where(t => t.PatientId == model.PatientId && t.RequestId == model.RequestId && t.FileRoot == model.FileRoot && t.FileDL == model.FileDL && t.FileXL == model.FileXL).ToList();
                    if (list.Count > 0)
                    {
                        RequestFileRoot _model = list[0];
                        _model.CollectDate = DateTime.Now;
                        _model.CollectPerson = model.CollectPerson;
                        _model.VisitDate = model.VisitDate;
                        db.SubmitChanges();
                        retModel.Result = true;
                        retModel.RetValue = _model.GUID;
                    }
                    else
                    {
                        db.RequestFileRoot.InsertOnSubmit(model);
                        db.SubmitChanges();
                        retModel.Result = true;
                        retModel.RetValue = model.GUID;
                    }
                }
            }
            catch (Exception exp)
            {
                retModel.Result = false;
                retModel.Msg = exp.ToString();
            }
            return retModel;
        }

        public List<RequestFileRoot> GetRequestFileRootByRequestIdAndFileRoot(string RequestId, int FileRoot)
        {
            return db.RequestFileRoot.Where(t => t.RequestId == RequestId && t.FileRoot == FileRoot).ToList();
        }

        /// <summary>
        /// 根据RequestId,获取病历所有信息
        /// </summary>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        public List<RequestFileRoot> GetRequestFileRootByRequestIdAll(string RequestId)
        {
            return db.RequestFileRoot.Where(t => t.RequestId == RequestId).ToList();
        }

        /// <summary>
        /// 根据RequestId,获取所有文件信息
        /// </summary>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        public List<RequestFile> GetRequestFileByRequestIdAll(string RequestId)
        {
            return db.RequestFile.Where(t => t.RequestId == RequestId).ToList();
        }

        /// <summary>
        /// 根据RequestId和StudyInstanceUID
        /// </summary>
        /// <param name="RequestId"></param>
        /// <param name="StudyInstanceUID"></param>
        /// <returns></returns>
        public List<RequestFileItem> GetAllRequestFileItems(string RequestId, string StudyInstanceUID)
        {
            //var StrSql = @"SELECT fi.* FROM RequestFileItem fi LEFT JOIN RequestFileRoot fr ON fi.FileRootId=fr.GUID
            //              WHERE  fr.RequestId='" + RequestId + "' AND fi.StudyInstanceUid='" + StudyInstanceUID + "' AND fi.Source<>9";
            string strTemp = string.Empty;
            if (!string.IsNullOrEmpty(StudyInstanceUID))
            {
                strTemp = "AND fi.StudyInstanceUid='" + StudyInstanceUID + "'";
            }
            string strSql = string.Format(@"SELECT fi.* FROM RequestFileItem fi LEFT JOIN RequestFileRoot fr ON fi.FileRootId=fr.GUID
                          WHERE  fr.RequestId='{0}' {1} AND fi.Source<>9", RequestId, strTemp);

            return db.ExecuteQuery<RequestFileItem>(strSql).ToList();
        }

        public List<RequestFile> GetAllFileListByRequestId(string RequestId, int type)
        {
            string StrSql = "";
            if (type == 2)
            {
                StrSql = "select * from RequestFile where RequestId='" + RequestId + "' and source in(8,9)";
            }
            else if (type == 0)
            {
                StrSql = "select * from RequestFile where RequestId='" + RequestId + "'";
            }
            else
            {
                StrSql = "select * from RequestFile where RequestId='" + RequestId + "' and source not in(8,9)";
            }
            return db.ExecuteQuery<RequestFile>(StrSql).ToList();
        }

        public ExcuteModel SaveFileItem(RequestFileItem model)
        {
            ExcuteModel retModel = new ExcuteModel();
            try
            {
                if (model != null)
                {
                    RequestFileItem obj = new RequestFileItem();
                    if (!string.IsNullOrEmpty(model.RequestId))
                    {
                        obj = db.RequestFileItem.Where(o => o.RequestId == model.RequestId && o.FileName == model.FileName).FirstOrDefault();
                    }
                    else
                    {
                        obj = db.RequestFileItem.Where(o => o.FileName == model.FileName).FirstOrDefault();
                    }
                    if (obj != null)
                    {
                        if (!string.IsNullOrEmpty(model.FileName))
                        {
                            obj.FileName = model.FileName;
                        }
                        if (!string.IsNullOrEmpty(model.FileRootId))
                        {
                            obj.FileRootId = model.FileRootId;
                        }
                        if (!string.IsNullOrEmpty(model.GUID))
                        {
                            obj.GUID = model.GUID;
                        }
                        if (!string.IsNullOrEmpty(model.FileType))
                        {
                            obj.FileType = model.FileType;
                        }
                        obj.VisitDate = model.VisitDate;
                        if (!string.IsNullOrEmpty(model.RequestId))
                        {
                            obj.RequestId = model.RequestId;
                        }
                        if (obj.FileType == "scuess" && model.FileType == "" && obj.Source == 15)
                        {
                            obj.FileType = "";
                        }
                        obj.CollectPerson = model.CollectPerson;
                        db.RequestFileItem.DeleteOnSubmit(obj);
                        db.RequestFileItem.InsertOnSubmit(model);
                    }
                    else
                    {
                        db.RequestFileItem.InsertOnSubmit(model);
                    }

                    db.SubmitChanges();
                }
                retModel.Result = true;
            }
            catch (Exception exp)
            {
                retModel.Result = false;
                retModel.Msg = exp.ToString();
            }
            return retModel;
        }

        public List<RequestFileItem> GetFileItemsBySource(int Source, string RequestId)
        {
            return db.RequestFileItem.Where(t => t.Source == Source && t.RequestId == RequestId).ToList();
        }

        public List<RequestFile> GetDicomFileByRequestIdAndMPID(string RequestId, string MPID)
        {
            return db.RequestFile.Where(t => t.RequestId == RequestId && t.MPID == MPID && t.Source == 8).ToList();
        }

        public RequestFile GetRequestFileByMPID(string RequestId, string MPID)
        {
            return db.RequestFile.FirstOrDefault(t => t.RequestId == RequestId && t.MPID == MPID && t.Source == 9);
        }

        public List<RequestFileItem> GetFileItemsByFileId(string FileId)
        {
            return db.RequestFileItem.Where(t => t.FileId == FileId).ToList();
        }

        public List<RequestFileItem> GetFileItemsByFileRootId(string FileRootId)
        {
            return db.RequestFileItem.Where(t => t.FileRootId == FileRootId).ToList();
        }

        public List<RequestFileItem> GetFileItemsByRequestId(string RequestId)
        {
            return db.RequestFileItem.Where(t => t.RequestId == RequestId).ToList();
        }

        /// <summary>
        /// 获取数据不为9的所有信息
        /// </summary>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        public List<RequestFileItem> GetNoFileItemsByRequestId(string RequestId, string StudyUid = null)
        {
            if (!string.IsNullOrEmpty(StudyUid))
            {
                return db.RequestFileItem.Where(t => t.RequestId == RequestId && t.StudyInstanceUid == StudyUid && t.Source != 9).ToList();
            }
            else
            {
                return db.RequestFileItem.Where(t => t.RequestId == RequestId && t.Source != 9).ToList();
            }
        }

        public ExcuteModel SaveFile(RequestFile model)
        {
            ExcuteModel retModel = new ExcuteModel();
            try
            {
                if (model != null)
                {
                    var tmodel = db.RequestFile.FirstOrDefault(t => t.RequestId == model.RequestId && t.FileName == model.FileName);
                    if (tmodel != null)
                    {
                        model.FileId = tmodel.FileId;
                        model.FileName = tmodel.FileName;
                        model.FileType = tmodel.FileType;
                        model.Source = tmodel.Source;
                        model.VisitDate = tmodel.VisitDate;
                        model.PatientId = tmodel.PatientId;
                    }
                    db.RequestFile.InsertOnSubmit(model);
                    db.SubmitChanges();
                }
                retModel.Result = true;
            }
            catch (Exception exp)
            {
                retModel.Result = false;
                retModel.Msg = exp.ToString();
            }
            return retModel;
        }

        public List<RequestFile> GetFileBySource(int Source, string RequestId)
        {
            return db.RequestFile.Where(t => t.Source == Source && t.RequestId == RequestId).ToList();
        }

        public ExcuteModel DeleteFile(string FileId)
        {
            ExcuteModel retModel = new ExcuteModel();
            try
            {
                RequestFileRoot model = db.RequestFileRoot.First(t => t.GUID == FileId);
                db.RequestFileRoot.DeleteOnSubmit(model);
                db.SubmitChanges();
                List<RequestFile> FileList = db.RequestFile.Where(t => t.FileRootId == FileId).ToList();
                for (int i = 0; i < FileList.Count; i++)
                {
                    db.RequestFile.DeleteOnSubmit(FileList[i]);
                    db.SubmitChanges();
                }
                List<RequestFileItem> ItemList = db.RequestFileItem.Where(t => t.FileRootId == FileId).ToList();
                for (int i = 0; i < ItemList.Count; i++)
                {
                    db.RequestFileItem.DeleteOnSubmit(ItemList[i]);
                    db.SubmitChanges();
                }
                retModel.Result = true;
            }
            catch (Exception exp)
            {
                retModel.Result = false;
                retModel.Msg = exp.ToString();
            }
            return retModel;
        }

        /// <summary>
        /// 删除单个文件
        /// </summary>
        /// <param name="FileItemId"></param>
        /// <returns></returns>
        public ExcuteModel DeleteFileItemByItemId(string FileItemId)
        {
            ExcuteModel retModel = new ExcuteModel();
            try
            {
                List<RequestFileItem> ItemList = db.RequestFileItem.Where(t => t.GUID == FileItemId).ToList();
                for (int i = 0; i < ItemList.Count; i++)
                {
                    db.RequestFileItem.DeleteOnSubmit(ItemList[i]);
                    db.SubmitChanges();

                    string _FileRootId = ItemList[0].FileRootId;
                    List<RequestFileItem> ItemListNew = db.RequestFileItem.Where(t => t.FileRootId == _FileRootId).ToList();
                    if (ItemListNew.Count == 0)
                    {
                        RequestFileRoot model = db.RequestFileRoot.FirstOrDefault(t => t.GUID == _FileRootId);
                        db.RequestFileRoot.DeleteOnSubmit(model);
                        db.SubmitChanges();
                    }
                }
                retModel.Result = true;
            }
            catch (Exception exp)
            {
                retModel.Result = false;
                retModel.Msg = exp.ToString();
            }
            return retModel;
        }
        #endregion

        #region 驳回
        public ExcuteModel SaveRefusedRecord(RefusedRecord Model)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                RefusedRecord model = db.RefusedRecord.FirstOrDefault(u => u.RequestId == Model.RequestId);
                if (model != null)
                {
                    model.RefusedReason = Model.RefusedReason;
                    model.RefusedPerson = Model.RefusedPerson;
                    model.RefusedDate = Model.RefusedDate;
                    model.RefusedType = Model.RefusedType;
                    model.DiagnosisType = Model.DiagnosisType;
                }
                else
                {
                    db.RefusedRecord.InsertOnSubmit(Model);
                }
                db.SubmitChanges();
                RetModel.Result = true;
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        public List<RefusedRecord> GetRefusedRecordByRequestId(string RequestId)
        {
            return db.RefusedRecord.Where(t => t.RequestId == RequestId).ToList();
        }
        #endregion

        #region 上传和下载标记
        /// <summary>
        /// 保存和更新数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveFileUpDownRecordDetail(FileUpDownRecord model)
        {
            ExcuteModel FileUpModel = new ExcuteModel();
            if (model == null)
            {
                //信息丢掉
                return new ExcuteModel
                {
                    Msg = "传递参数为null",
                    Result = false,
                    RetValue = ""
                };
            }
            //保存信息
            try
            {
                FileUpDownRecord detailModel = db.FileUpDownRecord.FirstOrDefault(t => t.RequestID == model.RequestID);
                if (detailModel == null)
                {
                    //将对象添加到集合中
                    db.FileUpDownRecord.InsertOnSubmit(model);
                }
                else
                {
                    //更新数据
                    detailModel.RequestID = model.RequestID;
                    if (model.UpLoadFlag != null)
                    {
                        detailModel.UpLoadFlag = model.UpLoadFlag;
                    }
                    if (model.UpLoadCount > 0)
                    {
                        detailModel.UpLoadCount = model.UpLoadCount == null ? 0 : model.UpLoadCount;
                    }
                    if (!string.IsNullOrEmpty(model.UpLoadPerson))
                    {
                        detailModel.UpLoadPerson = model.UpLoadPerson;
                    }
                    if (model.DownFlag != null)
                    {
                        detailModel.DownFlag = model.DownFlag == null ? 0 : model.DownFlag;
                    }
                    if (model.DownCount > 0)
                    {
                        detailModel.DownCount = model.DownCount == null ? 0 : model.DownCount;
                    }
                    if (!string.IsNullOrEmpty(model.DownPerson))
                    {
                        detailModel.DownPerson = model.DownPerson;
                    }
                    if (model.UpLoadDate != null)
                    {
                        detailModel.UpLoadDate = model.UpLoadDate;
                    }
                    if (model.DownDate != null)
                    {
                        detailModel.DownDate = model.DownDate;
                    }
                    if (model.UpGradeDate != null)
                    {
                        detailModel.UpGradeDate = model.UpGradeDate;
                    }
                    if (!string.IsNullOrEmpty(model.MarkDetail))
                    {
                        detailModel.MarkDetail = model.MarkDetail;
                    }
                }
                //提交至数据库
                db.SubmitChanges();
                FileUpModel.Msg = "数据操作成功";
                FileUpModel.Result = true;
            }
            catch (Exception ex)
            {
                FileUpModel.Result = false;
                FileUpModel.Msg = ex.Message;
            }

            return FileUpModel;
        }

        /// <summary>
        /// 获取上传下载记录信息
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="upLoadPerson"></param>
        /// <param name="downPerson"></param>
        /// <returns></returns>
        public List<FileUpDownRecord> GetFileUpDownRecordByRequestId(string requestId, string hospitalID, string upLoadPerson, string downPerson)
        {
            string StrSql = " select * from FileUpDownRecord where 1=1";
            if (!string.IsNullOrEmpty(requestId))
            {
                StrSql += "AND RequestID='" + requestId + "'";
            }
            if (!string.IsNullOrEmpty(hospitalID))
            {
                StrSql += " AND HospitalsID='" + hospitalID + "'";
            }
            if (!string.IsNullOrEmpty(upLoadPerson))
            {
                StrSql += " AND UpLoadPerson='" + upLoadPerson + "'";
            }
            if (!string.IsNullOrEmpty(downPerson))
            {
                StrSql += " AND DownPerson='" + downPerson + "'";
            }
            List<FileUpDownRecord> list = db.ExecuteQuery<FileUpDownRecord>(StrSql).ToList();
            return list;
        }

        /// <summary>
        /// 获取云端已上传患者
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="state"></param>
        /// <param name="dtStar"></param>
        /// <param name="dtEnd"></param>
        /// <returns></returns>
        public List<FileUpDownRecord> GetFileUpDownRecordByIDMList(string requestId, string hospitalID, int? uploadFlag, string state, DateTime dtStar, DateTime dtEnd)
        {
            string[] strTypes = state.Split(',');
            string strState = string.Empty;
            for (int k = 0; k < strTypes.Length; k++)
            {
                if (string.IsNullOrEmpty(strState))
                {
                    strState = "'" + strTypes[k] + "'";
                }
                else
                {
                    strState += ",'" + strTypes[k] + "'";
                }
            }
            string StrSql = " SELECT * FROM [RemoteDianose].[dbo].[FileUpDownRecord] where 1=1";
            if (!string.IsNullOrEmpty(requestId))
            {
                StrSql += "AND RequestID='" + requestId + "'";
            }
            if (!string.IsNullOrEmpty(hospitalID))
            {
                StrSql += "AND HospitalsID='" + hospitalID + "'";
            }
            if (!string.IsNullOrEmpty(strState))
            {
                StrSql += " AND MarkDetail IN(" + strState + ")";
            }
            if (uploadFlag != null)
            {
                StrSql += " AND UpLoadFlag=" + uploadFlag;
            }

            if (dtStar != Convert.ToDateTime("1900-01-01")) { StrSql += " AND UploadDate>=CONVERT(VARCHAR(100), '" + dtStar.ToString("yyyy-MM-dd HH:mm:ss") + "' ,23)"; }
            if (dtEnd != Convert.ToDateTime("1900-01-01")) { StrSql += " AND UploadDate<=CONVERT(VARCHAR(100), '" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "' ,23)"; }
            StrSql += " ORDER BY UpLoadDate DESC";
            StrSql = ReplaceInstance(StrSql, "GetFileUpDownRecordByIDMList：");
            List<FileUpDownRecord> list = db.ExecuteQuery<FileUpDownRecord>(StrSql).ToList();
            return list;
        }
        #endregion

        #region
        private string ReplaceInstance(string strSql, string strName)
        {
            try
            {
                //Sys_Settings SysModel = DbDataContext.Sys_Settings.FirstOrDefault(t => t.SysType == "SQLSERVER");
                //if (SysModel == null)
                //{
                //    LogHelper.WriteInfoLog(strName + "SQL无须转换：" + strSql);
                //    return strResult = strSql;
                //}
                //LogHelper.WriteInfoLog(strName + "数据库转换---strRemote：" + SysModel.SysValue + "--strPatient：" + SysModel.SysKey);
                //string strRemote = SysModel.SysValue;
                //string strPatient = SysModel.SysKey;
                //bool replaceState = false;

                //if (strRemote != "RemoteDianose")
                //{
                //    replaceState = true;
                //}
                //if (replaceState)
                //{
                //    strResult = strSql.Replace("RemoteDianose", strRemote).Replace("PatientVisit", strPatient);
                //    LogHelper.WriteInfoLog(strName + "转换后SQL---" + strResult);
                //}
                //else
                //{
                //    strResult = strSql;
                //    LogHelper.WriteInfoLog(strName + "库对比相同---" + strResult);
                //}
                LogHelper.WriteInfoLog(strName + "查询SQL---" + strSql);
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog(strName + "SQL异常---" + strSql, ex.Message);
            }
            return strSql;
        }
        #endregion

        #region 同步记录
        /// <summary>
        /// 添加同步记录
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        public ExcuteModel SaveSynRecord(SynRecord Model)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                db.SynRecord.InsertOnSubmit(Model);
                db.SubmitChanges();
                RetModel.Result = true;
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        /// <summary>
        /// 同步记录信息
        /// </summary>
        /// <param name="Type">1：已申请，2已诊断</param>
        /// <returns></returns>
        public List<SynRecord> GetSynRecordList(string HospitalId, DateTime dtStart, DateTime dtEnd, string PatientId, string RequestId)
        {
            List<SynRecord> List = new List<SynRecord>();
            try
            {
                db = new RemoteDianoseDataContext();
                string StrSql = @"SELECT * FROM SynRecord WHERE 1=1 ";

                if (dtStart != Convert.ToDateTime("1900-01-01"))
                {
                    StrSql += " AND  UploadDate>=CONVERT(varchar(100),'" + dtStart.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
                }
                if (dtEnd != Convert.ToDateTime("1900-01-01"))
                {
                    StrSql += " AND  UploadDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
                }
                if (HospitalId.Trim() != "")
                {
                    StrSql += " AND  HospitalId LIKE'%" + HospitalId + "%'";
                }
                if (PatientId.Trim() != "")
                {
                    StrSql += " AND  PatientId LIKE '%" + PatientId + "%' ";
                }
                if (RequestId.Trim() != "")
                {
                    StrSql += " AND RequestId ='" + RequestId + "'";
                }

                StrSql = ReplaceInstance(StrSql, "GetSynRecordList::");
                List = db.ExecuteQuery<SynRecord>(StrSql).ToList();
            }
            catch (Exception)
            {

            }
            return List;
        }

        /// <summary>
        /// 修改PatientId
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        public ExcuteModel UpdatePatientId(string newPatientId, string oldPatientId)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                string strSql = string.Format(@"update PatientInfo set PatientId='{0}' where PatientId ='{1}'", newPatientId, oldPatientId);
                db.ExecuteCommand(strSql);
                RetModel.Result = true;
                RetModel.Msg = "PatientID修改成功！";
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        /// <summary>
        /// 添加同步记录
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateSynRecord(SynRecord Model)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                string strSql = string.Format(@"update SynRecord set State=1, DeleteDate='{0}' where ID ={1}", Model.DeleteDate, Model.ID);
                db.ExecuteCommand(strSql);
                RetModel.Result = true;
                RetModel.Msg = "角色状态调整成功！";
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        #endregion

        #region 添加已拉取患者信息
        public ExcuteModel SavePullPatient(PullPatient model)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                PullPatient detailModel = db.PullPatient.FirstOrDefault(t => t.RequestID == model.RequestID);
                if (detailModel == null)
                {
                    db.PullPatient.InsertOnSubmit(model);
                    db.SubmitChanges();
                    RetModel.Result = true;
                }
                else
                {
                    RetModel.Result = true;
                }
            }
            catch (Exception ex)
            {
                RetModel.Result = false;
                RetModel.Msg = ex.ToString();
            }
            return RetModel;
        }

        /// <summary>
        /// 获取拉取患者信息
        /// </summary>
        /// <param name="strRequestID">申请ID</param>
        /// <param name="strPatientID">患者ID</param>
        /// <param name="starDate">开始时间</param>
        /// <param name="endDate">结束时间</param>
        /// <returns></returns>
        public List<PullPatient> GetPullPatientList(string strRequestID, string strPatientID)
        {
            List<PullPatient> result = new List<PullPatient>();
            string strSql = string.Empty;
            string strParam = string.Empty;
            if (!string.IsNullOrEmpty(strRequestID))
            {
                strParam += "AND RequestID='" + strRequestID + "'";
            }
            if (!string.IsNullOrEmpty(strPatientID))
            {
                strParam += "AND PatientID='" + strPatientID + "'";
            }
            strSql = "SELECT * FROM PullPatient WHERE 1=1" + strParam;
            result = db.ExecuteQuery<PullPatient>(strSql).ToList();
            return result;
        }
        #endregion

        #region 上传影像
        /// <summary>
        /// 保存和更新上传影像数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveUpDicomRecordDetail(UpDicomRecord model)
        {
            ExcuteModel FileUpModel = new ExcuteModel();
            if (model == null)
            {
                //信息丢掉
                return new ExcuteModel
                {
                    Msg = "传递参数为null",
                    Result = false,
                    RetValue = ""
                };
            }
            //保存信息
            try
            {
                UpDicomRecord detailModel = db.UpDicomRecord.FirstOrDefault(t => t.RequestID == model.RequestID);
                if (detailModel == null)
                {
                    //将对象添加到集合中
                    db.UpDicomRecord.InsertOnSubmit(model);
                }
                else
                {
                    //更新数据
                    detailModel.RequestID = model.RequestID;
                    detailModel.PatientID = model.PatientID;
                    detailModel.StudyInstanceUid = model.StudyInstanceUid;
                }
                //提交至数据库
                db.SubmitChanges();
                FileUpModel.Msg = "数据操作成功";
                FileUpModel.Result = true;
            }
            catch (Exception ex)
            {
                FileUpModel.Result = false;
                FileUpModel.Msg = ex.Message;
            }
            return FileUpModel;
        }

        /// <summary>
        /// 获取影像信息
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="dtStar"></param>
        /// <param name="dtEnd"></param>
        /// <returns></returns>
        public List<UpDicomRecord> GetUpDicomRecordByRequestId(string requestId, DateTime dtStar, DateTime dtEnd)
        {
            string StrSql = "SELECT * FROM  UpDicomRecord WHERE 1=1 ";
            if (!string.IsNullOrEmpty(requestId))
            {
                StrSql += " AND RequestID='" + requestId + "'";
            }
            if (dtStar != Convert.ToDateTime("1900-01-01"))
            {
                StrSql += " AND  CreateDate=CONVERT(varchar(100),'" + dtStar.ToString("yyyy-MM-dd HH:mm:ss") + "', 23)";
            }
            if (dtEnd != Convert.ToDateTime("1900-01-01"))
            {
                StrSql += " AND  CreateDate<=CONVERT(varchar(100),'" + dtEnd.ToString("yyyy-MM-dd HH:mm:ss") + "',23)";
            }
            StrSql += " ORDER BY CreateDate DESC";
            List<UpDicomRecord> list = db.ExecuteQuery<UpDicomRecord>(StrSql).ToList();
            return list;
        }

        /// <summary>
        /// 获取影像库中是否有数据
        /// </summary>
        /// <param name="requestId"></param>
        /// <returns></returns>
        public int GetUpDicomRecordCountByRequestId(string requestId, string studyInstanceUID)
        {
            int dataCount = -1;
            if (!string.IsNullOrEmpty(requestId) && !string.IsNullOrEmpty(studyInstanceUID))
            {
                dataCount = db.UpDicomRecord.Where(x => x.RequestID == requestId && x.StudyInstanceUid == studyInstanceUID).ToList().Count;
            }
            return dataCount;
        }
        #endregion

        #region 支付宝后台统计
        /// <summary>
        /// 统计总收入
        /// </summary>
        /// <param name="DateParam"></param>
        /// <returns></returns>
        public List<OutChargeCost> GetPayAllCount(string DateParam)
        {
            List<OutChargeCost> list = new List<OutChargeCost>();
            string StrSql = @"SELECT 
                              (CASE PayState 
                              WHEN 1 THEN '待缴费' 
                              WHEN 2 THEN '已缴费'
                              WHEN 3 THEN '已退费' 
                              ELSE '缴费状态' END) AS PayState,
                              SUM(TotalAmount) AS TotalAmount
                              FROM PayTransaction pt 
                              WHERE CONVERT(VARCHAR(7),CreateTime,23)='" + DateParam + "' AND HospitalID IS NOT NULL AND Type  IS NOT NULL GROUP BY  PayState";
            list = db.ExecuteQuery<OutChargeCost>(StrSql).ToList();
            return list;
        }

        /// <summary>
        /// 各机构总收入
        /// </summary>
        /// <param name="DateParam"></param>
        /// <returns></returns>
        public List<OutChargeCost> GetPayOrganCount(string DateParam)
        {
            List<OutChargeCost> list = new List<OutChargeCost>();
            string StrSql = @"SELECT DepType,
                              SUM(CASE WHEN PayState=1 THEN TotalAmount ELSE 0 END) AS WaitingCost,
                              SUM(CASE WHEN PayState=2 THEN TotalAmount ELSE 0 END) AS ChargeCost,
                              SUM(CASE WHEN PayState=3 THEN TotalAmount ELSE 0 END) AS ReturnCost
                              FROM  Sys_Dept sd 
                              LEFT JOIN PayTransaction pt on sd.DeptId=pt.HospitalID
                              WHERE CONVERT(VARCHAR(7),CreateTime,23)='" + DateParam + "' AND HospitalID IS NOT NULL AND Type  IS NOT NULL GROUP BY DepType";
            list = db.ExecuteQuery<OutChargeCost>(StrSql).ToList();
            return list;
        }

        /// <summary>
        /// 机构收入情况
        /// </summary>
        /// <param name="DateParam"></param>
        /// <returns></returns>
        public List<OutChargeCost> GetPayEveryOrganCount(string DateParam)
        {
            List<OutChargeCost> list = new List<OutChargeCost>();
            string StrSql = @"SELECT SD.DeptDisplayName AS HospitalName,
                              SUM(CASE WHEN PayState=1 THEN TotalAmount ELSE 0 END) AS WaitingCost,
                              SUM(CASE WHEN PayState=2 THEN TotalAmount ELSE 0 END) AS ChargeCost,
                              SUM(CASE WHEN PayState=3 THEN TotalAmount ELSE 0 END) AS ReturnCost 
                              FROM PayTransaction pt 
                              LEFT JOIN Sys_Dept SD ON pt.HospitalID=SD.DeptId
                              WHERE CONVERT(VARCHAR(7),CreateTime,23)='" + DateParam + "' AND HospitalID IS NOT NULL AND Type IS NOT NULL GROUP BY SD.DeptDisplayName";
            list = db.ExecuteQuery<OutChargeCost>(StrSql).ToList();
            return list;
        }

        /// <summary>
        /// 机构会诊费收入统计
        /// </summary>
        /// <returns></returns>
        public List<OutChargeCost> GetPayConsulOrganAllCount(string DateParam)
        {
            List<OutChargeCost> list = new List<OutChargeCost>();
            string StrSql = @"SELECT DepType,
                              SUM(CASE WHEN PayState=1 THEN TotalAmount ELSE 0 END) AS WaitingCost,
                              SUM(CASE WHEN PayState=2 THEN TotalAmount ELSE 0 END) AS ChargeCost,
                              SUM(CASE WHEN PayState=3 THEN TotalAmount ELSE 0 END) AS ReturnCost
                              FROM PayTransaction pt
                              LEFT JOIN RequestInfo ri on pt.RequestId=ri.RequestId
                              LEFT JOIN Sys_Dept sd on pt.HospitalID=sd.DeptId
                              WHERE ri.Diagnosticstate='Consultation' AND pt.HospitalID IS NOT NULL AND pt.Type IS NOT NULL AND
                              CONVERT(VARCHAR(7),CreateTime,23)='" + DateParam + "' GROUP BY DepType";
            list = db.ExecuteQuery<OutChargeCost>(StrSql).ToList();
            return list;
        }

        /// <summary>
        /// 支付方式会诊费收入
        /// </summary>
        /// <param name="DateParam"></param>
        /// <returns></returns>
        public List<OutChargeCost> GetPayConsulAllCount(string DateParam)
        {
            List<OutChargeCost> list = new List<OutChargeCost>();
            string StrSql = @"SELECT DepType,Type,
                              SUM(CASE WHEN PayState=1 THEN TotalAmount ELSE 0 END) AS WaitingCost,
                              SUM(CASE WHEN PayState=2 THEN TotalAmount ELSE 0 END) AS ChargeCost,
                              SUM(CASE WHEN PayState=3 THEN TotalAmount ELSE 0 END) AS ReturnCost
                              FROM PayTransaction pt
                              LEFT JOIN RequestInfo ri on pt.RequestId=ri.RequestId
                              LEFT JOIN Sys_Dept sd on pt.HospitalID=sd.DeptId
                              WHERE ri.Diagnosticstate='Consultation'AND pt.HospitalID IS NOT NULL AND pt.Type IS NOT NULL AND
                              CONVERT(VARCHAR(7),CreateTime,23)='" + DateParam + "' GROUP BY Type,DepType";
            list = db.ExecuteQuery<OutChargeCost>(StrSql).ToList();
            return list;
        }

        /// <summary>
        /// 服务项目会诊费收入
        /// </summary>
        /// <param name="DateParam"></param>
        /// <returns></returns>
        public List<OutChargeCost> GetPayConsulServiceAllCount(string DateParam)
        {
            List<OutChargeCost> list = new List<OutChargeCost>();
            string StrSql = @"SELECT DepType,[Subject] AS ServiceName,
                              SUM(CASE WHEN PayState=1 THEN TotalAmount ELSE 0 END) AS WaitingCost,
                              SUM(CASE WHEN PayState=2 THEN TotalAmount ELSE 0 END) AS ChargeCost,
                              SUM(CASE WHEN PayState=3 THEN TotalAmount ELSE 0 END) AS ReturnCost
                              FROM PayTransaction pt
                              LEFT JOIN RequestInfo ri on pt.RequestId=ri.RequestId
                              LEFT JOIN Sys_Dept sd on pt.HospitalID=sd.DeptId
                              WHERE ri.Diagnosticstate='Consultation' AND pt.HospitalID IS NOT NULL AND pt.Type IS NOT NULL AND
                              CONVERT(VARCHAR(7),CreateTime,23)='" + DateParam + "' GROUP BY  [Subject],DepType";
            list = db.ExecuteQuery<OutChargeCost>(StrSql).ToList();
            return list;
        }

        /// <summary>
        /// 机构统计收费报表
        /// </summary>
        /// <param name="DateParam"></param>
        /// <returns></returns>
        public List<OutChargeCost> GetPayOrganCountReport(string DateParam)
        {
            List<OutChargeCost> list = new List<OutChargeCost>();
            string StrSql = @"SELECT SD.DeptDisplayName AS HospitalName,[Subject] AS ServiceName,[Type],
                              SUM(CASE WHEN PayState=1 THEN TotalAmount ELSE 0 END) AS WaitingCost,
                              SUM(CASE WHEN PayState=2 THEN TotalAmount ELSE 0 END) AS ChargeCost,
                              SUM(CASE WHEN PayState=3 THEN TotalAmount ELSE 0 END) AS ReturnCost
                              FROM PayTransaction pt
                              LEFT JOIN Sys_Dept SD ON pt.HospitalID=SD.DeptId
                              WHERE pt.HospitalID IS NOT NULL AND pt.Type  IS NOT NULL AND
                              CONVERT(VARCHAR(7),CreateTime,23)='" + DateParam + "' GROUP BY  SD.DeptDisplayName,[Subject],[Type]";
            list = db.ExecuteQuery<OutChargeCost>(StrSql).ToList();
            return list;
        }

        /// <summary>
        ///  个人统计收费报表
        /// </summary>
        /// <param name="ServiceId"></param>
        /// <param name="PayType"></param>
        /// <param name="BeginDate"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        public List<OutChargeChart> GetPayPersonCountReport(string ServiceId, string PayType, DateTime BeginDate, DateTime EndDate)
        {
            //(CASE PIS.PatientSex WHEN 'F' THEN '女' WHEN 'M' THEN '男' ELSE '其他' END) AS Sex,
            List<OutChargeChart> list = new List<OutChargeChart>();
            StringBuilder sbApp = new StringBuilder();
            string StrSql = @"SELECT ROW_NUMBER() OVER(ORDER BY PT.PayTradeID) AS SerialID, PT.RequestId,CONVERT(VARCHAR(23),PT.CreateTime,120) AS CreateTime,SD.DeptDisplayName AS HospitalName,SU.Name AS RequestDcotor,
                            PIS.CPatientName AS PatientName, PIS.PatientSex AS Sex,SDS.ServiceName,TotalAmount,PT.Type AS PayType,
                            (CASE PayState WHEN 1 THEN '待缴费' WHEN 2 THEN '已缴费' WHEN 3 THEN '已退费' ELSE '其他' END) AS ChargeState
                            FROM PayTransaction PT
                            LEFT JOIN Sys_Dept SD ON PT.HospitalID=SD.DeptId
                            INNER JOIN RequestInfo RI ON RI.RequestId=PT.RequestId
                            LEFT JOIN PatientInfo PIS ON PIS.PatientId=RI.PatientId
                            LEFT JOIN Sys_Users SU ON SU.UserName=RI.RequestDoctor
                            LEFT JOIN Sys_DiagnoseService SDS ON RI.ServeiceType=SDS.ServiceId
                            WHERE PT.HospitalID IS NOT NULL AND PT.Type  IS NOT NULL ";
            sbApp.Append(StrSql);
            if (BeginDate != Convert.ToDateTime("1900-01-01"))
            {
                sbApp.Append(" AND PT.CreateTime>=CONVERT(VARCHAR(23),'" + BeginDate.ToString("yyyy-MM-dd HH:mm:ss") + "',23)");
            }
            if (EndDate != Convert.ToDateTime("1900-01-01"))
            {
                sbApp.Append(" AND PT.CreateTime<=CONVERT(VARCHAR(23),'" + EndDate.ToString("yyyy-MM-dd HH:mm:ss") + "',23)");
            }
            if (!string.IsNullOrEmpty(ServiceId))
            {
                sbApp.Append(" AND PT.Standby='" + ServiceId + "'");
            }
            if (!string.IsNullOrEmpty(PayType))
            {
                sbApp.Append(" AND pt.Type='" + PayType + "'");
            }
            list = db.ExecuteQuery<OutChargeChart>(sbApp.ToString()).ToList();
            return list;
        }
        #endregion

        #region 讨论主题

        /// <summary>
        /// 发布主题
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel SaveDiscussionTopic(XR_DiscussionTopic param)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                XR_DiscussionTopic _model = db.XR_DiscussionTopic.FirstOrDefault(t => t.ThemeTitle == param.ThemeTitle && t.ThemeUserID == param.ThemeUserID);
                if (_model != null)
                {
                    if (!string.IsNullOrEmpty(param.ThemeContent))
                    {
                        _model.ThemeContent = param.ThemeContent;
                    }
                    if (param.ThemeState > 1)
                    {
                        _model.ThemeState = param.ThemeState;
                    }
                    _model.ThemeUpdateDate = param.ThemeUpdateDate;
                    db.SubmitChanges();
                    RetModel.Msg = "数据更新成功！";
                }
                else
                {
                    if (param.ThemeState == null || param.ThemeState == 0)
                        param.ThemeState = 1;
                    db.XR_DiscussionTopic.InsertOnSubmit(param);
                    RetModel.Msg = "数据添加成功！";
                }
                db.SubmitChanges();
                RetModel.Result = true;
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        /// <summary>
        /// 删除主题
        /// </summary>
        /// <param name="themeID"></param>
        /// <returns></returns>
        public int? DeleteDiscussionTopicByID(int themeID)
        {
            var result = 0;
            string StrSql = @"DELETE FROM XR_DiscussionTopic WHERE ID=" + themeID;
            result = db.ExecuteCommand(StrSql);
            return result;
        }

        /// <summary>
        /// 发布的主题（我创建的主题、已结束的主题 状态为3）
        /// </summary>
        /// <returns></returns>
        public List<XR_DiscussionTopic> GetDiscussionTopics(XR_DiscussionTopic param)
        {
            List<XR_DiscussionTopic> list = new List<XR_DiscussionTopic>();
            string StrSql = @"SELECT * FROM XR_DiscussionTopic WHERE 1=1 ";
            if (!string.IsNullOrEmpty(param.ThemeUserID))
            {
                StrSql += " AND ThemeUserID='" + param.ThemeUserID + "'";
            }
            if (param.ThemeState > 0)
            {
                StrSql += " AND ThemeState=" + param.ThemeState;
            }
            if (!string.IsNullOrEmpty(param.ThemeTitle))
            {
                StrSql += " AND ThemeTitle='" + param.ThemeTitle + "'";
            }
            if (param.ID > 0)
            {
                StrSql += " AND ID=" + param.ID;
            }
            StrSql += " ORDER BY ThemeDate DESC";
            list = db.ExecuteQuery<XR_DiscussionTopic>(StrSql).ToList();
            return list;
        }

        /// <summary>
        /// 我参与的主题
        /// </summary>
        /// <returns></returns>
        public List<XR_DiscussionTopic> GetMyAttentionDiscussionTopics(XR_DiscussionTopic param)
        {
            List<XR_DiscussionTopic> list = new List<XR_DiscussionTopic>();
            string StrSql = @"SELECT D.ID ,D.PatientID ,D.RequestID ,D.StudyUID ,D.ThemeTitle ,D.ThemeContent ,D.ThemeState ,D.ThemeUserID,D.ThemeDate ,M.ID as Remark ,D.ThemeUpdateDate  FROM XR_DiscussionTopic D INNER JOIN XR_MyAttention M on D.ID = M.AttentionThemeID WHERE 1=1 ";
            if (!string.IsNullOrEmpty(param.ThemeUserID))
            {
                StrSql += " AND M.AttentionUserID='" + param.ThemeUserID + "'";
            }
            if (param.ThemeState > 0)
            {
                StrSql += " AND D.ThemeState=" + param.ThemeState;
            }
            StrSql += " ORDER BY D.ThemeDate DESC";
            list = db.ExecuteQuery<XR_DiscussionTopic>(StrSql).ToList();
            return list;
        }

        /// <summary>
        /// 关注或取消主题
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel SaveMyAttention(XR_MyAttention param)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                XR_MyAttention _model = db.XR_MyAttention.FirstOrDefault(t => t.ID == param.ID && t.AttentionUserID == param.AttentionUserID && t.AttentionThemeID == param.AttentionThemeID);
                if (_model != null)
                {
                    db.XR_MyAttention.DeleteOnSubmit(_model);
                    RetModel.Msg = "取消关注成功！";
                }
                else
                {
                    db.XR_MyAttention.InsertOnSubmit(param);
                    RetModel.Msg = "关注主题成功！";
                }
                db.SubmitChanges();
                RetModel.Result = true;
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }
        #endregion

        /// <summary>
        /// 保存关键词
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel SaveXRKeywords(XR_Keywords param)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                if (!string.IsNullOrEmpty(param.PatientID))
                {
                    XR_Keywords _model = db.XR_Keywords.FirstOrDefault(t => t.PatientID == param.PatientID && t.UserID == param.UserID && t.HospitalID == param.HospitalID);
                    if (_model != null)
                    {
                        param.CreateDate = DateTime.Now;
                        db.XR_Keywords.DeleteOnSubmit(_model);
                        db.XR_Keywords.InsertOnSubmit(param);
                        db.SubmitChanges();
                        RetModel.Msg = "数据更新成功！";
                    }
                    else
                    {
                        param.CreateDate = DateTime.Now;
                        db.XR_Keywords.InsertOnSubmit(param);
                        RetModel.Msg = "数据添加成功！";
                    }
                    db.SubmitChanges();
                    RetModel.Result = true;
                }
                else
                {
                    RetModel.Msg = "PatientID为空！";
                    RetModel.Result = false;
                }
            }
            catch (Exception exp)
            {
                RetModel.Result = false;
                RetModel.Msg = exp.ToString();
            }
            return RetModel;
        }

        /// <summary>
        /// 获取关键词中的PatientID
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<string> GetXRKeywords(XR_Keywords param)
        {
            List<string> list = new List<string>();
            try
            {
                string StrSql = @"SELECT patientID FROM [dbo].[XR_Keywords] AS S  WHERE  1=1 ";
                if (!string.IsNullOrEmpty(param.PatientID))
                {
                    StrSql += " AND PatientID LIKE '%" + param.PatientID + "%'";
                }
                if (!string.IsNullOrEmpty(param.HospitalID))
                {
                    StrSql += " AND HospitalID = '" + param.HospitalID + "'";
                }
                if (!string.IsNullOrEmpty(param.HospitalID))
                {
                    StrSql += " AND UserID = '" + param.UserID + "'";
                }
                StrSql += "ORDER BY CreateDate DESC";
                list = db.ExecuteQuery<string>(StrSql).ToList();
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("查询GetXRKeywords列表异常", "异常信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return list;
        }

        public ExcuteModel ClearKeywords(XR_Keywords param)
        {
            ExcuteModel RetModel = new ExcuteModel();
            try
            {
                string StrSql = @"DELETE FROM [dbo].[XR_Keywords] WHERE  1=1 ";
                if (!string.IsNullOrEmpty(param.HospitalID))
                {
                    StrSql += " AND HospitalID = '" + param.HospitalID + "'";
                }
                if (!string.IsNullOrEmpty(param.HospitalID))
                {
                    StrSql += " AND UserID = '" + param.UserID + "'";
                }
                var i = db.ExecuteCommand(StrSql);
                if (i > 0)
                {
                    RetModel.Result = true;
                    RetModel.Msg = "清理搜索记录成功!";
                }
                else
                {
                    RetModel.Result = true;
                    RetModel.Msg = "没有相关搜索记录!";
                }
            }
            catch (Exception ex)
            {
                RetModel.Result = false;
                RetModel.Msg = "清理搜索记录异常!";
                LogHelper.WriteErrorLog("查询GetXRKeywords列表异常", "异常信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return RetModel;
        }

        /// <summary>
        /// 查询未缴费患者列表(按照搜索)
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <param name="name"></param>
        /// <param name="patientId"></param>
        public List<RequestStudy> GetRequestStudyListByKeyWords(string hospitalID, string patientId, string userId)
        {
            List<RequestStudy> list = new List<RequestStudy>();
            try
            {
                XR_Keywords keywords = new XR_Keywords();
                keywords.UserID = userId;
                keywords.PatientID = patientId;
                string patientIds = "";
                string selectTop = " Top 30 ";
                if (string.IsNullOrEmpty(patientId))
                {
                    keywords.HospitalID = hospitalID;

                    var t = GetXRKeywords(keywords);
                    if (t != null)
                    {
                        selectTop = " Top 20 ";
                        foreach (var i in t)
                        {
                            patientIds += " PatientId Like '%" + i + "%' OR ";
                        }
                    }
                    patientIds = patientIds.Substring(0, patientIds.LastIndexOf("OR"));
                }
                string StrSql = @"SELECT " + selectTop + @" [GUID],[HospitalID],[ServerPartitionGUID] ,[StudyStorageGUID],[PatientGUID] ,[SpecificCharacterSet]
                             ,[StudyInstanceUid],[PatientsName],[PatientId],[IssuerOfPatientId],[PatientsBirthDate],[PatientsAge],[PatientsSex]
                             ,[StudyDate],[StudyTime],[AccessionNumber],[StudyId],[StudyDescription],[NumberOfStudyRelatedSeries]
                             ,[NumberOfStudyRelatedInstances],[StudySizeInKB],[ResponsiblePerson],[ResponsibleOrganization]
                             ,[QueryXml],[QCStatusEnum],[QCOutput],[QCUpdateTimeUtc]
                             ,[CostState] FROM [RemoteDianose].[dbo].[RequestStudy] AS S  WHERE  1=1 ";

                if (!string.IsNullOrEmpty(patientIds))
                {
                    StrSql += " AND ( " + patientIds + " )";
                }
                else
                {
                    if (!string.IsNullOrEmpty(patientId))
                    {
                        if (patientId.IndexOf(',') > -1 || patientId.IndexOf('，') > -1)
                        {
                            var t = patientId.Split(new char[] { ',', '，' }, StringSplitOptions.RemoveEmptyEntries);
                            StrSql += " AND ";
                            var tSql = " (";
                            foreach (var i in t)
                            {
                                tSql += "  PatientID LIKE '%" + i + "%' OR ";
                            }
                            tSql = tSql.Substring(0, tSql.LastIndexOf("OR"));
                            tSql += ")";
                            StrSql += tSql;
                        }
                        else
                        {
                            StrSql += " AND PatientId Like '%" + patientId + "%' ";
                        }
                    }
                }
                if (!string.IsNullOrEmpty(hospitalID))
                {
                    StrSql += " AND HospitalID = '" + hospitalID + "'";
                }
                StrSql += "ORDER BY StudyDate,StudyTime DESC";
                StrSql = ReplaceInstance(StrSql, "GetRequestStudyListByKeyWords");
                list = db.ExecuteQuery<RequestStudy>(StrSql).ToList();
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("查询GetRequestStudyListByKeyWords列表异常", "异常信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return list;
        }

        #region 查询会诊数量记录表
        /// <summary>
        /// 根据机构ID查询所有会诊数据量
        /// </summary>
        /// <param name="strHostitalID"></param>
        /// <returns></returns>
        public List<ConsulaDataRecord> GetConsulaDataRecordList(ConsulaDataRecord model)
        {
            List<ConsulaDataRecord> list = new List<ConsulaDataRecord>();
            if (!string.IsNullOrEmpty(model.ConHospital))
            {
                list = db.ConsulaDataRecord.Where(o => o.ConHospital == model.ConHospital).ToList();
            }
            else
            {
                list = db.ConsulaDataRecord.ToList();
            }
            return list;
        }

        /// <summary>
        /// 保存会诊数据记录信息
        /// 数量不做判断
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveConsulaDataRecord(ConsulaDataRecord model)
        {
            ExcuteModel resultValue = new ExcuteModel();
            ConsulaDataRecord cdrModel = db.ConsulaDataRecord.Where(o => o.ConGUID == model.ConGUID && o.ConHospital == model.ConHospital).FirstOrDefault();
            if (cdrModel == null)
            {
                db.ConsulaDataRecord.InsertOnSubmit(model);
                db.SubmitChanges();
                resultValue.Result = true;
                resultValue.Msg = "添加会诊数据成功！";
            }
            else
            {
                resultValue.Result = false;
                resultValue.Msg = "查询数据有问题";
            }
            return resultValue;
        }

        /// <summary>
        /// 更新ConAdd数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateConsulaDataRecordConAddGUID(ConsulaDataRecord model)
        {
            ExcuteModel retModel = new ExcuteModel();
            ConsulaDataRecord cdrModel = db.ConsulaDataRecord.Where(o => o.ConGUID == model.ConGUID).FirstOrDefault();
            if (cdrModel != null)
            {
                cdrModel.ConAdd = cdrModel.ConAdd - 1;
                cdrModel.ConNot = cdrModel.ConNot + 1;
                cdrModel.ConGUID = model.ConGUID;
                cdrModel.UpdateDate = DateTime.Now;
                db.SubmitChanges();
                retModel.Result = true;
                retModel.Msg = "更新ConAdd成功！";
            }
            else
            {
                retModel.Result = false;
                retModel.Msg = "更新ConAdd失败！";
            }
            return retModel;
        }

        /// <summary>
        /// 更新ConAll数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateConsulaDataRecordConAllGUID(ConsulaDataRecord model)
        {
            ExcuteModel retModel = new ExcuteModel();
            ConsulaDataRecord cdrModel = db.ConsulaDataRecord.Where(o => o.ConGUID == model.ConGUID).FirstOrDefault();
            if (cdrModel != null)
            {
                cdrModel.ConAll = cdrModel.ConAll - 1;
                cdrModel.ConNot = cdrModel.ConNot + 1;
                cdrModel.ConGUID = model.ConGUID;
                cdrModel.UpdateDate = DateTime.Now;
                db.SubmitChanges();
                retModel.Result = true;
                retModel.Msg = "更新ConAll成功！";
            }
            else
            {
                retModel.Result = false;
                retModel.Msg = "更新ConAll失败！";
            }
            return retModel;
        }

        /// <summary>
        /// 更新ConFree
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateConsulaDataRecordConFreeGUID(ConsulaDataRecord model)
        {
            ExcuteModel retModel = new ExcuteModel();
            ConsulaDataRecord cdrModel = db.ConsulaDataRecord.Where(o => o.ConGUID == model.ConGUID).FirstOrDefault();
            if (cdrModel != null)
            {
                cdrModel.ConFree = cdrModel.ConFree + 1;
                cdrModel.ConGUID = model.ConGUID;
                cdrModel.UpdateDate = DateTime.Now;
                db.SubmitChanges();
                retModel.Result = true;
                retModel.Msg = "更新ConFree成功！";
            }
            else
            {
                retModel.Result = false;
                retModel.Msg = "更新ConFree失败！";
            }
            return retModel;
        }

        /// <summary>
        /// 更新ConOver
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateConsulaDataRecordConOverGUID(ConsulaDataRecord model)
        {
            ExcuteModel retModel = new ExcuteModel();
            ConsulaDataRecord cdrModel = db.ConsulaDataRecord.Where(o => o.ConGUID == model.ConGUID).FirstOrDefault();
            if (cdrModel != null)
            {
                cdrModel.ConNot = cdrModel.ConNot + 1;
                cdrModel.ConOver = cdrModel.ConOver + 1;
                cdrModel.ConGUID = model.ConGUID;
                cdrModel.UpdateDate = DateTime.Now;
                db.SubmitChanges();
                retModel.Result = true;
                retModel.Msg = "更新ConOver成功！";
            }
            else
            {
                retModel.Result = false;
                retModel.Msg = "更新ConOver失败！";
            }
            return retModel;
        }

        /// <summary>
        /// 更新ConAlready
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateConsulaDataRecordConAlreadyGUID(ConsulaDataRecord model)
        {
            ExcuteModel retModel = new ExcuteModel();
            ConsulaDataRecord cdrModel = db.ConsulaDataRecord.Where(o => o.ConGUID == model.ConGUID).FirstOrDefault();
            if (cdrModel != null)
            {
                cdrModel.ConAlready = cdrModel.ConAlready + 1;
                cdrModel.ConNot = cdrModel.ConNot == 0 ? 0 : cdrModel.ConNot - 1;
                cdrModel.ConGUID = model.ConGUID;
                cdrModel.UpdateDate = DateTime.Now;
                db.SubmitChanges();
                retModel.Result = true;
                retModel.Msg = "更新ConAlready成功！";
            }
            else
            {
                retModel.Result = false;
                retModel.Msg = "更新ConAlready失败！";
            }
            return retModel;
        }

        /// <summary>
        /// 更新ConReturn
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateConsulaDataRecordConReturnGUID(ConsulaDataRecord model)
        {
            ExcuteModel retModel = new ExcuteModel();
            ConsulaDataRecord cdrModel = db.ConsulaDataRecord.Where(o => o.ConGUID == model.ConGUID).FirstOrDefault();
            if (cdrModel != null)
            {
                cdrModel.ConAll = cdrModel.ConAll + 1;
                cdrModel.ConNot = cdrModel.ConNot == 0 ? 0 : cdrModel.ConNot - 1;
                cdrModel.ConGUID = model.ConGUID;
                cdrModel.UpdateDate = DateTime.Now;
                db.SubmitChanges();
                retModel.Result = true;
                retModel.Msg = "更新ConReturn成功！";
            }
            else
            {
                retModel.Result = false;
                retModel.Msg = "更新ConReturn失败";
            }
            return retModel;
        }

        /// <summary>
        /// 如果约定内的数据用完则重新添加
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel AddConCount(ConsulaDataRecord model)
        {
            ExcuteModel retModel = new ExcuteModel();
            if (model != null)
            {
                ConsulaDataRecord cdrModel = db.ConsulaDataRecord.Where(o => o.ConHospital == model.ConHospital && o.ConGUID == model.ConGUID).FirstOrDefault();
                if (cdrModel != null)
                {
                    cdrModel.ConAdd = model.ConAdd;
                    cdrModel.UpdateDate = model.UpdateDate;
                    cdrModel.ConGUID = model.ConGUID;
                    db.SubmitChanges();
                    retModel.Result = true;
                    retModel.Msg = "添加会诊数据成功！";
                }
                else
                {
                    retModel.Result = false;
                    retModel.Msg = "未查询到数据ConsulaDataRecord：ConGUID--" + model.ConGUID + "--HospitalID:" + model.ConHospital;
                }
            }
            return retModel;
        }

        /// <summary>
        /// 根据机构、服务项目、年份
        /// 获取单个会诊记录数据
        /// </summary>
        /// <param name="strHospitalID"></param>
        /// <param name="strServiceUID"></param>
        /// <returns></returns>
        public ConsulaDataRecord GetConsulaDataRecordModel(ConsulaDataRecord modelParam)
        {
            ConsulaDataRecord model = new ConsulaDataRecord();
            if (!string.IsNullOrEmpty(modelParam.ConServicUID))
            {
                model = db.ConsulaDataRecord.Where(o => o.ConServicUID == modelParam.ConServicUID && o.ConHospital == modelParam.ConHospital).FirstOrDefault();
            }
            return model;
        }
        #endregion

        #region 获取医生信息
        /// <summary>
        /// 联合查询专家信息
        /// </summary>
        /// <returns></returns>
        public List<DoctorBaseInfo> GetDoctorInfoList()
        {
            List<DoctorBaseInfo> list = new List<DoctorBaseInfo>();
            try
            {
                string StrSql = @"SELECT su.UserId AS DoctorId,su.Name,di.DoctorName,su.UserName, su.Dept, su.Mobile AS DoctorTel, sy.DeptDisplayName AS HospitalName,su.UserImgURL,di.IntrodutionURL,di.Introduction,
                                  su.Sex,su.Age,su.[State],sd.[Description] AS DiagnoseName, su.DiagnosisType,di.DoctorTitle,di.DoctorGrade, di.DoctorOrderBy,su.UpState,su.DownState,su.APPState
                                  FROM Sys_Users su
                                  LEFT JOIN DoctorInfo di ON su.UserId=di.DoctorId
                                  LEFT JOIN Sys_Dept sy ON sy.DeptId=su.Dept
                                  LEFT JOIN Sys_Dictionary sd ON sd.Code=su.DiagnosisType 
                                  WHERE su.DiagnosisType IN('Diagnose','Consultation','Neuro','Multi') ORDER BY DiagnoseName ASC , di.DoctorOrderBy ASC";
                StrSql = ReplaceInstance(StrSql, "GetDoctorInfoList");
                list = db.ExecuteQuery<DoctorBaseInfo>(StrSql).ToList();
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("联合查询专家信息GetDoctorInfoList异常", "异常信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return list;
        }
        #endregion

        #region 关联机构信息
        /// <summary>
        /// 添加关联机构
        /// </summary>
        /// <returns></returns>
        public ExcuteModel InsertRelateHospital(RelateHospital model)
        {
            ExcuteModel em = new ExcuteModel();
            try
            {
                db.RelateHospital.InsertOnSubmit(model);
                db.SubmitChanges();
                em.Msg = "添加关联成功！";
                em.Result = true;
            }
            catch (Exception ex)
            {
                em.Msg = "添加关联失败！";
                em.Result = false;
                LogHelper.WriteErrorLog("InsertRelateHospital", "异常信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return em;
        }

        /// <summary>
        /// 设置默认机构
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateRelateHospital(RelateHospital model)
        {
            ExcuteModel em = new ExcuteModel();
            try
            {
                RelateHospital rh = db.RelateHospital.Where(o => o.SubHospitalID == model.SubHospitalID && o.UserID == model.UserID).FirstOrDefault();
                string StrSql = @"UPDATE RelateHospital SET BaseDefault=0  WHERE UserID='" + model.UserID + "'";
                db.ExecuteCommand(StrSql);
                if (rh != null)
                {
                    rh.BaseDefault = model.BaseDefault;
                    rh.HospitalGUID = rh.HospitalGUID;
                }
                else
                {
                    model.BaseDefault = model.BaseDefault;
                    model.HospitalGUID = Guid.NewGuid().ToString();
                    db.RelateHospital.InsertOnSubmit(model);
                }
                em.Result = true;
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                em.Result = false;
                em.Msg = "错误信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace;
                LogHelper.WriteErrorLog("UpdateRelateHospital", "错误信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return em;
        }

        /// <summary>
        /// 删除关联机构
        /// </summary>
        /// <returns></returns>
        public ExcuteModel DeleteRelateHospital(RelateHospital model)
        {
            ExcuteModel em = new ExcuteModel();
            string StrSql = string.Empty;
            try
            {
                StrSql = @"DELETE FROM RelateHospital WHERE HospitalGUID='" + model.HospitalGUID + "'";
                db.ExecuteCommand(StrSql);
                em.Msg = "移除关联成功！";
                em.Result = true;
            }
            catch (Exception ex)
            {
                em.Msg = "移除关联失败！";
                em.Result = false;
                LogHelper.WriteErrorLog("DeleteRelateHospital", "异常信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return em;
        }

        /// <summary>
        /// 子医院列表
        /// </summary>
        /// <returns></returns>
        public List<Sys_Dept> GetRelateSubHospitalList(string HospitalID, string userID)
        {
            List<Sys_Dept> RelateSubList = new List<Sys_Dept>();
            string StrSql = string.Empty;
            try
            {
                StrSql = @"SELECT DeptId,DeptDisplayName FROM Sys_Dept SD
                           WHERE SD.DeptId NOT IN(SELECT SubHospitalId FROM RelateHospital WHERE UserID='" + userID + "') AND DeptId<>'" + HospitalID + "'";
                RelateSubList = db.ExecuteQuery<Sys_Dept>(StrSql).ToList();
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("GetRelateHospitalList", "错误信息:" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return RelateSubList;
        }

        /// <summary>
        /// 子医院关联列表
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public List<RelateHospital> GetRelateHospitalList(string userID)
        {
            List<RelateHospital> RelateList = new List<RelateHospital>();
            string StrSql = string.Empty;
            try
            {
                RelateList = db.RelateHospital.Where(o => o.UserID == userID).OrderByDescending(o => o.BaseDefault).ToList();
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("GetRelateHospitalList", "错误信息:" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return RelateList;
        }
        #endregion

        #region 检查部位
        /// <summary>
        /// 获取检查部位信息
        /// </summary>
        /// <returns></returns>
        public List<ExaminePosition> GetExamPosList()
        {
            //List<ExaminePosition> objExamPos = db.ExaminePosition.Where(p => p.ParentNode == 0 && p.ExamineState == 1).ToList();
            List<ExaminePosition> objExamPos = db.ExaminePosition.ToList();
            return objExamPos;
        }

        /// <summary>
        /// 获取检查部位信息
        /// </summary>
        /// <returns></returns>
        public List<ExaminePosition> GetExamPosListNew()
        {
            List<ExaminePosition> objExamPos = db.ExaminePosition.ToList();
            return objExamPos;
        }

        /// <summary>
        /// 添加检查部位信息
        /// </summary>
        /// <param name="objExamPos"></param>
        /// <returns></returns>
        public ExcuteModel AddExamPositionInfo(ExaminePosition param)
        {
            ExcuteModel model = new ExcuteModel();
            try
            {
                ExaminePosition modelParam = db.ExaminePosition.Where(e => e.ExamineName == param.ExamineName).FirstOrDefault();
                if (modelParam == null)
                {
                    db.ExaminePosition.InsertOnSubmit(param);
                    db.SubmitChanges();
                    model.Result = true;
                }
                else
                {
                    model.Result = false;
                    model.Msg = "检查部位已存在";
                }
            }
            catch (Exception ex)
            {
                model.Result = false;
                model.Msg = ex.Message.ToString();
            }
            return model;
        }

        /// <summary>
        /// 根据编号获取检查部位列表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ExaminePosition GetExamPositionById(int id)
        {
            ExaminePosition objExamPos = db.ExaminePosition.Where(e => e.Id == id).FirstOrDefault<ExaminePosition>();

            return objExamPos;
        }

        /// <summary>
        /// 更新检查部位信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public ExcuteModel UpdateExamPositionInfo(int id, int state)
        {
            ExcuteModel model = new ExcuteModel();
            try
            {
                ExaminePosition objExamPos = db.ExaminePosition.Where(e => e.Id == id).FirstOrDefault();
                if (objExamPos != null)
                {
                    objExamPos.ExamineState = state;
                    db.SubmitChanges();
                    model.Result = true;
                }
            }
            catch (Exception ex)
            {
                model.Result = false;
                model.Msg = ex.Message.ToString();
            }
            return model;
        }

        /// <summary>
        /// 更新检查部位信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="objExamPos"></param>
        /// <returns></returns>
        public ExcuteModel UpdateExamPosition(int id, ExaminePosition objExamPos)
        {
            ExcuteModel model = new ExcuteModel();
            try
            {
                ExaminePosition objExamPosition = db.ExaminePosition.Where(e => e.Id == id).FirstOrDefault();
                if (objExamPosition != null)
                {
                    objExamPosition.ParentNode = objExamPos.ParentNode;
                    objExamPosition.ExamineName = objExamPos.ExamineName;
                    objExamPosition.OrderBy = objExamPos.OrderBy;
                    objExamPosition.Remark = objExamPos.Remark;
                    objExamPosition.ExamineSubMethod = objExamPos.ExamineSubMethod;
                    db.SubmitChanges();
                    model.Result = true;
                }
            }
            catch (Exception ex)
            {
                model.Result = false;
                model.Msg = ex.Message.ToString();
            }
            return model;
        }

        /// <summary>
        /// 更新检查部位信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="objExamPos"></param>
        /// <returns></returns>
        public ExcuteModel UpdateCheckPosition(int id, string subMethod, string mainMethod)
        {
            ExcuteModel model = new ExcuteModel();
            try
            {
                ExaminePosition objExamPosition = db.ExaminePosition.Where(e => e.Id == id).FirstOrDefault();
                if (objExamPosition != null)
                {
                    objExamPosition.ExamineMasteMethod = mainMethod;
                    objExamPosition.ExamineSubMethod = subMethod;
                    db.SubmitChanges();
                    model.Result = true;
                }
            }
            catch (Exception ex)
            {
                model.Result = false;
                model.Msg = ex.Message.ToString();
            }
            return model;
        }
        /// <summary>
        /// 更新检查部位状态
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel UpdateExaminePositionState(ExaminePosition param)
        {
            ExcuteModel em = new ExcuteModel();
            try
            {
                ExaminePosition modelParam = db.ExaminePosition.Where(o => o.Id == param.Id).FirstOrDefault();
                if (modelParam != null)
                {
                    modelParam.ExamineState = param.ExamineState;
                    db.SubmitChanges();
                    em.Result = true;
                }
            }
            catch (Exception ex)
            {
                em.Result = false;
                em.Msg = ex.Message;
            }
            return em;
        }

        /// <summary>
        ///  更新检查部位信息
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel UpdateExaminePosition(ExaminePosition param)
        {
            ExcuteModel em = new ExcuteModel();
            try
            {
                ExaminePosition modelParam = db.ExaminePosition.Where(o => o.Id == param.Id).FirstOrDefault();
                if (modelParam != null)
                {
                    modelParam.ParentNode = param.ParentNode;
                    modelParam.ExamineName = param.ExamineName;
                    modelParam.OrderBy = param.OrderBy;
                    modelParam.Remark = param.Remark;
                    modelParam.ExamineNamePinYin = param.ExamineNamePinYin;
                    db.SubmitChanges();
                    em.Result = true;
                }
            }
            catch (Exception ex)
            {
                em.Result = false;
                em.Msg = ex.Message;
            }
            return em;
        }

        public List<ExaminePosition> GetExaminePositionByParentNode(int parentNode)
        {
            return db.ExaminePosition.Where(p => p.ParentNode == parentNode && p.ExamineState == 1).ToList();
        }

        public List<ExaminePosition> GetExaminePositionByExamineName(string examineName)
        {
            string strSql = "select * from ExaminePosition where (ExamineName like '%{0}%' or ParentNodeName like '%{1}%' or ParentNamePinYin like '%{2}%' or ExamineNamePinYin like '%{3}%')  and ExamineState=1 and ParentNode<>0";
            strSql = string.Format(strSql, examineName, examineName, examineName, examineName);
            return db.ExecuteQuery<ExaminePosition>(strSql).ToList();
        }

        public ExcuteModel IsExistedExaminePositionName(string positionName)
        {
            ExcuteModel em = new ExcuteModel();
            string strSql = "select * from ExaminePosition where ExamineName='{0}'";
            strSql = string.Format(strSql, positionName);
            List<ExaminePosition> list = db.ExecuteQuery<ExaminePosition>(strSql).ToList();
            if (list.Count > 0)
            {
                em.Result = true;
            }
            else
            {
                em.Result = false;
            }
            return em;
        }

        /// <summary>
        /// 获取检查部位排序最大值
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public int? GetExaminePositionMax()
        {
            int? MaxValue = 0;
            return MaxValue = db.ExaminePosition.Max(o => o.OrderBy);
        }

        public List<ExaminePosition> GetSubExamPosList()
        {
            List<ExaminePosition> objExamPos = db.ExaminePosition.Where(p => p.ParentNode != 0 && p.ExamineState == 1).ToList();
            return objExamPos;
        }

        public List<ExaminePosition> GetExaminePositionCombineExamineName(List<string> parentNodeNameList, List<string> subNodeNameList)
        {
            List<ExaminePosition> list = new List<ExaminePosition>();
            string parentNodeName = string.Empty;
            string subNodeName = string.Empty;
            string strSql = string.Empty;
            try
            {
                strSql = "select * from ExaminePosition where 1=1";
                if (parentNodeNameList.Count == 0 && subNodeNameList.Count == 0)
                {
                    return list;
                }
                if (parentNodeNameList.Count != 0 && subNodeNameList.Count != 0)
                {
                    for (int i = 0; i < parentNodeNameList.Count; i++)
                    {
                        parentNodeName += "'" + parentNodeNameList[i] + "',";
                    }
                    parentNodeName = parentNodeName.Substring(0, parentNodeName.LastIndexOf(','));
                    strSql += string.Format(" and ParentNodeName in ({0})", parentNodeName);
                    for (int j = 0; j < subNodeNameList.Count; j++)
                    {
                        subNodeName += "'" + subNodeNameList[j] + "',";
                    }
                    subNodeName = subNodeName.Substring(0, subNodeName.LastIndexOf(','));
                    strSql += string.Format(" or ExamineName in ({0})", subNodeName);
                }
                strSql += "  and ParentNode<>0";
                list = db.ExecuteQuery<ExaminePosition>(strSql).ToList();
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("工作量统计大部位小部位异常GetExaminePositionCombineExamineName：" + strSql, ex.Message);
            }
            return list;
        }

        #endregion


        #region 查看影像记录

        /// <summary>
        /// 添加影像记录信息
        /// </summary>
        /// <param name="objExamPos"></param>
        /// <returns></returns>
        public ExcuteModel AddRequestImageViewInfo(RequestImageView param)
        {
            ExcuteModel model = new ExcuteModel();
            try
            {
                db.RequestImageView.InsertOnSubmit(param);
                db.SubmitChanges();
                model.Result = true;
            }
            catch (Exception ex)
            {
                model.Result = false;
                model.Msg = ex.Message.ToString();
            }
            return model;
        }

        /// <summary>
        /// 查看是否浏览影像
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ExcuteModel IsExistRequestImageView(string requestId, string userId)
        {
            ExcuteModel em = new ExcuteModel();
            string strSql = "select * from RequestImageView where RequestID='{0}' AND ViewUserId='{1}'";
            strSql = string.Format(strSql, requestId, userId);
            List<RequestImageView> list = db.ExecuteQuery<RequestImageView>(strSql).ToList();
            if (list.Count > 0)
            {
                em.Result = true;
            }
            else
            {
                em.Result = false;
            }
            return em;
        }

        #endregion

        #region 检查方法
        /// <summary>
        /// 获取检查方法信息
        /// </summary>
        /// <returns></returns>
        public List<ExamineMethod> GetExamMethodList()
        {
            //List<ExamineMethod> objExamMet = db.ExamineMethod.Where(p => p.ParentNode == 0 && p.ExamineState == 1).ToList();
            List<ExamineMethod> objExamMet = db.ExamineMethod.ToList();
            return objExamMet;
        }

        public List<ExamineMethod> GetExamMethodListNew()
        {
            List<ExamineMethod> objExamMet = db.ExamineMethod.ToList();
            return objExamMet;
        }

        /// <summary>
        /// 添加检查方法信息
        /// </summary>
        /// <param name="objExamPos"></param>
        /// <returns></returns>
        public ExcuteModel AddExamMethodInfo(ExamineMethod param)
        {
            ExcuteModel model = new ExcuteModel();
            try
            {
                ExamineMethod modelParam = db.ExamineMethod.Where(e => e.ExamineName == param.ExamineName && e.ParentNode == param.ParentNode).FirstOrDefault();
                if (modelParam == null)
                {
                    db.ExamineMethod.InsertOnSubmit(param);
                    db.SubmitChanges();
                    model.Result = true;
                }
                else
                {
                    model.Result = false;
                    model.Msg = "检查方法已存在";
                }
            }
            catch (Exception ex)
            {
                model.Result = false;
                model.Msg = ex.Message.ToString();
            }
            return model;
        }

        /// <summary>
        /// 更新检查方法信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public ExcuteModel UpdateExamMethodInfo(int id, int state)
        {
            ExcuteModel model = new ExcuteModel();
            try
            {
                ExamineMethod objExamMet = db.ExamineMethod.Where(e => e.Id == id).FirstOrDefault();
                if (objExamMet != null)
                {
                    objExamMet.ExamineState = state;
                    db.SubmitChanges();
                    model.Result = true;
                }
            }
            catch (Exception ex)
            {
                model.Result = false;
                model.Msg = ex.Message.ToString();
            }
            return model;
        }

        /// <summary>
        /// 根据编号获取检查方法列表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ExamineMethod GetExamMethodById(int id)
        {
            ExamineMethod objExamPos = db.ExamineMethod.Where(e => e.Id == id).FirstOrDefault<ExamineMethod>();
            return objExamPos;
        }

        /// <summary>
        /// 更新检查方法信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="objExamPos"></param>
        /// <returns></returns>
        public ExcuteModel UpdateExamMethod(int id, ExamineMethod objExamMet)
        {
            ExcuteModel model = new ExcuteModel();
            try
            {
                ExamineMethod objExamMethod = db.ExamineMethod.Where(e => e.Id == id).FirstOrDefault();
                if (objExamMethod != null)
                {
                    objExamMethod.ParentNode = objExamMet.ParentNode;
                    objExamMethod.ExamineName = objExamMet.ExamineName;
                    objExamMethod.OrderBy = objExamMet.OrderBy;
                    objExamMethod.Remark = objExamMet.Remark;
                    db.SubmitChanges();
                    model.Result = true;
                }
            }
            catch (Exception ex)
            {
                model.Result = false;
                model.Msg = ex.Message.ToString();
            }
            return model;
        }

        /// <summary>
        /// 更新检查方法状态
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel UpdateExamineMethodState(ExamineMethod param)
        {
            ExcuteModel em = new ExcuteModel();
            try
            {
                ExamineMethod modelParam = db.ExamineMethod.Where(o => o.Id == param.Id).FirstOrDefault();
                if (modelParam != null)
                {
                    modelParam.ExamineState = param.ExamineState;
                    db.SubmitChanges();
                    em.Result = true;
                }
            }
            catch (Exception ex)
            {
                em.Result = false;
                em.Msg = ex.Message;
            }
            return em;
        }

        /// <summary>
        ///  更新检查方法信息
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel UpdateExamineMethod(ExamineMethod param)
        {
            ExcuteModel em = new ExcuteModel();
            try
            {
                ExamineMethod modelParam = db.ExamineMethod.Where(o => o.Id == param.Id).FirstOrDefault();
                if (modelParam != null)
                {
                    modelParam.ParentNode = param.ParentNode;
                    modelParam.ExamineName = param.ExamineName;
                    modelParam.OrderBy = param.OrderBy;
                    modelParam.Remark = param.Remark;
                    db.SubmitChanges();
                    em.Result = true;
                }
            }
            catch (Exception ex)
            {
                em.Result = false;
                em.Msg = ex.Message;
            }
            return em;
        }

        /// <summary>
        /// 获取检查方法排序最大值
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public int? GetExamineMethodMax()
        {
            int? MaxValue = 0;
            return MaxValue = db.ExamineMethod.Max(o => o.OrderBy);
        }

        public List<ExamineMethod> GetExamineMethodByParentNode(int parentNode)
        {
            return db.ExamineMethod.Where(p => p.ParentNode == parentNode && p.ExamineState == 1).ToList();
        }

        public ExcuteModel IsExistedExamineMethodName(string methodName)
        {
            ExcuteModel em = new ExcuteModel();
            string strSql = "select * from ExamineMethod where ExamineName='{0}'";
            strSql = string.Format(strSql, methodName);
            List<ExamineMethod> list = db.ExecuteQuery<ExamineMethod>(strSql).ToList();
            if (list.Count > 0)
            {
                em.Result = true;
            }
            else
            {
                em.Result = false;
            }
            return em;
        }
        #endregion

        /// <summary>
        /// 添加消息框状态信息
        /// </summary>
        /// <param name="objMsgFrameState"></param>
        /// <returns></returns>
        public ExcuteModel AddMsgFrameState(MsgFrameState objMsgFrameState)
        {
            ExcuteModel model = new ExcuteModel();
            try
            {
                MsgFrameState modelMsgFrameState = db.MsgFrameState.Where(e => e.RequestId == objMsgFrameState.RequestId).FirstOrDefault();
                if (modelMsgFrameState == null)
                {
                    db.MsgFrameState.InsertOnSubmit(objMsgFrameState);
                    db.SubmitChanges();
                    model.Result = true;
                }
                else
                {
                    model.Result = false;
                    model.Msg = "检查方法已存在";
                }
            }
            catch (Exception ex)
            {
                model.Result = false;
                model.Msg = ex.Message.ToString();
            }
            return model;
        }

        public ExcuteModel UpdateMsgFrameState(string str, int intState)
        {
            ExcuteModel objExcuteModel = new ExcuteModel();
            List<string> sqlList = new List<string>();
            string[] strArray = str.Split(',');
            for (int i = 0; i < strArray.Length; i++)
            {
                string sql = "update MsgFrameState set RequestState={1} where RequestId='{0}'";
                sql = string.Format(sql, strArray[i], intState);
                sqlList.Add(sql);
            }

            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            try
            {
                conn.Open();
                cmd.Transaction = conn.BeginTransaction();//开启事务
                foreach (string itemSql in sqlList)
                {
                    cmd.CommandText = itemSql;
                    cmd.ExecuteNonQuery();
                }
                cmd.Transaction.Commit();//提交事务
                objExcuteModel.Result = true;
            }
            catch (Exception ex)
            {
                if (cmd.Transaction != null)
                    cmd.Transaction.Rollback();//回滚事务
                objExcuteModel.ErrorMessage = ex.Message.ToString();
                objExcuteModel.Result = false;
            }
            finally
            {
                if (cmd.Transaction != null)
                    cmd.Transaction = null;
                conn.Close();
            }
            return objExcuteModel;
        }

        public List<MsgFrameState> GetMsgFrameState(string str)
        {
            List<MsgFrameState> objList = new List<MsgFrameState>();
            string[] strArray = str.Split(',');
            for (int i = 0; i < strArray.Length; i++)
            {
                MsgFrameState objMsgFrameState = db.MsgFrameState.Where(m => m.RequestId == strArray[i] && m.RequestState == 0).FirstOrDefault();
                if (objMsgFrameState != null)
                {
                    objList.Add(objMsgFrameState);
                }
            }
            return objList;
        }

        /// <summary>
        /// 获取申请患者数据
        /// </summary>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        public List<MsgFrameState> GetMsgFrameRequestData(string strUserID, int intState)
        {
            List<MsgFrameState> objList = new List<MsgFrameState>();
            objList = db.MsgFrameState.Where(o => o.UserId == strUserID && o.RequestState == intState).ToList();
            return objList;
        }

        /// <summary>
        /// 此方法用处为2个地方，因此作一下修改
        /// 更新提醒数据时，使用传递值的方式
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="intState"></param>
        /// <returns></returns>
        public ExcuteModel UpdateSingleMsgFrameState(string requestId, int intState)
        {
            ExcuteModel objExcuteModel = new ExcuteModel();
            string sql = "update MsgFrameState set RequestState={1} where RequestId='{0}'";
            sql = string.Format(sql, requestId, intState);
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand(sql, conn);
            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
                objExcuteModel.Result = true;
            }
            catch (Exception ex)
            {
                objExcuteModel.Result = false;
                objExcuteModel.Msg = ex.ToString();
            }
            finally
            {
                conn.Close();
            }
            return objExcuteModel;

        }

        /// <summary>
        /// 初始化排班数据
        /// </summary>
        public ExcuteModel InitScheduleSettings(List<ScheduleSettings> list, string deptId, string userId, string createName)
        {
            ExcuteModel objExcuteModel = new ExcuteModel();
            List<string> sqlList = new List<string>();
            foreach (var item in list)
            {
                string sql = "insert into ScheduleSettings(Id,ShiftName,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday,StartTime,EndTime,DepartmentId,UserId,CreateName)";
                sql += " values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}')";
                sql = string.Format(sql, item.Id, item.ShiftName, item.Monday, item.Tuesday, item.Wednesday, item.Thursday, item.Friday, item.Saturday, item.Sunday, item.StartTime, item.EndTime, deptId, userId, createName);
                sqlList.Add(sql);
            }
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            try
            {
                conn.Open();
                cmd.Transaction = conn.BeginTransaction();
                foreach (string itemSql in sqlList)
                {
                    cmd.CommandText = itemSql;
                    cmd.ExecuteNonQuery();
                }
                cmd.Transaction.Commit();//提交事务
                objExcuteModel.Result = true;
            }
            catch (Exception ex)
            {
                if (cmd.Transaction != null)
                    cmd.Transaction.Rollback();//回滚事务
                objExcuteModel.ErrorMessage = ex.Message.ToString();
                objExcuteModel.Result = false;
            }
            finally
            {
                if (cmd.Transaction != null)
                    cmd.Transaction = null;
                conn.Close();
            }
            return objExcuteModel;
        }

        /// <summary>
        /// 保存排班信息
        /// </summary>
        /// <param name="sqlList"></param>
        /// <returns></returns>
        public ExcuteModel SaveScheduling(List<string> sqlList)
        {
            ExcuteModel objExcuteModel = new ExcuteModel();
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            try
            {
                conn.Open();
                cmd.Transaction = conn.BeginTransaction();//开启事务
                foreach (string itemSql in sqlList)
                {
                    cmd.CommandText = itemSql;
                    cmd.ExecuteNonQuery();
                }
                cmd.Transaction.Commit();//提交事务
                objExcuteModel.Result = true;
            }
            catch (Exception ex)
            {
                if (cmd.Transaction != null)
                    cmd.Transaction.Rollback();//回滚事务
                objExcuteModel.ErrorMessage = ex.Message.ToString();
                objExcuteModel.Result = false;
            }
            finally
            {
                if (cmd.Transaction != null)
                    cmd.Transaction = null;
                conn.Close();
            }
            return objExcuteModel;
        }

        /// <summary>
        /// 获取排班信息
        /// </summary>
        /// <returns></returns>
        public List<ScheduleSettings> GetScheduleSettings(string deptId, string startTime, string endTime)
        {
            return db.ScheduleSettings.Where(s => s.StartTime == startTime && s.EndTime == endTime && s.DepartmentId == deptId).ToList();
        }

        /// <summary>
        /// 获取用户类型
        /// </summary>
        /// <returns></returns>
        public List<UserType> GetUserType()
        {
            return db.UserType.ToList();
        }

        /// <summary>
        /// 添加班次信息
        /// </summary>
        /// <param name="shiftSettings"></param>
        /// <returns></returns>
        public ExcuteModel InsertShiftSettings(ShiftSettings shiftSettings)
        {
            ExcuteModel model = new ExcuteModel();
            try
            {
                ShiftSettings objShiftSettings = db.ShiftSettings.Where(s => s.ShiftName == shiftSettings.ShiftName).FirstOrDefault();
                if (objShiftSettings == null)
                {
                    db.ShiftSettings.InsertOnSubmit(shiftSettings);
                    db.SubmitChanges();
                    model.Result = true;
                }
                else
                {
                    model.Result = false;
                    model.Msg = "该班次已存在";
                }
            }
            catch (Exception ex)
            {
                model.Result = false;
                model.Msg = ex.Message.ToString();
            }
            return model;
        }

        /// <summary>
        /// 是否存在排班模板
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public bool IsExistsTemplate(string deptId, string startTime, string endTime)
        {
            string sql = "select count(*) from ScheduleTemplate";
            sql += " where StartTime='{0}' and EndTime='{1}' and DepartmentId='{2}'";
            sql = string.Format(sql, startTime, endTime, deptId);
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            int flag = 0;
            try
            {
                conn.Open();
                cmd.CommandText = sql;
                int rowCount = (int)cmd.ExecuteScalar();
                conn.Close();
                if (rowCount != 0)
                {
                    flag = 1;
                }
            }
            catch (Exception)
            { }
            return flag == 1 ? true : false;
        }

        /// <summary>
        /// 判断是否存在排班记录
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public bool IsExistsScheduleSettings(string deptId, string startTime, string endTime)
        {
            string sql = "select count(*) from ScheduleSettings";
            sql += " where StartTime='{0}' and EndTime='{1}' and DepartmentId='{2}'";
            sql = string.Format(sql, startTime, endTime, deptId);
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            int flag = 0;
            try
            {
                conn.Open();
                cmd.CommandText = sql;
                int rowCount = (int)cmd.ExecuteScalar();
                conn.Close();
                if (rowCount != 0)
                {
                    flag = 1;
                }
            }
            catch (Exception)
            { }
            return flag == 1 ? true : false;
        }
        /// <summary>
        /// 添加排班模板
        /// </summary>
        /// <param name="sqlList"></param>
        /// <returns></returns>
        public ExcuteModel AddScheduleTemplate(List<string> sqlList)
        {
            ExcuteModel excuteModel = new ExcuteModel();
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            try
            {
                conn.Open();
                cmd.Transaction = conn.BeginTransaction();
                foreach (var itemSql in sqlList)
                {
                    cmd.CommandText = itemSql;
                    cmd.ExecuteNonQuery();
                }
                cmd.Transaction.Commit();
                excuteModel.Result = true;
            }
            catch (Exception ex)
            {
                if (cmd.Transaction != null)
                    cmd.Transaction.Rollback();//回滚事务
                excuteModel.ErrorMessage = ex.Message.ToString();
                excuteModel.Result = false;
            }
            finally
            {
                if (cmd.Transaction != null)
                    cmd.Transaction = null;
                conn.Close();
            }
            return excuteModel;
        }

        /// <summary>
        /// 添加班次信息
        /// </summary>
        /// <param name="shiftSettings"></param>
        /// <returns></returns>
        public ExcuteModel AddShiftSettings(ShiftSettings shiftSettings)
        {
            ExcuteModel excuteModel = new ExcuteModel();
            try
            {
                ShiftSettings objShift = db.ShiftSettings.Where(s => s.ShiftName == shiftSettings.ShiftName && s.DepartmentId == shiftSettings.DepartmentId).FirstOrDefault();
                if (objShift == null)
                {
                    db.ShiftSettings.InsertOnSubmit(shiftSettings);
                    db.SubmitChanges();
                    excuteModel.Result = true;
                }
                else
                {
                    excuteModel.Result = false;
                    excuteModel.Msg = "该班次已存在！";
                }
            }
            catch (Exception ex)
            {
                excuteModel.Result = false;
                excuteModel.Msg = ex.Message.ToString();
            }
            return excuteModel;
        }

        /// <summary>
        /// 获取班次设置
        /// </summary>
        /// <returns></returns>
        public List<ShiftSettings> GetShiftSettings(string deptId)
        {
            List<ShiftSettings> list = new List<ShiftSettings>();
            string sql = "select ShiftId,ShiftName,UserTypeName,StartTime,EndTime from ShiftSettings inner join UserType";
            sql += " on ShiftSettings.UType=UserType.UserTypeId where DeleteState=0 and DepartmentId='{0}'";
            sql = string.Format(sql, deptId);
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            try
            {
                conn.Open();
                cmd.CommandText = sql;
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    list.Add(new ShiftSettings()
                    {
                        ShiftId = Convert.ToInt32(reader["ShiftId"]),
                        ShiftName = reader["ShiftName"].ToString(),
                        UType = reader["UserTypeName"].ToString(),
                        StartTime = reader["StartTime"].ToString(),
                        EndTime = reader["EndTime"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                conn.Close();
            }
            return list;
        }

        public ShiftSettings GetShiftSettingsById(int shiftId)
        {
            return db.ShiftSettings.Where(s => s.ShiftId == shiftId).FirstOrDefault();
        }

        public ExcuteModel UpdateScheduleTemplate(List<string> sqlList)
        {
            ExcuteModel excuteModel = new ExcuteModel();
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            try
            {
                conn.Open();
                cmd.Transaction = conn.BeginTransaction();
                foreach (var itemSql in sqlList)
                {
                    cmd.CommandText = itemSql;
                    cmd.ExecuteNonQuery();
                }
                cmd.Transaction.Commit();
                excuteModel.Result = true;
            }
            catch (Exception ex)
            {
                if (cmd.Transaction != null)
                    cmd.Transaction.Rollback();//回滚事务
                excuteModel.ErrorMessage = ex.Message.ToString();
                excuteModel.Result = false;
            }
            finally
            {
                if (cmd.Transaction != null)
                    cmd.Transaction = null;
                conn.Close();
            }
            return excuteModel;
        }

        /// <summary>
        /// 更新班次信息
        /// </summary>
        /// <param name="shiftSettings"></param>
        /// <returns></returns>
        public ExcuteModel UpdateShiftSettings(ShiftSettings shiftSettings)
        {
            ExcuteModel model = new ExcuteModel();
            try
            {
                ShiftSettings objShiftSettings = db.ShiftSettings.Where(e => e.ShiftId == shiftSettings.ShiftId).FirstOrDefault();
                if (objShiftSettings != null)
                {
                    objShiftSettings.ShiftName = shiftSettings.ShiftName;
                    objShiftSettings.UType = shiftSettings.UType;
                    objShiftSettings.StartTime = shiftSettings.StartTime;
                    objShiftSettings.EndTime = shiftSettings.EndTime;
                    db.SubmitChanges();
                    model.Result = true;
                }
            }
            catch (Exception ex)
            {
                model.Result = false;
                model.Msg = ex.Message.ToString();
            }
            return model;

        }

        /// <summary>
        /// 删除班次信息
        /// </summary>
        /// <param name="shiftId"></param>
        /// <returns></returns>
        public ExcuteModel DeleteShiftSettings(int shiftId)
        {
            ExcuteModel model = new ExcuteModel();
            try
            {
                ShiftSettings objShiftSettings = db.ShiftSettings.Where(e => e.ShiftId == shiftId).FirstOrDefault();
                if (objShiftSettings != null)
                {
                    objShiftSettings.DeleteState = 1;
                    db.SubmitChanges();
                    model.Result = true;
                }
            }
            catch (Exception ex)
            {
                model.Result = false;
                model.Msg = ex.Message.ToString();
            }
            return model;
        }

        public List<ScheduleTemplate> GetScheduleTemplate(string sql)
        {
            List<ScheduleTemplate> list = new List<ScheduleTemplate>();
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            SqlDataReader reader = null;
            try
            {
                conn.Open();
                cmd.CommandText = sql;
                reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    list.Add(new ScheduleTemplate()
                    {
                        TemplateId = reader["TemplateId"].ToString(),
                        TemplateName = reader["TemplateName"].ToString()
                    });
                }
            }
            catch (Exception ex)
            {

            }
            return list;
        }

        public List<ScheduleTemplate> GetScheduleTemplateById(string templateId)
        {
            return db.ScheduleTemplate.Where(s => s.TemplateId == templateId).ToList();
        }

        /// <summary>
        /// 更新排班信息
        /// </summary>
        /// <param name="sqlList"></param>
        /// <returns></returns>
        public ExcuteModel UpdateScheduleSettings(List<string> sqlList)
        {
            ExcuteModel excuteModel = new ExcuteModel();
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            try
            {
                conn.Open();
                cmd.Transaction = conn.BeginTransaction();
                foreach (var itemSql in sqlList)
                {
                    cmd.CommandText = itemSql;
                    cmd.ExecuteNonQuery();
                }
                cmd.Transaction.Commit();
                excuteModel.Result = true;
            }
            catch (Exception ex)
            {
                if (cmd.Transaction != null)
                    cmd.Transaction.Rollback();//回滚事务
                excuteModel.ErrorMessage = ex.Message.ToString();
                excuteModel.Result = false;
            }
            finally
            {
                if (cmd.Transaction != null)
                    cmd.Transaction = null;
                conn.Close();
            }
            return excuteModel;
        }

        /// <summary>
        /// 根据部门获取专家列表
        /// </summary>
        /// <param name="deptId"></param>
        /// <returns></returns>
        public List<DoctorInfo> GetDoctorInfoByDeptId(string deptId)
        {
            List<DoctorInfo> doctorList = new List<DoctorInfo>();
            doctorList = db.DoctorInfo.Where(d => d.Dept == deptId).ToList();
            return doctorList;
        }

        /// <summary>
        /// 根据部门和专家编号获取医生姓名
        /// </summary>
        /// <param name="deptId"></param>
        /// <param name="doctorId"></param>
        /// <returns></returns>
        public string GetDoctorNameByDeptIdAndId(string itemSql)
        {
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            string doctorName = string.Empty;
            try
            {
                conn.Open();
                cmd.CommandText = itemSql;
                doctorName = cmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                conn.Close();
            }
            return doctorName;
        }

        public ShiftSettings GetScheduleUserType(string shiftName, string deptId)
        {
            return db.ShiftSettings.Where(s => s.ShiftName == shiftName && s.DepartmentId == deptId && s.DeleteState == 0).FirstOrDefault();
        }

        #region 申请检查记录维护

        public ExcuteModel AddRequestStudyRecord(Dictionary<string, RequestStudyRecord> dictionary)
        {
            ExcuteModel excuteModel = new ExcuteModel();
            List<string> sqlList = new List<string>();
            foreach (RequestStudyRecord item in dictionary.Values)
            {
                string sql = "insert into RequestStudyRecord(RequestId,NewPatientId,HospitalId,StudyInstanceUid,PatientId,PatientsName,PatientsBirthDate,PatientsAge,PatientSex,StudyDate,StudyTime,AccessionNumber,StudyDescription,NumberOfStudyRelatedSeries,NumberOfStudyRelatedInstances,StudySizeInKB,IsMasterCheckRecord,Modality,IsUploadCase)";
                sql += " values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}',{13},{14},{15},{16},'{17}',{18})";
                sql = string.Format(sql, item.RequestId, item.NewPatientId, item.HospitalId, item.StudyInstanceUid, item.PatientId, item.PatientsName,
                    item.PatientsBirthDate, item.PatientsAge, item.PatientSex, item.StudyDate, item.StudyTime, item.AccessionNumber, item.StudyDescription, item.NumberOfStudyRelatedSeries, item.NumberOfStudyRelatedInstances, item.StudySizeInKB, item.IsMasterCheckRecord, item.Modality, item.IsUploadCase);
                sqlList.Add(sql);
            }
            SqlConnection conn = new SqlConnection(connString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            try
            {
                conn.Open();
                cmd.Transaction = conn.BeginTransaction();
                foreach (var itemSql in sqlList)
                {
                    cmd.CommandText = itemSql;
                    cmd.ExecuteNonQuery();
                }
                cmd.Transaction.Commit();
                excuteModel.Result = true;
            }
            catch (Exception ex)
            {
                if (cmd.Transaction != null)
                    cmd.Transaction.Rollback();//回滚事务
                excuteModel.ErrorMessage = ex.Message.ToString();
                excuteModel.Result = false;
            }
            finally
            {
                if (cmd.Transaction != null)
                    cmd.Transaction = null;
                conn.Close();
            }
            return excuteModel;
        }

        public List<RequestStudyRecord> GetRequestStudyRecord()
        {
            List<RequestStudyRecord> list = db.RequestStudyRecord.ToList();
            return list;
        }

        public ExcuteModel DeleteRequestStudyRecord(RequestStudyRecord requestRecord)
        {
            ExcuteModel model = new ExcuteModel();
            RequestStudyRecord objRequest = db.RequestStudyRecord.Where(r => r.RequestId == requestRecord.RequestId && r.StudyInstanceUid == requestRecord.StudyInstanceUid).FirstOrDefault();
            try
            {
                if (objRequest != null)
                {
                    objRequest.DeleteStatus = requestRecord.DeleteStatus;
                    db.SubmitChanges();
                    model.Result = true;
                }
                else
                {
                    model.Result = false;
                    model.Msg = "删除失败！";
                }
            }
            catch (Exception exp)
            {
                model.Result = false;
                model.ErrorMessage = exp.ToString();
            }
            return model;
        }

        public ExcuteModel UpdateRequestStudyRecord(RequestStudyRecord requestRecord)
        {
            ExcuteModel model = new ExcuteModel();
            RequestStudyRecord objRequest = db.RequestStudyRecord.Where(r => r.RequestId == requestRecord.RequestId && r.StudyInstanceUid == requestRecord.StudyInstanceUid).FirstOrDefault();
            try
            {
                if (objRequest != null)
                {
                    if (!string.IsNullOrEmpty(requestRecord.NewPatientId))
                    {
                        objRequest.NewPatientId = requestRecord.NewPatientId;
                    }
                    if (!string.IsNullOrEmpty(requestRecord.HospitalId))
                    {
                        objRequest.HospitalId = requestRecord.HospitalId;
                    }
                    if (!string.IsNullOrEmpty(requestRecord.PatientId))
                    {
                        objRequest.PatientId = requestRecord.PatientId;
                    }
                    if (!string.IsNullOrEmpty(requestRecord.PatientsName))
                    {
                        objRequest.PatientsName = requestRecord.PatientsName;
                    }
                    if (!string.IsNullOrEmpty(requestRecord.PatientsBirthDate))
                    {
                        objRequest.PatientsBirthDate = requestRecord.PatientsBirthDate;
                    }
                    if (!string.IsNullOrEmpty(requestRecord.PatientsAge))
                    {
                        objRequest.PatientsAge = requestRecord.PatientsAge;
                    }
                    if (!string.IsNullOrEmpty(requestRecord.PatientSex))
                    {
                        objRequest.PatientSex = requestRecord.PatientSex;
                    }
                    if (!string.IsNullOrEmpty(requestRecord.StudyDate))
                    {
                        objRequest.StudyDate = requestRecord.StudyDate;
                    }
                    if (!string.IsNullOrEmpty(requestRecord.StudyTime))
                    {
                        objRequest.StudyTime = requestRecord.StudyTime;
                    }
                    if (!string.IsNullOrEmpty(requestRecord.AccessionNumber))
                    {
                        objRequest.AccessionNumber = requestRecord.AccessionNumber;
                    }
                    if (!string.IsNullOrEmpty(requestRecord.StudyDescription))
                    {
                        objRequest.StudyDescription = requestRecord.StudyDescription;
                    }
                    if (!string.IsNullOrEmpty(requestRecord.ExaminePart))
                    {
                        objRequest.ExaminePart = requestRecord.ExaminePart;
                    }
                    if (!string.IsNullOrEmpty(requestRecord.Modality))
                    {
                        objRequest.Modality = requestRecord.Modality;
                    }
                    if (requestRecord.NumberOfStudyRelatedInstances > 0)
                    {
                        objRequest.NumberOfStudyRelatedInstances = requestRecord.NumberOfStudyRelatedInstances;
                    }
                    if (requestRecord.NumberOfStudyRelatedSeries > 0)
                    {
                        objRequest.NumberOfStudyRelatedSeries = requestRecord.NumberOfStudyRelatedSeries;
                    }
                    if (requestRecord.StudySizeInKB > 0)
                    {
                        objRequest.StudySizeInKB = requestRecord.StudySizeInKB;
                    }
                    if (requestRecord.IsMasterCheckRecord > 0)
                    {
                        objRequest.IsMasterCheckRecord = requestRecord.IsMasterCheckRecord;
                    }
                    db.SubmitChanges();
                    model.Result = true;
                }
                else
                {
                    model.Result = false;
                    model.Msg = "更新失败！";
                }
            }
            catch (Exception exp)
            {
                model.Result = false;
                model.ErrorMessage = exp.ToString();
            }
            return model;
        }

        public RequestStudyRecord GetRequestStudyRecordByUid(string strRequestID, string studyInstanceUid)
        {
            return db.RequestStudyRecord.Where(r => r.StudyInstanceUid == studyInstanceUid && r.RequestId == strRequestID).FirstOrDefault();
        }

        public ExcuteModel AddSingleRequestStudyRecord(RequestStudyRecord requestStudyRecord)
        {
            ExcuteModel model = new ExcuteModel();
            try
            {
                RequestStudyRecord obj = db.RequestStudyRecord.Where(r => r.StudyInstanceUid == requestStudyRecord.StudyInstanceUid && r.RequestId == requestStudyRecord.RequestId).FirstOrDefault();
                if (obj == null)
                {
                    db.RequestStudyRecord.InsertOnSubmit(requestStudyRecord);
                    model.Msg = "添加成功！";
                }
                db.SubmitChanges();
                model.Result = true;
            }
            catch (Exception ex)
            {
                model.Result = false;
                model.Msg = "添加失败！";
                model.ErrorMessage = ex.Message;
            }
            return model;

        }
        #endregion

        #region 消息聊天
        /// <summary>
        /// 查询用户是否存在
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel SearchXR_UserMessage(XR_UserMessage param)
        {
            ExcuteModel message = new ExcuteModel();
            int listCount = db.XR_UserMessage.Where(o => o.user_id == param.user_id).ToList().Count;
            if (listCount > 0)
            {
                message.Result = true;
            }
            else
            {
                message.Result = false;
            }
            return message;
        }
        #endregion

        #region 退费申请确认
        public List<RefundRecordManager> GetRefundStateRecordInfo(string patientId, string patientName, DateTime startTime, DateTime endTime, string hospitalId, string remark = null)
        {
            List<RefundRecordManager> list = new List<RefundRecordManager>();
            StringBuilder sql = new StringBuilder();
            sql.Append(@"SELECT		rs.RequestId AS RequestId,
	rs.IsRefund AS IsRefund,
	rs.Remark AS Remark,
	PatientName AS PatientName,
	CPatientName as CPatientName,
	PatientSex AS PatientSex,
	PatientAge AS PatientAge,
	NAME as SysName,
	RefundDate as RefundDate,
	HospitalId  as HospitalId,
	ri.PatientId as PatientId FROM
    RefundStateRecord rs
	INNER JOIN RequestInfo ri ON ri.RequestId= rs.RequestId
	INNER JOIN PatientInfo pi ON ri.PatientId= pi.PatientId
	INNER JOIN [PatientVisit].[dbo].[Sys_Users] ON rs.UserId= [PatientVisit].[dbo].[Sys_Users].UserId WHERE 1=1 ");
            if (!string.IsNullOrEmpty(hospitalId))
            {
                sql.AppendFormat(@" AND ri.HospitalId ='{0}'", hospitalId);
            }
            if (!string.IsNullOrEmpty(patientId))
            {
                sql.AppendFormat(@" AND pi.PatientId LIKE '%{0}%'", patientId);
            }
            if (!string.IsNullOrEmpty(patientName))
            {
                sql.AppendFormat(@" AND ((pi.PatientName LIKE '%{0}%') OR (pi.CPatientName LIKE '%{0}%'))", patientName);
            }
            if (!string.IsNullOrEmpty(remark))
            {
                sql.AppendFormat(@" AND rs.Remark LIKE '%{0}%'", remark);
            }

            if (!string.IsNullOrEmpty(startTime.ToString()) && !string.IsNullOrEmpty(endTime.ToString()))
            {
                sql.AppendFormat(" and rs.RefundDate>= CONVERT(VARCHAR(20),'{0}',23) and rs.RefundDate< CONVERT(VARCHAR(20),'{1}',23)", startTime, endTime);
            }
            try
            {
                list = db.ExecuteQuery<RefundRecordManager>(sql.ToString()).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }


        public ExcuteModel UpdateRefundStateRecord(string requestId, int isRefund)
        {
            ExcuteModel model = new ExcuteModel();
            try
            {
                RefundStateRecord refundRecord = db.RefundStateRecord.Where(r => r.RequestId == requestId).FirstOrDefault();
                if (refundRecord != null)
                {
                    refundRecord.IsRefund = isRefund;
                    db.SubmitChanges();
                    model.Result = true;
                }
            }
            catch (Exception ex)
            {
                model.Result = false;
                model.ErrorMessage = ex.Message.ToString();
            }
            return model;
        }

        public ExcuteModel UpdateReturnCause(string requestId, string returnCause)
        {
            ExcuteModel model = new ExcuteModel();
            try
            {
                RefundStateRecord refundRecord = db.RefundStateRecord.Where(r => r.RequestId == requestId).FirstOrDefault();
                if (refundRecord != null)
                {
                    refundRecord.ReturnCause = returnCause;
                    db.SubmitChanges();
                    model.Result = true;
                }
            }
            catch (Exception ex)
            {
                model.Result = false;
                model.ErrorMessage = ex.Message.ToString();
            }
            return model;
        }
        public ExcuteModel AddRefundStateRecord(RefundStateRecord refundRecord)
        {
            ExcuteModel model = new ExcuteModel();
            try
            {
                RefundStateRecord objRefundRecord = db.RefundStateRecord.Where(r => r.RequestId == refundRecord.RequestId).FirstOrDefault();
                if (objRefundRecord == null)
                {
                    db.RefundStateRecord.InsertOnSubmit(refundRecord);
                    db.SubmitChanges();
                    model.Result = true;
                }
            }
            catch (Exception ex)
            {
                model.Result = false;
                model.ErrorMessage = ex.Message.ToString();
            }
            return model;
        }

        public RefundStateRecord GetRefundStateRecordById(string requestId)
        {
            return db.RefundStateRecord.Where(r => r.RequestId == requestId).FirstOrDefault();
        }
        #endregion

        #region 单个患者多影像申请
        public ExcuteModel UpdateRequestStudyRecordExaminePort(string requestId, string studyInstanceUid, string examinePort)
        {
            ExcuteModel model = new ExcuteModel();
            try
            {
                RequestStudyRecord studyRecord = db.RequestStudyRecord.Where(r => r.RequestId == requestId && r.StudyInstanceUid == studyInstanceUid).FirstOrDefault();
                if (studyRecord == null)
                {
                    model.Result = false;
                    model.Msg = "未找到相关记录！";
                    return model;
                }
                studyRecord.ExaminePart = examinePort;
                db.SubmitChanges();
                model.Result = true;
            }
            catch (Exception ex)
            {
                model.Result = false;
                model.Msg = "数据更新异常！";
            }
            return model;
        }

        public ExcuteModel UpdateRequestStudyRecordExamineMethod(string requestId, string studyInstanceUid, string examineMethod)
        {
            ExcuteModel model = new ExcuteModel();
            try
            {
                RequestStudyRecord studyRecord = db.RequestStudyRecord.Where(r => r.RequestId == requestId && r.StudyInstanceUid == studyInstanceUid).FirstOrDefault();
                if (studyRecord == null)
                {
                    model.Result = false;
                    model.Msg = "未找到相关记录！";
                    return model;
                }
                studyRecord.Modality = examineMethod;
                db.SubmitChanges();
                model.Result = true;
            }
            catch (Exception ex)
            {
                model.Result = false;
                model.Msg = "数据更新异常！";
            }
            return model;
        }

        public ExcuteModel UpdateRequestInfoExaminePort(string requestId, string studyInstanceUid, string examinePort)
        {
            ExcuteModel model = new ExcuteModel();
            try
            {
                RequestInfo requestInfo = db.RequestInfo.Where(r => r.RequestId == requestId && r.StudyInstanceUid == studyInstanceUid).FirstOrDefault();
                if (requestInfo == null)
                {
                    model.Result = false;
                    model.Msg = "未找到相关记录！";
                    return model;
                }
                requestInfo.ExaminePart = examinePort;
                db.SubmitChanges();
                model.Result = true;
            }
            catch (Exception ex)
            {
                model.Result = false;
                model.Msg = "数据更新异常！";
            }
            return model;
        }

        public ExcuteModel UpdateRequestInfoExamineMethod(string requestId, string studyInstanceUid, string examineMethod)
        {
            ExcuteModel model = new ExcuteModel();
            try
            {
                RequestInfo requestInfo = db.RequestInfo.Where(r => r.RequestId == requestId && r.StudyInstanceUid == studyInstanceUid).FirstOrDefault();
                if (requestInfo == null)
                {
                    model.Result = false;
                    model.Msg = "未找到相关记录！";
                    return model;
                }
                requestInfo.ExamineMethod = examineMethod;
                db.SubmitChanges();
                model.Result = true;
            }
            catch (Exception ex)
            {
                model.Result = false;
                model.Msg = "数据更新异常！";
            }
            return model;
        }

        public ExcuteModel UpdateRequestStudyRecordUploadCase(string requestId, string studyInstanceUid, int isUploadCase)
        {
            ExcuteModel model = new ExcuteModel();
            try
            {
                RequestStudyRecord studyRecord = db.RequestStudyRecord.Where(r => r.RequestId == requestId && r.StudyInstanceUid == studyInstanceUid).FirstOrDefault();
                if (studyRecord == null)
                {
                    model.Result = false;
                    model.Msg = "未找到相关记录！";
                    return model;
                }
                studyRecord.IsUploadCase = isUploadCase;
                db.SubmitChanges();
                model.Result = true;
            }
            catch (Exception ex)
            {
                model.Result = false;
                model.Msg = "数据更新异常！";
            }
            return model;
        }

        public RequestStudyRecord GetMasterRecordRequestStudyRecord(string requestId, int isMasterCheckRecord)
        {
            return db.RequestStudyRecord.Where(r => r.RequestId == requestId && r.IsMasterCheckRecord == isMasterCheckRecord).FirstOrDefault();
        }

        public List<RequestStudyRecordInfo> GetRequestStudyRecordList(string requestId)
        {
            List<RequestStudyRecordInfo> list = new List<RequestStudyRecordInfo>();
            StringBuilder strAppend = new StringBuilder();
            strAppend.Append(@"SELECT  RequestId,NewPatientId,HospitalId,StudyInstanceUid,PatientId,PatientsName,(Dates+' '+Times) AS ExamineDate,Modality,ExaminePart,IsMasterCheckRecord,IsUploadCase,ExaminePart FROM
                               (SELECT *,CONVERT(VARCHAR(100),CONVERT(DATE,StudyDate,23),23) AS Dates, SUBSTRING(StudyTime,1,2)+':'+SUBSTRING(StudyTime,3,2)+':'+SUBSTRING(StudyTime,5,2) AS Times from RequestStudyRecord) vv
                               WHERE RequestId='" + requestId + "'");
            list = db.ExecuteQuery<RequestStudyRecordInfo>(strAppend.ToString()).ToList();
            return list;
        }
        #endregion

        #region 展示厅相应方法
        /// <summary>
        /// 获取医院总数
        /// </summary>
        /// <returns></returns>
        public int GetHospitalAllCounts(string HospitalID)
        {
            int resultAll = 0;
            //int deptCount = DbDataContext.Sys_Dept.Count();
            resultAll = db.SubHospital.Where(o => o.PHospitalId == HospitalID).Count();
            return resultAll;
        }

        /// <summary>
        /// 当前分诊总例数
        /// 统计当天
        /// </summary>
        /// <returns></returns>
        public int GetDiagnoseTodayCounts(string HospitalID)
        {
            int resultAll = 0;
            StringBuilder strAppend = new StringBuilder();
            string strDate = DateTime.Now.ToString("yyyy-MM-dd");
            strAppend.Append(@"SELECT COUNT(1) AS COUNTS FROM RequestInfo RI WHERE RI.HospitalId in(SELECT SubHospitalId FROM SubHospital WHERE PHospitalId='" + HospitalID + "') AND RI.Diagnosticstate='Diagnose' AND CONVERT(VARCHAR(20),RI.RequestDate,23)='" + strDate + "'");
            LogHelper.WriteInfoLog("当前分诊总例数GetDiagnoseTodayCounts：" + strAppend.ToString());
            resultAll = db.ExecuteQuery<int>(strAppend.ToString()).ToList().FirstOrDefault();
            return resultAll;
        }

        /// <summary>
        /// 报告平均时长
        /// 统计当天
        /// </summary>
        /// <returns></returns>
        public double GetReportTodayAvgTime(string HospitalID)
        {
            double tempDouble = 0;
            double resultAvg = 0;
            StringBuilder strAppend = new StringBuilder();
            string strDate = DateTime.Now.ToString("yyyy-MM-dd");
            strAppend.Append(@"SELECT RI.RequestDate,RI.DiagnoseDate FROM RequestInfo RI WHERE RI.HospitalId in(SELECT SubHospitalId FROM SubHospital WHERE PHospitalId='" + HospitalID + "') AND RI.Diagnosticstate='Diagnose' AND RI.RequestState=15 AND CONVERT(VARCHAR(20),RI.RequestDate,23)='" + strDate + "'");
            LogHelper.WriteInfoLog("报告平均时长GetReportTodayAvgTime：" + strAppend.ToString());
            var riList = db.ExecuteQuery<OutAvgTime>(strAppend.ToString()).ToList();
            if (riList.Count > 0)
            {
                foreach (OutAvgTime item in riList)
                {
                    if (item.RequestDate != null && item.DiagnoseDate != null)
                    {
                        double dic = Common.DateTimeDiff(Convert.ToDateTime(item.DiagnoseDate), Convert.ToDateTime(item.RequestDate));
                        if (tempDouble == 0)
                        {
                            tempDouble = dic;
                        }
                        else
                        {
                            tempDouble += dic;
                        }
                    }
                }
                if (tempDouble > 0)
                {
                    resultAvg = Math.Round(tempDouble / riList.Count, 2);
                }
            }
            return resultAvg;
        }

        /// <summary>
        /// 完成报告统计
        /// 表格统计
        /// </summary>
        public List<DeviceTypeModel> GetFinishedReportHistoryStatistics(string HospitalID)
        {
            List<DeviceTypeModel> list = new List<DeviceTypeModel>();
            string strCurrentDay = DateTime.Now.ToString("yyyy-MM-dd");
            string strCurrentMonth = DateTime.Now.ToString("yyyy-MM");
            string strYear = DateTime.Now.ToString("yyyy");
            string startYear = (DateTime.Now.AddMonths(-6).ToString("yyyy-MM-dd"));//DateTime.Now.ToString("yyyy") + "-" + (DateTime.Now.AddMonths(-6).ToString("MM-dd"))
            string endYear = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");//+ "-" + (DateTime.Now.AddMonths(1).ToString("MM-dd"))
            string strSql = string.Format(@"SELECT * FROM (
                                          SELECT ri.ExamineMethod AS 'ClassType',
                                          (SELECT COUNT(1) FROM RequestInfo r WHERE r.ExamineMethod=ri.ExamineMethod AND r.RequestState=15 AND r.HospitalId in(SELECT SubHospitalId FROM SubHospital WHERE PHospitalId='{0}') AND CONVERT(VARCHAR(100),DiagnoseDate,23)='{1}') AS 'CurrentTodayCounts',
                                          (SELECT COUNT(1) FROM RequestInfo r WHERE r.ExamineMethod=ri.ExamineMethod AND r.RequestState=15 AND r.HospitalId in(SELECT SubHospitalId FROM SubHospital WHERE PHospitalId='{2}') AND CONVERT(VARCHAR(7),DiagnoseDate,23)='{3}') AS 'CurrentMonthCounts',
                                          (SELECT COUNT(1) FROM RequestInfo r WHERE r.ExamineMethod=ri.ExamineMethod AND r.RequestState=15 AND r.HospitalId in(SELECT SubHospitalId FROM SubHospital WHERE PHospitalId='{4}') AND CONVERT(VARCHAR(7),DiagnoseDate,23)>='{5}' AND  CONVERT(VARCHAR(7),DiagnoseDate,23)<='{6}') AS 'HalfCounts',
                                          (SELECT COUNT(1) FROM RequestInfo r WHERE r.ExamineMethod=ri.ExamineMethod AND r.RequestState=15 AND r.HospitalId in(SELECT SubHospitalId FROM SubHospital WHERE PHospitalId='{7}') AND YEAR(DiagnoseDate)='{8}') AS 'YearCounts',
                                          (SELECT COUNT(1) FROM RequestInfo r WHERE r.ExamineMethod=ri.ExamineMethod AND r.RequestState=15 AND r.HospitalId in(SELECT SubHospitalId FROM SubHospital WHERE PHospitalId='{9}')) AS 'AllCounts'
                                          FROM RequestInfo ri GROUP BY ri.ExamineMethod) TT WHERE TT.YearCounts>1 AND TT.ClassType<>'' AND TT.ClassType IS NOT NULL", HospitalID, strCurrentDay, HospitalID, strCurrentMonth, HospitalID, startYear, endYear, HospitalID, strYear, HospitalID);
            LogHelper.WriteInfoLog("完成报告统计GetFinishedReportHistoryStatistics：" + strSql);
            list = db.ExecuteQuery<DeviceTypeModel>(strSql).ToList();
            return list;
        }

        /// <summary>
        /// 设备类型统计
        /// 饼状图统计
        /// 统计本月
        /// </summary>
        public List<OutDeviceTypeCounts> GetDeviceTypeStatistics(string HospitalID)
        {
            List<OutDeviceTypeCounts> list = new List<OutDeviceTypeCounts>();
            StringBuilder strAppend = new StringBuilder();
            string strDate = DateTime.Now.ToString("yyyy-MM");
            strAppend.Append(@"SELECT COUNT(1) AS Counts,ExamineMethod FROM RequestInfo RI WHERE RI.HospitalId in(SELECT SubHospitalId FROM SubHospital WHERE PHospitalId='" + HospitalID + "') AND RI.Diagnosticstate='Diagnose' AND RequestState>=11 AND RI.ExamineMethod<>'' AND RI.ExamineMethod IS NOT NULL AND  CONVERT(VARCHAR(7),RI.RequestDate,120)='" + strDate + "' GROUP BY ExamineMethod");
            LogHelper.WriteInfoLog("设备类型统计GetDeviceTypeStatistics：" + strAppend.ToString());
            list = db.ExecuteQuery<OutDeviceTypeCounts>(strAppend.ToString()).ToList();
            return list;
        }

        /// <summary>
        /// 分诊增长统计
        /// 柱形统计
        /// </summary>
        public List<OutReportCounts> GetIncreaseStatistics(string HospitalID)
        {
            List<OutReportCounts> list = new List<OutReportCounts>();
            string strDate = DateTime.Now.ToString("yyyy");
            string strSql = string.Format(
                            @"SELECT COUNT(1) AS Counts,DiagnoseDate FROM(
                            SELECT CONVERT(VARCHAR(7),DiagnoseDate,120) AS DiagnoseDate
                            FROM RequestInfo WHERE HospitalId in(SELECT SubHospitalId FROM SubHospital WHERE PHospitalId='{0}') 
                            AND Diagnosticstate='Diagnose' AND CONVERT(VARCHAR(4),DiagnoseDate,120)='{1}') AA GROUP BY DiagnoseDate", HospitalID, strDate);
            LogHelper.WriteInfoLog("分诊增长统计GetIncreaseStatistics：" + strSql);
            list = db.ExecuteQuery<OutReportCounts>(strSql).ToList();
            return list;
        }

        /// <summary>
        /// 完成报告统计
        /// 柱形统计
        /// </summary>
        public List<OutReportCounts> GetFinishedReportStatistics(string HospitalID)
        {
            List<OutReportCounts> list = new List<OutReportCounts>();
            string strDate = DateTime.Now.ToString("yyyy");
            string strSql = string.Format(
                            @"SELECT COUNT(1) AS Counts,DiagnoseDate FROM(
                            SELECT CONVERT(VARCHAR(7),DiagnoseDate,120) AS DiagnoseDate
                            FROM RequestInfo WHERE HospitalId in(SELECT SubHospitalId FROM SubHospital WHERE PHospitalId='{0}') 
                            AND RequestState=15 AND Diagnosticstate='Diagnose' AND CONVERT(VARCHAR(4),DiagnoseDate,120)='{1}') AA GROUP BY DiagnoseDate", HospitalID, strDate);
            LogHelper.WriteInfoLog("完成报告统计GetFinishedReportStatistics：" + strSql);
            list = db.ExecuteQuery<OutReportCounts>(strSql).ToList();
            return list;
        }
        #endregion

        #region 医院坐标管理
        /// <summary>
        /// 添加或更新坐标
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel InsertCoorDinate(S_CoorDinate param)
        {
            ExcuteModel result = new ExcuteModel();
            try
            {
                S_CoorDinate getModel = db.S_CoorDinate.FirstOrDefault(t => t.HospitalID == param.HospitalID);
                if (getModel == null)
                {
                    param.CreateDate = DateTime.Now;
                    param.UpdateDate = DateTime.MaxValue;
                    db.S_CoorDinate.InsertOnSubmit(param);
                    result.Msg = "添加成功！";
                }
                else
                {
                    if (param.Xcoordinate != null)
                    {
                        getModel.Xcoordinate = param.Xcoordinate;
                    }
                    if (param.Ycoordinate != null)
                    {
                        getModel.Ycoordinate = param.Ycoordinate;
                    }
                    getModel.UpdateDate = DateTime.Now;
                }
                db.SubmitChanges();
                result.Result = true;
            }
            catch (Exception ex)
            {
                result.Result = false;
                LogHelper.WriteErrorLog("InsertCoorDinate", "添加或更新坐标异常：" + ex.Message + "--堆栈异常：" + ex.StackTrace);
            }
            return result;
        }

        /// <summary>
        /// 删除坐标
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public int DeleteCoorDinate(string HospitalIdParam)
        {
            int result = 0;
            if (!string.IsNullOrEmpty(HospitalIdParam))
            {
                string strSQL = @"DELETE FROM S_CoorDinate WHERE HospitalID='" + HospitalIdParam + "'";
                result = db.ExecuteCommand(strSQL);
            }
            return result;
        }

        /// <summary>
        /// 获取所属中心医院的所有子医院坐标
        /// </summary>
        /// <param name="HospitalIdParam"></param>
        /// <returns></returns>
        public List<S_CoorDinate> GetCoorDinateList(string HospitalIdParam)
        {
            List<S_CoorDinate> list = new List<S_CoorDinate>();
            string strTemp = @"SELECT * FROM S_CoorDinate WHERE HospitalID IN (SELECT SubHospitalId FROM SubHospital WHERE PHospitalId='{0}')
                               UNION
                               SELECT * FROM S_CoorDinate WHERE HospitalID='{1}'";
            string strSQL = string.Format(strTemp, HospitalIdParam, HospitalIdParam);
            list = db.ExecuteQuery<S_CoorDinate>(strSQL).ToList();
            return list;
        }

        /// <summary>
        /// 获取单个医院坐标信息
        /// </summary>
        /// <param name="HospitalIdParam"></param>
        /// <returns></returns>
        public S_CoorDinate GetCoorDinateInfo(string HospitalIdParam)
        {
            S_CoorDinate model = new S_CoorDinate();
            if (!string.IsNullOrEmpty(HospitalIdParam))
            {
                model = db.S_CoorDinate.Where(o => o.HospitalID == HospitalIdParam).FirstOrDefault();
            }
            return model;
        }
        #endregion

        #region 删除用户组
        public ExcuteModel DeleteUserGroupInfo(string userID)
        {
            ExcuteModel model = new ExcuteModel();
            int result = 0;
            if (!string.IsNullOrEmpty(userID))
            {
                string strSQL = @"DELETE FROM UserGroup WHERE UserId='" + userID + "'";
                result = db.ExecuteCommand(strSQL);
            }
            if (result == 0)
            {
                model.Result = false;
            }
            else
            {
                model.Result = true;
            }
            return model;
        }

        public Groups GetGroupsById(string groupId)
        {
            Groups model = new Groups();
            if (!string.IsNullOrEmpty(groupId))
            {
                model = db.Groups.Where(o => o.GroupId == groupId).FirstOrDefault();
            }
            return model;
        }

        #endregion
        #region 收费端是否多次申请判断
        public ExcuteModel IsExistedPatientRequested(string patientId, string studyInstanceUid)
        {
            ExcuteModel em = new ExcuteModel();
            RequestInfo requestInfo = db.RequestInfo.Where(r => r.PatientId == patientId && r.StudyInstanceUid == studyInstanceUid).FirstOrDefault();
            if (requestInfo != null && !string.IsNullOrEmpty(requestInfo.RequestId))
            {
                em.RetValue = requestInfo.RequestState.ToString();
                em.Result = true;
            }
            else
            {
                em.RetValue = "0";
                em.Result = false;
            }
            return em;
        }
        #endregion

        #region 工作量统计
        /// <summary>
        /// 统计报告数
        /// </summary>
        /// <returns></returns>
        public List<StatisticsModel> AddUpReport(DateTime startTime, DateTime endTime, List<string> doctorNames, string userName, string userId)
        {
            List<StatisticsModel> List = new List<StatisticsModel>();
            string strSQL = string.Empty;
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.Append("WITH ct as (");
                sb.Append("select UserName as DoctorId,name as DoctorName,");
                sb.Append("(select COUNT(*) from RequestInfo r where 1=1");
                if (!string.IsNullOrEmpty(startTime.ToString()) && !string.IsNullOrEmpty(endTime.ToString()))
                {
                    sb.AppendFormat(" and DiagnoseDate >='{0}' and DiagnoseDate < '{1}'", startTime, endTime);
                }
                sb.Append(" and r.diagnoseDoctor=UserName and (requeststate = 15 or requeststate = 16)) as InitialReportNumber,");
                sb.Append("(SELECT	count(*) FROM RequestInfo r INNER JOIN RefusedRecord rr on r.requestId=rr.requestid WHERE 1 = 1");
                if (!string.IsNullOrEmpty(startTime.ToString()) && !string.IsNullOrEmpty(endTime.ToString()))
                {
                    sb.AppendFormat(" and DiagnoseDate >='{0}' and DiagnoseDate < '{1}'", startTime, endTime);
                }
                sb.Append(" AND rr.refusedperson = UserName	AND (requeststate = 15 or requeststate = 16) and ReviewStatus=1) as AuditReportNumber");
                sb.Append(" from Sys_Users d where 1=1 AND (DiagnosisType='Diagnose' or DiagnosisType = 'Consultation' or DiagnosisType = 'Multi')");
                if (doctorNames != null && doctorNames.Count > 0)
                {
                    string strDept = string.Empty;
                    string strUserId = string.Empty;
                    foreach (string key in doctorNames)
                    {
                        strUserId += "'" + key + "',";
                    }
                    strUserId = strUserId.Substring(0, strUserId.LastIndexOf(","));
                    sb.AppendFormat(" and UserId in ({0})", strUserId);

                }
                sb.Append(" )  SELECT * FROM ct WHERE 1 = 1 ");
                if (!string.IsNullOrEmpty(userName))
                {
                    sb.AppendFormat(" AND DoctorName LIKE '%{0}%'", userName);
                }
                if (!string.IsNullOrEmpty(userId))
                {
                    sb.AppendFormat(" AND DoctorId LIKE '%{0}%'", userId);
                }
                sb.AppendFormat(" ORDER BY InitialReportNumber desc");
                strSQL = sb.ToString();
                List = db.ExecuteQuery<StatisticsModel>(strSQL).ToList();
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("工作量统计异常AddUpReport：" + strSQL, ex.Message);
            }
            return List;
        }

        public List<StatisticsModel> AddUpExaminePosition(DateTime startTime, DateTime endTime, string examineName, string subExamineName)
        {
            List<StatisticsModel> list = new List<StatisticsModel>();
            string strSQL = string.Empty;
            StringBuilder sb = new StringBuilder();
            try
            {
                strSQL = "select * from (";
                strSQL += "select CombineExamineName,count(*) as ExamineNumber from";
                strSQL += @"(select a.RequestState as RequestState,a.RequestDate as RequestDate,CombineExamineName=substring(a.ExaminePart,b.number,charindex('/',a.ExaminePart+'/',b.number)-b.number) from RequestInfo a join master..spt_values b on b.type='P' where 1=1";
                if (!string.IsNullOrEmpty(startTime.ToString()))
                {
                    strSQL += string.Format(" and a.RequestDate>='{0}'", startTime);
                }
                if (!string.IsNullOrEmpty(endTime.ToString()))
                {
                    strSQL += string.Format(" and a.RequestDate<='{0}'", endTime);
                }
                if (!string.IsNullOrEmpty(examineName) && !string.IsNullOrEmpty(subExamineName))
                {
                    strSQL += string.Format(" and a.CombineExamineName", examineName + "_" + subExamineName);
                }
                strSQL += " and a.RequestState=15 and charindex('/','/'+a.ExaminePart,b.number)=b.number) c";
                strSQL += " where 1=1 group by c.CombineExamineName)as h";
                list = db.ExecuteQuery<StatisticsModel>(strSQL).ToList();
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("工作量统计检查部位异常AddUpExaminePosition：" + strSQL, ex.Message);
            }
            return list;
        }


        #endregion

        #region 统计

        /// <summary>
        /// 统计申请数据
        /// </summary>
        /// <param name="dtStart">开始时间</param>
        /// <param name="dtEnd">结束时间</param>
        /// <param name="hospitalId">机构ID</param>
        /// <param name="requestDoctor">申请医生</param>
        /// <returns></returns>
        public List<OutReportRequestInfo> GetRequestDataList(DateTime dtStart, DateTime dtEnd, List<string> hospitalIds, List<string> requestDoctors, string doctorName, string doctorId)
        {
            List<OutReportRequestInfo> list = new List<OutReportRequestInfo>();
            StringBuilder strTemp = new StringBuilder();
            strTemp.AppendFormat(@"with cr as( SELECT (SELECT DeptDisplayName FROM Sys_Dept WHERE DeptId= ri.HospitalId) AS HospitalName,
                               (SELECT Name FROM Sys_Users WHERE UserName=ri.RequestDoctor) AS DoctorName,ri.ExamineMethod, COUNT (ri.ExamineMethod) AS DiagnoseCount, 
                               (SELECT COUNT (*) FROM RequestInfo rii WHERE rii.HospitalId = ri.HospitalId AND rii.RequestDoctor = ri.RequestDoctor AND (rii.RequestState=15 or  rii.RequestState=16) AND rii.ReviewStatus=1 
                               AND (CONVERT(VARCHAR(100), rii.RequestDate,120) >= '{0}' AND CONVERT(VARCHAR(100), rii.RequestDate,120) <= '{1}')) AS TotalCount,RequestDoctor
                               FROM RequestInfo ri 
                               WHERE (ri.RequestState = 15 or ri.RequestState = 16) and ri.ReviewStatus=1 AND (CONVERT(VARCHAR(100), RequestDate,120) >= '{0}' AND CONVERT(VARCHAR(100), RequestDate,120) <= '{1}')", dtStart.ToString("yyyy-MM-dd 00:00:00"), dtEnd.ToString("yyyy-MM-dd 23:59:59"));
            var hospitalId = "";
            if (hospitalIds != null && hospitalIds.Count > 0)
            {
                foreach (var i in hospitalIds)
                {
                    hospitalId += "'" + i + "',";
                }
                hospitalId = hospitalId.TrimEnd(',');
            }
            if (!string.IsNullOrEmpty(hospitalId))
            {
                strTemp.AppendFormat(" AND ri.HospitalId in({0})", hospitalId);
            }
            var requestDoctor = "";
            if (requestDoctors != null && requestDoctors.Count > 0)
            {
                foreach (var j in requestDoctors)
                {
                    requestDoctor += "'" + j + "',";
                }
                requestDoctor = requestDoctor.TrimEnd(',');
            }
            if (!string.IsNullOrEmpty(requestDoctor))
            {
                strTemp.AppendFormat(" AND ri.RequestDoctor in ({0}) ", requestDoctor);
            }
            strTemp.Append(" GROUP BY ri.HospitalId, ri.RequestDoctor, ri.ExamineMethod ) select * from cr where 1=1 ");

            if (!string.IsNullOrEmpty(doctorName))
            {
                strTemp.AppendFormat("and DoctorName LIKE '%{0}%'", doctorName);
            }
            if (!string.IsNullOrEmpty(doctorId))
            {
                strTemp.AppendFormat("and RequestDoctor LIKE '%{0}%'", doctorId);
            }
            string strSQL = strTemp.ToString();
            LogHelper.WriteInfoLog(" 统计申请数据GetRequestDataList：" + strSQL);
            list = db.ExecuteQuery<OutReportRequestInfo>(strSQL).ToList();
            return list;
        }

        /// <summary>
        /// 统计申请数据
        /// </summary>
        /// <param name="dtStart">开始时间</param>
        /// <param name="dtEnd">结束时间</param>
        /// <param name="hospitalId">机构ID</param>
        /// <param name="requestDoctor">申请医生</param>
        /// <returns></returns>
        public List<OutExamineMethodCount> GetExamineMethodCountList(DateTime dtStart, DateTime dtEnd, string hospitalId)
        {
            List<OutExamineMethodCount> list = new List<OutExamineMethodCount>();
            StringBuilder strTemp = new StringBuilder();
            strTemp.AppendFormat(@"WITH cr AS( SELECT ri.ExamineMethod as ExamineMethodName ,COUNT (ri.ExamineMethod) AS ExamineMethodCount
            FROM RequestInfo ri INNER JOIN RefusedRecord rr on ri.RequestId = rr.RequestId WHERE (ri.RequestState = 15 or ri.RequestState = 16) and ri.ReviewStatus=1 and ri.Diagnosticstate in ('Diagnose','Multi')
            AND (HospitalId in (select SubHospitalId from SubHospital where PHospitalId='{0}') OR HospitalId='{0}')
            AND ( CONVERT ( VARCHAR (100),rr.RefusedDate,120) >= '{1}'	AND CONVERT (VARCHAR (100),	rr.RefusedDate,120) <= '{2}')
            GROUP BY ri.ExamineMethod) select * from cr ORDER BY ExamineMethodCount DESC", hospitalId, dtStart.ToString("yyyy-MM-dd 00:00:00"), dtEnd.ToString("yyyy-MM-dd 23:59:59"));
            string strSQL = strTemp.ToString();
            list = db.ExecuteQuery<OutExamineMethodCount>(strSQL).ToList();
            return list;
        }


        #endregion

        #region 检查项目维护
        /// <summary>
        /// 保存检查项目
        /// </summary>
        /// <param name="modelParam"></param>
        /// <returns></returns>
        public ExcuteModel SaveExamineProject(ExamineProject modelParam)
        {
            ExcuteModel retModel = new ExcuteModel();
            try
            {
                if (modelParam != null)
                {
                    ExamineProject obj = db.ExamineProject.Where(o => o.ID == modelParam.ID).FirstOrDefault();
                    if (obj != null)
                    {
                        if (!string.IsNullOrEmpty(modelParam.EXAM_B))
                        {
                            obj.EXAM_B = modelParam.EXAM_B;
                        }
                        if (!string.IsNullOrEmpty(modelParam.EXAM_G))
                        {
                            obj.EXAM_G = modelParam.EXAM_G;
                        }
                        if (!string.IsNullOrEmpty(modelParam.EXAM_T))
                        {
                            obj.EXAM_T = modelParam.EXAM_T;
                        }
                        if (!string.IsNullOrEmpty(modelParam.ModalityType))
                        {
                            obj.ModalityType = modelParam.ModalityType;
                        }
                        if (!string.IsNullOrEmpty(modelParam.SpellingName))
                        {
                            obj.SpellingName = modelParam.SpellingName;
                        }
                        if (!string.IsNullOrEmpty(modelParam.IsViable.ToString()))
                        {
                            obj.IsViable = modelParam.IsViable;
                        }
                        if (!string.IsNullOrEmpty(modelParam.Speci))
                        {
                            obj.Speci = modelParam.Speci;
                        }
                        if (modelParam.Exposure != null)
                        {
                            obj.Exposure = modelParam.Exposure;
                        }
                        if (modelParam.FilmCount != null)
                        {
                            obj.FilmCount = modelParam.FilmCount;
                        }
                        if (modelParam.DeviceID != null)
                        {
                            obj.DeviceID = modelParam.DeviceID;
                        }
                        obj.Sort = modelParam.Sort;
                    }
                    else
                    {
                        modelParam.CreateDate = DateTime.Now;
                        db.ExamineProject.InsertOnSubmit(modelParam);
                    }
                    db.SubmitChanges();
                    retModel.Result = true;
                }
                else
                {
                    retModel.Result = true;
                }
            }
            catch (Exception exp)
            {
                retModel.Result = false;
                retModel.Msg = exp.ToString();
            }
            return retModel;
        }

        /// <summary>
        /// 获取检查项目列表
        /// </summary>
        /// <param name="modelParam"></param>
        /// <returns></returns>
        public List<OutExamineProject> GetExamineProjectList(OutExamineProject model)
        {
            List<OutExamineProject> list = new List<OutExamineProject>();
            string StrSql = string.Empty;
            string Condition = string.Empty;
            if (!string.IsNullOrEmpty(model.EXAM_G))
            {
                Condition = @" AND EXAM_G='" + model.EXAM_G + "'";
            }
            if (!string.IsNullOrEmpty(model.ModalityType))
            {
                Condition += @" AND ModalityType='" + model.ModalityType + "'";
            }
            if (!string.IsNullOrEmpty(model.EXAM_B))
            {
                Condition += @" AND EXAM_B IN(" + model.EXAM_B + ")";
            }
            if (!string.IsNullOrEmpty(model.EXAM_T))
            {
                Condition += @" AND EXAM_T ='" + model.EXAM_T + "'";
            }
            if (model.DeviceID != null)
            {
                Condition += @" AND DeviceID=" + model.DeviceID;
            }
            StrSql = @"SELECT * FROM ExamineProject WHERE IsViable='1' " + Condition;
            list = db.ExecuteQuery<OutExamineProject>(StrSql).ToList();
            return list;
        }

        /// <summary>
        /// 获取排序最大值
        /// </summary>
        /// <returns></returns>
        public int? GetExamineProjectMaxSort()
        {
            int? result = 0;
            if (db.ExamineProject.Count() > 0)
            {
                result = db.ExamineProject.Max(o => o.Sort);
            }
            return result;
        }
        #endregion

        #region 检查部位维护
        /// <summary>
        /// 保存检查部位
        /// </summary>
        /// <param name="modelParam"></param>
        /// <returns></returns>
        public ExcuteModel SaveExaminePart(ExaminePart modelParam)
        {
            ExcuteModel retModel = new ExcuteModel();
            try
            {
                if (modelParam != null)
                {
                    ExaminePart obj = db.ExaminePart.Where(o => o.ID == modelParam.ID).FirstOrDefault();
                    if (obj != null)
                    {
                        if (!string.IsNullOrEmpty(modelParam.ExamineType))
                        {
                            obj.ExamineType = modelParam.ExamineType;
                        }
                        if (!string.IsNullOrEmpty(modelParam.PartName))
                        {
                            obj.PartName = modelParam.PartName;
                        }
                        if (!string.IsNullOrEmpty(modelParam.Description))
                        {
                            obj.Description = modelParam.Description;
                        }
                        if (modelParam.IsVisible != null)
                        {
                            obj.IsVisible = modelParam.IsVisible;
                        }
                        obj.Sort = modelParam.Sort;
                    }
                    else
                    {
                        db.ExaminePart.InsertOnSubmit(modelParam);
                    }
                    db.SubmitChanges();
                    retModel.Result = true;
                }
                else
                {
                    retModel.Result = true;
                }
            }
            catch (Exception exp)
            {
                retModel.Result = false;
                retModel.Msg = exp.ToString();
            }
            return retModel;
        }

        /// <summary>
        /// 获取检查部位列表
        /// </summary>
        /// <param name="modelParam"></param>
        /// <returns></returns>
        public List<ExaminePart> GetExaminePartList(ExaminePart modelParam)
        {
            List<ExaminePart> list = new List<ExaminePart>();
            //获取部位列表
            if (!string.IsNullOrEmpty(modelParam.ExamineType))
            {
                list = db.ExaminePart.Where(o => o.ExamineType == modelParam.ExamineType && o.IsVisible == '1').ToList();
            }
            else
            {
                list = db.ExaminePart.Where(o => o.IsVisible == '1').ToList();
            }
            return list;
        }

        /// <summary>
        /// 获取排序最大值
        /// </summary>
        /// <returns></returns>
        public int? GetExaminePartMaxSort()
        {
            int? result = 0;
            if (db.ExaminePart.Count() > 0)
            {
                result = db.ExaminePart.Max(o => o.Sort);
            }
            return result;
        }
        #endregion

        #region 申请驳回记录表
        public ExcuteModel SaveRequestRebutRecord(RequestRebutRecord modelParam)
        {
            ExcuteModel em = new ExcuteModel();
            try
            {
                db.RequestRebutRecord.InsertOnSubmit(modelParam);
                db.SubmitChanges();
                em.Result = true;
            }
            catch (Exception ex)
            {
                em.Result = false;
                em.Msg = ex.Message;
            }
            return em;
        }

        public List<RequestRebutRecord> GetRequestRebutRecordList(RequestRebutRecord modelParam)
        {
            List<RequestRebutRecord> list = new List<RequestRebutRecord>();
            list = db.RequestRebutRecord.Where(o => o.RequestId == modelParam.RequestId || o.RebutPerson == modelParam.RebutPerson).ToList();
            return list;
        }
        #endregion

        #region 获取机房数据
        public List<OutRqeustInfo> GetMachinePatientData(InMachineBase param)
        {
            List<OutRqeustInfo> List = new List<OutRqeustInfo>();
            try
            {
                string StrSql = @"SELECT RI.PatientId,RI.RequestId,P.CPatientName,P.PatientName,P.PatientAge,P.PatientSex,CONVERT(varchar(100),P.PatientBirth, 23) AS PatientBirth,SD.DeviceName,
                                  SD.MachineRoom,RI.PatientType,RI.ExamineType,RI.UrgentType,P.OutPatientNum,P.RegisterNum,RI.ExamineDate,
                                  '' AS RecordPerson,'' AS ShootTechnician,'' AS ShootTechnician,'' AS PositionTechnician, CONVERT(varchar(100),RI.RequestDate,120) AS RequestDate,
                                  P.PatientMobile,DE.DepartmentName,RI.UploadWay,RI.RequestState,RI.ExaminePart,RI.ExamineMethod,
                                  RS.Description AS RequestStateDescription,RI.DeviceId,FR.UpLoadFlag
                                  FROM RequestInfo RI 
                                  LEFT JOIN PatientInfo P ON RI.PatientId=P.PatientId
                                  LEFT JOIN Sys_Device SD ON SD.DeviceId=RI.DeviceId
                                  LEFT JOIN Sys_Department DE ON DE.DepartmentId=SD.DepartmentId
                                  LEFT JOIN  FileUpDownRecord FR ON FR.RequestID=RI.RequestId
                                  LEFT JOIN (SELECT * FROM Sys_Dictionary WHERE [Type]='RequestState') AS RS ON RS.Code=RI.RequestState
                                  WHERE 1=1";
                if (!string.IsNullOrEmpty(param.PatientId))
                {
                    StrSql += " AND RI.PatientId='" + param.PatientId + "'";
                }
                if (!string.IsNullOrEmpty(param.OutPatientNum))
                {
                    StrSql += " AND P.OutPatientNum='" + param.OutPatientNum + "'";
                }
                if (!string.IsNullOrEmpty(param.RegisterNum))
                {
                    StrSql += " AND P.RegisterNum='" + param.RegisterNum + "'";
                }
                if (!string.IsNullOrEmpty(param.CPatientName))
                {
                    StrSql += " AND P.CPatientName='" + param.CPatientName + "'";
                }
                if (!string.IsNullOrEmpty(param.PatientName))
                {
                    StrSql += " AND P.PatientName='" + param.PatientName + "'";
                }
                if (!string.IsNullOrEmpty(param.PatientIdentityNum))
                {
                    StrSql += " AND P.PatientIdentityNum='" + param.PatientIdentityNum + "'";
                }
                if (!string.IsNullOrEmpty(param.ReviewStatus))
                {
                    StrSql += " AND RI.ReviewStatus='" + param.ReviewStatus + "'";
                }
                if (!string.IsNullOrEmpty(param.StartDate))
                {
                    StrSql += " AND CONVERT(varchar(100),RI.ExamineDate, 23)>='" + param.StartDate + "'";
                }
                if (!string.IsNullOrEmpty(param.EndDate))
                {
                    StrSql += " AND CONVERT(varchar(100),RI.ExamineDate, 23)<='" + param.EndDate + "'";
                }
                if (!string.IsNullOrEmpty(param.DeviceID))
                {
                    string[] arrDeviceId = param.DeviceID.Split(',');
                    string strDeviceID = "";
                    for (int k = 0; k < arrDeviceId.Length; k++)
                    {
                        if (string.IsNullOrEmpty(strDeviceID))
                        {
                            strDeviceID = "'" + arrDeviceId[k] + "'";
                        }
                        else
                        {
                            strDeviceID += ",'" + arrDeviceId[k] + "'";
                        }
                    }
                    StrSql += " AND RI.DeviceId IN(" + strDeviceID + ")";
                }
                if (param.ReviewStatus == "0")
                {
                    StrSql += " ORDER BY RI.RequestDate ASC";
                }
                else
                {
                    StrSql += " ORDER BY RI.DiagnoseDate ASC";
                }
                StrSql = ReplaceInstance(StrSql, "GetMachinePatientData::");
                List = db.ExecuteQuery<OutRqeustInfo>(StrSql).ToList();
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("GetMachinePatientData", "错误信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
            }
            return List;
        }
        #endregion
    }
}