﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using System.Xml;
using WcfService.DB_Server;
using WcfService.DB_Server.SystemManage;
using WcfService.Models;

namespace WcfService
{
    // 注意: 使用“重构”菜单上的“重命名”命令，可以同时更改代码、svc 和配置文件中的类名“SystemManage”。
    // 注意: 为了启动 WCF 测试客户端以测试此服务，请在解决方案资源管理器中选择 SystemManage.svc 或 SystemManage.svc.cs，然后开始调试。
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class SystemManage : ISystemManage
    {
        public void DoWork()
        {
        }

        WcfService.DB_Server.SystemManage.SystemManageContext DAL = new DB_Server.SystemManage.SystemManageContext();

        #region   目录菜单维护
        public List<string> GetDiagnoseTip(string category)
        {
            return DAL.GetDiagnoseTip(category);
        }

        public string insertDiagnoseType(string DiagnoseType_name)
        {
            return DAL.insertDiagnoseType(DiagnoseType_name);
        }


        public ExcuteModel SaveDiagnoseType_Name(DiagnoseType model)
        {
            return DAL.SaveDiagnoseType_Name(model);
        }
        #endregion

        #region 用户管理

        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <param name="Type">0：获取全部用户，1:根据用户名获取单个用户2：获取角色RoleId下的用户，3：获取角色未添加的用户</param>
        /// <param name="UserName"></param>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        public List<Sys_Users> GetUsers(int Type, string UserName, string RoleId, string DeptId)
        {

            return DAL.GetUsers(Type, UserName, RoleId, DeptId);
        }

        /// <summary>
        /// save model Param 1: new Param 2: update
        /// </summary>
        /// <param name="model"></param>
        /// <returns>bool true,false</returns>
        public ExcuteModel SaveUsers(Sys_Users model)
        {
            return DAL.SaveUsers(model);
        }


        public ExcuteModel SaveUsers_Group(Sys_Users model, List<string> Group_id)
        {
            return DAL.SaveUsers_Group(model, Group_id);
        }

        /// <summary>
        /// 修改用户状态
        /// </summary>
        /// <param name="UserId">数组</param>
        /// <param name="State">状态，1正常，2禁用，3，登录锁定</param>
        /// <returns></returns>
        public ExcuteModel SetEnableUser(string[] UserId, int State)
        {
            return DAL.SetEnableUser(UserId, State);
        }

        /// <summary>
        /// 根据登录名获取用户对象
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public Sys_Users GetUserByUserName(string UserName)
        {
            return DAL.GetUserByUserName(UserName);
        }

        /// <summary>
        /// 根据用户id获取用户对象
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public Sys_Users GetUserByUserModel(string UserId)
        {
            return DAL.GetUserByUserModel(UserId);
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="OldPwd"></param>
        /// <param name="NewPwd"></param>
        /// <returns></returns>
        public ExcuteModel ResetPwd(string UserName, string OldPwd, string NewPwd)
        {
            return DAL.ResetPwd(UserName, OldPwd, NewPwd);
        }

        /// <summary>
        /// 重置密码
        /// </summary>
        /// <returns></returns>
        public ExcuteModel ResetPwdInfo(string UserID, string NewPwd)
        {
            return DAL.ResetPwdInfo(UserID, NewPwd);
        }

        /// <summary>
        /// 更新用户操作状态
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateState(Sys_Users model)
        {
            return DAL.UpdateState(model);
        }

        /// <summary>
        /// 更新机构信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateSysUserDept(Sys_Users model)
        {
            return DAL.UpdateSysUserDept(model);
        }
        #endregion

        #region 角色管理

        /// <summary>
        /// 获取角色列表
        /// </summary>
        /// <param name="RoleId"></param>
        /// <param name="RoleType"></param>
        /// <returns></returns>
        public List<Sys_Roles> GetRoles(string RoleId, int RoleType)
        {
            return DAL.GetRoles(RoleId, RoleType);
        }

        /// <summary>
        /// save model RoleId -1: new else update
        /// </summary>
        /// <param name="model"></param>
        /// <returns>bool true,false</returns>
        public ExcuteModel SaveRoles(Sys_Roles model)
        {
            return DAL.SaveRoles(model);
        }

        /// <summary>
        /// 调整角色状态
        /// </summary>
        /// <param name="RoleId"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public ExcuteModel SetEnableRole(string[] RoleId, int state)
        {
            return DAL.SetEnableRole(RoleId, state);
        }
        #endregion

        #region 用户角色
        /// <summary>
        /// 给用户划分角色
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel AddUserToRole(Sys_RoleGroup model)
        {
            return DAL.AddUserToRole(model);
        }

        public ExcuteModel DeleteUserFromRole(Sys_RoleGroup model)
        {
            return DAL.DeleteUserFromRole(model);
        }
        /// <summary>
        /// 根据用户名获取所在的角色
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public List<Sys_RoleGroup> GetRoleIdByUserName(string UserName)
        {
            return DAL.GetRoleIdByUserName(UserName);
        }
        #endregion

        #region 菜单角色
        public ExcuteModel AddMenuToRole(Sys_MenuGroup model)
        {
            return DAL.AddMenuToRole(model);
        }

        public ExcuteModel DeleteMenuFromRole(Sys_MenuGroup model)
        {
            return DAL.DeleteMenuFromRole(model);
        }
        #endregion
        //dt
        #region 权限应用
        public List<Sys_Menu> GetMenuListByUserName(string StrUserName)
        {
            return DAL.GetMenuListByUserName(StrUserName);
        }

        /// <summary>
        /// 获取根菜单list
        /// </summary>
        /// <param name="StrUserName"></param>
        /// <returns></returns>
        public List<Sys_Menu> GetMenuRootByUserName(string StrUserName)
        {
            return DAL.GetMenuRootByUserName(StrUserName);
        }

        /// <summary>
        /// 获取根菜单下的菜单项list
        /// </summary>
        /// <param name="StrUserName"></param>
        /// <returns></returns>
        public List<Sys_Menu> GetMenuListByUserNameAndParentId(string StrUserName, int ParentId)
        {
            return DAL.GetMenuListByUserNameAndParentId(StrUserName, ParentId);
        }
        #endregion

        #region 医生相关
        /// <summary>
        /// 获取医生列表 UserType 1：系统用户，2医生，3管理员，4测试用户
        /// </summary>
        /// <returns></returns>
        public List<Sys_Users> GetDoctorList(string strDoctorName)
        {
            return DAL.GetDoctorList(strDoctorName);
        }
        #endregion

        #region 菜单管理
        /// <summary>
        /// 获取菜单表记录 
        /// </summary>
        /// <param name="Type">0：获取全部菜单，1:根据MenuId获取单个菜单2：获取角色RoleId下的菜单，3：获取角色未添加的菜单</param>
        /// <param name="MenuId"> MenuId为空获取全部，不为空则获取一条记录</param>
        /// <returns>泛型列表</returns>
        public List<Sys_Menu> GetMenu(int Type, string MenuId, string RoleId)
        {
            return DAL.GetMenu(Type, MenuId, RoleId);
        }

        /// <summary>
        /// save model Param -1: new else 2 update
        /// </summary>
        /// <param name="model"></param>
        /// <returns>bool true,false</returns>
        public ExcuteModel SaveMenu(Sys_Menu model)
        {
            return DAL.SaveMenu(model);
        }

        /// <summary>
        /// 修订菜单状态
        /// </summary>
        /// <param name="MenuIds"></param>
        /// <param name="state">1启用，2禁用，3删除</param>
        /// <returns></returns>
        public ExcuteModel SetEnableMenu(string[] MenuIds, int state)
        {
            return DAL.SetEnableMenu(MenuIds, state);
        }
        #endregion

        #region 用户登录
        public ExcuteModel CheckLogin(InCheckLoginModel model)
        {
            return DAL.CheckLogin(model);
        }

        public ExcuteModel LoginOutLog(Sys_UserLoginLog model)
        {
            return DAL.LoginOutLog(model);
        }
        /// <summary>
        /// 获取登录日志
        /// </summary>
        /// <returns></returns>
        public List<Sys_UserLoginLog> GetLoginLogList()
        {
            return DAL.GetLoginLogList();
        }

        public List<Sys_UserLoginLog> GetNewLoginLogList(int pageSize, int pageIndex, out int totalRecord, out int totalPage)
        {
            return DAL.GetNewLoginLogList(pageSize, pageIndex,out totalRecord,out totalPage);
        }

        public List<Sys_UserLoginLog> GetLoginLogListByName(string name)
        {
            return DAL.GetLoginLogListByName(name);
        }
        #endregion

        #region 服务项目



        /// <summary>
        /// 根据站点id获取站点的服务项目
        /// </summary>
        /// <param name="SiteId"></param>
        /// <returns></returns>
        public List<Sys_DiagnoseService> GetDiagnoseServiceListBySiteId(string SiteId)
        {
            return DAL.GetDiagnoseServiceListBySiteId(SiteId);
        }
        /// <summary>
        /// 根据服务项目id获取服务项目
        /// </summary>
        /// <param name="ServiceItemId"></param>
        /// <returns></returns>
        public Sys_DiagnoseService GetDiagnoseServiceItemByServiceId(string ServiceId)
        {
            return DAL.GetDiagnoseServiceItemByServiceId(ServiceId);
        }

        /// <summary>
        /// 获取所有的服务项目
        /// </summary>
        /// <returns></returns>
        public List<Sys_DiagnoseService> GetDiagnoseServiceAllList()
        {
            return DAL.GetDiagnoseServiceAllList();
        }

        /// <summary>
        /// 添加或修改服务项目 Param 1new，2update
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveDiagnoseService(Sys_DiagnoseService model)
        {
            return DAL.SaveDiagnoseService(model);
        }

        /// <summary>
        /// 启用1，禁用2服务项目
        /// </summary>
        /// <param name="ServiceId"></param>
        /// <param name="State"></param>
        /// <returns></returns>
        public ExcuteModel SetEnableServiceItem(string ServiceId, int State)
        {
            return DAL.SetEnableServiceItem(ServiceId, State);
        }

        /// <summary>
        /// 添加套餐
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveDiagnoseServiceSub(WcfService.DB_Server.SystemManage.Sys_DiagnoseServiceSub model)
        {
            return DAL.SaveDiagnoseServiceSub(model);
        }

        /// <summary>
        /// 获取套餐列表
        /// </summary>
        /// <returns></returns>
        public List<WcfService.DB_Server.SystemManage.Sys_DiagnoseServiceSub> GetDiagnoseServiceSubList()
        {
            return DAL.GetDiagnoseServiceSubList();
        }
        #endregion

        #region 数据字典

        /// <summary>
        /// 添加字典项
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveDictionary(Sys_Dictionary model)
        {
            return DAL.SaveDictionary(model);
        }
        /// <summary>
        /// 获取字典列表
        /// </summary>
        /// <returns></returns>
        public List<Sys_Dictionary> GetDictionaryListByType(string Type)
        {
            return DAL.GetDictionaryListByType(Type);
        }

        /// <summary>
        /// 根据代码获取字典对象
        /// </summary>
        /// <param name="Code"></param>
        /// <returns></returns>
        public Sys_Dictionary GetDictionaryDescriptionByTypeCode(string Type, string Code)
        {
            return DAL.GetDictionaryDescriptionByTypeCode(Type, Code);
        }

        /// <summary>
        /// 根据唯一号获取字典对象
        /// </summary>
        /// <param name="GUID"></param>
        /// <returns></returns>
        public Sys_Dictionary GetDictionaryByTypeGUID(string GUID)
        {
            return DAL.GetDictionaryByTypeGUID(GUID);
        }
        #endregion

        #region 系统配置
        /// <summary>
        /// 获取系统配置
        /// </summary>
        /// <returns></returns>
        public Sys_Settings GetSysValueByKey(string Key)
        {
            return DAL.GetSysValueByKey(Key);
        }

        public List<Sys_Settings> GetSysList()
        {
            return DAL.GetSysList();
        }

        public List<Sys_Settings> GetSysSettingList(string key)
        {
            return DAL.GetSysSettingList(key);
        }

        public List<Sys_Settings> GetSysSettingsListBySysType(string sysType)
        {
            return DAL.GetSysSettingsListBySysType(sysType);
        }

        public ExcuteModel UpdateSysSetting(Sys_Settings model)
        {
            return DAL.UpdateSysSetting(model);
        }
        #endregion

        #region 系统初始化
        /// <summary>
        /// 获取系统初始化的文件夹
        /// </summary>
        /// <param name="SysCode"></param>
        /// <returns></returns>
        public List<Sys_Settings> GetInitFolder(string SysCode)
        {
            return DAL.GetInitFolder(SysCode);
        }
        #endregion

        #region 部门
        public Sys_Dept GetDetpByDetpId(string DeptId)
        {
            return DAL.GetDetpByDetpId(DeptId);
        }

        public List<Sys_Dept> GetAllDetps()
        {
            return DAL.GetAllDetps();
        }
        public List<Sys_Dept> GetAllDetps_All(string DeptId)
        {
            return DAL.GetAllDetps_All(DeptId);
        }

        //GetAllDetps_All
        public ExcuteModel del_SubHospital(SubHospital model)
        {
            return DAL.del_SubHospital(model);
        }


        public List<AllDept> GetAllDetpsAndSub()
        {
            return DAL.GetAllDetpsAndSub();
        }

        public List<AllDept> GetAllDetpsRelevance(string DeptId)
        {
            return DAL.GetAllDetpsRelevance(DeptId);
        }

        public ExcuteModel SaveDept(Sys_Dept model)
        {
            return DAL.SaveDept(model);
        }
        #endregion

        public bool SystemConnectionSate()
        {
            return true;
        }

        /// <summary>
        /// 添加用户帐号
        /// </summary>
        /// <param name="objCustAccount"></param>
        /// <returns></returns>
        public ExcuteModel AddUserAccount(CustAccount objCustAccount)
        {
            return DAL.AddUserAccount(objCustAccount);
        }

        /// <summary>
        /// 根据编号获取用户信息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Sys_Users GetSysUserById(string userId)
        {
            return DAL.GetSysUserById(userId);
        }

        public List<string> GetSysUserName(string userId)
        {
            return DAL.GetSysUserName(userId);
        }

        /// <summary>
        /// 获取部门名
        /// </summary>
        /// <param name="deptId"></param>
        /// <returns></returns>
        public string GetDepartmentName(string deptId)
        {
            return DAL.GetDepartmentName(deptId);
        }

        /// <summary>
        /// 获取身份证类型
        /// </summary>
        /// <param name="IdNum"></param>
        /// <returns></returns>
        public string GetIDTypeName(string IdNum)
        {
            return DAL.GetIDTypeName(IdNum);
        }

        public ExcuteModel SaveSysUserInfo(Sys_Users objSysUser)
        {
            return DAL.SaveSysUserInfo(objSysUser);
        }

        /// <summary>
        /// 将远端图片转换为流
        /// </summary>
        /// <param name="imagPath"></param>
        /// <returns></returns>
        public List<byte[]> GetImageData(string imagPath)
        {
            List<byte[]> list = new List<byte[]>();           
            if (File.Exists(imagPath))
            {
                FileStream fs = new FileStream(imagPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                byte[] byData = new byte[fs.Length];
                fs.Read(byData, 0, byData.Length);
                fs.Close();
                list.Add(byData);
            }
            return list;
        }

        /// <summary>
        /// 获取取模板数据流
        /// </summary>
        /// <param name="strTemplateName"></param>
        /// <returns></returns>
        public byte[] GetTemplateData(string strTemplateName)
        {
            byte[] byteData = null;
            if (File.Exists(strTemplateName))
            {
                FileStream fs = new FileStream(strTemplateName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                byteData = new byte[fs.Length];
                fs.Read(byteData, 0, byteData.Length);
                fs.Close();
            }
            return byteData;
        }

        #region 省份
        /// <summary>
        /// add by bob 2017-07-09
        /// 获取省份
        /// </summary>
        /// <returns></returns>
        public List<Base_Area> GetBaseArea()
        {
            return DAL.GetBaseArea();
        }
        #endregion

        #region add by bob 2017-07-11 服务类型
        /// <summary>
        /// 获取服务类型数据
        /// </summary>
        /// <returns></returns>
        public List<Sys_DiagnoseProgram> GetDiagnoseProgramList()
        {
            return DAL.GetDiagnoseProgramList();
        }

        /// <summary>
        /// 查询服务类型数据
        /// </summary>
        /// <param name="ServiceID"></param>
        /// <returns></returns>
        public Sys_DiagnoseProgram GetDiagnoseProgramModel(int ServiceID)
        {
            return DAL.GetDiagnoseProgramModel(ServiceID);
        }

        /// <summary>
        /// 添加服务类型
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel AddServiceProgram(Sys_DiagnoseProgram param)
        {
            return DAL.AddServiceProgram(param);
        }

        /// <summary>
        /// 更新服务类型
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel UpdateServiceProgram(Sys_DiagnoseProgram param)
        {
            return DAL.UpdateServiceProgram(param);
        }

        /// <summary>
        /// 更新状态成功
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel UpdateServiceProgramState(Sys_DiagnoseProgram param)
        {
            return DAL.UpdateServiceProgramState(param);
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel UpdateServiceProgramDel(Sys_DiagnoseProgram param)
        {
            return DAL.UpdateServiceProgramDel(param);
        }
        #endregion

        #region 用户组权限维护
        public List<ThePrivilegesOfTheUser> GetUserPrivileges(string userId)
        {
            return DAL.GetUserPrivileges(userId);
        }

        public Sys_Settings GetSys_SettingsInfo(string sysKey)
        {
            return DAL.GetSys_SettingsInfo(sysKey);
        }
        #endregion

        #region 工作量统计
        public List<Sys_Users> GetSysUsersByDept(string dept)
        {
            return DAL.GetSysUsersByDept(dept).ToList();
        }
        #endregion

        #region 消息维护

        public ExcuteModel AddAppUpdateLog(AppUpdateLog appUpdateLog)
        {
            return DAL.AddAppUpdateLog(appUpdateLog);            
        }

        public ExcuteModel SaveAppUpdateLog(AppUpdateLog appUpdateLog)
        {
            return DAL.SaveAppUpdateLog(appUpdateLog);          
        }

        public ExcuteModel DelAppUpdateLog(AppUpdateLog appUpdateLog)
        {
            return DAL.DelAppUpdateLog(appUpdateLog);
        }

        public AppUpdateLog GetAppUpdateLogById(string id)
        {
            return DAL.GetAppUpdateLogById(id);
        }

        public List<AppUpdateLog> GetAppUpdateLog()
        {
            return DAL.GetAppUpdateLog();
        }

        public List<AppUpdateLog> GetNewAppUpdateLogList(int pageSize, int pageIndex, out int totalRecord, out int totalPage)
        {
            return DAL.GetNewAppUpdateLogList(pageSize, pageIndex, out totalRecord, out totalPage);
        }

        public List<AppUpdateLog> GetAppUpdateLogByDateTime(DateTime datetime)
        {
            return DAL.GetAppUpdateLogByDateTime(datetime);
        }
        #endregion

        #region 解析License
        public UNLicense UnLicenseInfo(string fileNameParam)
        {
            UNLicense info = new UNLicense();
            info.CurrentDate = DateTime.Now;
            if (!File.Exists(fileNameParam))
            {
                info.OrganizationCode = "文件不存在";
                return info;
            }
            string strVer = string.Empty;
            string strCode = string.Empty;
            string strTemp = string.Empty;
            var license = OperateIniFile.ReadIniData("License", "License", "", fileNameParam);
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(license);
            XmlNodeList topM = xml.GetElementsByTagName(@"doc");
            if (topM.Count > 0)
            {
                foreach (XmlElement element in topM)
                {
                    strVer = element.GetElementsByTagName("ver")[0].InnerText;
                    strCode = element.GetElementsByTagName("code")[0].InnerText;
                }
            }

            var t1 = Base64helper.DecodingForString(strCode);
            var arrayCount = t1.Split(new char[1] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (arrayCount.Count() > 0)
            {
                foreach (string item in arrayCount)
                {
                    if (item.Contains("LicenseType"))
                    {
                        var value = item.Split(new char[1] { ':' })[1];
                        info.LicenseType = int.Parse(value);
                    }
                    else if (item.Contains("ConcurrentNumber"))
                    {
                        var value = item.Split(new char[1] { ':' })[1];
                        info.ConcurrentNumber = int.Parse(value);
                    }
                    else if (item.Contains("StartDate"))
                    {
                        var value = item.Substring(item.IndexOf(":") + 1);
                        info.StartDate = DateTime.Parse(value);
                    }
                    else if (item.Contains("EndDate"))
                    {
                        var value = item.Substring(item.IndexOf(":") + 1);
                        info.EndDate = DateTime.Parse(value);
                    }
                    else if (item.Contains("OrganizationCode"))
                    {
                        var value = item.Split(new char[1] { ':' })[1];
                        info.OrganizationCode = value;
                    }
                    else if (item.Contains("ContractNo"))
                    {
                        var value = item.Split(new char[1] { ':' })[1];
                        info.ContractNo = value;
                    }

                    else if (item.Contains("CompanyName"))
                    {
                        var value = item.Split(new char[1] { ':' })[1];
                        info.CompanyName = value;
                    }
                    else if (item.Contains("SoftwareName"))
                    {
                        var value = item.Split(new char[1] { ':' })[1];
                        info.SoftwareName = value;
                    }
                    else if (item.Contains("SoftwareEdition"))
                    {
                        var value = item.Split(new char[1] { ':' })[1];
                        info.SoftwareEdition = value;
                    }
                    else if (item.Contains("UserName"))
                    {
                        var value = item.Split(new char[1] { ':' })[1];
                        info.UserName = value;
                    }
                    else if (item.Contains("ApplyDate"))
                    {
                        var value = item.Substring(item.IndexOf(":") + 1);
                        info.ApplyDate = DateTime.Parse(value); ;
                    }
                    else if (item.Contains("DeptName"))
                    {
                        var value = item.Split(new char[1] { ':' })[1];
                        info.DeptName = value;
                    }
                    else if (item.Contains("CustomerName"))
                    {
                        var value = item.Split(new char[1] { ':' })[1];
                        info.CustomerName = value;
                    }
                }
            }
            return info;
        }
        #endregion

        #region 科室类型维护
        /// <summary>
        /// 添加科室
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel AddDepartment(Sys_Department model)
        {
            return DAL.AddDepartment(model);
        }

         /// <summary>
        /// 更新科室表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateDepartment(Sys_Department model)
        {
            return DAL.UpdateDepartment(model);
        }

        /// <summary>
        /// 获取科室类型
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public List<Sys_Department> GetDepartmentList(Sys_Department model)
        {
            return DAL.GetDepartmentList(model);
        }
        #endregion

        #region 设备类型维护
        /// <summary>
        /// 添加设备类型
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel AddDevice(Sys_Device model)
        {
            return DAL.AddDevice(model);
        }

        /// <summary>
        /// 更新设备类型表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateDevice(Sys_Device model)
        {
            return DAL.UpdateDevice(model);
        }

        /// <summary>
        /// 获取设备类型
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public List<OutDevice> GetDeviceList(OutDevice model)
        {
            return DAL.GetDeviceList(model);
        }

        /// <summary>
        /// 根据检查科室获取设备类型
        /// </summary>
        /// <param name="DepartmentID"></param>
        /// <returns></returns>
        public List<DiagnoseType> GetDiagnoseTypeData(int DepartmentID)
        {
            return DAL.GetDiagnoseTypeData(DepartmentID);
        }
        #endregion

        #region 胶片规格维护
        /// <summary>
        /// 添加规格
        /// </summary>
        /// <param name="model"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public ExcuteModel AddFilmSpeci(Sys_FilmSpeci model)
        {
            return DAL.AddFilmSpeci(model);
        }

        /// <summary>
        /// 更新规格
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateFilmSpeci(Sys_FilmSpeci model)
        {
            return DAL.UpdateFilmSpeci(model);
        }

        /// <summary>
        /// 获取所有规格
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<Sys_FilmSpeci> GetFilmSpeciList(Sys_FilmSpeci model)
        {
            return DAL.GetFilmSpeciList(model);
        }
        #endregion

        #region 患者影像号累计
        /// <summary>
        /// 批量创建影像号
        /// </summary>
        /// <returns></returns>
        public ExcuteModel CreatePatientNumber()
        {
            return DAL.CreatePatientNumber();
        }
        /// <summary>
        /// 获取影像号
        /// 同时判断，影像号的数量
        /// </summary>
        /// <returns></returns>
        public string GetPatientNumber()
        {
            return DAL.GetPatientNumber();
        }
        /// <summary>
        /// 删除影像号
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel DeletePatientNumber(string param)
        {
            return DAL.DeletePatientNumber(param);
        }

         /// <summary>
        /// 修改字段值
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel UpdatePatientNumber(string param)
        {
            return DAL.UpdatePatientNumber(param);
        }
        #endregion

        #region 获取服务器时间
        public DateTime GetSystemTime()
        {
            return DateTime.Now;
        }
        #endregion
    }
}
