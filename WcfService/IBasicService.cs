﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WcfService.DB_Server;
using WcfService.DB_Server.SystemManage;
using WcfService.Models;

namespace WcfService
{
    [ServiceContract]
    public interface IBasicService
    {
        [OperationContract]
        void DoWork();
        
        #region 上传文件
        [OperationContract]
        bool CreateFile(string StrPath, string fileName);

        [OperationContract]
        bool Append(string StrPath, string fileName, byte[] buffer);
        [OperationContract]
        bool Verify(string StrPath, string fileName, string md5);
        #endregion

        [OperationContract]
        bool Up(byte[] data, string filename);
        
        #region 下载文件
        [OperationContract]
        byte[] Down(string StrPath, string filename);

        [OperationContract]
        string GetMd5CodeByServerFileName(string StrPath, string filename);
        [OperationContract]
        int GetSplitCountByFileName(string StrPath, string filename, int SplitByte);

        [OperationContract]
        byte[] DownFileByFileIdAndSplitBype(string StrPath, string filename, int SplitBype, int SplitPage);

        /// <summary>
        /// 以流的形式下载文件
        /// </summary>
        /// <param name="StrPath"></param>
        /// <param name="filename"></param>
        /// <param name="SplitBype"></param>
        /// <param name="SplitPage"></param>
        /// <returns></returns>
        [OperationContract]
        byte[] DownFileBytes(string StrPath, long SplitBype, long SplitPage);
        #endregion

        #region 上传文件到前置机
        //创建服务文件夹
        [OperationContract]
        bool CreateFileOnFM(string StrPath, string StrPatientId, string fileName);

        //向压缩文件夹中添加数据
        [OperationContract]
        bool AppendDetailOnFM(string StrPath, string StrPatientId, string fileName, byte[] buffer);

        /// <summary>
        /// 判断文件是否上传成功
        /// </summary>
        /// <param name="StrPath">路径</param>
        /// <param name="fileName">文件</param>
        /// <param name="md5">加密</param>
        /// <returns></returns>
        [OperationContract]
        bool VerifyOnFM(string StrPath, string StrPatientId, string fileName, string md5);

        /// <summary>
        /// 获取文件名称
        /// </summary>
        /// <param name="Path"></param>
        /// <returns></returns>
        [OperationContract]
        List<string> GetFileZip(string Path);

         /// <summary>
        /// 获取文件名称和文件大小
        /// </summary>
        /// <param name="paramPath"></param>
        /// <returns></returns>
        [OperationContract]
        List<FileInfoModel> GetFileNameSize(string paramPath);
         /// <summary>
        /// 获取指定文件大小
        /// </summary>
        /// <param name="paramPath"></param>
        /// <returns></returns>
        [OperationContract]
        long GetSingleFileNameSize(string paramPath);
        #endregion

        #region 服务项目
        [OperationContract]
        List<Sys_DiagnoseService> GetDiagnoseServiceListBySiteId(string SiteId);
        [OperationContract]
        Sys_DiagnoseService GetDiagnoseServiceItemByServiceId(string ServiceItemId);
        [OperationContract]
        ExcuteModel SaveDiagnoseService(Sys_DiagnoseService model);
        [OperationContract]
        ExcuteModel SetEnableServiceItem(string ServiceId, int State);
        #endregion

        #region 数据字典
        /// <summary>
        /// 添加字典项
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel SaveDictionary(Sys_Dictionary model);
        /// <summary>
        /// 获取字典列表
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<Sys_Dictionary> GetDictionaryListByType(string Type);

        /// <summary>
        /// 根据代码获取字典对象
        /// </summary>
        /// <param name="Code"></param>
        /// <returns></returns>
        [OperationContract]
        Sys_Dictionary GetDictionaryDescriptionByTypeCode(string Type, string Code);

        /// <summary>
        /// 根据唯一号获取字典对象
        /// </summary>
        /// <param name="GUID"></param>
        /// <returns></returns>
        [OperationContract]
        Sys_Dictionary GetDictionaryByTypeGUID(string GUID);
        #endregion

        #region 系统配置
        /// <summary>
        /// 获取系统配置
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        Sys_Settings GetSysValueByKey(string Key);
        #endregion

        #region 日志记录
        /// <summary>
        ///日志记录
        /// </summary>
        /// <param name="strState">日志状态</param>
        /// <param name="strContent">日志内容</param>
        /// <param name="ex">错误信息</param>
        [OperationContract]
        void WriteLogInfoContent(string strState, string strContent, string strEx);

        /// <summary>
        /// 日志
        /// </summary>
        /// <param name="logState"></param>
        /// <param name="strContent"></param>
        /// <param name="strEx"></param>
        [OperationContract]
        void WriteLogContent(LogParam logState, string strContent, string strEx);

         /// <summary>
        /// add by bob 2017-05-05
        /// 添加仅适用于添加服务日志
        /// </summary>
        /// <param name="logState"></param>
        /// <param name="strContent"></param>
        /// <param name="strEx"></param>
        [OperationContract]
        void OnlyServiceLogContent(LogParam logState, string strContent, string strEx);

         /// <summary>
        /// 添加仅适用于api服务日志
        /// </summary>
        /// <param name="logState"></param>
        /// <param name="strContent"></param>
        /// <param name="strEx"></param>
        [OperationContract]
        void OnlyApiLogContent(LogParam logState, string strContent, string strEx);

        /// <summary>
        /// 添加仅适用于文件上传服务日志
        /// </summary>
        /// <param name="logState"></param>
        /// <param name="strContent"></param>
        /// <param name="strEx"></param>
        [OperationContract]
        void OnlyUploadServiceLogContent(LogParam logState, string strContent, string strEx);

        /// <summary>
        /// 添加仅适用于影像数据同步服务日志
        /// </summary>
        /// <param name="logState"></param>
        /// <param name="strContent"></param>
        /// <param name="strEx"></param>
        [OperationContract]
        void OnlyImageServiceLogContent(LogParam logState, string strContent, string strEx);
        
        #endregion
        [OperationContract]
        string GetServerDateTime();
    }
    
    [MessageContract]
    public class FileTransferMessage
    {
        [MessageHeader(MustUnderstand = true)]
        public string SavePath;//文件保存路径

        [MessageHeader(MustUnderstand = true)]
        public string FileName;//文件名称

        [MessageHeader(MustUnderstand = true)]
        public string PatientId;//创建文件夹

        [MessageBodyMember(Order = 1)]
        public Stream FileData;//文件传输数据
    }
}
