﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using WcfService.DB_Server;
using WcfService.DB_Server.SystemManage;
using WcfService.Models;
using System.Threading;

namespace WcfService
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class BasicService : IBasicService
    {
        public void DoWork()
        {
        }

        SystemManageContext DAL = new SystemManageContext();

        #region 上传下载
        #region 上传文件
        public bool CreateFile(string StrPath, string fileName)
        {
            bool isCreate = true;
            try
            {
                if (!Directory.Exists(StrPath))
                {
                    Directory.CreateDirectory(StrPath);
                }
                fileName = Path.Combine(StrPath + Path.GetFileName(fileName));
                FileStream fs = new FileStream(fileName, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
                fs.Close();
            }
            catch (Exception)
            {
                isCreate = false;
            }
            return isCreate;
        }

        public bool Append(string StrPath, string fileName, byte[] buffer)
        {
            bool isAppend = true;
            try
            {
                fileName = Path.Combine(StrPath + Path.GetFileName(fileName));
                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(buffer, 0, buffer.Length);
                fs.Close();
            }
            catch
            {
                isAppend = false;
            }
            return isAppend;
        }

        public bool Verify(string StrPath, string fileName, string md5)
        {
            bool isVerify = true;
            try
            {
                fileName = Path.Combine(StrPath + Path.GetFileName(fileName));
                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
                MD5CryptoServiceProvider p = new MD5CryptoServiceProvider();
                byte[] md5buffer = p.ComputeHash(fs);
                fs.Close();
                string md5Str = "";
                List<string> strList = new List<string>();
                for (int i = 0; i < md5buffer.Length; i++)
                {
                    md5Str += md5buffer[i].ToString("x2");
                }
                if (md5 != md5Str)
                {
                    isVerify = false;
                }
            }
            catch
            {
                isVerify = false;
            }
            return isVerify;
        }
        #endregion

        #region

        public bool Up(byte[] data, string filename)
        {
            try
            {
                FileStream fs = File.Create(GetSysValueByKey("ServerFilePath").SysValue + filename);
                fs.Write(data, 0, data.Length);
                fs.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region 下载文件
        public byte[] Down(string StrPath, string filename)
        {
            string filepath = StrPath + filename;
            if (File.Exists(filepath))
            {
                try
                {
                    FileStream s = File.OpenRead(filepath);
                    byte[] data = new byte[s.Length];
                    s.Read(data, 0, data.Length);
                    s.Close();
                    return data;
                }
                catch
                {
                    return new byte[0];
                }
            }
            else
            {
                return new byte[0];
            }
        }

        public string GetMd5CodeByServerFileName(string StrPath, string filename)
        {
            //string filepath = StrPath + filename;
            //if (File.Exists(filepath))
            //{
            //    return Unities.Encrypt.GetMD5HashFromFile(filepath);
            //}
            //else
            //{
            //    return "文件不存在";
            //}
            return "";
        }

        public int GetSplitCountByFileName(string StrPath, string filename, int SplitByte)
        {
            string filepath = StrPath + filename;
            if (File.Exists(filepath))
            {
                try
                {
                    FileStream s = new FileStream(filepath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
                    long SplitCount = s.Length / SplitByte;
                    if (s.Length % SplitByte != 0)
                    {
                        SplitCount += 1;
                    }
                    s.Close();
                    s.Dispose();
                    return Convert.ToInt32(SplitCount);
                }
                catch (Exception ex)
                {
                    LogHelper.WriteErrorLog("GetSplitCountByFileName", "异常信息：" + ex.Message + "--堆栈信息：" + ex.StackTrace);
                    return -1;
                }
            }
            else
            {
                return 0;
            }
        }

        public byte[] DownFileByFileIdAndSplitBype(string StrPath, string filename, int SplitBype, int SplitPage)
        {
            string filepath = StrPath + filename;
            if (File.Exists(filepath))
            {
                FileStream s = new FileStream(filepath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
                try
                {
                    s.Position = SplitBype * SplitPage;
                    int lastSplit = Convert.ToInt32(s.Length % SplitBype);
                    byte[] data = new byte[SplitBype];
                    if ((lastSplit != 0) && (GetSplitCountByFileName(StrPath, filename, SplitBype) - 1) == SplitPage)
                    {
                        data = new byte[lastSplit];
                    }
                    s.Read(data, 0, data.Length);
                    s.Close();
                    return data;
                }
                catch
                {
                    s.Close();
                    return new byte[0];
                }
            }
            else
            {
                return new byte[0];
            }
        }

        /// <summary>
        /// 以流的形式下载文件
        /// </summary>
        /// <param name="StrPath"></param>
        /// <param name="filename"></param>
        /// <param name="SplitBype"></param>
        /// <param name="SplitPage"></param>
        /// <returns></returns>
        public byte[] DownFileBytes(string StrPath, long SplitBype, long SplitPage)
        {
            if (File.Exists(StrPath))
            {
                try
                {
                    using (FileStream s = new FileStream(StrPath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
                    {
                        s.Position = SplitPage;//初始化为0
                        byte[] data = new byte[SplitBype];
                        s.Read(data, 0, data.Length);
                        return data;
                    }
                }
                catch
                {
                    return new byte[0];
                }
            }
            else
            {
                return new byte[0];
            }
        }

        #endregion
        #endregion

        #region  上传文件到前置机
        /// <summary>
        /// 在服务上创建病人文件夹
        /// </summary>
        /// <param name="StrPath">目录</param>
        /// <param name="StrPatientId">病人ID</param>
        /// <param name="fileName">文件名称</param>
        /// <returns></returns>
        public bool CreateFileOnFM(string StrPath, string StrPatientId, string fileName)
        {
            bool isCreate = true;
            try
            {
                StrPath = GetSysValueByKey("ServerMobileApiPath").SysValue;
                //创建文件夹
                if (!Directory.Exists(StrPath))
                {
                    Directory.CreateDirectory(StrPath);
                }
                //创建病人文件夹
                StrPath = StrPath + StrPatientId;
                if (!Directory.Exists(StrPath))
                {
                    Directory.CreateDirectory(StrPath);
                }
                fileName = Path.Combine(StrPath + @"\" + Path.GetFileName(fileName));
                FileStream fs = new FileStream(fileName, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
                fs.Close();
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("BasicService", ex.Message);
                isCreate = false;
            }
            return isCreate;
        }

        /// <summary>
        /// 向压缩文件夹中添加数据
        /// </summary>
        /// <param name="StrPath"></param>
        /// <param name="StrPatientId"></param>
        /// <param name="fileName"></param>
        /// <param name="buffer"></param>
        /// <returns></returns>
        public bool AppendDetailOnFM(string StrPath, string StrPatientId, string fileName, byte[] buffer)
        {
            bool isAppend = true;
            try
            {
                StrPath = GetSysValueByKey("ServerFilePath").SysValue;
                StrPath = StrPath + StrPatientId + @"\";
                fileName = Path.Combine(StrPath + Path.GetFileName(fileName));
                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(buffer, 0, buffer.Length);
                fs.Close();
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("AppendDetailOnFM", ex.Message);
                isAppend = false;
            }
            return isAppend;
        }

        /// <summary>
        /// 判断文件是否上传成功
        /// </summary>
        /// <param name="StrPath"></param>
        /// <param name="fileName"></param>
        /// <param name="md5"></param>
        /// <returns></returns>
        public bool VerifyOnFM(string StrPath, string StrPatientId, string fileName, string md5)
        {
            bool isVerify = true;
            try
            {
                StrPath = GetSysValueByKey("ServerFilePath").SysValue;
                StrPath = StrPath + StrPatientId + @"\";
                fileName = Path.Combine(StrPath + Path.GetFileName(fileName));
                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
                MD5CryptoServiceProvider p = new MD5CryptoServiceProvider();
                byte[] md5buffer = p.ComputeHash(fs);
                fs.Close();
                string md5Str = "";
                List<string> strList = new List<string>();
                for (int i = 0; i < md5buffer.Length; i++)
                {
                    md5Str += md5buffer[i].ToString("x2");
                }
                if (md5 != md5Str)
                {
                    isVerify = false;
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("AppendDetailOnFM", ex.Message);
                isVerify = false;
            }
            return isVerify;
        }

        /// <summary>
        /// 获取文件名称
        /// </summary>
        /// <param name="Path"></param>
        /// <returns></returns>
        public List<string> GetFileZip(string Path)
        {
            List<string> strZip = new List<string>();
            if (!Directory.Exists(Path))
                return strZip;
            DirectoryInfo folder = new DirectoryInfo(Path);
            foreach (FileInfo fileitem in folder.GetFiles("*.zip"))
            {
                strZip.Add(fileitem.Name);
            }
            return strZip;
        }

        /// <summary>
        /// 获取文件名称和文件大小
        /// </summary>
        /// <param name="paramPath"></param>
        /// <returns></returns>
        public List<FileInfoModel> GetFileNameSize(string paramPath)
        {
            List<FileInfoModel> strZip = new List<FileInfoModel>();
            if (!Directory.Exists(paramPath))
                return strZip;
            DirectoryInfo folder = new DirectoryInfo(paramPath);
            foreach (FileInfo fileitem in folder.GetFiles("*.zip"))
            {
                strZip.Add(new FileInfoModel { FileName = fileitem.Name, FileSize = fileitem.Length });
            }
            return strZip;
        }
        /// <summary>
        /// 获取指定文件大小
        /// </summary>
        /// <param name="paramPath"></param>
        /// <returns></returns>
        public long GetSingleFileNameSize(string paramPath)
        {
            long size = 0;
            if (File.Exists(paramPath))
            {
               FileInfo fi = new FileInfo(paramPath);
               size = fi.Length;
            }
            return size;
        }
        #endregion

        #region 服务项目
        /// <summary>
        /// 根据站点id获取站点的服务项目
        /// </summary>
        /// <param name="SiteId"></param>
        /// <returns></returns>
        public List<Sys_DiagnoseService> GetDiagnoseServiceListBySiteId(string SiteId)
        {
            return DAL.GetDiagnoseServiceListBySiteId(SiteId);
        }
        /// <summary>
        /// 根据服务项目id获取服务项目
        /// </summary>
        /// <param name="ServiceItemId"></param>
        /// <returns></returns>
        public Sys_DiagnoseService GetDiagnoseServiceItemByServiceId(string ServiceItemId)
        {
            return DAL.GetDiagnoseServiceItemByServiceId(ServiceItemId);
        }

        /// <summary>
        /// 添加或修改服务项目
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveDiagnoseService(Sys_DiagnoseService model)
        {
            return DAL.SaveDiagnoseService(model);
        }
        /// <summary>
        /// 启用1，禁用2服务项目
        /// </summary>
        /// <param name="ServiceId"></param>
        /// <param name="State"></param>
        /// <returns></returns>
        public ExcuteModel SetEnableServiceItem(string ServiceId, int State)
        {
            return DAL.SetEnableServiceItem(ServiceId, State);
        }
        #endregion

        #region 数据字典
        /// <summary>
        /// 添加字典项
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveDictionary(Sys_Dictionary model)
        {
            return DAL.SaveDictionary(model);
        }
        /// <summary>
        /// 获取字典列表
        /// </summary>
        /// <returns></returns>
        public List<Sys_Dictionary> GetDictionaryListByType(string Type)
        {
            return DAL.GetDictionaryListByType(Type);
        }

        /// <summary>
        /// 根据代码获取字典对象
        /// </summary>
        /// <param name="Code"></param>
        /// <returns></returns>
        public Sys_Dictionary GetDictionaryDescriptionByTypeCode(string Type, string Code)
        {
            return DAL.GetDictionaryDescriptionByTypeCode(Type, Code);
        }
        /// <summary>
        /// 根据唯一号获取字典对象
        /// </summary>
        /// <param name="GUID"></param>
        /// <returns></returns>
        public Sys_Dictionary GetDictionaryByTypeGUID(string GUID)
        {
            return DAL.GetDictionaryByTypeGUID(GUID);
        }
        #endregion

        #region 数据日志
        ///// <summary>
        ///// 记录操作日志
        ///// </summary>
        ///// <param name="ClientIp"></param>
        ///// <param name="UserName"></param>
        ///// <param name="LogItem"></param>
        ///// <returns></returns>
        //public bool AppOprLog(string ClientIp, string UserName, List<SysLogItem> LogItem)
        //{
        //    return DAL.AppOprLog(ClientIp,  UserName, LogItem);
        //}
        #endregion

        #region 系统配置
        /// <summary>
        /// 获取系统配置
        /// </summary>
        /// <returns></returns>
        public Sys_Settings GetSysValueByKey(string Key)
        {
            return DAL.GetSysValueByKey(Key);
        }
        #endregion

        public string GetServerDateTime()
        {
            return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }

        #region 日志记录
        /// <summary>
        /// 日志记录
        /// </summary>
        /// <param name="strState">日志状态</param>
        /// <param name="strContent">日志内容</param>
        /// <param name="ex">错误信息</param>
        public void WriteLogInfoContent(string strState, string strContent, string strEx)
        {
            if (strState == "E")
            {
                LogHelper.WriteErrorLog(strContent, strEx);
            }
            else if (strState == "I")
            {
                LogHelper.WriteInfoLog(strContent);
            }
            else if (strState == "W")
            {
                LogHelper.WriteWarningLog(strContent);
            }
        }

        public void WriteLogContent(LogParam logState, string strContent, string strEx)
        {
            if (logState == LogParam.E)
            {
                LogHelper.WriteErrorLog(strContent, strEx);
            }
            else if (logState == LogParam.I)
            {
                LogHelper.WriteInfoLog(strContent + "--" + strEx);
            }
            else if (logState == LogParam.W)
            {
                LogHelper.WriteWarningLog(strContent + "--" + strEx);
            }
        }

        /// <summary>
        /// 添加仅适用于添加服务日志
        /// </summary>
        /// <param name="logState"></param>
        /// <param name="strContent"></param>
        /// <param name="strEx"></param>
        public void OnlyServiceLogContent(LogParam logState, string strContent, string strEx)
        {
            if (logState == LogParam.E)
            {
                LogHelper.ServiceErrorLog(strContent, strEx);
            }
            else if (logState == LogParam.I)
            {
                LogHelper.ServiceInfoLog(strContent + "--" + strEx);
            }
            else if (logState == LogParam.W)
            {
                LogHelper.ServiceWarningLog(strContent + "--" + strEx);
            }
        }

        /// <summary>
        /// 添加仅适用于api服务日志
        /// </summary>
        /// <param name="logState"></param>
        /// <param name="strContent"></param>
        /// <param name="strEx"></param>
        public void OnlyApiLogContent(LogParam logState, string strContent, string strEx)
        {
            if (logState == LogParam.E)
            {
                LogHelper.ApiErrorLog(strContent, strEx);
            }
            else if (logState == LogParam.I)
            {
                LogHelper.ApiInfoLog(strContent + "--" + strEx);
            }
            else if (logState == LogParam.W)
            {
                LogHelper.ApiWarningLog(strContent + "--" + strEx);
            }
        }

        /// <summary>
        /// 添加仅适用于文件上传服务日志
        /// </summary>
        /// <param name="logState"></param>
        /// <param name="strContent"></param>
        /// <param name="strEx"></param>
        public void OnlyUploadServiceLogContent(LogParam logState, string strContent, string strEx)
        {
            if (logState == LogParam.E)
            {
                LogHelper.UploadServiceErrorLog(strContent, strEx);
            }
            else if (logState == LogParam.I)
            {
                LogHelper.UploadServiceInfoLog(strContent + "--" + strEx);
            }
        }

        /// <summary>
        /// 添加仅适用于影像数据同步服务日志
        /// </summary>
        /// <param name="logState"></param>
        /// <param name="strContent"></param>
        /// <param name="strEx"></param>
        public void OnlyImageServiceLogContent(LogParam logState, string strContent, string strEx)
        {
            if (logState == LogParam.E)
            {
                LogHelper.ImageServiceErrorLog(strContent, strEx);
            }
            else if (logState == LogParam.I)
            {
                LogHelper.ImageServiceInfoLog(strContent + "--" + strEx);
            }
        }

        #endregion
    }
}
