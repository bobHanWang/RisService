﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WcfService.DB_Server.WorklistManage;

namespace WcfService
{
    [ServiceContract]
    public interface IWorklistService
    {
        #region worklist数据操作
        /// <summary>
        /// 添加数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel AddWorklistInfo(Worklist model);

        /// <summary>
        /// 获取数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        List<Worklist> GetWorklistInfo(Worklist model);

         /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel UpdateWorklistInfo(Worklist model);

         /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [OperationContract]
        ExcuteModel DeleteWorklistInfo(string param);
        #endregion
    }
}
