﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Web;
using WcfService.Models;

namespace WcfService
{
    public enum LogParam
    {
        I,
        E,
        W,
    }
    public enum SettlementType
    {
        ApplyDoctor,
        Platform,
        Channel,
        Experts
    }
    public class DistributionPattern
    {
        public string ApplyDoctor { get; set; }
        public string Platform { get; set; }
        public string Channel { get; set; }
        public string Experts { get; set; }
    }
    public class Common
    {
        
        /// <summary>
        /// 推算两个时间差
        /// </summary>
        /// <param name="DateTime1"></param>
        /// <param name="DateTime2"></param>
        /// <returns></returns>
        public static double DateTimeDiff(DateTime DateTime1, DateTime DateTime2)
        {
            TimeSpan ts1 = new TimeSpan(DateTime1.Ticks);
            TimeSpan ts2 = new TimeSpan(DateTime2.Ticks);
            TimeSpan ts = ts1.Subtract(ts2).Duration();
            return ts.TotalHours;
        }
    }
}