﻿using HslCommunication.BasicFramework;
using ICSharpCode.SharpZipLib.Checksums;
using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using System.Threading;
using System.Transactions;
using System.Web;
using System.Windows.Forms;
using WcfService.DB_Server;
using WcfService.DB_Server.SystemManage;
using WcfService.DB_Server.WorklistManage;
using WcfService.Models;

namespace WcfService
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class BusinessService : IBusinessService
    {
        RemoteDianoseContext dbContext = new RemoteDianoseContext();
        SystemManageContext DAL = new SystemManageContext();
        WorklistManageContext workContent = new WorklistManageContext();
        //ImageServerDataContext DBContext = new ImageServerDataContext();

        #region   写报告给医生余额转账
        public ExcuteModel SaveSettleAccountsMain(SettleAccountsMain model)
        {
            return dbContext.SaveSettleAccountsMain(model);
        }

        /// <summary>
        /// 获取结算主表信息
        /// </summary>
        /// <param name="RequestID"></param>
        /// <returns></returns>
        public List<SettleAccountsMain> GetSettleAccountsMain(string RequestID)
        {
            return dbContext.GetSettleAccountsMain(RequestID);
        }

        public ExcuteModel SaveSettleAccounts(SettleAccounts model)
        {
            return dbContext.SaveSettleAccounts(model);
        }

        public ExcuteModel UpdateCustSettleAccountsState(SettleAccounts model)
        {
            return dbContext.UpdateCustSettleAccountsState(model);
        }

        public List<SettleAccountsList> GetSettleAccountsList(SettleAccountsList model)
        {
            return dbContext.GetSettleAccountsList(model);
        }

        /// <summary>
        /// 结算平分
        /// </summary>
        /// <param name="paramRequestInfo"></param>
        /// <param name="paramUsers"></param>
        /// <returns></returns>
        public ExcuteModel SettlementBisection(RequestInfo paramRequestInfo, string paramUserID)
        {
            return dbContext.SettlementBisection(paramRequestInfo, paramUserID);
        }
        #endregion

        #region  数据同步

        //public ExcuteModel SynPublicALLSaveImageServerData(Study model)
        //{
        //    return dbContext.SynPublicALLSaveImageServerData(model);
        //}
        //public List<ServerPartition> GetServerPartitionList(ServerPartition model)
        //{
        //    return dbContext.GetServerPartitionList(model);
        //}
        //public ExcuteModel SaveServerPartition(ServerPartition model, int type)
        //{
        //    return dbContext.SaveServerPartition(model, type);
        //}
        //public List<Patient> GetPatientList(Patient model)
        //{
        //    return dbContext.GetPatientList(model);
        //}
        //public ExcuteModel SavePatient(Patient model, int type)
        //{
        //    return dbContext.SavePatient(model, type);
        //}
        //public List<RequestAttributes> GetRequestAttributesList(RequestAttributes model)
        //{
        //    return dbContext.GetRequestAttributesList(model);
        //}
        //public ExcuteModel SaveRequestAttributes(RequestAttributes model, int type)
        //{
        //    return dbContext.SaveRequestAttributes(model, type);
        //}
        //public List<Series> GetSeriesList(Series model)
        //{
        //    return dbContext.GetSeriesList(model);
        //}
        //public ExcuteModel SaveSeries(Series model, int type)
        //{
        //    return dbContext.SaveSeries(model, type);
        //}
        //public List<Study> GetStudyList_All(Study model)
        //{
        //    return dbContext.GetStudyList_All(model);
        //}
        //public List<Study> GetStudyList(Study model, DateTime BeginTime, DateTime EndTime)
        //{
        //    return dbContext.GetStudyList(model, BeginTime, EndTime);
        //}
        //public ExcuteModel SaveStudy(Study model, int type)
        //{
        //    return dbContext.SaveStudy(model, type);
        //}
        #endregion

        #region   开通个人账户
        public ExcuteModel OpenCustAccount(CustAccount model)
        {
            return dbContext.OpenCustAccount(model);
        }
        public ExcuteModel JudgeCustAccount(CustAccount model)
        {
            return dbContext.JudgeCustAccount(model);
        }

        public List<CustAccount> GetCustAccountList(CustAccount model)
        {
            return dbContext.GetCustAccountList(model);
        }
        #endregion

        #region   充值提现
        public ExcuteModel RechargeWithDrawBILL(RechargeWithDrawBILL model, CustAccountModel CustAccount_Model)
        {
            return dbContext.RechargeWithDrawBILL(model, CustAccount_Model);
        }
        public ExcuteModel RechargeWithDrawBILL_Log(RechargeWithDrawBILL model, CustAccountModel CustAccount_Model)
        {
            return dbContext.RechargeWithDrawBILL_Log(model, CustAccount_Model);
        }

        public ExcuteModel UpdateRechargeWithDrawBILLState(RechargeWithDrawBILL model, CustAccountModel CustAccount_Model)
        {
            return dbContext.UpdateRechargeWithDrawBILLState(model, CustAccount_Model);
        }
        public List<RechargeWithDrawBILL> GetRechargeWithDrawBILLList(RechargeWithDrawBILL model, DateTime? startDate = null, DateTime? endDate = null)
        {
            return dbContext.GetRechargeWithDrawBILLList(model, startDate, endDate);
        }
        #endregion

        #region   修改列表
        public ExcuteModel UpdateRequestInfo(string RequestId)
        {
            return dbContext.UpdateRequestInfo(RequestId);
        }
        #endregion

        #region  手机app  支付接口
        public ExcuteModel RefundInterface(string RequestId, string refund_reason, string Amount, string people)
        {
            return dbContext.RefundInterface(RequestId, refund_reason, Amount, people);
        }

        #endregion

        #region   支付宝支付订单记录表

        public ExcuteModel SavePayTransactionLog(PayTransactionLog model)
        {
            return dbContext.SavePayTransactionLog(model);
        }

        public ExcuteModel SavePayTransaction_State(PayTransaction model)
        {
            return dbContext.SavePayTransaction_State(model);
        }
        public ExcuteModel SavePayTransaction(PayTransaction model)
        {
            return dbContext.SavePayTransaction(model);
        }

        /// <summary>
        /// 针对平台保存收费信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SavePayTransactionQRCode(PayTransaction model)
        {
            return dbContext.SavePayTransactionQRCode(model);
        }

        public ExcuteModel SavePayTransaction_Recharge(PayTransaction model)
        {
            return dbContext.SavePayTransaction_Recharge(model);
        }


        public List<PayTransaction> GetPayTransactionDetail(string PayTradeID, string RequestId)
        {
            return dbContext.GetPayTransactionDetail(PayTradeID, RequestId);
        }

        /// <summary>
        /// 根据时间查询订单信息
        /// </summary>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <returns></returns>
        public List<PayTransaction> SearchPayTransactionList(DateTime dtStart, DateTime dtEnd)
        {
            return dbContext.SearchPayTransactionList(dtStart, dtEnd);
        }

        /// <summary>
        /// 获取申请单状态
        /// </summary>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        public int GetPayTransactionState(string RequestId)
        {
            return dbContext.GetPayTransactionState(RequestId);
        }

        /// <summary>
        /// 修改支付表中的状态
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel ChargePayTransactionState(PayTransaction param)
        {
            return dbContext.ChargePayTransactionState(param);
        }
        #endregion

        #region  机构 支付账号维护
        public ExcuteModel SaveOrganPayAccounts(OrganPayAccount model)
        {
            return dbContext.SaveOrganPayAccounts(model);
        }

        public List<OrganPayAccount> GetOrganPayAccountList(OrganPayAccount model)
        {
            return dbContext.GetOrganPayAccountList(model);
        }

        public List<OrganPayAccount> GetOneOrganPayAccount(string PaymentID, string HospitalID)
        {
            return dbContext.GetOneOrganPayAccount(PaymentID, HospitalID);
        }

        public int DeleteOrganPayAccount(string PaymentID)
        {
            return dbContext.DeleteOrganPayAccount(PaymentID);
        }
        #endregion

        #region

        public ExcuteModel SaveAuditRecord(AuditRecord Model)
        {
            return dbContext.SaveAuditRecord(Model);
        }

        public List<AuditRecord> GetAuditRecordByRequestId(string RequestId)
        {
            return dbContext.GetAuditRecordByRequestId(RequestId);
        }

        #endregion

        #region  操作权限组   用户组 维护

        public List<Operationlimits> GetAllOlimits(string UserId)
        {
            return dbContext.GetAllOlimits(UserId);
        }

        /// <summary>
        /// 获取用户对报告的操作权限
        /// </summary>
        /// <param name="UserID">用户ID</param>
        /// <param name="GroupID">权限组ID</param>
        /// <returns></returns>
        public List<string> GetAllOlimitsData(string UserID, string GroupID)
        {
            return dbContext.GetAllOlimitsData(UserID, GroupID);
        }


        public ExcuteModel SaveOperationlimits(Operationlimits model)
        {
            return dbContext.SaveOperationlimits(model);
        }

        public List<Operationlimits> GetOperationlimitsList(string name)
        {
            return dbContext.GetOperationlimitsList(name);
        }

        public ExcuteModel SaveGroups(Groups model)
        {
            return dbContext.SaveGroups(model);
        }

        public ExcuteModel SaveGroups_ArrayList(Groups model, string ArrayList)
        {
            return dbContext.SaveGroups_ArrayList(model, ArrayList);
        }

        public List<Groups> GetGroupsList(string name)
        {
            return dbContext.GetGroupsList(name);
        }

        public ExcuteModel SaveUserGroup(UserGroup model)
        {
            return dbContext.SaveUserGroup(model);
        }

        public List<UserGroup> GetUserGroupList(string name)
        {
            return dbContext.GetUserGroup_List(name);
        }

        public List<UserGroup> GetUserGroupInfo(string userID)
        {
            return dbContext.GetUserGroupInfo(userID);
        }

        public ExcuteModel SaveOperationGroup(OperationGroup model)
        {
            return dbContext.SaveOperationGroup(model);
        }

        public List<OperationGroup> GetOperationGroupList(string name)
        {
            return dbContext.GetOperationGroupList(name);
        }

        public List<Operationlimits> GetOneOperationlimits(string Groupid, string olimitid)
        {
            return dbContext.GetOneOperationlimits(Groupid, olimitid);
        }

        public List<GroupOperation> GetOneGroupOperation(string Groupid, string olimitid)
        {
            return dbContext.GetOneGroupOperation(Groupid, olimitid);
        }


        public List<UserGroup> GetOneusergroup(string Groupid, string UserId)
        {
            return dbContext.GetOneusergroup(Groupid, UserId);
        }

        #endregion

        #region
        public ExcuteModel SaveSubHospital_relevance(SubHospital model)
        {
            return dbContext.SaveSubHospital_relevance(model);
        }

        public ExcuteModel SaveSubHospital(SubHospital model)
        {
            return dbContext.SaveSubHospital(model);
        }

        public List<SubHospital> GetAllSubHospital(string PHospitalId, string name, string txt_name)
        {
            return dbContext.GetAllSubHospital(PHospitalId, name, txt_name);
        }

        /// <summary>
        /// 根据医院获取相关中心医院
        /// </summary>
        /// <param name="strHospitalID"></param>
        /// <returns></returns>
        public List<SubHospital> GetSubHospitalList(string strHospitalID)
        {
            return dbContext.GetSubHospitalList(strHospitalID);
        }

        #endregion

        #region

        /// <summary>
        /// 创建文件夹
        /// </summary>
        /// <param name="path"></param>
        public void CreateDirectory(string path)
        {
            if (!Directory.Exists(path))//如果不存在就创建file文件夹
            {
                Directory.CreateDirectory(path);
            }
        }
        public bool JudgeFile(string path)
        {
            if (!File.Exists(path))
            {
                return false;
            }
            return true;
        }
        public string GetRoot()
        {
            return null;// dbContext.GetRoot();
        }
        public List<string> Get_path(string patientid, string studyInstanceUid)
        {
            return null;//dbContext.GetStudyFolder(patientid, studyInstanceUid);
        }
        public List<string> StudyFolder()
        {
            return null;//dbContext.StudyFolder();
        }
        //public List<Study> GetStudy()
        //{
        //    return null;//dbContext.GetStudy();
        //}
        //public List<Study> GetStudyByPatientId(string patientId, string studyInstanceUID)
        //{
        //    return null;//dbContext.GetStudyByPatientId(patientId, studyInstanceUID);
        //}

        public List<Patient_requestDetail> GetPatient_requestDetail(string patientId, string HospitalId, string D_SH_type_txt, string tB__SHname_txt)
        {
            return null;//dbContext.GetPatient_requestDetail(patientId, HospitalId, D_SH_type_txt, tB__SHname_txt);
        }

        public List<Patient_requestDetail> GetDoctors_AuditRecords(string patientId, string HospitalId, string ReviewStatus, string CPatientName, string DiagnoseDoctor)
        {
            return dbContext.GetDoctors_AuditRecords(patientId, HospitalId, ReviewStatus, CPatientName, DiagnoseDoctor);
        }

        public List<Patient_requestDetail> Getwrite_Report(string patientId, string HospitalId, string ReviewStatus, string CPatientName, string DiagnoseDoctor)
        {
            return dbContext.Getwrite_Report(patientId, HospitalId, ReviewStatus, CPatientName, DiagnoseDoctor);
        }
        public List<Patient_requestDetail> Alreadypaycost_Nowrite_Report(string patientId, string HospitalId, string ReviewStatus, string CPatientName, string DiagnoseDoctor)
        {
            return dbContext.Alreadypaycost_Nowrite_Report(patientId, HospitalId, ReviewStatus, CPatientName, DiagnoseDoctor);
        }
        //public List<Study> GetSearchStudy(string idorname, DateTime datetime1, DateTime datetime2, string patientId)
        //{
        //    return dbContext.GetSearchStudy(idorname, datetime1, datetime2, patientId);
        //}

        /// <summary>
        /// 查询未缴费患者列表
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <param name="name"></param>
        /// <param name="patientId"></param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        public List<RequestStudy> GetRequestStudyList(string hospitalID, string name, string patientId, int? costState, DateTime dtStart, DateTime dtEnd)
        {
            return dbContext.GetRequestStudyList(hospitalID, name, patientId, costState, dtStart, dtEnd);
        }

        //public List<Study> GetSearchStudyAPP(string idorname, DateTime datetime1, DateTime datetime2, string patientId, string HospitalId)
        //{
        //    return dbContext.GetSearchStudyAPP(idorname, datetime1, datetime2, patientId, HospitalId);
        //}

        ///// <summary>
        ///// 同步数据请勿修改
        ///// </summary>
        ///// <param name="strPatientId"></param>
        ///// <param name="strAccessNumber"></param>
        ///// <param name="strStudyInstanceUid"></param>
        ///// <returns></returns>
        //public List<Study> SearchStudyList(string strPatientId, string strAccessNumber, string strStudyInstanceUid)
        //{
        //    return dbContext.SearchStudyList(strPatientId, strAccessNumber, strStudyInstanceUid);
        //}

        public PatientInfo GetOnePatientInfo(string downloadT_ID)
        {
            return dbContext.GetOnePatientInfo(downloadT_ID);
        }

        #endregion

        #region 上传文件
        public bool CreateFile(string StrPath, string fileName)
        {
            bool isCreate = true;
            try
            { //StrPath + filename;//
                fileName = Path.Combine(StrPath + Path.GetFileName(fileName));// Path.Combine(ServerPath.SysValue + Path.GetFileName(fileName));
                FileStream fs = new FileStream(fileName, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
                fs.Close();
            }
            catch
            {
                isCreate = false;
            }
            return isCreate;
        }

        public bool Append(string StrPath, string fileName, byte[] buffer)
        {
            bool isAppend = true;
            try
            {
                fileName = Path.Combine(StrPath + Path.GetFileName(fileName));// Path.Combine(ServerPath.SysValue + Path.GetFileName(fileName));
                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
                fs.Seek(0, SeekOrigin.End);
                fs.Write(buffer, 0, buffer.Length);
                fs.Close();
            }
            catch
            {
                isAppend = false;
            }
            return isAppend;
        }

        public bool Verify(string StrPath, string fileName, string md5)
        {
            bool isVerify = true;
            try
            {

                fileName = Path.Combine(StrPath + Path.GetFileName(fileName));// Path.Combine(ServerPath.SysValue + Path.GetFileName(fileName));
                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
                MD5CryptoServiceProvider p = new MD5CryptoServiceProvider();
                byte[] md5buffer = p.ComputeHash(fs);
                fs.Close();
                string md5Str = "";
                List<string> strList = new List<string>();
                for (int i = 0; i < md5buffer.Length; i++)
                {
                    md5Str += md5buffer[i].ToString("x2");
                }
                if (md5 != md5Str)
                    isVerify = false;
            }
            catch
            {
                isVerify = false;
            }
            return isVerify;
        }
        #endregion

        #region 下载文件
        public byte[] Down(string StrPath, string filename)
        {
            string filepath = StrPath + filename;//ServerPath.SysValue + filename;
            if (File.Exists(filepath))
            {
                try
                {
                    FileStream s = File.OpenRead(filepath);
                    byte[] data = new byte[s.Length];
                    s.Read(data, 0, data.Length);
                    s.Close();
                    return data;
                }
                catch
                {
                    return new byte[0];
                }
            }
            else
            {
                return new byte[0];
            }
        }

        public int GetSplitCountByFileName(string StrPath, string filename, int SplitByte)
        {
            string filepath = StrPath + filename;
            if (File.Exists(filepath))
            {
                try
                {
                    FileStream s = new System.IO.FileStream(filepath, System.IO.FileMode.Open, System.IO.FileAccess.Read, FileShare.ReadWrite);
                    long SplitCount = s.Length / SplitByte;
                    if (s.Length % SplitByte != 0)
                    {
                        SplitCount += 1;
                    }
                    s.Close();
                    return Convert.ToInt32(SplitCount);
                }
                catch (Exception ex)
                {
                    LogHelper.WriteErrorLog("GetSplitCountByFileName", ex.Message);
                    return -1;
                }
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// 服务器上保存图片
        /// </summary>
        /// <param name="savePath">保存路径</param>
        /// <param name="filePath">文件路径</param>
        /// <param name="fileName">文件名</param>
        public void CreatServiceFile(string savePath, string filePath, string fileName)
        {
            try
            {
                LogHelper.WriteInfoLog("保存路径：" + savePath + "--文件路径：" + filePath);
                int SplitByte = 1024 * 128;
                FileStream fs = File.Create(savePath + fileName);
                int SplitCount = GetSplitCountByFileName(filePath, fileName, SplitByte);
                for (int split = 0; split < SplitCount; split++)
                {
                    byte[] GetByte = DownFileByFileIdAndSplitBype(filePath, fileName, SplitByte, split);
                    fs.Write(GetByte, 0, GetByte.Length);
                }
                fs.Close();
            }
            catch (Exception ex)
            {
                LogHelper.WriteErrorLog("CreatServiceFile" + savePath, ex.Message);
            }
        }
        public byte[] DownFileByFileIdAndSplitBype(string StrPath, string filename, int SplitBype, int SplitPage)
        {

            string filepath = StrPath + filename;// ServerPath.SysValue + filename;
            if (File.Exists(filepath))
            {
                try
                {
                    FileStream s = new System.IO.FileStream(filepath, System.IO.FileMode.Open, System.IO.FileAccess.Read, FileShare.ReadWrite);
                    //FileStream s = File.OpenRead(filepath);
                    s.Position = SplitBype * SplitPage;
                    int lastSplit = Convert.ToInt32(s.Length % SplitBype);
                    byte[] data = new byte[SplitBype];
                    if ((lastSplit != 0) && (GetSplitCountByFileName(StrPath, filename, SplitBype) - 1) == SplitPage)
                    {
                        data = new byte[lastSplit];
                    }
                    s.Read(data, 0, data.Length);
                    s.Close();
                    return data;
                }
                catch (Exception ex)
                {
                    LogHelper.WriteErrorLog("DownFileByFileIdAndSplitBype", ex.Message);
                    return new byte[0];
                }
            }
            else
            {
                return new byte[0];
            }
        }
        #endregion

        #region  目录菜单维护
        public ExcuteModel SaveDiagnoseType_Name(DiagnoseType model)
        {
            return dbContext.SaveDiagnoseTypeName(model);
        }
        public List<DiagnoseType> GetAllDiagnoseType()
        {
            return dbContext.GetAllDiagnoseType();
        }
        public ExcuteModel DelDiagnoseType_Name(DiagnoseType model)
        {
            return dbContext.DelDiagnoseTypeName(model);
        }
        public DiagnoseType GetONEDiagnoseType(string DiagnosePart_ID)
        {
            return dbContext.GetONEDiagnoseType(DiagnosePart_ID);
        }
        public ExcuteModel judge_Diagnosetype_name(DiagnoseType model)
        {
            return dbContext.JudgeDiagnoseTypeName(model);
        }
        #region 报告模板维护
        /// <summary>
        /// 模板操作
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel SaveDiagnoseTemp(DiagnoseTemp param)
        {
            ExcuteModel retModel = new ExcuteModel();
            retModel = dbContext.SaveDiagnoseTemp(param);
            return retModel;
        }
        /// <summary>
        /// 保存模板内容信息
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel SaveDiagnoseTempContent(DiagnoseTemp param)
        {
            ExcuteModel retModel = new ExcuteModel();
            retModel = dbContext.SaveDiagnoseTempContent(param);
            return retModel;
        }
        /// <summary>
        /// 删除节点
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel DelDiagnoseTempNode(DiagnoseTemp param)
        {
            ExcuteModel retModel = new ExcuteModel();
            retModel = dbContext.DelDiagnoseTempNode(param);
            return retModel;
        }
        /// <summary>
        /// 获取单个模板内容
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public DiagnoseTemp GetDiagnoseTemp(DiagnoseTemp param)
        {
            DiagnoseTemp di = dbContext.GetDiagnoseTemp(param);
            return di;
        }
        /// <summary>
        /// 获取相应值的最大结果
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public int GetParentOrderbyValue(int param, string strName)
        {
            int getValue = dbContext.GetParentOrderbyValue(param, strName);
            return getValue;
        }
        /// <summary>
        /// 获取父节点值
        /// </summary>
        /// <returns></returns>
        public int GetPatientIdValue()
        {
            return dbContext.GetPatientIdValue();
        }
        /// <summary>
        /// 判断是否有根节点
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public int GetPatientNodeCout(int param)
        {
            return dbContext.GetPatientNodeCout(param);
        }
        /// <summary>
        /// 查询主节点和二级节点，排序为降序的公共模板
        /// </summary>
        /// <returns></returns>
        public List<DiagnoseTemp> GetDiagnoseTempPatient(int isPublic, string userName)
        {
            return dbContext.GetDiagnoseTempPatient(isPublic, userName);
        }
        /// <summary>
        /// 获取单个节点数据
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<DiagnoseTemp> GetDiagnoseTempNodeData(int param, int nodeTypeParam, int nodeOrderParam, int isPublic, string userName)
        {
            return dbContext.GetDiagnoseTempNodeData(param, nodeTypeParam, nodeOrderParam, isPublic, userName);
        }
        /// <summary>
        /// 获取自动编号
        /// </summary>
        /// <returns></returns>
        public int GetDiagnoseTempRootID()
        {
            return dbContext.GetDiagnoseTempRootID();
        }
        /// <summary>
        /// 获取父节点排序值，为查询做准备
        /// </summary>
        /// <returns></returns>
        public int GetParentNodeOrderValue()
        {
            return dbContext.GetParentNodeOrderValue();
        }
        /// <summary>
        /// 获取DiagnsoeTemp表数据总数
        /// </summary>
        /// <returns></returns>
        public int GetDiagnoseTempCount()
        {
            return dbContext.GetDiagnoseTempCount();
        }
        /// <summary>
        /// 拖动节点修改值
        /// </summary>
        /// <param name="rootID"></param>
        /// <param name="nodeTypes"></param>
        /// <param name="pRootID"></param>
        /// <param name="pNodeTypes"></param>
        /// <param name="pNodeOrder"></param>
        /// <param name="strTempName"></param>
        public void UpdateDiagnoseTemp(int rootID, int nodeTypes, int pRootID, int pNodeTypes, int pNodeOrder, string strTempName)
        {
            dbContext.UpdateDiagnoseTemp(rootID, nodeTypes, pRootID, pNodeTypes, pNodeOrder, strTempName);
        }
        #endregion

        #endregion

        /// <summary>
        /// 获取申请单列表
        /// </summary>
        /// <param name="Type"></param>
        /// <returns></returns>
        public List<OutRqeustInfo> GetRequestList(string Type, string HospitalId, string FeeState, DateTime dtStart, DateTime dtEnd, string PatientName, string PatientId, int smallAge, int bigAge, string Sex, string accessionNumber = null, string RequestDoctor = "", string requestJDR = null, string PatientType = "", string ExamineType = "")
        {
            return dbContext.GetRequestList(Type, HospitalId, FeeState, dtStart, dtEnd, PatientName, PatientId, smallAge, bigAge, Sex, accessionNumber, RequestDoctor, requestJDR, PatientType, ExamineType);
        }
        public List<OutRqeustInfo> GetRequestList_DSH(string Type, string HospitalId, string FeeState, DateTime dtStart, DateTime dtEnd, string PatientName, string PatientId, string docator_name)
        {
            return dbContext.GetRequestList_DSH(Type, HospitalId, FeeState, dtStart, dtEnd, PatientName, PatientId, docator_name);
        }
        public List<OutRqeustInfo> GetRequestList_YSH(string Type, string HospitalId, string FeeState, DateTime dtStart, DateTime dtEnd, string PatientName, string PatientId, string docator_name)
        {
            return dbContext.GetRequestList_YSH(Type, HospitalId, FeeState, dtStart, dtEnd, PatientName, PatientId, docator_name);
        }
        /// <summary>
        /// 获取待审核病人信息
        /// </summary>
        /// <param name="HospitalId"></param>
        /// <param name="PatientName"></param>
        /// <param name="PatientId"></param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <returns></returns>
        public List<OutRqeustInfo> GetPatient_CheckingDetail(string Type, string HospitalId, string PatientName, string PatientId, int ReviwState, DateTime dtStart, DateTime dtEnd, int Age = 0, string Sex = "", string roleType = "", string RequestDcotor = "")
        {
            return dbContext.GetPatient_CheckingDetail(Type, HospitalId, PatientName, PatientId, ReviwState, dtStart, dtEnd, Age, Sex, roleType, RequestDcotor);
        }
        public List<OutRqeustInfo> GetPatient_CheckingDetail_bg(string Type, string HospitalId, string PatientName, string PatientId, int ReviwState, DateTime dtStart, DateTime dtEnd, int Age = 0, string Sex = "", string roleType = "", string RequestDcotor = "")
        {
            return dbContext.GetPatient_CheckingDetail_bg(Type, HospitalId, PatientName, PatientId, ReviwState, dtStart, dtEnd, Age, Sex, roleType, RequestDcotor);
        }
        /// <summary>
        /// 获取缴费信息
        /// </summary>
        /// <param name="Type"></param>
        /// <param name="HospitalId"></param>
        /// <param name="FeeState"></param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <param name="PatientName"></param>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        public List<OutPayInfo> GetPayInfoList(string Type, string HospitalId, string FeeState, DateTime dtStart, DateTime dtEnd, string PatientName, string PatientId)
        {
            return dbContext.GetPayInfoList(Type, HospitalId, FeeState, dtStart, dtEnd, PatientName, PatientId);
        }
        public List<OutRqeustInfo> GetPayInfoList_alllist(string Type, string HospitalId, string FeeState, DateTime dtStart, DateTime dtEnd, string PatientName, string PatientId)
        {
            return dbContext.GetPayInfoList_alllist(Type, HospitalId, FeeState, dtStart, dtEnd, PatientName, PatientId);
        }
        /// <summary>
        /// 查询中心推荐、指定专家、已写报告会诊
        /// </summary>
        /// <param name="PatientName">病人姓名</param>
        /// <param name="ChooseType">类型</param>
        /// <param name="strDoctor">专家</param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <returns></returns>
        public List<OutRqeustInfo> GetPatient_ConsultationDetail(string PatientName, string PatientId, int ChooseType, string strDoctor, DateTime dtStart, DateTime dtEnd, int Age = 0, string Sex = null)
        {
            return dbContext.GetPatient_ConsultationDetail(PatientName, PatientId, ChooseType, strDoctor, dtStart, dtEnd, Age, Sex);
        }
        /// <summary>
        /// 统计会诊数据量
        /// </summary>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <returns></returns>
        public List<OutRqeustInfo> GetConsulationStatistics(string HospitalID, DateTime dtStart, DateTime dtEnd)
        {
            return dbContext.GetConsulationStatistics(HospitalID, dtStart, dtEnd);
        }
        /// <summary>
        /// 获取申请、诊断或会诊患者数量
        /// </summary>
        /// <param name="strDoctor"></param>
        /// <param name="strHospitalID"></param>
        /// <param name="strConsulation"></param>
        /// <returns></returns>
        public int GetPatientCounts(string strDoctor, string strHospitalID, string strConsulation)
        {
            return dbContext.GetPatientCounts(strDoctor, strHospitalID, strConsulation);
        }
        public List<OutRqeustInfo> GetPatient_ConsultationDetail_allList(string state, string PatientName, string PatientId, int ChooseType, string strDoctor, DateTime dtStart, DateTime dtEnd, int Age = 0, string Sex = null)
        {
            return dbContext.GetPatient_ConsultationDetail_allList(state, PatientName, PatientId, ChooseType, strDoctor, dtStart, dtEnd, Age, Sex);
        }
        /// <summary>
        /// 获取诊断患者信息列表
        /// </summary>
        /// <param name="RequestStateID"></param>
        /// <param name="HospitalId"></param>
        /// <param name="PatientName"></param>
        /// <param name="PatientId"></param>
        /// <param name="ReviwState"></param>
        /// <param name="PageIndex"></param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <param name="Age"></param>
        /// <param name="Sex"></param>
        /// <param name="RequestDcotor"></param>
        /// <param name="pageCount"></param>
        /// <param name="patientCount"></param>
        /// <returns></returns>
        public List<OutRqeustInfo> GetDiagnosePatientInfoList(string RequestStateID, string HospitalId, string PatientName, string PatientId, int ReviwState, int PageIndex, DateTime dtStart, DateTime dtEnd, int smallAge, int bigAge, string Sex, string RequestDcotor, out int pageCount, out int patientCount)
        {
            return dbContext.GetDiagnosePatientInfoList(RequestStateID, HospitalId, PatientName, PatientId, ReviwState, PageIndex, dtStart, dtEnd, smallAge, bigAge, Sex, RequestDcotor, out pageCount, out patientCount);
        }

        public List<OutRqeustInfo> GetDiagnosePatientBaseList(string requestState, string hospitalId, string hospitalSon, string patientName, string patientId, int reviwState, int pageIndex, DateTime dtStart, DateTime dtEnd, int smallAge, int bigAge, string Sex, string requestDcotor, string diagnoseDoctor, string examineType, out int pageCount, out int patientCount)
        {
            return dbContext.GetDiagnosePatientBaseList(requestState, hospitalId, hospitalSon, patientName, patientId, reviwState, pageIndex, dtStart, dtEnd, smallAge, bigAge, Sex, requestDcotor, diagnoseDoctor, examineType, out pageCount, out patientCount);
        }
        /// <summary>
        /// 获取会诊患者信息列表
        /// </summary>
        /// <param name="PatientName"></param>
        /// <param name="PatientId"></param>
        /// <param name="HospitalID"></param>
        /// <param name="PageIndex"></param>
        /// <param name="ChooseType"></param>
        /// <param name="strDoctor"></param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <param name="Age"></param>
        /// <param name="Sex"></param>
        /// <param name="pageCount"></param>
        /// <param name="patientCount"></param>
        /// <returns></returns>
        public List<OutRqeustInfo> GetConsulationPatientInfoList(string PatientName, string PatientId, string HospitalID, int PageIndex, int ChooseType, string strDoctor, DateTime dtStart, DateTime dtEnd, int smallAge, int bigAge, string Sex, out int pageCount, out int patientCount)
        {
            return dbContext.GetConsulationPatientInfoList(PatientName, PatientId, HospitalID, PageIndex, ChooseType, strDoctor, dtStart, dtEnd, smallAge, bigAge, Sex, out pageCount, out patientCount);
        }

        /// <summary>
        /// 报告端数据查询
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<OutRqeustInfo> GetDiagnoseBaseList(InBaseParam param)
        {
            return dbContext.GetDiagnoseBaseList(param);
        }
        /// <summary>
        /// 获取子表记录信息
        /// </summary>
        /// <param name="requestidParam"></param>
        /// <returns></returns>
        public List<StudyRecordInfo> GetStudyRecordInfoList(string requestidParam)
        {
            return dbContext.GetStudyRecordInfoList(requestidParam);
        }
        /// <summary>
        /// 查询子列表数据
        /// </summary>
        /// <param name="requestParam"></param>
        /// <returns></returns>
        public List<StudyRecordInfo> GetRequestStudyRecordAllList(string requestParam)
        {
            return dbContext.GetRequestStudyRecordAllList(requestParam);
        }
        /// <summary>
        /// 根据PatientId 获取病人信息
        /// </summary>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        public PatientInfo GetPatientInfoByPatientId(string PatientId)
        {
            return dbContext.GetPatientInfoByPatientId(PatientId);
        }
        /// <summary>
        /// 修改PatientId
        /// </summary>
        /// <param name="newPatientId"></param>
        /// <param name="oldPatientId"></param>
        /// <returns></returns>
        public ExcuteModel UpdatePatientId(string newPatientId, string oldPatientId)
        {
            return dbContext.UpdatePatientId(newPatientId, oldPatientId);
        }
        /// <summary>
        /// 根据RequestId获取申请单记录
        /// </summary>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        public RequestInfo GetRequestInfoByRequestId(string RequestId)
        {
            return dbContext.GetRequestInfoByRequestId(RequestId);
        }
        public List<RequestInfoStatis> GetRequestStudyRecordByRequestId(string RequestId)
        {
            return dbContext.GetRequestStudyRecordByRequestId(RequestId);
        }
        /// <summary>
        /// 在保存患者信息时，判断同一患者是否连续上传
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<RequestInfo> GetRequestInfoListByStudyInstance(string hospital, string param)
        {
            return dbContext.GetRequestInfoListByStudyInstance(hospital, param);
        }
        /// <summary>
        /// 错误方法
        /// </summary>
        /// <param name="RequestId"></param>
        /// <param name="StudyId"></param>
        /// <returns></returns>
        public RequestInfo GetRequestInfoByRequestIdOrStudyId(string RequestId, string StudyId)
        {
            return dbContext.GetRequestInfoByRequestId(RequestId, StudyId);
        }
        /// <summary>
        /// 获取同一个人的历史申请单
        /// </summary>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        public List<RequestInfo> GetRequestInfoByPatientId(string PatientId)
        {
            return dbContext.GetRequestInfoByPatientId(PatientId);
        }
        /// <summary>
        /// 保存 RequestInfo
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveRequestInfo(RequestInfo model)
        {
            return dbContext.SaveRequestInfo(model);
        }
        /// <summary>
        /// 保存并更新诊断时间
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveUpdateRequestInfo(RequestInfo model)
        {
            return dbContext.SaveUpdateRequestInfo(model);
        }

        /// <summary>
        /// 修改打印状态
        /// </summary>
        /// <param name="RequestID"></param>
        /// <returns></returns>
        public ExcuteModel UpdateRequestPrintState(string RequestID)
        {
            return dbContext.UpdateRequestPrintState(RequestID);
        }
        public PatientInfo GetPatientInfoByZJHM(string ZJLX, string ZJHM)
        {
            return dbContext.GetPatientInfoByZJHM(ZJLX, ZJHM);
        }
        /// <summary>
        /// 保存 PatientInfo
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SavePatientInfo(PatientInfo model)
        {
            return dbContext.SavePatientInfo(model);
        }
        /// <summary>
        /// 修改患者基本信息
        /// 只做修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdatePatientInfo(PatientInfo model)
        {
            return dbContext.UpdatePatientInfo(model);
        }
        /// <summary>
        /// 申请会诊
        /// </summary>
        /// <param name="InModel"></param>
        /// <returns></returns>
        public ExcuteModel RequestSubmit(InRequest InModel)
        {
            return dbContext.RequestSubmit(InModel);
        }
        /// <summary>
        /// 保存 RequestInfo 更新申请单状态
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel ChangeRequestState(InRequestReport InModel)
        {
            return dbContext.ChangeRequestState(InModel);
        }
        /// <summary>
        /// 获取申请单状态
        /// </summary>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        public int GetRequestState(string RequestId)
        {
            return dbContext.GetRequestState(RequestId);
        }
        /// <summary>
        /// 保存报告
        /// </summary>
        /// <param name="InModel"></param>
        /// <returns></returns>
        public ExcuteModel SaveDiagnoseReport(InDianose InModel)
        {
            return dbContext.SaveDiagnoseReportNew(InModel);
        }
        /// <summary>
        /// 获取报告诊断
        /// </summary>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        public Diagnose GetDianoseByRequestId(string RequestId)
        {
            return dbContext.GetDianoseByRequestId(RequestId);
        }
        /// <summary>
        /// 更新报告状态
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel ChangeDiagnoseState(Diagnose model)
        {
            return dbContext.ChangeDiagnoseState(model);
        }
        /// <summary>
        /// 获取诊断报告内容
        /// </summary>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        public Diagnose GetDiagnoseReportByRequesId(string RequestId)
        {
            return dbContext.GetDiagnoseReportByRequesId(RequestId);
        }
        /// <summary>
        /// 缴费
        /// </summary>
        /// <param name="InModel"></param>
        /// <returns></returns>
        public ExcuteModel Charge(InChage InModel)
        {
            return dbContext.Charge(InModel);
        }
        /// <summary>
        /// 修改需要会诊的医生
        /// </summary>
        /// <param name="RequestId"></param>
        /// <param name="DoctorName"></param>
        /// <param name="ServiceId">服务项目ID可选填</param>
        /// <returns></returns>
        public ExcuteModel ChangeDianoseDoctor(string RequestId, string DoctorName, string ServiceId = "")
        {
            return dbContext.ChangeDianoseDoctor(RequestId, DoctorName, ServiceId);
        }
        #region
        public ExcuteModel AddRequestImageViewInfo(RequestImageView param)
        {
            return dbContext.AddRequestImageViewInfo(param);
        }

        /// <summary>
        /// 查看是否浏览影像
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ExcuteModel IsExistRequestImageView(string requestId, string userId)
        {
            return dbContext.IsExistRequestImageView(requestId, userId);
        }
        #endregion

        #region 费用

        /// <summary>
        /// 保存会诊申请服务和费用信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveRequestServiceCost(RequestInfo model)
        {
            return dbContext.SaveRequestServiceCost(model);
        }
        #endregion

        #region 病人文件
        public ExcuteModel SaveRequestFileRoot(RequestFileRoot model)
        {
            return dbContext.SaveRequestFileRoot(model);
        }
        public List<RequestFileRoot> GetRequestFileRootByRequestIdAndFileRoot(string RequestId, int FileRoot)
        {
            return dbContext.GetRequestFileRootByRequestIdAndFileRoot(RequestId, FileRoot);
        }

        /// <summary>
        /// 获取病人所有病例文件
        /// </summary>
        /// <param name="RequestId"></param>
        /// <returns></returns>        
        public List<RequestFileItem> GetAllRequestFileItems(string RequestId, string StudyInstanceUID)
        {
            return dbContext.GetAllRequestFileItems(RequestId, StudyInstanceUID);
        }

        /// <summary>
        /// 根据RequestId,获取病历所有信息
        /// </summary>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        public List<RequestFileRoot> GetRequestFileRootByRequestIdAll(string RequestId)
        {
            return dbContext.GetRequestFileRootByRequestIdAll(RequestId);
        }
        /// <summary>
        /// 根据RequestId,获取所有文件信息
        /// </summary>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        public List<RequestFile> GetRequestFileByRequestIdAll(string RequestId)
        {
            return dbContext.GetRequestFileByRequestIdAll(RequestId);
        }
        public List<RequestFile> GetAllFileListByRequestId(string RequestId, int type)
        {
            return dbContext.GetAllFileListByRequestId(RequestId, type);
        }
        public ExcuteModel SaveFileItem(RequestFileItem model)
        {
            return dbContext.SaveFileItem(model);
        }
        public List<RequestFileItem> GetFileItemsBySource(int Source, string RequestId)
        {
            return dbContext.GetFileItemsBySource(Source, RequestId);
        }
        public List<RequestFile> GetDicomFileByRequestIdAndMPID(string RequestId, string MPID)
        {
            return dbContext.GetDicomFileByRequestIdAndMPID(RequestId, MPID);
        }
        public RequestFile GetRequestFileByMPID(string RequestId, string MPID)
        {
            return dbContext.GetRequestFileByMPID(RequestId, MPID);
        }
        public List<RequestFileItem> GetFileItemsByFileId(string FileId)
        {
            return dbContext.GetFileItemsByFileId(FileId);
        }
        public List<RequestFileItem> GetFileItemsByFileRootId(string FileRootId)
        {
            return dbContext.GetFileItemsByFileRootId(FileRootId);
        }
        public List<RequestFileItem> GetFileItemsByRequestId(string RequestId)
        {
            return dbContext.GetFileItemsByRequestId(RequestId);
        }
        /// <summary>
        /// 获取数据不为9的所有信息
        /// </summary>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        public List<RequestFileItem> GetNoFileItemsByRequestId(string RequestId, string StudyUid = null)
        {
            return dbContext.GetNoFileItemsByRequestId(RequestId, StudyUid);
        }
        public ExcuteModel SaveFile(RequestFile model)
        {
            return dbContext.SaveFile(model);
        }
        public List<RequestFile> GetFileBySource(int Source, string RequestId)
        {
            return dbContext.GetFileBySource(Source, RequestId);
        }
        public ExcuteModel DeleteFile(string FileId)
        {
            return dbContext.DeleteFile(FileId);
        }
        /// <summary>
        /// 删除单个文件
        /// </summary>
        /// <param name="FileItemId"></param>
        /// <returns></returns>
        public ExcuteModel DeleteFileItemByItemId(string FileItemId)
        {
            return dbContext.DeleteFileItemByItemId(FileItemId);
        }
        #endregion

        #region 驳回
        public ExcuteModel SaveRefusedRecord(RefusedRecord Model)
        {
            return dbContext.SaveRefusedRecord(Model);
        }
        public List<RefusedRecord> GetRefusedRecordByRequestId(string RequestId)
        {
            return dbContext.GetRefusedRecordByRequestId(RequestId);
        }
        #endregion

        #region 系统配置
        /// <summary>
        /// 获取系统配置
        /// </summary>
        /// <returns></returns>
        public Sys_Settings GetSysValueByKey(string Key)
        {
            return DAL.GetSysValueByKey(Key);
        }

        public List<Sys_Settings> GetSysList()
        {
            return DAL.GetSysList();
        }

        public ExcuteModel SaveSys_Settings(Sys_Settings InModel)
        {
            return DAL.SaveSys_Settings(InModel);
        }

        public ExcuteModel DeleteSys_Settings(Sys_Settings model)
        {
            return DAL.DeleteSys_Settings(model);
        }

        #endregion

        #region   专家会诊记录
        public List<ExpertConsultationRecord> ExpertRecord(string ServiceId, string dept_id, string DiagnoseDoctor, string txtname)
        {

            return dbContext.ExpertRecord(ServiceId, dept_id, DiagnoseDoctor, txtname);
        }

        public List<ExpertConsultationRecord> Search_ExpertRecord(string ServiceId, string dept_id)
        {
            return null;
        }
        #endregion

        #region 上传和下载标记
        /// <summary>
        /// 保存和更新数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveFileUpDownRecordDetail(FileUpDownRecord model)
        {
            return dbContext.SaveFileUpDownRecordDetail(model);
        }

        /// <summary>
        /// 获取上传下载记录信息
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="upLoadPerson"></param>
        /// <param name="downPerson"></param>
        /// <returns></returns>
        public List<FileUpDownRecord> GetFileUpDownRecordByRequestId(string requestId, string hospitalID, string upLoadPerson, string downPerson)
        {
            return dbContext.GetFileUpDownRecordByRequestId(requestId, hospitalID, upLoadPerson, downPerson);
        }

        /// <summary>
        /// 获取云端已上传患者
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="state"></param>
        /// <param name="dtStar"></param>
        /// <param name="dtEnd"></param>
        /// <returns></returns>
        public List<FileUpDownRecord> GetFileUpDownRecordByIDMList(string requestId, string hospitalID, int? uploadFlag, string state, DateTime dtStar, DateTime dtEnd)
        {
            return dbContext.GetFileUpDownRecordByIDMList(requestId, hospitalID, uploadFlag, state, dtStar, dtEnd);
        }
        #endregion

        #region 更新和关闭报告状态
        /// <summary>
        /// 正常关闭报告状态
        /// </summary>
        /// <param name="requestID"></param>
        /// <returns></returns>
        public void NormalRequestInfoReportState(string requestID)
        {
            dbContext.NormalRequestInfoReportState(requestID);
        }

        /// <summary>
        /// 更新报告状态
        /// </summary>
        /// <param name="requestID"></param>
        /// <param name="reportPerson"></param>
        /// <returns></returns>
        public ExcuteModel UpdateRequestInfoReportState(string requestID, string reportPerson)
        {
            return dbContext.UpdateRequestInfoReportState(requestID, reportPerson);
        }

        /// <summary>
        /// 获取诊断报告列表信息
        /// </summary>
        /// <param name="PatientId"></param>
        /// <param name="RequestState"></param>
        /// <param name="ReviewState"></param>
        /// <param name="RequestId"></param>
        /// <param name="DtStart"></param>
        /// <param name="DtEnd"></param>
        /// <returns></returns>
        public List<OutDiagnoseReport> GetDiagnoseReportList(string PatientId, string HospitalId, string RequestId, string RequestState, int ReviewState, DateTime DtStart, DateTime DtEnd)
        {
            return dbContext.GetDiagnoseReportList(PatientId, HospitalId, RequestId, RequestState, ReviewState, DtStart, DtEnd);
        }

        #endregion
        /// <summary>
        /// 获取数据库连接
        /// </summary>
        /// <returns></returns>
        public string GetConnStr()
        {
            try
            {
                var conn = ConfigurationManager.ConnectionStrings["InsideRISConnectionString"].ToString();
                return conn;
            }
            catch (Exception)
            {
                return "";
            }
        }

        /// <summary>
        /// 删除文件
        /// </summary>
        /// <param name="fileName"></param>
        public void DeleteFiles(string fileName)
        {
            try
            {
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }
            }
            catch { }
        }

        /// <summary>
        /// 删除文件夹
        /// </summary>
        /// <param name="pathParam"></param>
        public void DeleteDir(string pathParam)
        {
            if (Directory.Exists(pathParam))
            {
                try
                {
                    DirectoryInfo dir = new DirectoryInfo(pathParam);
                    FileSystemInfo[] fileinfo = dir.GetFileSystemInfos();  //返回目录中所有文件和子目录
                    foreach (FileSystemInfo i in fileinfo)
                    {
                        if (i is DirectoryInfo)            //判断是否文件夹
                        {
                            DirectoryInfo subdir = new DirectoryInfo(i.FullName);
                            subdir.Delete(true);          //删除子目录和文件
                        }
                        else
                        {
                            File.Delete(i.FullName);      //删除指定文件
                        }
                    }
                }
                catch (Exception e)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// 保存同步记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveSynRecord(SynRecord model)
        {
            return dbContext.SaveSynRecord(model);
        }

        /// <summary>
        /// 获取同步记录
        /// </summary>
        /// <param name="HospitalId"></param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <param name="PatientId"></param>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        public List<SynRecord> GetSynRecordList(string HospitalId, DateTime dtStart, DateTime dtEnd, string PatientId, string RequestId)
        {
            return dbContext.GetSynRecordList(HospitalId, dtStart, dtEnd, PatientId, RequestId);
        }

        /// <summary>
        /// 修改状态
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateSynRecord(SynRecord Model)
        {
            return dbContext.UpdateSynRecord(Model);
        }

        #region 拉取患者表信息
        /// <summary>
        /// 保存已拉取患者数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SavePullPatient(PullPatient model)
        {
            return dbContext.SavePullPatient(model);
        }

        /// <summary>
        /// 获取拉取患者信息
        /// </summary>
        /// <param name="strRequestID">申请ID</param>
        /// <param name="strPatientID">患者ID</param>
        /// <param name="starDate">开始时间</param>
        /// <param name="endDate">结束时间</param>
        /// <returns></returns>
        public List<PullPatient> GetPullPatientList(string strRequestID, string strPatientID)
        {
            return dbContext.GetPullPatientList(strRequestID, strPatientID);
        }

        public void DeleteFileInfo(string path, string name, string pwd, string domain)
        {

            if (path != null && !path.Equals(""))
            {
                Process process = new Process();//创建进程对象  
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = "cmd.exe";//设定需要执行的命令  
                startInfo.Arguments = "/C " + path;//“/C”表示执行完命令后马上退出  
                startInfo.UseShellExecute = false;//不使用系统外壳程序启动 
                startInfo.RedirectStandardInput = false;//不重定向输入  
                startInfo.RedirectStandardOutput = true; //重定向输出  
                startInfo.CreateNoWindow = true;//不创建窗口  
                process.StartInfo = startInfo;
                try
                {
                    if (process.Start())//开始进程  
                    {
                        if (0 == 0)
                        {
                            process.WaitForExit();//这里无限等待进程结束  
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);//捕获异常，输出异常信息
                }
                finally
                {
                    if (process != null)
                    {
                        process.Close();
                    }
                }
            }
        }
        #endregion

        #region 记录手机端上传影像
        /// <summary>
        /// 保存和更新上传影像数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveUpDicomRecordDetail(UpDicomRecord model)
        {
            return dbContext.SaveUpDicomRecordDetail(model);
        }

        /// <summary>
        /// 获取影像信息
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="upLoadPerson"></param>
        /// <param name="downPerson"></param>
        /// <returns></returns>
        public List<UpDicomRecord> GetUpDicomRecordByRequestId(string requestId, DateTime dtStar, DateTime dtEnd)
        {
            return dbContext.GetUpDicomRecordByRequestId(requestId, dtStar, dtEnd);
        }

        /// <summary>
        /// 获取影像库中是否有数据
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="studyInstanceUID"></param>
        /// <returns></returns>
        public int GetUpDicomRecordCountByRequestId(string requestId, string studyInstanceUID)
        {
            return dbContext.GetUpDicomRecordCountByRequestId(requestId, studyInstanceUID);
        }
        #endregion

        #region 同步ImageServer库数据
        ////保存ServerPartition表
        //public ExcuteModel SaveServerPartitionBase(ServerPartition model)
        //{
        //    return dbContext.SaveServerPartitionBase(model);
        //}

        //public List<ServerPartition> GetServerPartitionAllList()
        //{
        //    return dbContext.GetServerPartitionAllList();
        //}

        ////获取患者基本信息
        //public Patient GetPatientBase(Patient model)
        //{
        //    return dbContext.GetPatientBase(model);
        //}

        ////保存Patient基本信息
        //public ExcuteModel SavePatientBase(Patient model)
        //{
        //    return dbContext.SavePatientBase(model);
        //}

        ////查询StudyStorage基本信息
        //public StudyStorage GetStudyStorageBase(StudyStorage model)
        //{
        //    return dbContext.GetStudyStorageBase(model);
        //}

        ////保存 StudyStorage基本信息
        //public ExcuteModel SaveStudyStorageBase(StudyStorage model)
        //{
        //    return dbContext.SaveStudyStorageBase(model);
        //}

        ////查询Series基本信息
        //public List<Series> GetSeriesStudyList(Series model)
        //{
        //    return dbContext.GetSeriesStudyList(model);
        //}

        ////保存Series基本信息
        //public ExcuteModel SaveSeriesStudyBase(Series model)
        //{
        //    return dbContext.SaveSeriesStudyBase(model);
        //}

        ////查询Study基本信息
        //public List<Study> GetStudyBaseList(Study model, DateTime BeginTime, DateTime EndTime)
        //{
        //    return dbContext.GetStudyBaseList(model, BeginTime, EndTime);
        //}

        ////保存Study基本信息
        //public ExcuteModel SaveStudyBase(Study model)
        //{
        //    return dbContext.SaveStudyBase(model);
        //}

        /// <summary>
        /// 保存未缴费患者信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveRequestStudy(RequestStudy model)
        {
            return null;// dbContext.SaveRequestStudy(model);
        }

        /// <summary>
        /// 更新缴费状态
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateRequestStudyState(RequestStudy model)
        {
            return null;//dbContext.UpdateRequestStudyState(model);
        }
        #endregion

        #region 支付宝后台统计
        /// <summary>
        /// 统计总收入
        /// </summary>
        /// <param name="DateParam"></param>
        /// <returns></returns>
        public List<OutChargeCost> GetPayAllCount(string DateParam)
        {
            return dbContext.GetPayAllCount(DateParam);
        }

        /// <summary>
        /// 各机构总收入
        /// </summary>
        /// <param name="DateParam"></param>
        /// <returns></returns>
        public List<OutChargeCost> GetPayOrganCount(string DateParam)
        {
            return dbContext.GetPayOrganCount(DateParam);
        }

        /// <summary>
        /// 机构收入情况
        /// </summary>
        /// <param name="DateParam"></param>
        /// <returns></returns>
        public List<OutChargeCost> GetPayEveryOrganCount(string DateParam)
        {
            return dbContext.GetPayEveryOrganCount(DateParam);
        }


        /// <summary>
        /// 机构会诊费收入统计
        /// </summary>
        /// <returns></returns>
        public List<OutChargeCost> GetPayConsulOrganAllCount(string DateParam)
        {
            return dbContext.GetPayConsulOrganAllCount(DateParam);
        }

        /// <summary>
        /// 支付方式会诊费收入
        /// </summary>
        /// <param name="DateParam"></param>
        /// <returns></returns>
        public List<OutChargeCost> GetPayConsulAllCount(string DateParam)
        {
            return dbContext.GetPayConsulAllCount(DateParam);
        }

        /// <summary>
        /// 服务项目会诊费收入
        /// </summary>
        /// <param name="DateParam"></param>
        /// <returns></returns>
        public List<OutChargeCost> GetPayConsulServiceAllCount(string DateParam)
        {
            return dbContext.GetPayConsulServiceAllCount(DateParam);
        }

        /// <summary>
        /// 机构统计收费报表
        /// </summary>
        /// <param name="DateParam"></param>
        /// <returns></returns>
        public List<OutChargeCost> GetPayOrganCountReport(string DateParam)
        {
            return dbContext.GetPayOrganCountReport(DateParam);
        }

        /// <summary>
        ///  个人统计收费报表
        /// </summary>
        /// <param name="ServiceId"></param>
        /// <param name="PayType"></param>
        /// <param name="BeginDate"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        public List<OutChargeChart> GetPayPersonCountReport(string ServiceId, string PayType, DateTime BeginDate, DateTime EndDate)
        {
            return dbContext.GetPayPersonCountReport(ServiceId, PayType, BeginDate, EndDate);
        }
        #endregion

        #region 讨论主题

        /// <summary>
        /// 发布主题
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel SaveDiscussionTopic(XR_DiscussionTopic param)
        {
            return dbContext.SaveDiscussionTopic(param);
        }

        /// <summary>
        /// 删除主题
        /// </summary>
        /// <param name="themeID"></param>
        /// <returns></returns>
        public int? DeleteDiscussionTopicByID(int themeID)
        {
            return dbContext.DeleteDiscussionTopicByID(themeID);
        }

        /// <summary>
        /// 发布的主题（我创建的主题、已结束的主题 状态为3）
        /// </summary>
        /// <returns></returns>
        public List<XR_DiscussionTopic> GetDiscussionTopics(XR_DiscussionTopic param)
        {
            return dbContext.GetDiscussionTopics(param);
        }

        /// <summary>
        /// 我参与的主题
        /// </summary>
        /// <returns></returns>
        public List<XR_DiscussionTopic> GetMyAttentionDiscussionTopics(XR_DiscussionTopic param)
        {
            return dbContext.GetMyAttentionDiscussionTopics(param);
        }

        /// <summary>
        /// 我关注的主题
        /// </summary>
        /// <param name="param"></param>
        public ExcuteModel SaveMyAttention(XR_MyAttention param)
        {
            return dbContext.SaveMyAttention(param);
        }
        #endregion

        /// <summary>
        /// </summary>
        /// <param name="Type"></param>
        /// <param name="HospitalId"></param>
        /// <param name="FeeState"></param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <param name="PatientName"></param>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        public List<OutRqeustInfo> GetConDiscussionInfoList(string Type, string HospitalId, string FeeState, string dtStart, string dtEnd, string PatientName, string PatientId, string strUserID = "", string ExaminePosition = "", string ExamineMethod = "", string IdentityNum = "", string Sex = "")
        {
            return dbContext.GetConDiscussionInfoList(Type, HospitalId, FeeState, dtStart, dtEnd, PatientName, PatientId, strUserID, ExaminePosition, ExamineMethod, IdentityNum, Sex);
        }

        /// <summary>
        /// </summary>
        /// <param name="RequestStates"></param>
        /// <param name="HospitalId"></param>
        /// <param name="FeeState"></param>
        /// <param name="PageIndex"></param>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <param name="PatientName"></param>
        /// <param name="PatientId"></param>
        /// <param name="pageCount"></param>
        /// <param name="patientCount"></param>
        /// <returns></returns>
        public List<OutRqeustInfo> GetConDiscussionPatientList(string RequestStates, string HospitalId, string FeeState, string strUserID, int PageIndex, string dtStart, string dtEnd, string PatientName, string PatientId, string strSex, int intAge, out int pageCount, out int patientCount)
        {
            return dbContext.GetConDiscussionPatientList(RequestStates, HospitalId, FeeState, strUserID, PageIndex, dtStart, dtEnd, PatientName, PatientId, strSex, intAge, out pageCount, out patientCount);
        }

        /// <summary>
        /// 保存关键词
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel SaveXRKeywords(XR_Keywords param)
        {
            return dbContext.SaveXRKeywords(param);
        }

        /// <summary>
        /// 获取关键词中的PatientID
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public List<string> GetXRKeywords(XR_Keywords param)
        {
            return dbContext.GetXRKeywords(param);
        }

        /// <summary>
        /// 清除搜索记录
        /// </summary>
        /// <param name="param"></param>
        public ExcuteModel ClearXRKeywords(XR_Keywords param)
        {
            return dbContext.ClearKeywords(param);
        }

        /// <summary>
        /// 查询未缴费患者列表(按照搜索)
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <param name="name"></param>
        /// <param name="patientId"></param>
        public List<RequestStudy> GetRequestStudyListByKeyWords(string hospitalID, string patientId, string userId)
        {
            return dbContext.GetRequestStudyListByKeyWords(hospitalID, patientId, userId);
        }

        public ExcuteModel AddUserAccount(CustAccount objCustAccount)
        {
            return DAL.AddUserAccount(objCustAccount);
        }

        #region 医生信息
        /// <summary>
        /// 添加医生
        /// </summary>
        /// <param name="objCustAccount"></param>
        /// <returns></returns>        
        public ExcuteModel AddDoctInfo(DoctorInfo objDoctorInfo)
        {
            return DAL.AddDoctorInfo(objDoctorInfo);
        }

        /// <summary>
        /// 更新专家机构信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateDoctorInfo(DoctorInfo model)
        {
            return DAL.UpdateDoctorInfo(model);
        }

        /// <summary>
        /// 更新专家状态
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateDoctorInfoState(DoctorInfo model)
        {
            return DAL.UpdateDoctorInfoState(model);
        }

        /// <summary>
        /// 获取医生
        /// </summary>
        /// <returns></returns>
        public List<DoctorInfo> GetDoctorInfo()
        {
            return DAL.GetDoctorInfo();
        }

        public DoctorInfo GetDoctorInfoById(string doctorId)
        {
            return DAL.GetDoctorInfoById(doctorId);
        }

        /// <summary>
        /// 保存并更新医生信息
        /// </summary>
        /// <param name="objDoctorInfo"></param>
        /// <returns></returns>
        public ExcuteModel SaveDoctorInfo(DoctorInfo objDoctorInfo)
        {
            return DAL.SaveDoctorInfo(objDoctorInfo);
        }

        public int? GetMaxOrderValue()
        {
            return DAL.GetMaxOrderValue();
        }

        /// <summary>
        /// 联合查询专家信息
        /// </summary>
        /// <returns></returns>
        public List<DoctorBaseInfo> GetDoctorInfoList()
        {
            return dbContext.GetDoctorInfoList();
        }

        #endregion

        #region 查询会诊数量记录表
        /// <summary>
        /// 根据机构ID查询所有会诊数据量
        /// </summary>
        /// <param name="strHostitalID"></param>
        /// <returns></returns>
        public List<ConsulaDataRecord> GetConsulaDataRecordList(ConsulaDataRecord model)
        {
            return dbContext.GetConsulaDataRecordList(model);
        }

        /// <summary>
        /// 保存会诊数据记录信息
        /// 数量不做判断
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel SaveConsulaDataRecord(ConsulaDataRecord model)
        {
            return dbContext.SaveConsulaDataRecord(model);
        }

        /// <summary>
        /// 更新ConAdd数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateConsulaDataRecordConAddGUID(ConsulaDataRecord model)
        {
            return dbContext.UpdateConsulaDataRecordConAddGUID(model);
        }

        /// <summary>
        /// 更新ConAll数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateConsulaDataRecordConAllGUID(ConsulaDataRecord model)
        {
            return dbContext.UpdateConsulaDataRecordConAllGUID(model);
        }

        /// <summary>
        /// 更新ConFree
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateConsulaDataRecordConFreeGUID(ConsulaDataRecord model)
        {
            return dbContext.UpdateConsulaDataRecordConFreeGUID(model);
        }

        /// <summary>
        /// 更新ConOver
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateConsulaDataRecordConOverGUID(ConsulaDataRecord model)
        {
            return dbContext.UpdateConsulaDataRecordConOverGUID(model);
        }

        /// <summary>
        /// 更新ConAlready
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateConsulaDataRecordConAlreadyGUID(ConsulaDataRecord model)
        {
            return dbContext.UpdateConsulaDataRecordConAlreadyGUID(model);
        }

        /// <summary>
        /// 更新ConReturn
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateConsulaDataRecordConReturnGUID(ConsulaDataRecord model)
        {
            return dbContext.UpdateConsulaDataRecordConReturnGUID(model);
        }

        /// <summary>
        /// 如果约定内的数据用完则重新添加
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel AddConCount(ConsulaDataRecord model)
        {
            return dbContext.AddConCount(model);
        }

        /// <summary>
        /// 根据机构、服务项目、年份
        /// 获取单个会诊记录数据
        /// </summary>
        /// <param name="strHospitalID"></param>
        /// <param name="strServiceUID"></param>
        /// <returns></returns>
        public ConsulaDataRecord GetConsulaDataRecordModel(ConsulaDataRecord modelParam)
        {
            return dbContext.GetConsulaDataRecordModel(modelParam);
        }
        #endregion

        #region 关联机构信息
        /// <summary>
        /// 添加关联机构
        /// </summary>
        /// <returns></returns>
        public ExcuteModel InsertRelateHospital(RelateHospital model)
        {
            model.HospitalGUID = Guid.NewGuid().ToString();
            return dbContext.InsertRelateHospital(model);
        }


        /// <summary>
        /// 设置默认机构
        /// 仅限于左右移动
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ExcuteModel UpdateRelateHospital(RelateHospital model)
        {
            return dbContext.UpdateRelateHospital(model);
        }

        /// <summary>
        /// 删除关联机构
        /// </summary>
        /// <returns></returns>
        public ExcuteModel DeleteRelateHospital(RelateHospital model)
        {
            return dbContext.DeleteRelateHospital(model);
        }

        /// <summary>
        /// 子医院列表
        /// </summary>
        /// <returns></returns>
        public List<Sys_Dept> GetRelateSubHospitalList(string hospitalID, string userID)
        {
            return dbContext.GetRelateSubHospitalList(hospitalID, userID);
        }

        /// <summary>
        /// 子医院关联列表
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public List<RelateHospital> GetRelateHospitalList(string userID)
        {
            return dbContext.GetRelateHospitalList(userID);
        }
        #endregion

        #region 检查部位
        /// <summary>
        /// 获取检查部位列表
        /// </summary>
        /// <returns></returns>
        public List<ExaminePosition> GetExamPosList()
        {
            return dbContext.GetExamPosList();
        }

        public List<ExaminePosition> GetExamPosListNew()
        {
            return dbContext.GetExamPosListNew();
        }

        /// <summary>
        /// 添加检查部位列表
        /// </summary>
        /// <param name="objExaminePos"></param>
        /// <returns></returns>
        public ExcuteModel AddExamPositionInfo(ExaminePosition objExaminePos)
        {
            return dbContext.AddExamPositionInfo(objExaminePos);
        }
        /// <summary>
        /// 根据编号获取检查部位信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ExaminePosition GetExamPositionById(int id)
        {
            return dbContext.GetExamPositionById(id);
        }
        /// <summary>
        /// 更新检查部位状态
        /// </summary>
        /// <param name="id"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public ExcuteModel UpdateExamPositionInfo(int id, int state)
        {
            return dbContext.UpdateExamPositionInfo(id, state);
        }
        /// <summary>
        /// 更新检查部位信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public ExcuteModel UpdateExamPosition(int id, ExaminePosition objExamPos)
        {
            return dbContext.UpdateExamPosition(id, objExamPos);
        }

        /// <summary>
        /// 更新检查部位信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public ExcuteModel UpdateCheckPosition(int id, string subMethod, string mainMethod)
        {
            return dbContext.UpdateCheckPosition(id, subMethod, mainMethod);
        }
        /// <summary>
        /// 更新检查部位状态
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel UpdateExaminePositionState(ExaminePosition param)
        {
            return dbContext.UpdateExaminePositionState(param);
        }

        /// <summary>
        ///  更新检查部位信息
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel UpdateExaminePosition(ExaminePosition param)
        {
            return dbContext.UpdateExaminePosition(param);
        }

        public List<ExaminePosition> GetExaminePositionByParentNode(int parentNode)
        {
            return dbContext.GetExaminePositionByParentNode(parentNode);
        }
        /// <summary>
        /// 获取检查部位排序最大值
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public int? GetExaminePositionMax()
        {
            return dbContext.GetExaminePositionMax();
        }

        public ExcuteModel IsExistedExaminePositionName(string positionName)
        {
            return dbContext.IsExistedExaminePositionName(positionName);
        }

        public List<ExaminePosition> GetSubExamPosList()
        {
            return dbContext.GetSubExamPosList();
        }

        #endregion

        #region 检查方法
        /// <summary>
        /// 获取检查方法列表
        /// </summary>
        /// <returns></returns>
        public List<ExamineMethod> GetExamMethodList()
        {
            return dbContext.GetExamMethodList();
        }

        public List<ExamineMethod> GetExamMethodListNew()
        {
            return dbContext.GetExamMethodListNew();
        }
        /// <summary>
        /// 添加检查方法列表
        /// </summary>
        /// <param name="objExaminePos"></param>
        /// <returns></returns>
        public ExcuteModel AddExamMethodInfo(ExamineMethod objExamineMet)
        {
            return dbContext.AddExamMethodInfo(objExamineMet);
        }

        /// <summary>
        /// 更新检查方法状态
        /// </summary>
        /// <param name="id"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public ExcuteModel UpdateExamMethodInfo(int id, int state)
        {
            return dbContext.UpdateExamMethodInfo(id, state);
        }

        /// <summary>
        /// 根据编号获取检查方法信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ExamineMethod GetExamMethodById(int id)
        {
            return dbContext.GetExamMethodById(id);
        }

        /// <summary>
        /// 更新检查方法信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public ExcuteModel UpdateExamMethod(int id, ExamineMethod objExamMet)
        {
            return dbContext.UpdateExamMethod(id, objExamMet);
        }

        /// <summary>
        /// 更新检查方法状态
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel UpdateExamineMethodState(ExamineMethod param)
        {
            return dbContext.UpdateExamineMethodState(param);
        }

        /// <summary>
        ///  更新检查方法信息
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel UpdateExamineMethod(ExamineMethod param)
        {
            return dbContext.UpdateExamineMethod(param);
        }

        /// <summary>
        /// 获取检查方法排序最大值
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public int? GetExamineMethodMax()
        {
            return dbContext.GetExamineMethodMax();
        }

        public List<ExamineMethod> GetExamineMethodByParentNode(int parentNode)
        {
            return dbContext.GetExamineMethodByParentNode(parentNode);
        }

        public List<ExaminePosition> GetExaminePositionByExamineName(string examineName)
        {
            return dbContext.GetExaminePositionByExamineName(examineName);
        }

        public ExcuteModel IsExistedExamineMethodName(string positionName)
        {
            return dbContext.IsExistedExamineMethodName(positionName);
        }

        public List<ExaminePosition> GetExaminePositionCombineExamineName(List<string> parentNodeNameList, List<string> subNodeNameList)
        {
            return dbContext.GetExaminePositionCombineExamineName(parentNodeNameList, subNodeNameList);
        }

        #endregion

        #region 消息提醒

        public ExcuteModel AddMsgFrameState(MsgFrameState objMsgFrameState)
        {
            return dbContext.AddMsgFrameState(objMsgFrameState);
        }

        public ExcuteModel UpdateMsgFrameState(string str, int intState)
        {
            return dbContext.UpdateMsgFrameState(str, intState);
        }

        public List<MsgFrameState> GetMsgFrameState(string str)
        {
            return dbContext.GetMsgFrameState(str);
        }

        /// <summary>
        /// 获取申请患者数据
        /// </summary>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        public List<MsgFrameState> GetMsgFrameRequestData(string strUserID, int intState)
        {
            return dbContext.GetMsgFrameRequestData(strUserID, intState);
        }

        /// <summary>
        /// 更新提醒数据时，使用传递值的方式
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="intState"></param>
        /// <returns></returns>
        public ExcuteModel UpdateSingleMsgFrameState(string requestId, int intState)
        {
            return dbContext.UpdateSingleMsgFrameState(requestId, intState);
        }
        #endregion

        #region 排班
        /// <summary>
        /// 获取排班信息
        /// </summary>
        /// <returns></returns>
        public List<ScheduleSettings> GetScheduleSettings(string deptId, string startTime, string endTime)
        {
            return dbContext.GetScheduleSettings(deptId, startTime, endTime);
        }

        /// <summary>
        /// 初始化排班信息
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public ExcuteModel InitScheduleSettings(List<ScheduleSettings> list, string deptId, string userId, string createName)
        {
            return dbContext.InitScheduleSettings(list, deptId, userId, createName);
        }

        /// <summary>
        /// 保存排班记录
        /// </summary>
        /// <param name="sqlList"></param>
        /// <returns></returns>
        public ExcuteModel SaveScheduling(List<string> sqlList)
        {
            return dbContext.SaveScheduling(sqlList);
        }

        /// <summary>
        /// 获取用户类型
        /// </summary>
        /// <returns></returns>
        public List<UserType> GetUserType()
        {
            return dbContext.GetUserType();
        }

        public ExcuteModel InsertShiftSettings(ShiftSettings shiftSettings)
        {
            return dbContext.InsertShiftSettings(shiftSettings);
        }

        public bool IsExistsTemplate(string deptId, string startTime, string endTime)
        {
            return dbContext.IsExistsTemplate(deptId, startTime, endTime);
        }

        public List<ScheduleTemplate> GetScheduleTemplate(string sql)
        {
            return dbContext.GetScheduleTemplate(sql);
        }

        public ExcuteModel AddScheduleTemplate(List<string> sqlList)
        {
            return dbContext.AddScheduleTemplate(sqlList);
        }

        public ExcuteModel AddShiftSettings(ShiftSettings shiftSettings)
        {
            return dbContext.AddShiftSettings(shiftSettings);
        }

        public List<ShiftSettings> GetShiftSettings(string deptId)
        {
            return dbContext.GetShiftSettings(deptId);
        }

        public ExcuteModel DeleteShiftSettings(int shiftId)
        {
            return dbContext.DeleteShiftSettings(shiftId);
        }

        public ShiftSettings GetShiftSettingsById(int shiftId)
        {
            return dbContext.GetShiftSettingsById(shiftId);
        }

        public bool IsExistsScheduleSettings(string deptId, string startTime, string endTime)
        {
            return dbContext.IsExistsScheduleSettings(deptId, startTime, endTime);
        }

        public ExcuteModel UpdateScheduleTemplate(List<string> sqlList)
        {
            return dbContext.UpdateScheduleTemplate(sqlList);
        }

        public ExcuteModel UpdateShiftSettings(ShiftSettings shiftSettings)
        {
            return dbContext.UpdateShiftSettings(shiftSettings);
        }

        public List<ScheduleTemplate> GetScheduleTemplateById(string deptId)
        {
            return dbContext.GetScheduleTemplateById(deptId);
        }

        public ExcuteModel UpdateScheduleSettings(List<string> sqlList)
        {
            return dbContext.UpdateScheduleSettings(sqlList);
        }

        public List<DoctorInfo> GetDoctorInfoByDeptId(string deptId)
        {
            return dbContext.GetDoctorInfoByDeptId(deptId);
        }

        public string GetDoctorNameByDeptIdAndId(string sql)
        {
            return dbContext.GetDoctorNameByDeptIdAndId(sql);
        }

        public ShiftSettings GetScheduleUserType(string shiftName, string deptId)
        {
            return dbContext.GetScheduleUserType(shiftName, deptId);
        }
        #endregion

        #region 申请检查记录接口
        public ExcuteModel AddRequestStudyRecord(Dictionary<string, RequestStudyRecord> dictionary)
        {
            return dbContext.AddRequestStudyRecord(dictionary);
        }

        public RequestStudyRecord GetRequestStudyRecordByUid(string strRequestID, string studyInstanceUid)
        {
            return dbContext.GetRequestStudyRecordByUid(strRequestID, studyInstanceUid);
        }

        public ExcuteModel AddSingleRequestStudyRecord(RequestStudyRecord requestStudyRecord)
        {
            return dbContext.AddSingleRequestStudyRecord(requestStudyRecord);
        }
        #endregion

        #region 不用的方法

        public ExcuteModel UpdateRequestStudyRecord(RequestStudyRecord requestRecord)
        {
            return dbContext.UpdateRequestStudyRecord(requestRecord);
        }

        public ExcuteModel DeleteRequestStudyRecord(RequestStudyRecord requestRecord)
        {
            return dbContext.DeleteRequestStudyRecord(requestRecord);
        }

        public List<RequestStudyRecord> GetRequestStudyRecord()
        {
            return dbContext.GetRequestStudyRecord();
        }

        #endregion

        #region 退费申请确认
        public List<RefundRecordManager> GetRefundStateRecordInfo(string patientId, string patientName, DateTime startTime, DateTime endTime, string hospitalId, string remark = null)
        {
            return dbContext.GetRefundStateRecordInfo(patientId, patientName, startTime, endTime, hospitalId, remark);
        }
        public ExcuteModel UpdateRefundStateRecord(string requestId, int isRefund)
        {
            return dbContext.UpdateRefundStateRecord(requestId, isRefund);
        }

        public ExcuteModel UpdateReturnCause(string requestId, string returnCause)
        {
            return dbContext.UpdateReturnCause(requestId, returnCause);
        }
        public ExcuteModel AddRefundStateRecord(RefundStateRecord refundRecord)
        {
            return dbContext.AddRefundStateRecord(refundRecord);
        }
        public RefundStateRecord GetRefundStateRecordById(string requestId)
        {
            return dbContext.GetRefundStateRecordById(requestId);
        }
        #endregion

        #region 单个患者多影像申请
        public ExcuteModel UpdateRequestStudyRecordExaminePort(string requestId, string studyInstanceUid, string examinePort)
        {
            return dbContext.UpdateRequestStudyRecordExaminePort(requestId, studyInstanceUid, examinePort);
        }
        public ExcuteModel UpdateRequestStudyRecordExamineMethod(string requestId, string studyInstanceUid, string examineMethod)
        {
            return dbContext.UpdateRequestStudyRecordExamineMethod(requestId, studyInstanceUid, examineMethod);
        }

        public ExcuteModel UpdateRequestInfoExaminePort(string requestId, string studyInstanceUid, string examinePort)
        {
            return dbContext.UpdateRequestInfoExaminePort(requestId, studyInstanceUid, examinePort);
        }

        public ExcuteModel UpdateRequestInfoExamineMethod(string requestId, string studyInstanceUid, string examineMethod)
        {
            return dbContext.UpdateRequestInfoExamineMethod(requestId, studyInstanceUid, examineMethod);
        }

        public ExcuteModel UpdateRequestStudyRecordUploadCase(string requestId, string studyInstanceUid, int isUploadCase)
        {
            return dbContext.UpdateRequestStudyRecordUploadCase(requestId, studyInstanceUid, isUploadCase);
        }

        public List<RequestStudyRecordInfo> GetRequestStudyRecordList(string requestId)
        {
            return dbContext.GetRequestStudyRecordList(requestId);
        }
        #endregion

        #region 图片转换
        /// <summary>
        /// 图片转换成DICOM文件
        /// </summary>
        /// <param name="para"></param>
        public void ImageToDicom(DateTime startDate, DateTime endDate)
        {
            var filePath = GetSysValueByKey("ServerMobileApiPath").SysValue;
            int pageCount = 0;
            int patientCount = 0;
            int tempCount = 0;
            var requestInfos = GetConsulationPatientInfoList("", "", "", 1, 24, "", startDate, endDate, 0, 0, "", out pageCount, out patientCount);
            tempCount = pageCount;
            List<OutRqeustInfo> lst = new List<OutRqeustInfo>();
            if (requestInfos != null && requestInfos.Count > 0)
            {
                lst.AddRange(requestInfos);
            }
            for (int i = 1; i < tempCount; i++)
            {
                requestInfos = new List<OutRqeustInfo>();
                requestInfos = GetConsulationPatientInfoList("", "", "", i + 1, 24, "", startDate, endDate, 0, 0, "", out pageCount, out patientCount);
                lst.AddRange(requestInfos);
            }
            GetRequestFileItem(lst, filePath);
        }

        /// <summary>
        /// 获取所有的文件信息
        /// </summary>
        /// <param name="requestInfos"></param>
        public void GetRequestFileItem(List<OutRqeustInfo> requestInfos, string filePath)
        {
            try
            {
                if (requestInfos != null && requestInfos.Count > 0)
                {
                    foreach (var item in requestInfos)
                    {
                        var requestFileItem = GetFileItemsByRequestId(item.RequestId).ToList();//Where(x => x.Source != 9 && x.FileType != "success").
                        ConvertToSQDImage(item, requestFileItem, item.RequestState, filePath);//生成申请单图片和报告图片                       
                    }
                    ImageHelper t = new ImageHelper();
                    t.GroupFileType(requestInfos, filePath);
                }
            }
            catch (Exception ex)
            {
                LogHelper.ServiceInfoLog("GetRequestFileItem异常内容：" + ex.Message.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        /// <summary>
        /// 生成申请单图片
        /// </summary>
        /// <param name="item"></param>
        /// <param name="requestFileItem"></param>
        private void ConvertToSQDImage(OutRqeustInfo item, List<RequestFileItem> requestFileItem, int requestState, string filePath)
        {
            var imageFileName = "";
            int source = 11;
            if (requestState == 11)
            {
                source = 11;
                imageFileName = item.RequestId + "_s.jpg";
                GetFileItemImage(requestFileItem, source, imageFileName);
            }
            else if (item.RequestState == 15)//已完成的报告，生成申请单和报告图片
            {
                source = 15;//报告类型
                imageFileName = item.RequestId + "_b.jpg";
                GetFileItemImage(requestFileItem, source, imageFileName);
                source = 11;
                imageFileName = item.RequestId + "_s.jpg";
                GetFileItemImage(requestFileItem, source, imageFileName);
            }
            var requestFileItemNew = GetFileItemsByRequestId(item.RequestId).Where(x => x.Source != 9 && x.FileType != "success").ToList();
            foreach (var i in requestFileItemNew)
            {
                if (i.Source == 11 || i.Source == 15 && (i.FileType == null || i.FileType == "" || !i.FileType.Equals("success")))
                {
                    ImageHelper t = new ImageHelper();
                    t.ConvertTemp(new Tuple<OutRqeustInfo, string, string>(item, filePath, i.FileName));
                }
            }
        }

        /// <summary>
        /// 判断是否有申请单或者报告图片，如果没有则进行添加
        /// </summary>
        /// <param name="requestFileItem"></param>
        /// <param name="source"></param>
        private void GetFileItemImage(List<RequestFileItem> requestFileItem, int source, string imageFileName)
        {
            var sqd = requestFileItem.Where(x => x.FileName == imageFileName).ToList();
            if (sqd == null || sqd.Count == 0)
            {
                SaveFileItem(new RequestFileItem
                {
                    FileName = imageFileName,
                    FileRootId = requestFileItem.FirstOrDefault().FileRootId,
                    Source = source,//申请单图片
                    VisitDate = DateTime.Now,
                    RequestId = requestFileItem.FirstOrDefault().RequestId,
                    GUID = Guid.NewGuid().ToString(),
                    FileId = requestFileItem.FirstOrDefault().FileId,
                    CollectDate = DateTime.Now,
                    StudyInstanceUid = requestFileItem.FirstOrDefault().StudyInstanceUid
                });
            }
        }
        #endregion

        #region 展示厅相应方法
        /// <summary>
        /// 获取医院总数
        /// </summary>
        /// <returns></returns>
        public int GetHospitalAllCounts(string HospitalID)
        {
            return dbContext.GetHospitalAllCounts(HospitalID);
        }

        /// <summary>
        /// 当前分诊总例数
        /// 统计当天
        /// </summary>
        /// <returns></returns>
        public int GetDiagnoseTodayCounts(string HospitalID)
        {
            return dbContext.GetDiagnoseTodayCounts(HospitalID);
        }

        /// <summary>
        /// 报告平均时长
        /// 统计当天
        /// </summary>
        /// <returns></returns>
        public double GetReportTodayAvgTime(string HospitalID)
        {
            return dbContext.GetReportTodayAvgTime(HospitalID);
        }

        /// <summary>
        /// 完成报告统计
        /// 表格统计
        /// </summary>
        public List<DeviceTypeModel> GetFinishedReportHistoryStatistics(string HospitalID)
        {
            return dbContext.GetFinishedReportHistoryStatistics(HospitalID);
        }

        /// <summary>
        /// 设备类型统计
        /// 饼状图统计
        /// 统计本月
        /// </summary>
        public List<OutDeviceTypeCounts> GetDeviceTypeStatistics(string HospitalID)
        {
            return dbContext.GetDeviceTypeStatistics(HospitalID);
        }

        /// <summary>
        /// 分诊增长统计
        /// 柱形统计
        /// </summary>
        public List<OutReportCounts> GetIncreaseStatistics(string HospitalID)
        {
            return dbContext.GetIncreaseStatistics(HospitalID);
        }

        /// <summary>
        /// 完成报告统计
        /// 柱形统计
        /// </summary>
        public List<OutReportCounts> GetFinishedReportStatistics(string HospitalID)
        {
            return dbContext.GetFinishedReportStatistics(HospitalID);
        }
        #endregion

        #region 医院坐标管理
        /// <summary>
        /// 添加或更新坐标
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public ExcuteModel InsertCoorDinate(S_CoorDinate param)
        {
            return dbContext.InsertCoorDinate(param);
        }

        /// <summary>
        /// 删除坐标
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public int DeleteCoorDinate(string HospitalIdParam)
        {
            return dbContext.DeleteCoorDinate(HospitalIdParam);
        }

        /// <summary>
        /// 获取所属中心医院的所有子医院坐标
        /// </summary>
        /// <param name="HospitalIdParam"></param>
        /// <returns></returns>
        public List<S_CoorDinate> GetCoorDinateList(string HospitalIdParam)
        {
            return dbContext.GetCoorDinateList(HospitalIdParam);
        }

        /// <summary>
        /// 获取单个医院坐标信息
        /// </summary>
        /// <param name="HospitalIdParam"></param>
        /// <returns></returns>
        public S_CoorDinate GetCoorDinateInfo(string HospitalIdParam)
        {
            return dbContext.GetCoorDinateInfo(HospitalIdParam);
        }
        #endregion

        #region 删除用户组
        public ExcuteModel DeleteUserGroupInfo(string userID)
        {
            return dbContext.DeleteUserGroupInfo(userID);
        }

        public Groups GetGroupsById(string groupId)
        {
            return dbContext.GetGroupsById(groupId);
        }
        #endregion

        #region 收费端是否需要多次申请判断
        public ExcuteModel IsExistedPatientRequested(string patientId, string studyInstanceUid)
        {
            return dbContext.IsExistedPatientRequested(patientId, studyInstanceUid);
        }
        #endregion

        #region 工作量统计
        /// <summary>
        /// 统计报告数
        /// </summary>
        /// <returns></returns>
        public List<StatisticsModel> AddUpReport(DateTime startTime, DateTime endTime, List<string> doctorNames, string userName, string userId)
        {
            return dbContext.AddUpReport(startTime, endTime, doctorNames, userName, userId);
        }

        public List<OutReportRequestInfo> GetRequestDataList(DateTime dtStart, DateTime dtEnd, List<string> hospitalIds, List<string> requestDoctors, string doctorName, string doctorId)
        {
            return dbContext.GetRequestDataList(dtStart, dtEnd, hospitalIds, requestDoctors, doctorName, doctorId);
        }

        /// <summary>
        /// 按照检查方式进行统计
        /// </summary>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <returns></returns>
        public List<OutExamineMethodCount> GetExamineMethodCountList(DateTime dtStart, DateTime dtEnd, string hospitalId)
        {
            return dbContext.GetExamineMethodCountList(dtStart, dtEnd, hospitalId);
        }
        #endregion

        #region 检查项目
        /// <summary>
        /// 保存检查项目
        /// </summary>
        /// <param name="modelParam"></param>
        /// <returns></returns>
        public ExcuteModel SaveExamineProject(ExamineProject modelParam)
        {
            return dbContext.SaveExamineProject(modelParam);
        }

        /// <summary>
        /// 获取检查项目列表
        /// </summary>
        /// <param name="modelParam"></param>
        /// <returns></returns>
        public List<OutExamineProject> GetExamineProjectList(OutExamineProject modelParam)
        {
            return dbContext.GetExamineProjectList(modelParam);
        }

        /// <summary>
        /// 获取排序最大值
        /// </summary>
        /// <returns></returns>
        public int? GetExamineProjectMaxSort()
        {
            return dbContext.GetExamineProjectMaxSort();
        }
        #endregion

        #region 检查部位维护
        /// <summary>
        /// 保存检查部位
        /// </summary>
        /// <param name="modelParam"></param>
        /// <returns></returns>
        public ExcuteModel SaveExaminePart(ExaminePart modelParam)
        {
            return dbContext.SaveExaminePart(modelParam);
        }

        /// <summary>
        /// 获取检查部位列表
        /// </summary>
        /// <param name="modelParam"></param>
        /// <returns></returns>
        public List<ExaminePart> GetExaminePartList(ExaminePart modelParam)
        {
            return dbContext.GetExaminePartList(modelParam);
        }

        /// <summary>
        /// 获取排序最大值
        /// </summary>
        /// <returns></returns>
        public int? GetExaminePartMaxSort()
        {
            return dbContext.GetExaminePartMaxSort();
        }
        #endregion

        #region 临时方法
        public List<DiagnosePart> GetALLDiagnosePart_name()
        {
            List<DiagnosePart> List = new List<DiagnosePart>();
            return List;
        }
        #endregion

        #region 申请驳回记录表
        /// <summary>
        /// 保存驳回申请记录信息
        /// </summary>
        /// <param name="modelParam"></param>
        /// <returns></returns>
        public ExcuteModel SaveRequestRebutRecord(RequestRebutRecord modelParam)
        {
            return dbContext.SaveRequestRebutRecord(modelParam);
        }
        /// <summary>
        /// 获取驳回申请记录信息列表
        /// </summary>
        /// <param name="modelParam"></param>
        /// <returns></returns>
        public List<RequestRebutRecord> GetRequestRebutRecordList(RequestRebutRecord modelParam)
        {
            return dbContext.GetRequestRebutRecordList(modelParam);
        }
        #endregion

        #region 患者基本信息登记
        /// <summary>
        /// 患者信息登记
        /// </summary>
        /// <param name="patientModel"></param>
        /// <param name="requestModel"></param>
        /// <param name="workModel"></param>
        /// <returns></returns>
        public ExcuteModel SavePatientBaseInfo(PatientInfo patientModel, RequestInfo requestModel, Worklist workModel)
        {
            ExcuteModel em = new ExcuteModel();
            TransactionOptions option = new TransactionOptions();
            option.IsolationLevel = IsolationLevel.ReadCommitted;
            using (TransactionScope scop = new TransactionScope(TransactionScopeOption.Required, option))
            {
                var patientResult = SavePatientInfo(patientModel);
                if (patientResult.Result)
                {
                    var requestInfoResult = SaveRequestInfo(requestModel);
                    if (requestInfoResult.Result)
                    {
                        var workResult = workContent.AddWorklistInfo(workModel);
                        if (workResult.Result)
                        {
                            em.Result = true;
                            scop.Complete();
                        }
                        else
                        {
                            em.Msg = workResult.Msg;
                            em.Result = workResult.Result;
                        }
                    }
                    else
                    {
                        em.Msg = requestInfoResult.Msg;
                        em.Result = requestInfoResult.Result;
                    }
                }
                else
                {
                    em.Msg = patientResult.Msg;
                    em.Result = patientResult.Result;
                }
            }
            return em;
        }

        //SoftNumericalOrder number = new SoftNumericalOrder("A", "yyyyMMdd", 7, Application.StartupPath + @"\number.txt");
        /// <summary>
        /// 获取登记流水号（或供扫码使用）
        /// 设备类型+时间+位数+随机数（0-9）
        /// 例如：CT+20180907+0001+3=CT2018090700000013
        /// </summary>
        /// <param name="paramType"></param>
        /// <returns></returns>
        public ExcuteModel GetAccessionNumber(string paramType, int length)
        {
            ExcuteModel em = new ExcuteModel();
            try
            {
                Random rd = new Random();
                em.RetValue = NumericalOrder.Instance.AutoNumber(paramType, "yyyyMMdd", length) + rd.Next(0, 9).ToString();
                em.Result = true;
            }
            catch (Exception ex)
            {
                em.Result = false;
                em.Msg = ex.Message;
            }
            return em;
        }

        #region 获取机房数据
        public List<OutRqeustInfo> GetMachinePatientData(InMachineBase param)
        {
            return dbContext.GetMachinePatientData(param);
        }
        #endregion
        #endregion
    }
    public class DiagnosePart
    {
        public string DiagnosePartName { get; set; }
        public int DiagnosePart_ID { get; set; }
        public string Param { get; set; }
        public string description { get; set; }
    }
}
